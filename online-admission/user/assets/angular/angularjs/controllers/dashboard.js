var csrfHash = $('#csrfHash').val();


 
app.controller('dashboardCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        
        if(localStorage.getItem("StudentLoggedIn") == "False" || localStorage.getItem("StudentLoggedIn") == null){
            window.location = "/online-admission/index.html";
        }
        else{
            
            $scope.singleStudentArray =[];
            $scope.paymentid = "";
            $scope.getStudentList();
            $scope.new_url = new_url;
        }
    }
    
    $scope.getStudentList = function () {
        
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'UserDashboard/getStudentList', {
            'csrf_token_name': csrfHash,
            student_id : JSON.parse(localStorage.getItem("StudentId")),
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.singleStudentArray = response.data.data;
                
                $scope.leng = (response.data.data).length;
                console.log($scope.singleStudentArray);
                $scope.courseName =response.data.data[0].course_name;
                $scope.courseYear =response.data.data[0].course_year;
                $scope.showEligibilityNoDiv();
                $scope.SubjectListArray = [];
                $scope.getSubjectList();
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }
   
    $scope.showEligibilityNoDiv = function(){
        if($scope.courseYear == "First Year"){
            $scope.ifSecondThirdYear = false;
            $scope.ifFirstYear= true;
        }else{
            $scope.ifSecondThirdYear = true;
            $scope.ifFirstYear= false;
        }

    }

    $scope.getSubjectList = function(){
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'EditProfile/getSubjectList', {
            'csrf_token_name': csrfHash,
            courseName : $scope.courseName,
            courseYear : $scope.courseYear,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.optinalSubjectArray = [];
                $scope.optinalSubjectArraysem2 = [];
                $scope.compulsorySubjectArray = [];
                $scope.compulsorySubjectArraysem2 = [];
                $scope.valueAddedSubjectArraysem2 =[];
                $scope.valueAddedSubjectArray = [];
                $scope.SubjectListArray = response.data.data;
                
                var arraylen = $scope.SubjectListArray.length;
                for(i=0;i<arraylen ; i++){
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.optinalSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.optinalSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.compulsorySubjectArray.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.compulsorySubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'  && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.valueAddedSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'&& ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.valueAddedSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                }
               console.log($scope.SubjectListArray);
              $scope.optionalLeng = ($scope.optinalSubjectArray).length;
              $scope.valueAddedLeng = ($scope.valueAddedSubjectArray).length;
              $scope.valueAddedsem2Leng = ($scope.valueAddedSubjectArraysem2).length;
                
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })      
    }
    $scope.logOut = function(){
        
        localStorage.setItem("StudentLoggedIn", "False");
        localStorage.setItem("StudentId", "");
        setTimeout(() => {
                window.location = new_url+ "online-admission/index.html";
                },0)
    }


    $scope.printToCart = function(viewStudentDetails) {
   
    
        var innerContents = document.getElementById(viewStudentDetails).innerHTML;
        var popupWin; 
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/styles/style.css"/>
                <link rel="stylesheet" href="assets/fonts/themify-icons/themify-icons.css"/>
                <link rel="stylesheet" href="assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css"/>
                <link rel="stylesheet" href="assets/plugin/waves/waves.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/media/css/dataTables.bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css"/>
                <script src="assets/scripts/modernizr.min.js"></script>
                <script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/scripts/main.min.js"></script>
                <script src="assets/angular/angular.min.js"></script>
                <script src="assets/angular/ui-bootstrap-tpls-1.0.3.js"></script>
                <script src="assets/scripts/jquery.min.js"></script>
                <script src="assets/angular/angularjs/app.js"></script>
                <script src="assets/scripts/jquery.min.js"></script>
                <script src="assets/scripts/modernizr.min.js"></script>
                <script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
                <script src="assets/plugin/nprogress/nprogress.js"></script>
                <script src="assets/plugin/sweet-alert/sweetalert.min.js"></script>
                <script src="assets/plugin/waves/waves.min.js"></script>
                
            </head>
            <body onload="window.print();window.close()">${innerContents}
            
            </body>
        </html>`
        );
      
        popupWin.document.close();
      }


});