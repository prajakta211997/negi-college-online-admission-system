var csrfHash = $('#csrfHash').val();


app.controller('loginServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
       
        $scope.studEmail = '';
        $scope.studPassword = '';
        
    }
   

    $scope.loginService = function () {
        if ($scope.studEmail) {
            if ($scope.studPassword) {
                                
                var transform = function(data) {
                    return $.param(data);
                } 

                $http.post(window.site_url + 'UserLogin/checkLogin', {
                    'csrf_token_name': csrfHash,

                    studEmail : $scope.studEmail ,
                    studPassword : $scope.studPassword ,

                }, {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                    transformRequest: transform
                })
                .then(function(response) {
                    if (response.data.status.status == "1") {
                        // localStorage.setItem("StudentId",response.data.data);
                        // localStorage.setItem("StudentLoggedIn", "True");
                        // toastr.success(response.data.status.message);
                        // setTimeout(() => {
                        //     window.location = "dashboard.html";
                        // },1000)
                    }else{
                        toastr.error(response.data.status.message);
                    }
                }, function(response) {
                })
                                
            } else {
                $('#studPassword').focus();
                toastr.error("password required.");
            }

        } else {
            $('#studEmail').focus();
            toastr.error("EmailId is required.");
        }
               
    }
});


