var csrfHash = $('#csrfHash').val();


app.controller('paymentDetailsCtrl', function ($scope, $http, toastr) {

    $scope.currentPage = 1;
    $scope.currentPage1 = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
      $scope.currentPage1 = pageNo;
    };

    $scope.init = function () {
        
        if(localStorage.getItem("StudentLoggedIn") == "False" || localStorage.getItem("StudentLoggedIn") == null){
            window.location = "/online-admission/index.html";
        }
        else{
            $scope.paymentDataArray = [];
            $scope.getPaymentList();
            $scope.getTotalFees();
            $scope.new_url = new_url;
        }
    }

    
    $scope.logOut = function(){
        
        localStorage.setItem("StudentLoggedIn", "False");
        localStorage.setItem("StudentId", "");
        setTimeout(() => {
                window.location = new_url+ "online-admission/index.html";
                },0)
    }

    $scope.getPaymentList = function () {
            
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'UserPaymentDetails/getPaymentList', {
            'csrf_token_name': csrfHash,
            StudentIdForPayment : JSON.parse(localStorage.getItem("StudentId")),
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.paymentDataArray = response.data.data;
            var leng = (response.data.data).length;

                console.log("paymentlist ",$scope.paymentDataArray);
            
        
                $scope.paymentRecive =0;
                for(var i=0; i<leng;i++)
                {
                var x= Number(response.data.data[i].add_amount || 0);
                    if(response.data.data[i].is_payment_recieved == 1)
                    {
                        $scope.paymentRecive = $scope.paymentRecive +  x;
                    }
                }
                
            
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })

    }

    $scope.getTotalFees = function()
    { // alert(stud_id);
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'UserPaymentDetails/getTotalFees', {
            'csrf_token_name': csrfHash,
            stud_id : JSON.parse(localStorage.getItem("StudentId")),
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
            $scope.totalFees = response.data.data;
            $scope.remainingFees=0;
            $scope.remainingFees= $scope.totalFees - $scope.paymentRecive;
            
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })

    }

    
});