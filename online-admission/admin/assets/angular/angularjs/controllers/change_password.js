var csrfHash = $('#csrfHash').val();


app.controller('changePasswordServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }
        else{
            $scope.oldPass = '';
            $scope.newPass = '';
            $scope.confPass = '';
        }
    }

    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
                       
            setTimeout(() => {
                window.location = "index.html";
                },1000)
    }
    $scope.changePassword = function () {
        if ($scope.oldPass) {
            if ($scope.newPass) {
                if ($scope.confPass) {
                        var transform = function(data) {
                            return $.param(data);
                        } 

                        $http.post(window.site_url + 'AdminLogin/changePassword', {
                            'csrf_token_name': csrfHash,

                            oldPass : $scope.oldPass ,
                            newPass : $scope.newPass ,

                        }, {
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                            transformRequest: transform
                        })
                        .then(function(response) {
                            if (response.data.status.status == "1") {
                                
                                toastr.success(response.data.status.message);
                                
                            }else{
                                toastr.error(response.data.status.message);
                            }
                        }, function(response) {
                        })
                } else {
                    $('#confPass').focus();
                    toastr.error(" conf password required.");
                }              
            } else {
                $('#newPass').focus();
                toastr.error(" New password required.");
            }

        } else {
            $('#oldPass').focus();
            toastr.error("Old Password is required.");
        }
               
    }
});


