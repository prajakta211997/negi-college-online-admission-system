
var csrfHash = $('#csrfHash').val();
app.controller('manageStudentAddRegistrationServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.addSubjectDiv = false;
            $scope.addSubjectButton = true;
            $scope.updateSubjectButton = false;
            $scope.courseNameArray =[];
            $scope.StudentListArray = [];
            $scope.getAllCoursesName();
            $scope.getStudentList();
            $scope.disabledCreateSubjectButton = false;
            $scope.disabledupdateSubjectButton = false;
            $scope.disabledDeleteSubjectButton = false;
            $scope.disabledResetUserPasswordButton = false;
            
            $scope.academicYear = "";
            $scope.courseName = "";
            $scope.courseYear = "";
            $scope.firstName = "";
            $scope.middleName = "";
            $scope.lastName = "";
             $scope.studEmail = "";
              $scope.studMobile = "";
               $scope.studPassword = "";
        }
       
        
    }
    var year = 2015;
    var range = [];
    for (var i = 2015; i <= new Date().getFullYear() ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;
    
    
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    }

    $scope.showaddSubjectDiv = function () {
        if($scope.addSubjectDiv == true){
          $scope.addSubjectDiv = false;
          $scope.updateSubjectButton = false;
          $scope.addSubjectButton = true;
        }else{
          $scope.addSubjectDiv = true;
          $scope.updateSubjectButton = false;
          $scope.addSubjectButton = true;
        }  
        $scope.courseName = '';
        $scope.courseYear = '';
        $scope.subjectType = '';
        $scope.subjectName = '';
        $scope.courseYearSemester = '';
        $scope.optionalSubTYpe = '';
       
     }
 
     $scope.editUserClick = function (Subject) {
         console.log(Subject);
         $scope.addSubjectDiv = true;
         $scope.addSubjectButton = false;
         $scope.updateSubjectButton = true;
         
         $scope.updateSubjectId =Subject.subject_id;
         $scope.courseName = Subject.course_name;
         $scope.courseYearSemester= Subject.course_semester;
         $scope.optionalSubTYpe = Subject.optional_sub_type;
         $scope.courseYear = Subject.course_year;
         $scope.subjectType = Subject.subject_type;
         $scope.subjectName = Subject.subject_name;
         
         $('html, body').animate({scrollTop: '0px'}, 1000);
     }
    var namePattern = /^[a-zA-z\s][a-zA-Z ._-\s]+$/;
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    var mobileNumberPattern = /^[7-9][0-9]{9}$/;
    

    $scope.addSubject = function () {
        
          if($scope.academicYear){
            if ($scope.courseName) {
                if ($scope.courseYear) {
                    if ($scope.firstName) {
                        if(!($scope.firstName.match(namePattern))){
                         $('#firstName').focus();
                         toastr.error("Please enter valid First Name.");
                         return false;
                        }
                     
                        if ($scope.middleName) {
                             if(!($scope.middleName.match(namePattern))){
                                 $('#middleName').focus();
                                 toastr.error("Please enter valid Middle Name.");
                                 return false;
                             }
                         
                            if($scope.lastName) {
                                 if(!($scope.lastName.match(namePattern))){
                                     $('#lastName').focus();
                                         toastr.error("Please enter valid Last Name.");
                                         return false;
                                 }
                            if ($scope.studEmail) {
                                if(!( $scope.studEmail.match(emailPattern))){
                                    $('#studEmail').focus();
                                    toastr.error("Please enter valid Email Address.");
                                    return false;   
                                }
                                    if ($scope.studMobile) {
                                        if(!( $scope.studMobile.match(mobileNumberPattern))){
                                            $('#studMobile').focus();
                                            toastr.error("Please enter valid Mobile Number.");
                                            return false;   
                                        }
                                            if ($scope.studPassword) {
                                               
                                                         var transform = function(data) {
                                                              return $.param(data);
                                                          } 
  
                                                          $http.post(window.site_url + 'StudentRegistration/studentRegistration', {
                                                              'csrf_token_name': csrfHash,
                                                              courseName : $scope.courseName ,
                                                              courseYear : $scope.courseYear ,
                                                              firstName : $scope.firstName ,
                                                              middleName : $scope.middleName ,
                                                              lastName : $scope.lastName,
                                                              studEmail : $scope.studEmail ,
                                                              studMobile : $scope.studMobile ,
                                                              studPassword : $scope.studPassword ,
                                                              academicYear :$scope.academicYear,
                                                              
                                                          }, {
                                                              headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                                              transformRequest: transform
                                                          })
                                                          .then(function(response) {
                                                              if (response.data.status.status == "1") {
  
                                                                  toastr.success(response.data.status.message);
                                                                   $scope.getStudentList();
                                                                   $scope.init();
                                                                  
                                                              }else{
                                                                  toastr.error(response.data.status.message);
                                                              }
                                                          }, function(response) {
                                                          })
                                
                                            } else {
                                                $('#studPassword').focus();
                                                toastr.error("Password is required.");
                                            }

                                    } else {
                                        $('#studMobile').focus();
                                        toastr.error("Student Mobile Number is required.");
                                    }
                            } else {
                                $('#studEmail').focus();
                                toastr.error("Student Email Address is required.");
                            }
                            } else {
                                $('#lastName').focus();
                                toastr.error("Last Name is required.");
                                }
    
                        } else {
                        $('#middleName').focus();
                        toastr.error("Middle Name is required.");
                        }
    
                    } else {
                    $('#firstName').focus();
                    toastr.error("First Name is required.");
                    }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course Year is required.");
                }
            } else {
                $('#courseName').focus();
                toastr.error(" Course is required.");
            }
        }else {
            $('#academicYear').focus();
            toastr.error(" Academic Year is Required.")
        }
    }

    $scope.getStudentList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageStudent/getStudentList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.StudentListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageStudent/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }


    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ManageStudent/searchBySubjects', {
            'csrf_token_name': csrfHash,
            SubjectSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.StudentListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
}

$scope.updateSubject = function(){
    
    if ($scope.courseName) {
        if ($scope.courseYear) {
           if($scope.courseYearSemester){
                if ($scope.subjectType) {
                        if($scope.subjectName){
                            if(!( $scope.subjectName.match(namePattern))){
                                    $('#subjectName').focus();
                                    toastr.error("Please enter valid Subject Name.");
                                    return false;   
                                    }
                        
                                        var transform = function(data) {
                                            return $.param(data);
                                        } 

                                        $http.post(window.site_url + 'ManageStudent/updateSubject', {
                                            'csrf_token_name': csrfHash,
                                                updateSubjectId: $scope.updateSubjectId,
                                                courseName :  $scope.courseName,
                                                courseYear : $scope.courseYear,
                                                courseYearSemester : $scope.courseYearSemester,
                                                subjectType : $scope.subjectType,
                                                subjectName :  $scope.subjectName,
                                                optionalSubTYpe :$scope.optionalSubTYpe
                                                
                                            }, {
                                                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                                transformRequest: transform
                                            })
                                            .then(function(response) {
                                                if (response.data.status.status == "1") {

                                                    toastr.success(response.data.status.message);
                                                    $scope.updateSubjectId = '';
                                                    $scope.init();
                                                
                                                }else{
                                                    toastr.error(response.data.status.message);
                                                }
                                                $scope.disabledCreateSubjectButton = false;
                                                $("#loadingDiv").css('display','none');
                                            }, function(response) {
                                                $scope.disabledCreateSubjectButton = false;
                                                $("#loadingDiv").css('display','none');
                                            })
                            } else {
                                $('#subjectName').focus();
                                toastr.error("Subject Name is required.");
                            } 
                    } else {
                        $('#subjectType').focus();
                        toastr.error("Subject Type is required.");
                    }               
            } else {
                $('#courseYearSemester').focus();
                toastr.error("Course Semester is required.");
                
            }
        } else {
            $('#courseYear').focus();
                toastr.error("Course Year is required.");
        }
    } else {
        $('#courseName').focus();
        toastr.error("Course Name is required.");
    }
}

$scope.deleteUserClick = function (Subject) {
    $scope.updateSubjectId = Subject.student_id;
    var transform = function(data) {
        return $.param(data);
    } 
    $scope.disabledDeleteSubjectButton = true;
    $http.post(window.site_url + 'ManageStudent/deleteStudent', {
        'csrf_token_name': csrfHash,
        updateSubjectId : Subject.student_id
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            toastr.success(response.data.status.message);
            $scope.updateSubjectId = "";
            $scope.init();
        }else {
            toastr.error(response.data.status.message);
        }
        $scope.disabledDeleteSubjectButton = false;
    }, function(response) {
        $scope.disabledDeleteSubjectButton = false;
    })
}


});



