var csrfHash = $('#csrfHash').val();
app.controller('shortlistedStudentListServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.casteListArray = [];
            $scope.religionListArray = [];
            $scope.courseNameArray =[];
            $scope.shortlistedStudentListArray =[];
            $scope.singleStudentArray = [];
            // $scope.exportData = [];
            // $scope.getAllDataForExcel();
            $scope.fileName = "AllShortlistedStudentList";
            $scope.getReligionList();
            $scope.getCasteList();
            $scope.getShortlistedStudentList();
            $scope.getAllCoursesName();
            $scope.new_url = new_url;
            $scope.subjectwisecourseName = "";
            $scope.subjectwisecourseYear ="";
            $scope.obtainedMarksFilter = "";
            $scope.courseName ="";
            $scope.courseYear ="";
            $scope.leaveDateTo = "";
            $scope.leaveDateFrom ="";
            $('#leaveDateFrom').datepicker('setDate', null);
            $('#leaveDateTo').datepicker('setDate', null);
        }
        
    }
    var year = 2015;
    var range = [];
    var range1 = [];
   
    for (var i = 2015; i <= new Date().getFullYear()  ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;

    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
            window.location = "index.html";
            },1000)
    }

    $scope.getCasteList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ShortlistedStudentList/getCasteList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.casteListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }


    $scope.stopPaymentLink = function(index,studentList){
        $scope.studentId = studentList.student_id;
        var transform = function(data) {
            return $.param(data);
        } 
        
        $http.post(window.site_url + 'ShortlistedStudentList/stopPaymentLink', {
            'csrf_token_name': csrfHash,
            studentId:studentList.student_id
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
                $scope.shortlistedStudentListArray[index].stop_payment_link = 1;
            }else {
                toastr.error(response.data.status.message);
            }
           
        }, function(response) {
           
        })
    }

    $scope.startPaymentLink = function(index,studentList){
        $scope.studentId = studentList.student_id;
        var transform = function(data) {
            return $.param(data);
        }
        
        $http.post(window.site_url + 'ShortlistedStudentList/onPaymentLink', {
            'csrf_token_name': csrfHash,
            studentId:studentList.student_id
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
                $scope.shortlistedStudentListArray[index].stop_payment_link = 0;
            }else {
                toastr.error(response.data.status.message);
            }
          
        }, function(response) {
          
        })
    }

    $scope.getReligionList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ShortlistedStudentList/getReligionList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.religionListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ShortlistedStudentList/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.showEligibilityNoDiv = function(){
        if($scope.subjectwisecourseYear == "First Year"){
            $scope.ifSecondThirdYear = false;
            $scope.ifFirstYear= true;
        }else{
            $scope.ifSecondThirdYear = true;
            $scope.ifFirstYear= false;
        }

    }
    $scope.getSubjectList = function(){
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ShortlistedStudentList/getSubjectList', {
            'csrf_token_name': csrfHash,
            courseName : $scope.subjectwisecourseName,
            courseYear : $scope.subjectwisecourseYear,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.optinalSubjectArray = [];
                $scope.optinalSubjectArraysem2 = [];
                $scope.compulsorySubjectArray = [];
                $scope.compulsorySubjectArraysem2 = [];
                $scope.valueAddedSubjectArraysem2 =[];
                $scope.valueAddedSubjectArray = [];
                $scope.SubjectListArray = response.data.data;
                
                var arraylen = $scope.SubjectListArray.length;
                for(i=0;i<arraylen ; i++){
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.optinalSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.optinalSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.compulsorySubjectArray.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.compulsorySubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'  && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.valueAddedSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'&& ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.valueAddedSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                }
                
                $scope.optionalLeng = ($scope.optinalSubjectArray).length;
                $scope.valueAddedLeng = ($scope.valueAddedSubjectArray).length;
                $scope.valueAddedsem2Leng = ($scope.valueAddedSubjectArraysem2).length;
              
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })      
    }
    
    $scope.sendPaymentReminder = function (student,paymentReminderFor) {
       
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManagePaymentReminder/sendPaymentReminderIndiusalStudent', {
            'csrf_token_name': csrfHash,
            studentId:  student.student_id,
            reminderFor: paymentReminderFor,
            courseName : student.course_name,
            courseYear: student.course_year,
            academicYear :  student.stud_academic_year,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }
    
    
    $scope.getShortlistedStudentList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ShortlistedStudentList/getShortlistedStudentList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.shortlistedStudentListArray = response.data.data;
                console.log(response.data.data);
                $scope.exportData = [];
            $scope.getAllDataForExcel();
                $scope.leng =(response.data.data).length;
                
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }


    $scope.searchOnDB = function (value) {
        
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ShortlistedStudentList/searchByDetails', {
            'csrf_token_name': csrfHash,
            detailSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.shortlistedStudentListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
    
    }

    $scope.getFilterData = function(){
    $scope.leaveDateFrom = $("#leaveDateFrom").val();
        $scope.leaveDateTo = $("#leaveDateTo").val();
        if($scope.leaveDateFrom){
            if($scope.leaveDateTo){
        if($scope.obtainedMarksFilter != ''){
            
            if($scope.courseName == ''){
                
                $('#courseName').focus();
                
                toastr.error("Please Select course name.");
                return false; 
            }
            if($scope.courseYear == ''){
                $('#courseYear').focus();
                toastr.error("Please Select course Year.");
                return false;
            }
        }
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ShortlistedStudentList/getFilterData', {
            'csrf_token_name': csrfHash,
            collegeName : $scope.collegeName,
            courseName : $scope.courseName,
            courseYear :$scope.courseYear, 
            academicYear : $scope.academicYear,
            religionName: $scope.religionName,
            casteName : $scope.casteName,
            subCasteName : $scope.subCasteName,
            admissionStatus : $scope.admissionStatus,
            obtainedMarksFilter : $scope.obtainedMarksFilter,
            leaveDateFrom : $scope.leaveDateFrom,
            leaveDateTo : $scope.leaveDateTo
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.shortlistedStudentListArray = response.data.data;
                console.log("acsdesc",response.data.data);
                $scope.exportData = [];
            $scope.getAllDataForExcel();
                $scope.leng = (response.data.data).length;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })       
            }else{
                $('#leaveDateTo').focus();
                    toastr.error("Please select date to is required.");
                    return false;
                }
        }else{
            $('#leaveDateFrom').focus();
                toastr.error("Please select date from is required.");
                return false;
            }  
    }


    $scope.clearfilter = function(){
        $scope.courseName ='';
        $scope.courseYear= ''; 
        $scope.academicYear ='';
        $scope.collegeName ="";
        $scope.religionName = "";
        $scope.casteName= "";
        $scope.subCasteName = "";
        $scope.admissionStatus ="";
        $scope.leaveDateTo = "";
        $scope.leaveDateFrom ="";
        $('#leaveDateFrom').datepicker('setDate', null);
        $('#leaveDateTo').datepicker('setDate', null);
        $scope.getShortlistedStudentList();
    }

    $scope.getSingleStudentDetails = function (student) {
        $scope.student_id = student.student_id;
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ShortlistedStudentList/getSingleStudentDetails', {
            'csrf_token_name': csrfHash,
            student_id:student.student_id,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.singleStudentArray = response.data.data;
                $scope.subjectwisecourseName = response.data.data[0].course_name;
                $scope.subjectwisecourseYear = response.data.data[0].course_year;
                $scope.showEligibilityNoDiv();
                $scope.SubjectListArray = [];
                $scope.getSubjectList();
                console.log("single student ",$scope.singleStudentArray);
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
    
    }

    $scope.removeFromShortlist = function(studentList){
        $scope.studentId = studentList.student_id;
        var transform = function(data) {
            return $.param(data);
        } 
        
        $http.post(window.site_url + 'ShortlistedStudentList/removeFromShortlist', {
            'csrf_token_name': csrfHash,
            studentId:studentList.student_id
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
                $scope.studentId = "";
                $scope.init();
            }else {
                toastr.error(response.data.status.message);
            }
           
        }, function(response) {
           
        })
    }

    $scope.addToConfirmed = function(studentList){
        $scope.studentId = studentList.student_id;
        var transform = function(data) {
            return $.param(data);
        } 
        
        $http.post(window.site_url + 'ShortlistedStudentList/addToConfirmed', {
            'csrf_token_name': csrfHash,
            studentId:studentList.student_id
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
                $scope.studentId = "";
                $scope.init();
            }else {
                toastr.error(response.data.status.message);
            }
           
        }, function(response) {
           
        })
    }

    $scope.getAllDataForExcel = function () {
        $scope.exportData = [];

        $scope.exportData.push(["Student Name","Student Mobile Number","Student Email","Student Academic Year","Student Course","Student Course Year","Gender", "Date of Birth","Mother Tongue","Address","Religion","Category","Cast","Eligibility Number", "Optional Subject Selected(Sem I/Sem III/ Sem V)","Optional Subject Selected(Sem II/Sem IV/ Sem VI)","SSC Percentage","HSC Percentage","FY Percentage","SY Percentage","TY BCom Percentage","Is Physically Handicap"]);
        angular.forEach($scope.shortlistedStudentListArray, function(value, key) {
            
            $scope.exportData.push([value.student_name, value.student_mobile, value.student_email, value.stud_academic_year, value.course_name, value.course_year, value.gender, value.date_of_birth,value.mother_tounge,value.local_address,value.religion,value.category,value.caste,value.eligibility_number,value.optinal_subject_selected,value.optinal_subject_selected_sem2,value.ssc_percentage,value.hsc_percentage,value.fy_percentage,value.sy_percentage,value.tybcom_percentage,value.is_physically_handi]);
            });
    }

    $scope.CheckUncheckHeader = function () {
        $scope.IsAllChecked = true;
        
       
        for (var i = 0; i < $scope.leng; i++) {
            if (!$scope.shortlistedStudentListArray[i].Selected) {
                $scope.IsAllChecked = false;
                break;
            }
        };
    };
    $scope.CheckUncheckHeader();

    $scope.CheckUncheckAll = function () {
        for (var i = 0; i < $scope.leng; i++) {
            $scope.shortlistedStudentListArray[i].Selected = $scope.IsAllChecked;
        }
    };

    $scope.addSelectedToConfirm = function () {
        $scope.selected = new Array();
        for (var i = 0; i < $scope.leng; i++) {
            if ($scope.shortlistedStudentListArray[i].Selected) {
                $scope.selected.push($scope.shortlistedStudentListArray[i].student_id);
            }
        }
        

        var transform = function(data) {
            return $.param(data);
        } 
        
        $http.post(window.site_url + 'ShortlistedStudentList/addSelectedToConfirm', {
            'csrf_token_name': csrfHash,
            studentId:$scope.selected
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
                $scope.selected = "";
                $scope.init();
            }else {
                toastr.error(response.data.status.message);
            }
           
        }, function(response) {
           
        })
    };

    $scope.printToCart = function(viewStudentDetails) {
        var innerContents = document.getElementById(viewStudentDetails).innerHTML;
        var popupWin; 
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/styles/style.css"/>
                <link rel="stylesheet" href="assets/fonts/themify-icons/themify-icons.css"/>
                <link rel="stylesheet" href="assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css"/>
                <link rel="stylesheet" href="assets/plugin/waves/waves.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/media/css/dataTables.bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css"/>
                <script src="assets/scripts/modernizr.min.js"></script>
                <script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/scripts/main.min.js"></script>
                <script src="assets/angular/angular.min.js"></script>
                <script src="assets/angular/ui-bootstrap-tpls-1.0.3.js"></script>
                <script src="assets/scripts/jquery.min.js"></script>
                <script src="assets/angular/angularjs/app.js"></script>
        <body onload="window.print();window.close()">${innerContents}</body>
        </html>`
        );
        popupWin.document.close();
      }


});



