
var csrfHash = $('#csrfHash').val();

app.controller('addFormFeesServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        $scope.addFormFeesDiv = false;
        $scope.addFormFeesButton = true;
        $scope.updateFormFeesButton = false;
        $scope.coursesListArray = [];
        $scope.singleCourseArray = [];
        $scope.courseNameArray =[];
        $scope.exportData = [];
        $scope.getFormFeesList();
        $scope.getAllCoursesName();
        $scope.getAllDataForExcel();
        $scope.fileName = "AllFormFeesData";
        $scope.file = "";
        $("#exampleInputFile").val('');
        $scope.disabledCreateFormFeesButton = false;
        $scope.disabledupdateFormFeesButton = false;
        $scope.disabledDeleteFormButton = false;
      // $scope.disabledResetUserPasswordButton = false;
        
       
        
    }
    var year = 2015;
    var range = [];
    var range1 = [];
   
    for (var i = 2015; i <= new Date().getFullYear()  ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;
    
   
    $scope.showaddFormFeesDiv = function () {
        if($scope.addFormFeesDiv == true){
          $scope.addFormFeesDiv = false;
          $scope.updateFormFeesButton = false;
          $scope.addFormFeesButton = true;
        }else{
          $scope.addFormFeesDiv = true;
          $scope.updateFormFeesButton = false;
          $scope.addFormFeesButton = true;
        }  
        $scope.academicYear = "";
        $scope.courseName = "";
        $scope.courseYear = "";
        $scope.formFees = "";
        
       
     }
 
    var NumberPattern = /^[0-9]{1,12}$/;

    $scope.addFormFees= function(){
        if($scope.file){
            $scope.disabledCreateFormFeesButton = true;
            $("#loadingDiv").css('display','block');
            $http({
                method: 'POST',
                url: window.site_url +'AddFormFees/excelFormFeesUpload',
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();                    
                    formData.append("FormFeesList", $scope.file);        
                    formData.append("csrf_token_name", csrfHash);                                
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).then((response) => {
                if (response.data.status.status == "1") {
                    toastr.success(response.data.status.message);
                    $scope.init();
                   // $scope.hideAddStudentDiv();
                    $scope.file = "";
                    $("#exampleInputFile").val('');
                    //$scope.getStudentGroup();
                }else {
                    toastr.error(response.data.status.message);
                    $scope.init();
                    $scope.disabledCreateFormFeesButton = false;
                    $("#loadingDiv").css('display','none');
                }
                $scope.disabledCreateFormFeesButton = false;
                $("#loadingDiv").css('display','none');
            });
        }else{
            toastr.error('Please select Form Fees excel file');
        }
    }
    
    $scope.addSingleFormFees = function(){
     
        if ($scope.academicYear) {
            if ($scope.courseName) {
                if ($scope.courseYear) {
                    if ($scope.formFees) {
                       if(!( $scope.formFees.match(NumberPattern))){
                            $('#formFees').focus();
                            toastr.error("Please enter valid Form Fees.");
                            return false;   
                        }
                        
                                                                                                                                                                                                                                                                                                        
                                var transform = function(data) {
                                    return $.param(data);
                                } 

                                $http.post(window.site_url + 'AddFormFees/addSingleFormFee', {
                                    'csrf_token_name': csrfHash,
                                    academicYear: $scope.academicYear,
                                    courseName :$scope.courseName,
                                    courseYear:$scope.courseYear,
                                    formFees: $scope.formFees,
                                    
                                }, {
                                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                    transformRequest: transform
                                })
                                .then(function(response) {
                                    if (response.data.status.status == "1") {

                                        toastr.success(response.data.status.message);
                                        $scope.academicYear = "";
                                        $scope.courseName = "";
                                        $scope.courseYear = "";
                                        $scope.formFees = "";
                                        $scope.init();
                                    
                                    }else{
                                        toastr.error(response.data.status.message);
                                    }
                                    $scope.disabledCreateFormFeesButton = false;
                                    $("#loadingDiv").css('display','none');
                                }, function(response) {
                                    $scope.disabledCreateFormFeesButton = false;
                                            $("#loadingDiv").css('display','none');
                                })
                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                        
                    } else {
                        $('#formFees').focus();
                        toastr.error("form Fee is required.");
                        }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course Year is required.");
                    }
            } else {
                $('#courseName').focus();
                toastr.error("course name is required.");
                }
        } else {
            $('#academicYear').focus();
            toastr.error("Academic Year is required.");
            }
        
    }


    $scope.getFormFeesList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddFormFees/getFormFeesList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }


    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddFormFees/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

   



    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'AddFormFees/searchByCourses', {
            'csrf_token_name': csrfHash,
            coursesSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
    }


    $scope.deleteUserClick = function (courses) {
        $scope.updateFormFeesId = courses.form_fee_id;
        var transform = function(data) {
            return $.param(data);
        } 
        $scope.disabledDeleteCoursesButton = true;
        $http.post(window.site_url + 'AddFormFees/deleteFormFees', {
            'csrf_token_name': csrfHash,
            updateFormFeesId:courses.form_fee_id
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
                $scope.updateFormFeesId = "";
                $scope.init();
            }else {
                toastr.error(response.data.status.message);
            }
            $scope.disabledDeleteFormButton = false;
        }, function(response) {
            $scope.disabledDeleteFormButton = false;
        })
    }


    $scope.getFilterData = function(){
    
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddFormFees/getFilterData', {
            'csrf_token_name': csrfHash,
            courseName : $scope.courseName,
            courseYear :$scope.courseYear, 
            academicYear : $scope.academicYear,
            
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })              
    }


    $scope.clearfilter = function(){
        $scope.courseName ='';
        $scope.courseYear= ''; 
        $scope.academicYear ='';
        $scope.getFormFeesList();
    }


    $scope.uploadedFile = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 5){
                alert("file size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 5 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'xlsx' || file.name.split('.').pop() == 'xls') {
                    var reader = new FileReader();
                    $scope.file = element.files[0];   
                    reader.readAsDataURL(file);
                } else {
                    toastr.error('Please upload correct File , File extension should be (.xls|.xlsx)');
                    return false;
                }
            }
        });
    }

  
    $scope.editUserClick = function (courses) {
        console.log(courses);
        $scope.addFormFeesDiv = true;
        $scope.addFormFeesButton = false;
        $scope.updateFormFeesButton = true;
        
        $scope.updateFormFeesId =courses.form_fee_id;
       
        $scope.academicYear = courses.academic_year ;
        $scope.courseName = courses.course_name ;
        $scope.formFees = courses.form_fees;
        $scope.courseYear = courses.course_year;
       
        
        $('html, body').animate({scrollTop: '0px'}, 1000);
    }

    $scope.updateFormFees = function(){
       
        if ($scope.academicYear) {
            if ($scope.courseName) {
                if ($scope.courseYear) {
                    if ($scope.formFees) {
                        if(!( $scope.formFees.match(NumberPattern))){
                            $('#formFees').focus();
                                toastr.error("Please enter valid Form Fees.");
                                return false;   
                                }
                                        var transform = function(data) {
                                            return $.param(data);
                                        } 

                                        $http.post(window.site_url + 'AddFormFees/updateFormFee', {
                                            'csrf_token_name': csrfHash,
                                            updateFormFeesId: $scope.updateFormFeesId,
                                            academicYear: $scope.academicYear,
                                            courseName :$scope.courseName,
                                            courseYear:$scope.courseYear,
                                            formFees: $scope.formFees,
                                            
                                            
                                        }, {
                                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                            transformRequest: transform
                                        })
                                        .then(function(response) {
                                            if (response.data.status.status == "1") {

                                                toastr.success(response.data.status.message);
                                                $scope.academicYear = "";
                                                $scope.courseName = "";
                                                $scope.courseYear = "";
                                                $scope.formFees = "";
                                                $scope.init();
                                            
                                            }else{
                                                toastr.error(response.data.status.message);
                                            }
                                            $scope.disabledCreateFormFeesButton = false;
                                            $("#loadingDiv").css('display','none');
                                            $scope.init();
                                        }, function(response) {
                                            $scope.disabledCreateFormFeesButton = false;
                                            $("#loadingDiv").css('display','none');
                                            
                                        })
                                                                                                                                                                                                                                                                                                                
                    } else {
                        $('#formFees').focus();
                        toastr.error("Form Fee is required.");
                        }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course Year is required.");
                    }
            } else {
                $('#courseName').focus();
                toastr.error("course name is required.");
                }
        } else {
            $('#academicYear').focus();
            toastr.error("Academic Year is required.");
            }

    }

    $scope.getAllDataForExcel = function () {
        $scope.disabledDownloadAllStudentButton = true;
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddFormFees/getAllDataForExcel', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.exportData = response.data.data;
                
                $scope.disabledDownloadAllCourseFeesButton = false;
            }else {
                toastr.error(response.data.status.message);
                $scope.disabledDownloadAllCourseFeesButton = false;
             }
        }, function(response) {
            $scope.disabledDownloadAllCourseFeesButton = false;
        })
    }

    

});


