
var csrfHash = $('#csrfHash').val();
app.controller('managePaymentInstallmentServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.addPaymentInstallmentDiv = false;
            $scope.addPaymentInstallmentButton = true;
            $scope.updatePaymentInstallmentButton = false;
            $scope.courseNameArray =[];
            $scope.PaymentInstallmentListArray = [];
            $scope.getPaymentInstallmentList();
            $scope.getAllCoursesName();
            
            $scope.courseName = '';
            $scope.courseYear = '' ;
            $scope.academicYear ='';
            $scope.totalFee="";
            $scope.firstInstallment ="";
            $scope.secondInstallment ="";
            $scope.thirdInstallment ="";
            $scope.courseFeesType ="";
            $scope.disabledCreatePaymentInstallmentButton = false;
            $scope.disabledupdatePaymentInstallmentButton = false;
            $scope.disabledDeletePaymentInstallmentButton = false;
            $scope.disabledResetUserPasswordButton = false;
        }
         
    }
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    } 
    $scope.showaddPaymentInstallmentDiv = function () {
        if($scope.addPaymentInstallmentDiv == true){
          $scope.addPaymentInstallmentDiv = false;
          $scope.updatePaymentInstallmentButton = false;
          $scope.addPaymentInstallmentButton = true;
        }else{
          $scope.addPaymentInstallmentDiv = true;
          $scope.updatePaymentInstallmentButton = false;
          $scope.addPaymentInstallmentButton = true;
        }  
        $scope.PaymentInstallmentName = '';
        $scope.courseName = '';
        $scope.courseYear = '' ;
        $scope.academicYear ='';
        $scope.totalFee ="";
        $scope.firstInstallment ="";
        $scope.secondInstallment ="";
        $scope.thirdInstallment ="";
        $scope.courseFeesType = "";
     }
 
     var year = 2015;
    var range = [];
    var range1 = [];
   
    for (var i = 2015; i <= new Date().getFullYear()  ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;
     $scope.editUserClick = function (PaymentInstallment) {
         console.log(PaymentInstallment);
         $scope.addPaymentInstallmentDiv = true;
         $scope.addPaymentInstallmentButton = false;
         $scope.updatePaymentInstallmentButton = true;
         
         $scope.updatePaymentInstallmentId =PaymentInstallment.payment_install_id;
         $scope.courseName =PaymentInstallment.course_name;
         $scope.courseYear =PaymentInstallment.course_year;
         $scope.academicYear =PaymentInstallment.academic_year;
         $scope.totalFee =PaymentInstallment.total_fee;
         $scope.firstInstallment =PaymentInstallment.first_installment;
         $scope.secondInstallment =PaymentInstallment.second_installment;
         $scope.thirdInstallment =PaymentInstallment.third_installment;
         $scope.courseFeesType = PaymentInstallment.fees_type;
         
         $('html, body').animate({scrollTop: '0px'}, 1000);
     }
    var namePattern = /^[a-zA-z\s][a-zA-Z ._-\s]+$/;
    
    $scope.getCourseFees = function(){
        if($scope.courseName != '' && $scope.courseYear != '' && $scope.academicYear !='' || $scope.courseFeesType !=''){
            var transform = function(data) {
                return $.param(data);
            } 
            $http.post(window.site_url + 'ManagePaymentInstallment/getCourseFees', {
                'csrf_token_name': csrfHash,
                courseName : $scope.courseName,
                courseYear : $scope.courseYear,
                academicYear: $scope.academicYear,
                courseFeesType : $scope.courseFeesType
            }, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                transformRequest: transform
            })
            .then(function(response) {
                if (response.data.status.status == "1") {
                    $scope.totalFee = response.data.data;
                     
                }else {
                    toastr.error(response.data.status.message);
                }
            }, function(response) {
            })           
        }
    }

    $scope.addPaymentInstallment = function () {
        
        if($scope.academicYear){
            if ($scope.courseName) {
                if($scope.courseYear){
                    if($scope.firstInstallment){
                
                        var transform = function(data) {
                            return $.param(data);
                        } 

                        $http.post(window.site_url + 'ManagePaymentInstallment/addPaymentInstallment', {
                            'csrf_token_name': csrfHash,
                            courseName : $scope.courseName,
                            courseYear: $scope.courseYear,
                            academicYear :  $scope.academicYear,
                            totalFee: $scope.totalFee,
                            firstInstallment: $scope.firstInstallment,
                            secondInstallment:  $scope.secondInstallment,
                            thirdInstallment:  $scope.thirdInstallment,
                            courseFeesType : $scope.courseFeesType
                            
                        }, {
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                            transformRequest: transform
                        })
                        .then(function(response) {
                            if (response.data.status.status == "1") {

                                toastr.success(response.data.status.message);
                                $scope.courseName = '';
                                $scope.courseYear = '' ;
                                $scope.academicYear ='';
                                $scope.totalFee="";
                                $scope.firstInstallment ="";
                                $scope.secondInstallment ="";
                                $scope.thirdInstallment ="";
                                $scope.courseFeesType="";
                                $scope.init();
                            
                            }else{
                                toastr.error(response.data.status.message);
                            }
                            $scope.disabledCreatePaymentInstallmentButton = false;
                            $("#loadingDiv").css('display','none');
                        }, function(response) {
                            $scope.disabledCreatePaymentInstallmentButton = false;
                                    $("#loadingDiv").css('display','none');
                        })
                    } else {
                        $('#firstInstallment').focus();
                        toastr.error("Please enter first installment is required.");
                    }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course year is required.");
                }
            } else {
                $('#courseName').focus();
                toastr.error("Coursename is required.");
            }
                                
        } else {
            $('#academicYear').focus();
            toastr.error("Academic Year is required.");
        }
    }

    $scope.getPaymentInstallmentList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManagePaymentInstallment/getPaymentInstallmentList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.PaymentInstallmentListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManagePaymentInstallment/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ManagePaymentInstallment/searchByPaymentInstallments', {
            'csrf_token_name': csrfHash,
            PaymentInstallmentSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.PaymentInstallmentListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
}

$scope.updatePaymentInstallment = function(){
    
    if($scope.academicYear){
        if ($scope.courseName) {
            if($scope.courseYear){
                if($scope.firstInstallment){
                
                    var transform = function(data) {
                        return $.param(data);
                    } 

                    $http.post(window.site_url + 'ManagePaymentInstallment/updatePaymentInstallment', {
                        'csrf_token_name': csrfHash,
                        updatePaymentInstallmentId: $scope.updatePaymentInstallmentId,
                        courseName : $scope.courseName,
                        courseYear: $scope.courseYear,
                        academicYear :  $scope.academicYear,
                        totalFee: $scope.totalFee,
                        firstInstallment: $scope.firstInstallment,
                        secondInstallment:  $scope.secondInstallment,
                        thirdInstallment:  $scope.thirdInstallment,
                        courseFeesType : $scope.courseFeesType
                        
                        
                    }, {
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                        transformRequest: transform
                    })
                    .then(function(response) {
                        if (response.data.status.status == "1") {

                            toastr.success(response.data.status.message);
                                $scope.courseName = '';
                                $scope.courseYear = '' ;
                                $scope.academicYear ='';
                                $scope.totalFee="";
                                $scope.firstInstallment ="";
                                $scope.secondInstallment ="";
                                $scope.thirdInstallment ="";
                                $scope.courseFeesType ="";
                                $scope.init();
                        }else{
                            toastr.error(response.data.status.message);
                        }
                        $scope.disabledCreatePaymentInstallmentButton = false;
                        $("#loadingDiv").css('display','none');
                    }, function(response) {
                        $scope.disabledCreatePaymentInstallmentButton = false;
                                $("#loadingDiv").css('display','none');
                    })
                                
                } else {
                    $('#firstInstallment').focus();
                    toastr.error("Please enter first installment is required.");
                }
            } else {
                $('#courseYear').focus();
                toastr.error("Course year is required.");
            }
        } else {
            $('#courseName').focus();
            toastr.error("Coursename is required.");
        }
                            
    } else {
        $('#academicYear').focus();
        toastr.error("Academic Year is required.");
    }

}

$scope.deleteUserClick = function (PaymentInstallment) {
    $scope.updatePaymentInstallmentId = PaymentInstallment.payment_install_id;
    var transform = function(data) {
        return $.param(data);
    } 
    $scope.disabledDeletePaymentInstallmentButton = true;
    $http.post(window.site_url + 'ManagePaymentInstallment/deletePaymentInstallment', {
        'csrf_token_name': csrfHash,
        updatePaymentInstallmentId:PaymentInstallment.payment_install_id
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            toastr.success(response.data.status.message);
            $scope.updatePaymentInstallmentId = "";
            $scope.init();
        }else {
            toastr.error(response.data.status.message);
        }
        $scope.disabledDeletePaymentInstallmentButton = false;
    }, function(response) {
        $scope.disabledDeletePaymentInstallmentButton = false;
    })
}


});



