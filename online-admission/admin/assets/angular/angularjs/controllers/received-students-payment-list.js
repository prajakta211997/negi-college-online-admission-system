
var csrfHash = $('#csrfHash').val();
app.controller('receivedStudentsPaymentListServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.casteListArray = [];
            $scope.religionListArray = [];
            $scope.courseNameArray =[];
            $scope.shortlistedStudentListArray =[];
            $scope.singleStudentArray = [];
            // $scope.exportData = [];
            // $scope.getAllDataForExcel();
            $scope.fileName = "AllShortlistedStudentList";
            $scope.getReligionList();
            $scope.getCasteList();
            $scope.getShortlistedStudentList();
            $scope.getAllCoursesName();
            $scope.new_url = new_url;
            $scope.subjectwisecourseName = "";
            $scope.subjectwisecourseYear ="";
            $scope.obtainedMarksFilter = "";
            $scope.courseName ="";
            $scope.courseYear ="";
            $scope.leaveDateTo = "";
            $scope.leaveDateFrom ="";
            $('#leaveDateFrom').datepicker('setDate', null);
            $('#leaveDateTo').datepicker('setDate', null);
        }
        
    }
    var year = 2015;
    var range = [];
    var range1 = [];
   
    for (var i = 2015; i <= new Date().getFullYear()  ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;

    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
            window.location = "index.html";
            },1000)
    }

    $scope.getCasteList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ReceivedStudentsPaymentList/getCasteList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.casteListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getReligionList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ReceivedStudentsPaymentList/getReligionList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.religionListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ReceivedStudentsPaymentList/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.showEligibilityNoDiv = function(){
        if($scope.subjectwisecourseYear == "First Year"){
            $scope.ifSecondThirdYear = false;
            $scope.ifFirstYear= true;
        }else{
            $scope.ifSecondThirdYear = true;
            $scope.ifFirstYear= false;
        }

    }
    
    $scope.getSubjectList = function(){
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ReceivedStudentsPaymentList/getSubjectList', {
            'csrf_token_name': csrfHash,
            courseName : $scope.subjectwisecourseName,
            courseYear : $scope.subjectwisecourseYear,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.optinalSubjectArray = [];
                $scope.optinalSubjectArraysem2 = [];
                $scope.compulsorySubjectArray = [];
                $scope.compulsorySubjectArraysem2 = [];
                $scope.valueAddedSubjectArraysem2 =[];
                $scope.valueAddedSubjectArray = [];
                $scope.SubjectListArray = response.data.data;
                
                var arraylen = $scope.SubjectListArray.length;
                for(i=0;i<arraylen ; i++){
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.optinalSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.optinalSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.compulsorySubjectArray.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.compulsorySubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'  && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.valueAddedSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'&& ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.valueAddedSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                }
                
                $scope.optionalLeng = ($scope.optinalSubjectArray).length;
                $scope.valueAddedLeng = ($scope.valueAddedSubjectArray).length;
                $scope.valueAddedsem2Leng = ($scope.valueAddedSubjectArraysem2).length;
              
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })      
    }
    
   
    
    
    $scope.getShortlistedStudentList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ReceivedStudentsPaymentList/getPaymentRecivedStudentList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.shortlistedStudentListArray = response.data.data;
                $scope.getTotal();
                $scope.exportData = [];
            $scope.getAllDataForExcel();
                $scope.leng =(response.data.data).length;
                
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }


    $scope.searchOnDB = function (value) {
        
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ReceivedStudentsPaymentList/searchByDetails', {
            'csrf_token_name': csrfHash,
            detailSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.shortlistedStudentListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
    
    }
    
    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.shortlistedStudentListArray.length; i++){
            var product = $scope.shortlistedStudentListArray[i];
            total += parseInt(product.add_amount);
        }
        return total;
    }

    $scope.getFilterData = function(){
    $scope.leaveDateFrom = $("#leaveDateFrom").val();
        $scope.leaveDateTo = $("#leaveDateTo").val();
        if($scope.leaveDateFrom){
            if($scope.leaveDateTo){
       
                var transform = function(data) {
                    return $.param(data);
                } 
                $http.post(window.site_url + 'ReceivedStudentsPaymentList/getFilterData', {
                    'csrf_token_name': csrfHash,
                    collegeName : $scope.collegeName,
                    courseName : $scope.courseName,
                    courseYear :$scope.courseYear, 
                    academicYear : $scope.academicYear,
                    leaveDateFrom : $scope.leaveDateFrom,
                    leaveDateTo : $scope.leaveDateTo
                }, {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                    transformRequest: transform
                })
                .then(function(response) {
                    if (response.data.status.status == "1") {
                        $scope.shortlistedStudentListArray = response.data.data;
                        $scope.getTotal();
                        $scope.exportData = [];
                        $scope.getAllDataForExcel();
                        $scope.leng = (response.data.data).length;
                    }else {
                        toastr.error(response.data.status.message);
                    }
                }, function(response) {
                })       
            }else{
                $('#leaveDateTo').focus();
                    toastr.error("Please select date to is required.");
                    return false;
                }
        }else{
            $('#leaveDateFrom').focus();
                toastr.error("Please select date from is required.");
                return false;
            }  
    }


    $scope.clearfilter = function(){
        $scope.courseName ='';
        $scope.courseYear= ''; 
        $scope.academicYear ='';
        $scope.collegeName ="";
        $scope.religionName = "";
        $scope.casteName= "";
        $scope.subCasteName = "";
        $scope.admissionStatus ="";
        $scope.leaveDateTo = "";
        $scope.leaveDateFrom ="";
        $('#leaveDateFrom').datepicker('setDate', null);
        $('#leaveDateTo').datepicker('setDate', null);
        $scope.getShortlistedStudentList();
        $scope.getTotal();
    }

    

    $scope.getAllDataForExcel = function () {
        $scope.exportData = [];

        $scope.exportData.push(["Student Name","Student Mobile Number","Student Email","Student Academic Year","Student Course","Student Course Year","Amount","Payment Recieved Date"]);
        angular.forEach($scope.shortlistedStudentListArray, function(value, key) {
            
            $scope.exportData.push([value.student_name, value.student_mobile, value.student_email, value.stud_academic_year, value.course_name, value.course_year, value.add_amount, value.payment_recieved_date]);
            });
    }

    $scope.CheckUncheckHeader = function () {
        $scope.IsAllChecked = true;
        
       
        for (var i = 0; i < $scope.leng; i++) {
            if (!$scope.shortlistedStudentListArray[i].Selected) {
                $scope.IsAllChecked = false;
                break;
            }
        };
    };
    $scope.CheckUncheckHeader();

    $scope.CheckUncheckAll = function () {
        for (var i = 0; i < $scope.leng; i++) {
            $scope.shortlistedStudentListArray[i].Selected = $scope.IsAllChecked;
        }
    };


    $scope.printToCart = function(viewStudentDetails) {
        var innerContents = document.getElementById(viewStudentDetails).innerHTML;
        var popupWin; 
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/styles/style.css"/>
                <link rel="stylesheet" href="assets/fonts/themify-icons/themify-icons.css"/>
                <link rel="stylesheet" href="assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css"/>
                <link rel="stylesheet" href="assets/plugin/waves/waves.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/media/css/dataTables.bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css"/>
                <script src="assets/scripts/modernizr.min.js"></script>
                <script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/scripts/main.min.js"></script>
                <script src="assets/angular/angular.min.js"></script>
                <script src="assets/angular/ui-bootstrap-tpls-1.0.3.js"></script>
                <script src="assets/scripts/jquery.min.js"></script>
                <script src="assets/angular/angularjs/app.js"></script>
        <body onload="window.print();window.close()">${innerContents}</body>
        </html>`
        );
        popupWin.document.close();
      }


});



