var csrfHash = $('#csrfHash').val();

var secretKey = "whitecode";

//var app = angular.module('myApp', ['ui.bootstrap', 'toastr']);



app.controller('loginServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
       $scope.getUserBrowserKey = localStorage.getItem('whitecode_user_key');
       if($scope.getUserBrowserKey == "" || $scope.getUserBrowserKey == null){
            $scope.getUserBrowserKey = $scope.generateUniqueKey()
            localStorage.setItem('whitecode_user_key', $scope.getUserBrowserKey);
       }else{
            $scope.getUserBrowserKey = localStorage.getItem('whitecode_user_key');
       }
        $scope.displayDeviceBitDesktop = true;
        $scope.displayDeviceBitMobile = false;
        $scope.os = (navigator.appVersion).toString();  
       // $.getJSON("https://api.ipify.org/?format=json", function(e) {
            $scope.userIP = "120.12.11.03";
       // });
        
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        //alert(isMobile);
		if (isMobile) {
  	     	$scope.displayDeviceBitMobile = true;
  	     	$scope.displayDeviceBitDesktop = false;
		} else {
	    	$scope.displayDeviceBitDesktop = true;
	    	$scope.displayDeviceBitMobile = false;
		}
		
    }

    $scope.generateUniqueKey = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
          var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
    }

    $scope.loginService = function () {
        //alert($scope.userName);
        if ($scope.userName) {
            if ($scope.userPassword) {
                var decryptedUsername = $scope.userName;
                var decryptedPassword = $scope.userPassword;
                var transform = function(data) {
                    return $.param(data);
                } 

                $http.post(window.site_url + 'AdminLogin/checkLogin', {
                    'csrf_token_name': csrfHash,
                     userName: decryptedUsername,
                     userPassword: decryptedPassword,
                     userBrowserKey:$scope.getUserBrowserKey,
                     os:$scope.os,
                     userIP:$scope.userIP.toString()
                }, {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                    transformRequest: transform
                })
                .then(function(response) {
                    
                    if (response.data.status.status == "1") {
                        //localStorage.setItem("AdminId",response.data.data.user_id);
                        localStorage.setItem("AdminLoggedIn", "True");
                        if(response.data.data.user_role == 'admin'){
                            setTimeout(() => {
                                window.location = "all-students-list.html";
                                },1000)
                            
                            toastr.success("Login successful.");
                        }else if(response.data.data.user_role == 'college'){
                            setTimeout(() => {
                                window.location = "all-students-list.html";
                                },1000)
                            toastr.success("Login successful.");
                        }
                    }else{
                        toastr.error(response.data.status.message);
                    }
                }, function(response) {
                })
            } else {
                $('#userPassword').focus();
                toastr.error("Password is required.");
            }
        } else {
            $('#userName').focus();
            toastr.error("Username is required.");
        }
    }

    $scope.encryptCodes = function(content) {
        var result = []; var passLen = secretKey.length ;
        for(var i = 0  ; i < content.length ; i++) {
            var passOffset = i%passLen ;
            var calAscii = (content.charCodeAt(i)+secretKey.charCodeAt(passOffset));
            result.push(calAscii);
        }
        return JSON.stringify(result) ;
    }

    $scope.decryptCodes = function(content) {
        var result = [];var str = '';
        var codesArr = JSON.parse(content);var passLen = secretKey.length ;
        for(var i = 0  ; i < codesArr.length ; i++) {
            var passOffset = i%passLen ;
            var calAscii = (codesArr[i]-secretKey.charCodeAt(passOffset));
            result.push(calAscii) ;
        }
        for(var i = 0 ; i < result.length ; i++) {
            var ch = String.fromCharCode(result[i]); str += ch ;
        }
        return str ;
    }

});









//var csrfHash = $('#csrfHash').val();


// app.controller('loginServicesCtrl', function ($scope, $http, toastr) {

//     $scope.init = function () {
       
//         $scope.userName = '';
//         $scope.password = '';
        
//     }

//     $scope.loginService = function () {
//         if ($scope.userName) {
//             if ($scope.password) {
                                
//                 var transform = function(data) {
//                     return $.param(data);
//                 } 

//                 $http.post(window.site_url + 'StudentLogin/checkLogin', {
//                     'csrf_token_name': csrfHash,

//                     userName : $scope.userName ,
//                     password : $scope.password ,

//                 }, {
//                     headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
//                     transformRequest: transform
//                 })
//                 .then(function(response) {
//                     if (response.data.status.status == "1") {
                        
//                         toastr.success(response.data.status.message);
//                         setTimeout(() => {
//                             window.location = "dashboard.html";
//                         },1000)
//                     }else{
//                         toastr.error(response.data.status.message);
//                     }
//                 }, function(response) {
//                 })
                                
//             } else {
//                 $('#password').focus();
//                 toastr.error("password required.");
//             }

//         } else {
//             $('#userName').focus();
//             toastr.error("Username is required.");
//         }
               
//     }
// });


