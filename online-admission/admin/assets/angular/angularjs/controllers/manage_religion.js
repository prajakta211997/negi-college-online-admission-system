
var csrfHash = $('#csrfHash').val();
app.controller('manageReligionServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.addReligionDiv = false;
            $scope.addReligionButton = true;
            $scope.updateReligionButton = false;
            $scope.religionListArray = [];
            $scope.getReligionList();
            $scope.disabledCreateReligionButton = false;
            $scope.disabledupdateReligionButton = false;
            $scope.disabledDeleteReligionButton = false;
            $scope.disabledResetUserPasswordButton = false;
        }
         
    }
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    } 
    $scope.showaddReligionDiv = function () {
        if($scope.addReligionDiv == true){
          $scope.addReligionDiv = false;
          $scope.updateReligionButton = false;
          $scope.addReligionButton = true;
        }else{
          $scope.addReligionDiv = true;
          $scope.updateReligionButton = false;
          $scope.addReligionButton = true;
        }  
        $scope.religionName = '';
        
       
     }
 
     $scope.editUserClick = function (Religion) {
         console.log(Religion);
         $scope.addReligionDiv = true;
         $scope.addReligionButton = false;
         $scope.updateReligionButton = true;
         
         $scope.updateReligionId =Religion.religion_id;
        
         $scope.religionName = Religion.religion_name;
         
         $('html, body').animate({scrollTop: '0px'}, 1000);
     }
    var namePattern = /^[a-zA-z\s][a-zA-Z ._-\s]+$/;
    
    

    $scope.addReligion = function () {
        
        if ($scope.religionName) {
            if(!( $scope.religionName.match(namePattern))){
                toastr.error("Please enter valid Name.");
                return false;   
                }
                
                    var transform = function(data) {
                        return $.param(data);
                    } 

                    $http.post(window.site_url + 'ManageReligion/addReligion', {
                        'csrf_token_name': csrfHash,
                        religionName : $scope.religionName ,
                        
                    }, {
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                        transformRequest: transform
                    })
                    .then(function(response) {
                        if (response.data.status.status == "1") {

                            toastr.success(response.data.status.message);
                            $scope.religionName = '';
                             $scope.init();
                        
                        }else{
                            toastr.error(response.data.status.message);
                        }
                        $scope.disabledCreateReligionButton = false;
                        $("#loadingDiv").css('display','none');
                    }, function(response) {
                        $scope.disabledCreateReligionButton = false;
                                $("#loadingDiv").css('display','none');
                    })
                                
        } else {
            $('#religionName').focus();
            toastr.error(" Religion Name is required.");
        }
    }

    $scope.getReligionList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageReligion/getReligionList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.religionListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ManageReligion/searchByReligions', {
            'csrf_token_name': csrfHash,
            ReligionSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.religionListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
}

$scope.updateReligion = function(){
    
       
        if ($scope.religionName) {
            if(!( $scope.religionName.match(namePattern))){
                toastr.error("Please enter valid Name.");
                return false;   
                }
                
                    var transform = function(data) {
                        return $.param(data);
                    } 

                    $http.post(window.site_url + 'ManageReligion/updateReligion', {
                        'csrf_token_name': csrfHash,
                        updateReligionId: $scope.updateReligionId,
                        religionName : $scope.religionName ,
                        
                        
                    }, {
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                        transformRequest: transform
                    })
                    .then(function(response) {
                        if (response.data.status.status == "1") {

                            toastr.success(response.data.status.message);
                            $scope.religionName = '';
                            $scope.init();
                        
                        }else{
                            toastr.error(response.data.status.message);
                        }
                        $scope.disabledCreateReligionButton = false;
                        $("#loadingDiv").css('display','none');
                    }, function(response) {
                        $scope.disabledCreateReligionButton = false;
                                $("#loadingDiv").css('display','none');
                    })
                                
        } else {
            $('#religionName').focus();
            toastr.error(" Religion Name is required.");
        }

}

$scope.deleteUserClick = function (Religion) {
    $scope.updateReligionId = Religion.religion_id;
    var transform = function(data) {
        return $.param(data);
    } 
    $scope.disabledDeleteReligionButton = true;
    $http.post(window.site_url + 'ManageReligion/deleteReligion', {
        'csrf_token_name': csrfHash,
        updateReligionId:Religion.religion_id
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            toastr.success(response.data.status.message);
            $scope.updateReligionId = "";
            $scope.init();
        }else {
            toastr.error(response.data.status.message);
        }
        $scope.disabledDeleteReligionButton = false;
    }, function(response) {
        $scope.disabledDeleteReligionButton = false;
    })
}


});



