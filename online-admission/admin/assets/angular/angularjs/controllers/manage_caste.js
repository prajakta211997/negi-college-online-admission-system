
var csrfHash = $('#csrfHash').val();
app.controller('manageCasteServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.addCasteDiv = false;
            $scope.addCasteButton = true;
            $scope.updateCasteButton = false;
            $scope.CasteListArray = [];
            $scope.religionListArray = [];
            $scope.getReligionList();
            $scope.getCasteList();
            $scope.disabledCreateCasteButton = false;
            $scope.disabledupdateCasteButton = false;
            $scope.disabledDeleteCasteButton = false;
            $scope.disabledResetUserPasswordButton = false;
        }
       
        
    }
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    }

    $scope.showaddCasteDiv = function () {
        if($scope.addCasteDiv == true){
          $scope.addCasteDiv = false;
          $scope.updateCasteButton = false;
          $scope.addCasteButton = true;
        }else{
          $scope.addCasteDiv = true;
          $scope.updateCasteButton = false;
          $scope.addCasteButton = true;
        }  
        $scope.religionName = '';
        $scope.casteName = '';
        $scope.subCasteName = '';
        
       
     }
 
     $scope.editUserClick = function (Caste) {
         console.log(Caste);
         $scope.addCasteDiv = true;
         $scope.addCasteButton = false;
         $scope.updateCasteButton = true;
         
         $scope.updateCasteId =Caste.caste_id;
         $scope.religionName = Caste.religion_id;
         $scope.casteName = Caste.caste_name;
         $scope.subCasteName = Caste.sub_caste_name;
         
         $('html, body').animate({scrollTop: '0px'}, 1000);
     }
    var namePattern = /^[a-zA-z\s][a-zA-Z .,/_-\s]+$/;
    
    

    $scope.addCaste = function () {
        
        if ($scope.religionName) {
            if ($scope.casteName) {
                if(!( $scope.casteName.match(namePattern))){
                    $('#casteName').focus();
                    toastr.error("Please enter valid Caste Name.");
                    return false;   
                    }
                    if ($scope.subCasteName) {
                    
                        var transform = function(data) {
                            return $.param(data);
                        } 

                        $http.post(window.site_url + 'ManageCaste/addCaste', {
                            'csrf_token_name': csrfHash,
                            religionName : $scope.religionName,
                            casteName : $scope.casteName ,
                            subCasteName : $scope.subCasteName,
                            
                        }, {
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                            transformRequest: transform
                        })
                        .then(function(response) {
                            if (response.data.status.status == "1") {

                                toastr.success(response.data.status.message);
                                $scope.religionName = '';
                                $scope.casteName = '';
                                $scope.subCasteName = '';
                                $scope.init();
                            
                            }else{
                                toastr.error(response.data.status.message);
                            }
                            $scope.disabledCreateCasteButton = false;
                            $("#loadingDiv").css('display','none');
                        }, function(response) {
                            $scope.disabledCreateCasteButton = false;
                            $("#loadingDiv").css('display','none');
                        })
                    } else {
                        $('#subCasteName').focus();
                        toastr.error(" SubCaste is required.");
                    }               
            } else {
                $('#casteName').focus();
                toastr.error(" Caste Name is required.");
            }
        } else {
            $('#religionName').focus();
            toastr.error(" Religion Name is required.");
        }
    }

    $scope.getCasteList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageCaste/getCasteList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.CasteListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getReligionList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageCaste/getReligionList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.religionListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ManageCaste/searchByCastes', {
            'csrf_token_name': csrfHash,
            CasteSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.CasteListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
}

$scope.updateCaste = function(){
    
       if ($scope.religionName) {
            if ($scope.casteName) {
                if(!( $scope.casteName.match(namePattern))){
                    $('#casteName').focus();
                    toastr.error("Please enter valid Name.");
                    return false;   
                    }
                    if ($scope.subCasteName) {
                    
                        var transform = function(data) {
                            return $.param(data);
                        } 

                        $http.post(window.site_url + 'ManageCaste/updateCaste', {
                            'csrf_token_name': csrfHash,
                            updateCasteId: $scope.updateCasteId,
                            religionName : $scope.religionName,
                            casteName : $scope.casteName ,
                            subCasteName : $scope.subCasteName,
                            
                            
                        }, {
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                            transformRequest: transform
                        })
                        .then(function(response) {
                            if (response.data.status.status == "1") {

                                toastr.success(response.data.status.message);
                                $scope.religionName = '';
                                $scope.casteName = '';
                                $scope.subCasteName = '';
                                $scope.init();
                            
                            }else{
                                toastr.error(response.data.status.message);
                            }
                            $scope.disabledCreateCasteButton = false;
                            $("#loadingDiv").css('display','none');
                        }, function(response) {
                            $scope.disabledCreateCasteButton = false;
                                    $("#loadingDiv").css('display','none');
                        })
                    } else {
                        $('#subCasteName').focus();
                        toastr.error("SubCaste is required.");
                    }                  
            } else {
                $('#casteName').focus();
                toastr.error(" Caste Name is required.");
            }
        } else {
            $('#religionName').focus();
            toastr.error(" Religion Name is required.");
        }
}

$scope.deleteUserClick = function (Caste) {
    $scope.updateCasteId = Caste.caste_id;
    var transform = function(data) {
        return $.param(data);
    } 
    $scope.disabledDeleteCasteButton = true;
    $http.post(window.site_url + 'ManageCaste/deleteCaste', {
        'csrf_token_name': csrfHash,
        updateCasteId:Caste.caste_id
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            toastr.success(response.data.status.message);
            $scope.updateCasteId = "";
            $scope.init();
        }else {
            toastr.error(response.data.status.message);
        }
        $scope.disabledDeleteCasteButton = false;
    }, function(response) {
        $scope.disabledDeleteCasteButton = false;
    })
}


});



