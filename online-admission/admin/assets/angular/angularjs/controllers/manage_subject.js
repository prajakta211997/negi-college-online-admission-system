
var csrfHash = $('#csrfHash').val();
app.controller('manageSubjectServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.addSubjectDiv = false;
            $scope.addSubjectButton = true;
            $scope.updateSubjectButton = false;
            $scope.courseNameArray =[];
            $scope.SubjectListArray = [];
            $scope.getAllCoursesName();
            $scope.getSubjectList();
            $scope.disabledCreateSubjectButton = false;
            $scope.disabledupdateSubjectButton = false;
            $scope.disabledDeleteSubjectButton = false;
            $scope.disabledResetUserPasswordButton = false;
        }
       
        
    }
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    }

    $scope.showaddSubjectDiv = function () {
        if($scope.addSubjectDiv == true){
          $scope.addSubjectDiv = false;
          $scope.updateSubjectButton = false;
          $scope.addSubjectButton = true;
        }else{
          $scope.addSubjectDiv = true;
          $scope.updateSubjectButton = false;
          $scope.addSubjectButton = true;
        }  
        $scope.courseName = '';
        $scope.courseYear = '';
        $scope.subjectType = '';
        $scope.subjectName = '';
        $scope.courseYearSemester = '';
        $scope.optionalSubTYpe = '';
       
     }
 
     $scope.editUserClick = function (Subject) {
         console.log(Subject);
         $scope.addSubjectDiv = true;
         $scope.addSubjectButton = false;
         $scope.updateSubjectButton = true;
         
         $scope.updateSubjectId =Subject.subject_id;
         $scope.courseName = Subject.course_name;
         $scope.courseYearSemester= Subject.course_semester;
         $scope.optionalSubTYpe = Subject.optional_sub_type;
         $scope.courseYear = Subject.course_year;
         $scope.subjectType = Subject.subject_type;
         $scope.subjectName = Subject.subject_name;
         
         $('html, body').animate({scrollTop: '0px'}, 1000);
     }
    var namePattern = /^[a-zA-z\s][a-zA-Z0-9().,/_-\s]+$/;
    
    

    $scope.addSubject = function () {
        
        if ($scope.courseName) {
            if ($scope.courseYear) {
                if($scope.courseYearSemester){
                    if ($scope.subjectType) {
                            if($scope.subjectName){
                                if(!( $scope.subjectName.match(namePattern))){
                                        $('#subjectName').focus();
                                        toastr.error("Please enter valid Subject Name.");
                                        return false;   
                                        }
                        
                                            var transform = function(data) {
                                                return $.param(data);
                                            } 

                                            $http.post(window.site_url + 'ManageSubject/addSubject', {
                                                'csrf_token_name': csrfHash,
                                                courseName :  $scope.courseName,
                                                courseYear : $scope.courseYear,
                                                courseYearSemester : $scope.courseYearSemester,
                                                subjectType : $scope.subjectType,
                                                subjectName :  $scope.subjectName,
                                                optionalSubTYpe : $scope.optionalSubTYpe
                                                
                                            }, {
                                                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                                transformRequest: transform
                                            })
                                            .then(function(response) {
                                                if (response.data.status.status == "1") {

                                                    toastr.success(response.data.status.message);
                                                    $scope.subjectName = '';
                                                    //$scope.courseYearSemester = '';
                                                    $scope.optionalSubTYpe = '';
                                                    //$scope.init();
                                                    $scope.getSubjectList();
                                                }else{
                                                    toastr.error(response.data.status.message);
                                                }
                                                $scope.disabledCreateSubjectButton = false;
                                                $("#loadingDiv").css('display','none');
                                            }, function(response) {
                                                $scope.disabledCreateSubjectButton = false;
                                                $("#loadingDiv").css('display','none');
                                            })
                            } else {
                                $('#subjectName').focus();
                                toastr.error("Subject Name is required.");
                            } 
                    } else {
                        $('#subjectType').focus();
                        toastr.error("Subject Type is required.");
                    } 
                } else {
                    $('#courseYearSemester').focus();
                    toastr.error("Course Semester is required.");
                }              
            } else {
                $('#courseYear').focus();
                toastr.error("Course Year is required.");
            }
        } else {
            $('#courseName').focus();
            toastr.error("Course Name is required.");
        }
    }

    $scope.getSubjectList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageSubject/getSubjectList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.SubjectListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageSubject/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }


    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ManageSubject/searchBySubjects', {
            'csrf_token_name': csrfHash,
            SubjectSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.SubjectListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
}

$scope.updateSubject = function(){
    
    if ($scope.courseName) {
        if ($scope.courseYear) {
           if($scope.courseYearSemester){
                if ($scope.subjectType) {
                        if($scope.subjectName){
                            if(!( $scope.subjectName.match(namePattern))){
                                    $('#subjectName').focus();
                                    toastr.error("Please enter valid Subject Name.");
                                    return false;   
                                    }
                        
                                        var transform = function(data) {
                                            return $.param(data);
                                        } 

                                        $http.post(window.site_url + 'ManageSubject/updateSubject', {
                                            'csrf_token_name': csrfHash,
                                                updateSubjectId: $scope.updateSubjectId,
                                                courseName :  $scope.courseName,
                                                courseYear : $scope.courseYear,
                                                courseYearSemester : $scope.courseYearSemester,
                                                subjectType : $scope.subjectType,
                                                subjectName :  $scope.subjectName,
                                                optionalSubTYpe :$scope.optionalSubTYpe
                                                
                                            }, {
                                                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                                transformRequest: transform
                                            })
                                            .then(function(response) {
                                                if (response.data.status.status == "1") {

                                                    toastr.success(response.data.status.message);
                                                    $scope.updateSubjectId = '';
                                                    $scope.init();
                                                
                                                }else{
                                                    toastr.error(response.data.status.message);
                                                }
                                                $scope.disabledCreateSubjectButton = false;
                                                $("#loadingDiv").css('display','none');
                                            }, function(response) {
                                                $scope.disabledCreateSubjectButton = false;
                                                $("#loadingDiv").css('display','none');
                                            })
                            } else {
                                $('#subjectName').focus();
                                toastr.error("Subject Name is required.");
                            } 
                    } else {
                        $('#subjectType').focus();
                        toastr.error("Subject Type is required.");
                    }               
            } else {
                $('#courseYearSemester').focus();
                toastr.error("Course Semester is required.");
                
            }
        } else {
            $('#courseYear').focus();
                toastr.error("Course Year is required.");
        }
    } else {
        $('#courseName').focus();
        toastr.error("Course Name is required.");
    }
}

$scope.deleteUserClick = function (Subject) {
    $scope.updateSubjectId = Subject.subject_id;
    var transform = function(data) {
        return $.param(data);
    } 
    $scope.disabledDeleteSubjectButton = true;
    $http.post(window.site_url + 'ManageSubject/deleteSubject', {
        'csrf_token_name': csrfHash,
        updateSubjectId : Subject.subject_id
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            toastr.success(response.data.status.message);
            $scope.updateSubjectId = "";
            $scope.init();
        }else {
            toastr.error(response.data.status.message);
        }
        $scope.disabledDeleteSubjectButton = false;
    }, function(response) {
        $scope.disabledDeleteSubjectButton = false;
    })
}


});



