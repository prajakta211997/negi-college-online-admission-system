<?php 

class StudentRegistration_model extends CI_Model {




    function studentRegistration(){
        $academicYear = $_POST['academicYear'];
        $courseName = $_POST['courseName'] ;
        $courseYear = $_POST['courseYear'];
        $firstName = $_POST['firstName'];
        $middleName = $_POST['middleName'];
        $lastName = $_POST['lastName'];
        $studName = $lastName.' '.$firstName.' '.$middleName ;
        $studEmail = $_POST['studEmail'];
        $studMobile = $_POST['studMobile'];
        $studPassword = $_POST['studPassword'];
       
        $query = $this->db->get_where('student_registration', array('student_email' => $studEmail,'student_password'=>$studPassword,'payment_status' => "success",'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array('stud_academic_year' =>$academicYear,
                    'course_name' => $courseName,
                    'course_year' => $courseYear,
                    'student_name' => $studName,
                    'student_email' => $studEmail,
                    'student_mobile' => $studMobile,
                    'student_password'=>$studPassword
                    );
                
                $insert = $this->db->insert('student_registration', $data);
                $insert_id =$this->db->insert_id();
                // $result['name']= $studName;
                // $result['email']= $studEmail;
                // $result['studpassword']= $studPassword;
                // $result['subject'] = 'Thank you for Registering at  Thakur Sen Negi Government College';
                // $template = 'email-template/online-admission-registration';
                // $sendMail = sendMail($result,$template);

        // $username="mucollege";
        // $password ="Mucollegesms@20";
        // $number=$studMobile;
        // $sender="MUCBLP";
        // $message="Thank you for registering with M U College  of Commerce for the ".$courseName ." ". $courseYear.". You will be  notified if you are shortlisted for the course.";
        // $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3'); 
        // $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $curl_scraped_page = curl_exec($ch);
        // curl_close($ch);                
                
                //return $this->db->affected_rows() > 0 ? 1 : 0;
                return $insert_id;
             }
    }

    function updatePaymentStatus(){
         $data = json_decode(file_get_contents("php://input"));
      
        $lastInsetId = $data->lastInsetId;
     
          // $lastInsetId = $_POST['lastInsetId'];
          // print_r($lastInsetId);

            $data = array(
                'payment_status' => "success"
            );
            $this->db->where('student_id', $lastInsetId);
            $this->db->update('student_registration', $data);
            
            $this->db->select("*");
            $this->db->from("student_registration");
            $this->db->where('student_id', $lastInsetId);
            $formFeeArray= $this->db->get()->result_array();
            
            $studName =$formFeeArray[0]['student_name'];
            $studEmail=$formFeeArray[0]['student_email'];
            $studPassword=$formFeeArray[0]['student_password'];
            
            $result['name']= $studName;
            $result['email']= $studEmail;
            $result['studpassword']= $studPassword;
            $result['subject'] = 'Thank you for Registering at  Thakur Sen Negi Government College';
            $template = 'email-template/online-admission-registration';
            $sendMail = sendMail($result,$template);
        
            return true;
    }

    function getAllCoursesName(){
        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }
    
    function getFormFees(){

        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
         
            $this->db->select("*");
            $this->db->from("manage_form_fees");
            $this->db->where('delete_bit', '0');
            $formFeeArray= $this->db->get()->result_array();
        
            foreach($formFeeArray as $formfee) {
                if(($formfee['course_name']== $courseName)&& ($formfee['course_year'] == $courseYear) && $formfee['academic_year']==$academicYear ){
                    $fees = $formfee['form_fees'];
                    return $fees;
                }
            }
            
    }
    
    
    function getAdmissionLinkStatus(){

        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
        $today = date("Y-m-d");
         
        $this->db->select("*");
        $this->db->from("manage_course");
        $this->db->where('course_name', $courseName);
        $this->db->where('courses_year', $courseYear);
        $formFeeArray= $this->db->get()->result_array();
        
        $returnValue = "0";
       foreach($formFeeArray as $formfee) {
           if($formfee['end_date'] <= $today){
            $returnValue = "1";
           }
       }
       return $returnValue;
       
    }
    
}

?>