<?php 

class ManageStudent_model extends CI_Model {

    function addSubject(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $subjectType = $_POST['subjectType'];
        $subjectName = $_POST['subjectName'];
        $courseYearSemester = $_POST['courseYearSemester'];
        $optionalSubTYpe = $_POST['optionalSubTYpe'];
        $query = $this->db->get_where('manage_subject', array(
            'course_name' => $courseName, 
            'course_year' => $courseYear,
            'course_semester'=>$courseYearSemester,
            'subject_type' => $subjectType,
            'optional_sub_type' => $optionalSubTYpe,
            'subject_name' => $subjectName,
            'delete_bit'=>'0'
            ));
            if ($query->num_rows() == 1) {
                 return -1;
            }else{
                $data = array(
                    'course_name' => $courseName,
                    'course_year' => $courseYear,
                    'course_semester'=>$courseYearSemester,
                    'subject_type' => $subjectType,
                    'optional_sub_type' => $optionalSubTYpe,
                    'subject_name' => $subjectName
                );
                
                $insert = $this->db->insert('manage_subject', $data);

                 return $this->db->affected_rows() > 0 ? 1 : 0;
            }
    }

    function getAllCoursesName(){
        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    

    function getStudentList(){
        $this->db->select("*");
        $this->db->from("student_registration");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function searchBySubjects(){
        $SubjectSearch = $_POST['SubjectSearch'];
        $this->db->select("*");
        $this->db->from("manage_subject");
        // $this->db->like('course_name', $SubjectSearch);
        // $this->db->or_like('course_year', $SubjectSearch);
        // $this->db->or_like('subject_type', $SubjectSearch);
        $this->db->like('subject_name', $SubjectSearch);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function updateSubject(){
        $updateSubjectId = $_POST['updateSubjectId'];
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $subjectType = $_POST['subjectType'];
        $subjectName = $_POST['subjectName'];
        $courseYearSemester = $_POST['courseYearSemester'];
        $optionalSubTYpe = $_POST['optionalSubTYpe'];
        $query = $this->db->get_where('manage_subject', array(
            'course_name' => $courseName,
            'course_year' => $courseYear,
            'course_semester'=>$courseYearSemester,
            'subject_type' => $subjectType,
            'optional_sub_type' => $optionalSubTYpe,
            'subject_name' => $subjectName, 
            'delete_bit'=>'0'
        ));
            if ($query->num_rows() == 1) {
                 return -1;
            }else{
                $data = array(
                    'course_name' => $courseName,
                    'course_year' => $courseYear,
                    'course_semester'=>$courseYearSemester,
                    'subject_type' => $subjectType,
                    'optional_sub_type' => $optionalSubTYpe,
                    'subject_name' => $subjectName
                );
              
                $this->db->where('subject_id', $updateSubjectId);
                $this->db->update('manage_subject', $data);
                return true; //return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    
    }
    function deleteStudent(){
        $updateSubjectId = $_POST['updateSubjectId'];
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('student_id', $updateSubjectId);
        $this->db->update('student_registration', $data);
       return true;
       
    }
  
    
    
    
}

?>