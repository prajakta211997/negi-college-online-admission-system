<?php 

class AddFormFees_model extends CI_Model {

    function addSingleFormFee(){
        
        $academicYear = $_POST['academicYear'];
        $courseName  = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $formFees = $_POST['formFees'];
        
       
        $query = $this->db->get_where('manage_form_fees', array('course_name'=> $courseName, 
                                    'academic_year'=> $academicYear, 
                                    'course_year' => $courseYear, 
                                    'form_fees' => $formFees,
                                    'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array('course_name'=> $courseName, 
                'academic_year'=> $academicYear, 
                'course_year' => $courseYear, 
                'form_fees' => $formFees
                
                    );
                
                $insert = $this->db->insert('manage_form_fees', $data);

                 return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    }

    function excelFormFeesUpload($finalarray){
        $finalArray1 = array();
      
        foreach($finalarray as $finalarr){
            
       
        $academicYear = $finalarr['academic_year'];
        $courseName  = $finalarr['course_name'];
        $courseYear = $finalarr['course_year'];
        $formFees = $finalarr['form_fees'];
        
        
            $query = $this->db->get_where('manage_form_fees', array('course_name'=> $courseName, 'academic_year'=> $academicYear,  'course_year' => $courseYear, 'form_fees' => $formFees,'delete_bit'=>'0'));
            if ($query->num_rows()) 
            {
               return -1;
            }
            else{
                $dataArray = array(
                    'course_name'=> $courseName, 
                    'academic_year'=> $academicYear, 
					'course_year' => $courseYear, 
					'form_fees' => $formFees
                    
            );
            }
       $finalArray1[] = $dataArray;
         
        }
       
        $this->db->insert_batch('manage_form_fees',$finalArray1);  
    
       return 1;
    }
    
  

    function getFormFeesList(){
        $this->db->select("*");
        $this->db->from("manage_form_fees");
        $this->db->where('delete_bit', '0');
        //$this->db->where('mc.delete_bit', '0');
        //$this->db->join("manage_course mc " ,"mc.course_id = mff.course_id");
        return $this->db->get()->result_array();
    }

    function searchByCourses(){
        $coursesSearch = $_POST['coursesSearch'];
        $this->db->select("*");
        $this->db->from("manage_form_fees mff");
        $this->db->like('mc.course_name', $coursesSearch);
        $this->db->or_like('mff.course_year', $coursesSearch);
        $this->db->or_like('mff.form_fees', $coursesSearch);
        $this->db->where('mff.delete_bit', '0');
        $this->db->where('mc.delete_bit', '0');
        $this->db->join("manage_course mc " ,"mc.course_name= mff.course_name");
        return $this->db->get()->result_array();
    }

    function updateFormFee(){
        $formFeesId = $_POST['updateFormFeesId'];
        $academicYear = $_POST['academicYear'];
        $courseName  = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $formFees = $_POST['formFees'];
        
       
        $query = $this->db->get_where('manage_form_fees', array('course_name'=> $courseName, 
        'academic_year'=> $academicYear, 
        'course_year' => $courseYear, 
        'form_fees' => $formFees,
        'delete_bit'=>'0'));


            if ($query->num_rows() == 1) {
            return -1;
            }else{
                $data = array('course_name'=> $courseName, 
                'academic_year'=> $academicYear, 
                'course_year' => $courseYear, 
                'form_fees' => $formFees
                
                );
                $this->db->where('form_fee_id', $formFeesId);
                $this->db->update('manage_form_fees', $data);
                return true;
             }
    
    }
    
    function deleteFormFees(){
        $updateFormFeesId = $_POST['updateFormFeesId'];
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('form_fee_id', $updateFormFeesId);
        $this->db->update('manage_form_fees', $data);
       
        return true;
       
    }
    
    function getFilterData(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
        
        $this->db->select("*");
        $this->db->from("manage_form_fees");
       
        $this->db->where('delete_bit', '0');
        if($courseName){
        $this->db->where('course_name',$courseName);}
        if($courseYear){
        $this->db->where('course_year',$courseYear);}
        if($academicYear){
        $this->db->where('academic_year',$academicYear);}
        return $this->db->get()->result_array();  
    }

    function getAllCoursesName(){
        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getAllDataForExcel(){
        $this->db->select("*");
        $this->db->from("manage_form_fees");
        $this->db->where('delete_bit', "0");

        $CourseArrayFromDB = $this->db->get()->result_array();
        $returnArray=array();  
        $key = 1;  
        $returnArray[0] = array("Course Name","Academic Year","Course Year","Form Fees");  
        foreach($CourseArrayFromDB as $CourseArray){
            $returnArray[$key] = array($CourseArray['course_name'], $CourseArray['academic_year'] , $CourseArray['course_year'], $CourseArray['form_fees']);  
            $key++;   
        }
        return $returnArray;
    }

    
}

?>