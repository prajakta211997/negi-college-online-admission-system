<?php 

class AdminLogin_model extends CI_Model {

    function checkLogin(){
        $userName = $_POST['userName'];
        $userPassword = $this->my_simple_crypt($_POST['userPassword'],'e');
        $userBrowserKey = $_POST['userBrowserKey'];
        $os = $_POST['os'];
        $userIP = $_POST['userIP'];
        $query = $this->db->get_where('user_login', array('user_email' => $userName, 'user_password' => $userPassword ,'user_delete_bit' => '0'));
        if ($query->num_rows() == 1) {
            $loginArrayFromDB = $query->result_array();
            foreach($loginArrayFromDB as $loginArrayv){
                if($loginArrayv['user_role'] != 'admin'){
                    //if(!empty($loginArrayv['user_login_browser_key'])){
                        //if($loginArrayv['user_login_browser_key'] == $userBrowserKey){
                            if($loginArrayv['user_admin_approval_bit']==0){
                                return 2;
                            }else if($loginArrayv['user_admin_approval_bit'] == '2') {
                                return -4;
                            }else{
                                $newdata = array(
                                    'user_id' => $loginArrayv['user_id'],
                                    'user_name' => $loginArrayv['user_name'],
                                    'user_role' => $loginArrayv['user_role'],
                                    'logged_in' => TRUE,
                                    'os' => $os,
                                    'userIP' => $userIP
                                );        
                                $this->session->set_userdata($newdata);
                               // $this->insert_user_log("Application Login Successfully");
                                return $newdata;
                            }
                        // }else{
                        //     $insertUpdateArray['user_login_browser_key']= $userBrowserKey;
                        //     $insertUpdateArray['user_admin_approval_bit']= 0;
                        //     $this->db->where('user_id', $loginArrayv['user_id']);
                        //     $this->db->update('user_login', $insertUpdateArray);
                        //     //$this->insert_user_log("Try to Login");
                        //     return -5;
                        // }
                    // }else{
                    //     $insertUpdateArray['user_login_browser_key']= $userBrowserKey;
                    //     $this->db->where('user_id', $loginArrayv['user_id']);
                    //     $this->db->update('user_login', $insertUpdateArray);
                    //     $result['name']= $_POST['userName'];
                    //     $result['email']= admin_email;
                    //     $result['subject'] = 'User login request';
                    //     $template = 'email-template/user-login-request';
                    //     $sendMail = sendUserDetailsMail($result,$template);
                    //     //$this->insert_user_log("Try to Login");
                    //     return -2;
                    // } 
                   
                }else{
                    $newdata = array(
                        'user_id' => $loginArrayv['user_id'],
                        'user_name' => $loginArrayv['user_name'],
                        'user_role' => $loginArrayv['user_role'],
                        'logged_in' => TRUE,
                        'os' => $os,
                        'userIP' => $userIP
                    );        
                    $this->session->set_userdata($newdata);
                    //$this->insert_user_log("Application Login Successfully");
                    return $newdata;
                }
            }
        } 
        else {
            return -3;
        }
        //return $userName;
    }

    // function insert_user_log ($user_activity) {
    //     $log_user_id = $this->session->userdata('user_id');
    //     $user_ip_address = $this->session->userdata('userIP');
    //     $user_devices_details = $this->session->userdata('os');
    //     $data = array(
    //         'log_user_id' => $log_user_id,
    //         'user_ip_address' => $user_ip_address,
    //         'user_devices_details' => $user_devices_details,
    //         'user_activity' => $user_activity
    //     );
    //     $insert = $this->db->insert('user_log', $data);
    // }
    function changePassword(){ 
        $oldPass = $_POST['oldPass'];
        $newPass = $this->my_simple_crypt($_POST['newPass'],'e');
        $data = array(
        'user_password' => $newPass,
        );
        
        $this->db->where('user_id', '1');
         $this->db->update('user_login', $data);
         return true;
        }



    function my_simple_crypt( $string, $action) {
        // you may change these values to your own
        $secret_key = 'whitecode_key';
        $secret_iv = 'whitecode_iv';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    }
    
}

?>