<?php 

class ManagePaymentReminder_model extends CI_Model {

    function sendPaymentReminder(){
        
        
        $courseName = $_POST['courseName'] ;
        $courseYear = $_POST['courseYear'] ;
        $academicYear = $_POST['academicYear'] ;
        
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.stud_academic_year', $academicYear);
        $this->db->where('sr.course_name', $courseName);
        $this->db->where('sr.course_year', $courseYear);
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
        $this->db->where('parentinfo.student_admission_status', '2');
        //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        $shortlistedStudArray = $this->db->get()->result_array();
       $i=0;

            
        foreach($shortlistedStudArray as $student){
            $studName = $student['student_name'];
            $studEmail = $student['student_email'];
            $studentId = $student['student_id'];
            $studMobile = $student['student_mobile'];
            
            $this->db->select("*");
            $this->db->from("manage_payment_installment");
            $this->db->where('academic_year', $academicYear);
            $this->db->where('course_name', $courseName);
            $this->db->where('course_year', $courseYear);
            $this->db->where('fees_type', $student['course_fees_type']);
            $this->db->where('delete_bit', '0');
            $paymentInstallmentArray = $this->db->get()->result_array();
           
            $TotalFees = $paymentInstallmentArray[$i]['total_fee'];
            $firstInstallment = $paymentInstallmentArray[$i]['first_installment'];
            $secondInstallment = $paymentInstallmentArray[$i]['second_installment'];
            $thirdInstallment = $paymentInstallmentArray[$i]['third_installment'];
           

            if($student['first_installment_bit'] == '0'){
                $studfeesdetails = $studName."|".$studEmail."|".$studentId."|".$firstInstallment;
                $data = array(
                    'first_installment_bit' => "1"
                );
                
                $encodedStudIdCourseFee = base64_encode($studfeesdetails);
                $genarateURL = "https://tsnegigcreckongpeo.com/online-admission/paidfees.php?FeesDeatils=$encodedStudIdCourseFee";
                
                $this->db->where('student_id', $studentId);
                $this->db->update('student_admission_form_parent_info', $data);
             
                // $result['name']= $studName;
                // $result['email']= $studEmail;
                // $result['genarateURL']= $genarateURL;
                // $result['subject'] = 'You are shortlisted. Please confirm your admission.';
                // $template = 'email-template/online-admission-payment_reminder';
                // $sendMail = sendMail($result,$template);

// $username="mucollege";
// $password ="Mucollegesms@20";
// $number=$studMobile;
// $sender="MUCBLP";
// $message="Congrats!You are shortlisted for the ".$courseName ." ". $courseYear." to verification of original documents.  Click this link to pay the fee in 4 days else your stake will be cancelled. 
// Payment Link:- ".$genarateURL;
// $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3'); 
// $ch = curl_init($url);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// $curl_scraped_page = curl_exec($ch);
// curl_close($ch);

            }
            // else if($student['second_installment_bit'] == '0'){
            //     $studfeesdetails = $studName."|".$studEmail."|".$studentId."|".$secondInstallment;
            //     $data = array(
            //         'second_installment_bit' => "1"
            //     );

            // }else {
            //     $studfeesdetails = $studName."|".$studEmail."|".$studentId."|".$thirdInstallment;
            //     $data = array(
            //         'third_installment_bit' => "1"
            //     );
            // }
            
            
            
        
            

        }
        return true;      

        }

        function getAllCoursesName(){
            $this->db->distinct();
            $this->db->select("course_name");
            $this->db->from("manage_course");
    
            $this->db->where('delete_bit', '0');
            return $this->db->get()->result_array();
        }
        
        
        
        function checkPayentLinkIsStopOrNot(){
            $studID = $_POST['studID'] ;
            $this->db->select("*");
            $this->db->from("student_registration");
            $this->db->where('student_id', $studID);
            $studArray = $this->db->get()->result_array();
            $retValu = 0;
            foreach($studArray as $student){
                if($student['stop_payment_link'] == 1){
                    $retValu = 1;
                }
            }
            return $retValu;
        }
        
        
        
        function updateStudentPaymentStatus(){
            $studID = $_POST['studID'] ;
            $studName = $_POST['studName'] ;
            $studPayFees = $_POST['studPayFees'] ;
            
            $data = array(
                        'student_id' => $studID,
                        'add_amount' => $studPayFees,
                        'payment_recieved_date' => date("d-m-Y"),
                        'comment' => "Pay By Online",
                );
                
            $insert = $this->db->insert('student_course_fees_payment_details', $data);

            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
    

    

  function sendPaymentReminderIndiusalStudent(){
        
        $studentId = $_POST['studentId'] ;
        $courseName = $_POST['courseName'] ;
        $courseYear = $_POST['courseYear'] ;
        $academicYear = $_POST['academicYear'];
        $reminderFor = $_POST['reminderFor'];
        
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.student_id', $studentId);
        $this->db->where('sr.stud_academic_year', $academicYear);
        $this->db->where('sr.course_name', $courseName);
        $this->db->where('sr.course_year', $courseYear);
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
        //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        $shortlistedStudArray = $this->db->get()->result_array();
       $i=0;

           
        foreach($shortlistedStudArray as $student){
            $studName = $student['student_name'];
            $studEmail = $student['student_email'];
            $studentId = $student['student_id'];
            $studMobile = $student['student_mobile'];
            
            $this->db->select("*");
            $this->db->from("manage_payment_installment");
            $this->db->where('academic_year', $academicYear);
            $this->db->where('course_name', $courseName);
            $this->db->where('course_year', $courseYear);
            $this->db->where('fees_type', $student['course_fees_type']);
            $this->db->where('delete_bit', '0');
            $paymentInstallmentArray = $this->db->get()->result_array();
           
            $TotalFees = $paymentInstallmentArray[$i]['total_fee'];
            $firstInstallment = $paymentInstallmentArray[$i]['first_installment'];
            $secondInstallment = $paymentInstallmentArray[$i]['second_installment'];
            $thirdInstallment = $paymentInstallmentArray[$i]['third_installment'];
           

            if($reminderFor == '1'){
                if($firstInstallment){
                    $studfeesdetails = $studName."|".$studEmail."|".$studentId."|".$firstInstallment;
                    $data = array(
                        'first_installment_bit' => "1"
                    );
                    $encodedStudIdCourseFee = base64_encode($studfeesdetails);
                    $genarateURL = "https://tsnegigcreckongpeo.com/online-admission/paidfees.php?FeesDeatils=$encodedStudIdCourseFee";
                    
                    $this->db->where('student_id', $studentId);
                    $this->db->update('student_admission_form_parent_info', $data);
                 
                    // $result['name']= $studName;
                    // $result['email']= $studEmail;
                    // $result['courseName'] = $courseName;
                    // $result['genarateURL']= $genarateURL;
                    // $result['subject'] = 'You are shortlisted. Please confirm your admission.';
                    // $template = 'email-template/online-admission-payment_reminder';
                    // $sendMail = sendMail($result,$template);

// $username="mucollege";
// $password ="Mucollegesms@20";
// $number=$studMobile;
// $sender="MUCBLP";
// $message="Congrats!You are shortlisted for the ".$courseName ." ". $courseYear." to verification of original documents.  Click this link to pay the fee in 3 days else your stake will be cancelled. 
// Payment Link:- ".$genarateURL;
// $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3'); 
// $ch = curl_init($url);
// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// $curl_scraped_page = curl_exec($ch);
// curl_close($ch);
                }
            }
            if($reminderFor == '2'){
                if($secondInstallment){
                    $studfeesdetails = $studName."|".$studEmail."|".$studentId."|".$secondInstallment;
                    $data = array(
                        'second_installment_bit' => "1"
                    );
                    $encodedStudIdCourseFee = base64_encode($studfeesdetails);
                    $genarateURL = "https://tsnegigcreckongpeo.com/online-admission/paidfees.php?FeesDeatils=$encodedStudIdCourseFee";
                    $this->db->where('student_id', $studentId);
                    $this->db->update('student_admission_form_parent_info', $data);
                 
                    // $result['name']= $studName;
                    // $result['email']= $studEmail;
                    // $result['courseName'] = $courseName;
                    // $result['genarateURL']= $genarateURL;
                    // $result['subject'] = 'You are shortlisted. Please confirm your admission.';
                    // $template = 'email-template/online-admission-payment_reminder';
                    // $sendMail = sendMail($result,$template);


                }
            }
            if($reminderFor == '3'){
                if($thirdInstallment){
                    $studfeesdetails = $studName."|".$studEmail."|".$studentId."|".$thirdInstallment;
                    $data = array(
                        'third_installment_bit' => "1"
                    );
                    $encodedStudIdCourseFee = base64_encode($studfeesdetails);
                    $genarateURL = "https://tsnegigcreckongpeo.com/online-admission/paidfees.php?FeesDeatils=$encodedStudIdCourseFee";
                    $this->db->where('student_id', $studentId);
                    $this->db->update('student_admission_form_parent_info', $data);
                 
                    // $result['name']= $studName;
                    // $result['email']= $studEmail;
                    // $result['courseName'] = $courseName;
                    // $result['genarateURL']= $genarateURL;
                    // $result['subject'] = 'You are shortlisted. Please confirm your admission.';
                    // $template = 'email-template/online-admission-payment_reminder';
                    // $sendMail = sendMail($result,$template);
                }
            }
        }
        return true;      

        }

    


    
    
    
    
}

?>