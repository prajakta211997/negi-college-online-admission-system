<?php 

class AdmissionForm_model extends CI_Model {

    function submitPersonalInfo(){
        $studentId = $_POST['studentId'];
        $isSindhi =  $_POST['isSindhi'];
        $fromJaihind = $_POST['fromJaihind'];
        $firstName = $_POST['firstName'];
        $middleName = $_POST['middleName'];
        $lastName = $_POST['lastName'];
        $studName = $lastName.' '.$firstName.' '.$middleName;
        $motherName = $_POST['motherName'];
        $emailId = $_POST['emailId'];
        $mobileNumber = $_POST['mobileNumber'];
        $alterMobileNumber = $_POST['alterMobileNumber'];
        $aadharNumber = $_POST['aadharNumber'];
        $voterNumber = $_POST['voterNumber'];
        $localAddress = $_POST['localAddress'];
        $nativePlace = $_POST['nativePlace'];
        $bloodGroup = $_POST['bloodGroup'];
        $gender = $_POST['gender'];
        $maritalstatus = $_POST['maritalstatus'];
        $dateOfBirth = $_POST['dateOfBirth'];
        $birthPlace = $_POST['birthPlace'];
        $dobInWord = $_POST['dobInWord'];
        $district = $_POST['district'];
        $state = $_POST['state'];
        $nationality = $_POST['nationality'];
        $Passport = $_POST['Passport'];
        $religion = $_POST['religion'];
        $caste = $_POST['caste'];
        $category = $_POST['category'];
        $motherTongue = $_POST['motherTongue'];
        $isCastCertificate  = $_POST['isCastCertificate'];
        $physicallyhandi = $_POST['physicallyhandi'];
        $eligibilityNumber = $_POST['eligibilityNumber'];
        $rollNumber = $_POST['rollNumber'];
        $correspondanceAddress = $_POST['correspondanceAddress'];
        $guardianAddress = $_POST['guardianAddress'];
        $game = $_POST['game'];
        $hobbies = $_POST['hobbies'];
        
        
        $admissionType = $_POST['admissionType'];
        $regdNumber = $_POST['regdNumber'];
        
        $location = "uploads/".$firstName." ".$lastName;
        if(!is_dir($location)){
            mkdir($location,0755,TRUE);
        }

        if(isset($_FILES['photo']) && !empty( $_FILES["photo"])){
            $photo_data = $_FILES['photo'];
            $photoname = $_FILES['photo']['name']; 
            $photolocation = $location."/".$photoname;
            move_uploaded_file($_FILES['photo']['tmp_name'],$photolocation);
            
        }   
        if(isset($_FILES['signature'])){   
            $signature_data = $_FILES['signature']['name']; 
            $signature_path = $_FILES['signature']['tmp_name'];
            $signlocation =$location. "/".$signature_data;
            move_uploaded_file($signature_path,$signlocation);
            
        } 
        if(isset($_FILES['casteCertificate'])){     
            $casteCertificate_data = $_FILES['casteCertificate']['name'];  
            $casteCertificate_path = $_FILES['casteCertificate']['tmp_name'];
            $certificatelocation = $location."/".$casteCertificate_data;
            move_uploaded_file($casteCertificate_path,$certificatelocation);
        }
        if(isset($_FILES['physicallychallengedCertificate'])){     
            $physicallychallengedCertificate_data = $_FILES['physicallychallengedCertificate']['name'];  
            $physicallychallengedCertificate_path = $_FILES['physicallychallengedCertificate']['tmp_name'];
            $physicallychallengedCertificatelocation = $location."/".$physicallychallengedCertificate_data;
            move_uploaded_file($physicallychallengedCertificate_path,$physicallychallengedCertificatelocation);
        } 
        if(isset($_FILES['parentSignature'])){   
            $parentSignature_data = $_FILES['parentSignature']['name']; 
            $parentSignature_path = $_FILES['parentSignature']['tmp_name'];
            $parentSignaturelocation =$location. "/".$parentSignature_data;
            move_uploaded_file($parentSignature_path,$parentSignaturelocation);
            
        }
        for($i=0; $i<5; $i++){
            if(isset($_FILES['multipleDocument'.$i]))
            { 
                $multipleDocument_data[$i]=$_FILES['multipleDocument'.$i]['name'];
                $multipleDocument_path[$i]=$_FILES['multipleDocument'.$i]['tmp_name'];
                $multiDocumlocation[$i]= $location."/".$multipleDocument_data[$i];
                move_uploaded_file($multipleDocument_path[$i],$multiDocumlocation[$i]);
                $loc= implode(',',$multiDocumlocation);
            } 
        }
      
 

        $query = $this->db->get_where('student_admission_form_personal_info', array(
            'student_id' => $studentId
        ));
        if ($query->num_rows() == 1) {
            $query1 = $this->db->get_where('student_admission_form_personal_info', array(
                        'is_sindhi_category'=>$isSindhi,
                        'is_jaihind_college'=> $fromJaihind,
                        'first_name'=> $firstName,
                        'middle_name' =>$middleName,
                        'last_name' => $lastName,
                        'mother_name '=>$motherName,
                        'email_address'=> $emailId,
                        'mobile_number'=>$mobileNumber,
                        'alter_mobile_number'=>$alterMobileNumber,
                        'adhar_card_id' => $aadharNumber,
                        'voter_id'=>$voterNumber,
                        'local_address' => $localAddress,
                        'roll_number' => $rollNumber,
                        'correspondance_address' => $correspondanceAddress,
                        'guardian_address' =>$guardianAddress,
                        'native_place' => $nativePlace,
                        'blood_group'=> $bloodGroup,
                        'gender' => $gender,
                        'marital_status'=> $maritalstatus,
                        'date_of_birth'=> $dateOfBirth,
                        'birth_place'=>$birthPlace,
                        'dob_in_word'=> $dobInWord,
                        'district' => $district,
                        'state' =>$state,
                        'nationality' => $nationality,
                        'passport' => $Passport,
                        'religion' => $religion,
                        'caste'=> $caste,
                        'category'=>$category,
                        'mother_tounge' => $motherTongue,
                        'is_cast_certificate'=> $isCastCertificate,
                        'is_physically_handi' => $physicallyhandi,
                        'eligibility_number' => $eligibilityNumber,
                        'game_played' => $game,
                        'hobbies' => $hobbies,
                        'admission_type'=>$admissionType,
                        'regd_number'=> $regdNumber,
                        
                    ));

            if(isset($_FILES['photo'])){
            $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_photo'=>$photolocation));
            }
            if(isset($_FILES['signature'])){
            $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_signature' => $signlocation));
            }
            if(isset($_FILES['casteCertificate'])){
            $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_caste_certificate' =>$certificatelocation));
            }
            if(isset($_FILES['physicallychallengedCertificate'])){
                $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_physically_handi_certificate' =>$physicallychallengedCertificatelocation));
            }
            if(isset($_FILES['parentSignature'])){
                $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_parent_signature' => $parentSignaturelocation));
            }
            if(isset($_FILES['multipleDocument0'])){
            $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_other_documents'=>$loc));
            }
            if ($query1->num_rows() == 1) {
                 return -1;
            }else{

           $data = array(
                        'is_sindhi_category'=>$isSindhi,
                        'is_jaihind_college'=> $fromJaihind,
                        'first_name'=> $firstName,
                        'middle_name' =>$middleName,
                        'last_name' => $lastName,
                        'mother_name '=>$motherName,
                        'email_address'=> $emailId,
                        'mobile_number'=>$mobileNumber,
                        'alter_mobile_number'=>$alterMobileNumber,
                        'adhar_card_id' => $aadharNumber,
                        'voter_id'=>$voterNumber,
                        'local_address' => $localAddress,
                        'roll_number' => $rollNumber,
                        'correspondance_address' => $correspondanceAddress,
                        'guardian_address' => $guardianAddress,
                        'native_place' => $nativePlace,
                        'blood_group'=> $bloodGroup,
                        'gender' => $gender,
                        'marital_status'=> $maritalstatus,
                        'date_of_birth'=> $dateOfBirth,
                        'birth_place'=>$birthPlace,
                        'dob_in_word'=> $dobInWord,
                        'district' => $district,
                        'state' =>$state,
                        'nationality' => $nationality,
                        'passport' => $Passport,
                        'religion' => $religion,
                        'caste'=> $caste,
                        'category'=>$category,
                        'mother_tounge' => $motherTongue,
                        'is_cast_certificate'=> $isCastCertificate,
                        'is_physically_handi' => $physicallyhandi,
                        'eligibility_number' => $eligibilityNumber,
                        'game_played' => $game,
                        'hobbies' => $hobbies,
                        'admission_type'=>$admissionType,
                        'regd_number'=> $regdNumber,
                        
                       
                    );
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_personal_info', $data);
                    
                    if(isset($_FILES['photo'])){
                        $photo =array('uploaded_photo'=>$photolocation);
                        $photoArray = array_merge($data,$photo);
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_admission_form_personal_info', $photoArray);
                    }
                    if(isset($_FILES['signature'])){
                        $sign =array('uploaded_signature' => $signlocation,);
                        $signArray = array_merge($data,$sign);
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_admission_form_personal_info', $signArray);
                    }
                    if(isset($_FILES['casteCertificate'])){
                        $casteCertificate =array('uploaded_caste_certificate' =>$certificatelocation,);
                        $casteArray = array_merge($data,$casteCertificate);
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_admission_form_personal_info', $casteArray);
                    }
                    if(isset($_FILES['physicallychallengedCertificate'])){
                        $physicallychallengedCertificate =array('uploaded_physically_handi_certificate' =>$physicallychallengedCertificatelocation,);
                        $physicallyhandiArray = array_merge($data,$physicallychallengedCertificate);
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_admission_form_personal_info', $physicallyhandiArray);
                    }
                    if(isset($_FILES['parentSignature'])){
                        $parentSignature =array('uploaded_parent_signature' => $parentSignaturelocation,);
                        $parentSignatureArray = array_merge($data,$parentSignature);
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_admission_form_personal_info', $parentSignatureArray);
                    }
                    if(isset($_FILES['multipleDocument0'])){
                        $multipleDocument =array('uploaded_other_documents'=>$loc,);
                        $docArray = array_merge($data,$multipleDocument);
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_admission_form_personal_info', $docArray);
                    }
                    if($firstName != '' &&  $middleName != '' && $lastName != '' && $emailId != '' && $mobileNumber != '' &&  $aadharNumber != '' && $localAddress != '' && $nativePlace != '' && $bloodGroup != ''&&  $gender != '' && $maritalstatus != '' && $dateOfBirth != ''  && $district != ''  && $state != ''  && $nationality != ''  && $motherTongue != ''  && $isCastCertificate != ''    )
                    {  
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_registration', array('admission_step_one'=> '1'));
                        
                    }
                    
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_registration', array('student_name' => $studName));
                    
                
                    
                    
           return 2;}
             }else{

                $data = array('student_id' => $studentId,
                'is_sindhi_category'=>$isSindhi,
                'is_jaihind_college'=> $fromJaihind,
                'first_name'=> $firstName,
                'middle_name' =>$middleName,
                'last_name' => $lastName,
                'mother_name '=>$motherName,
                'email_address'=> $emailId,
                'mobile_number'=>$mobileNumber,
                'alter_mobile_number'=>$alterMobileNumber,
                'adhar_card_id' => $aadharNumber,
                'voter_id'=>$voterNumber,
                'local_address' => $localAddress,
                'roll_number' => $rollNumber,
                'correspondance_address' => $correspondanceAddress,
                'guardian_address' => $guardianAddress,
                'native_place' => $nativePlace,
                'blood_group'=> $bloodGroup,
                'gender' => $gender,
                'marital_status'=> $maritalstatus,
                'date_of_birth'=> $dateOfBirth,
                'birth_place'=>$birthPlace,
                'dob_in_word'=> $dobInWord,
                'district' => $district,
                'state' =>$state,
                'nationality' => $nationality,
                'passport' => $Passport,
                'religion' => $religion,
                'caste'=> $caste,
                'category' =>$category,
                'mother_tounge' => $motherTongue,
                'is_cast_certificate'=> $isCastCertificate,
                'is_physically_handi' => $physicallyhandi,
                'eligibility_number' => $eligibilityNumber,
                'game_played' => $game,
                'hobbies' => $hobbies,
                'admission_type'=>$admissionType,
                'regd_number'=> $regdNumber,
                
                
                    );
                    $insert = $this->db->insert('student_admission_form_personal_info', $data);
                        if(isset($_FILES['photo'])){
                            $photo =array('uploaded_photo'=>$photolocation);
                            $photoArray = array_merge($data,$photo);
                            
                            $this->db->where('student_id',$studentId);
                            $this->db->update('student_admission_form_personal_info', $photoArray);
                        }
                    
                        if(isset($_FILES['signature'])){
                            $sign =array('uploaded_signature' => $signlocation,);
                            $signArray = array_merge($data,$sign);
                            $this->db->where('student_id',$studentId);
                            $this->db->update('student_admission_form_personal_info', $signArray);
                        } 
                        
                        if(isset($_FILES['casteCertificate'])){
                            $casteCertificate =array('uploaded_caste_certificate' =>$certificatelocation,);
                            $casteArray = array_merge($data,$casteCertificate);
                            $this->db->where('student_id',$studentId);
                            $this->db->update('student_admission_form_personal_info', $casteArray);
                        }
                        if(isset($_FILES['physicallychallengedCertificate'])){
                            $physicallychallengedCertificate =array('uploaded_physically_handi_certificate' =>$physicallychallengedCertificatelocation,);
                            $physicallyhandiArray = array_merge($data,$physicallychallengedCertificate);
                            $this->db->where('student_id',$studentId);
                            $this->db->update('student_admission_form_personal_info', $physicallyhandiArray);
                        }
                        
                        if(isset($_FILES['parentSignature'])){
                            $parentSignature =array('uploaded_parent_signature' => $parentSignaturelocation);
                            $parentSignatureArray = array_merge($data,$parentSignature);
                            $this->db->where('student_id',$studentId);
                            $this->db->update('student_admission_form_personal_info', $parentSignatureArray);
                        }
                        if(isset($_FILES['multipleDocument0'])){
                            $multipleDocument =array('uploaded_other_documents'=>$loc,);
                            $docArray = array_merge($data,$multipleDocument);
                            $this->db->where('student_id',$studentId);
                            $this->db->update('student_admission_form_personal_info', $docArray);
                        } 
                   
                        if($firstName != '' &&  $middleName != '' && $lastName != '' && $emailId != '' && $mobileNumber != '' &&  $aadharNumber != '' && $localAddress != '' && $nativePlace != '' && $bloodGroup != ''&&  $gender != '' && $maritalstatus != '' && $dateOfBirth != ''  && $district != ''  && $state != ''  && $nationality != ''   && $motherTongue != ''  && $isCastCertificate != ''   )
                    {  
                        $this->db->where('student_id',$studentId);
                        $this->db->update('student_registration', array('admission_step_one'=> '1'));
                        
                    }
                    // if($studName != ''){
                    // $this->db->where('student_id',$studentId);
                    // $this->db->update('student_registration', array('student_name' => $studName));
                    // }
                 return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    }

    function submitAcademicInfo(){
        $studentId = $_POST['studentId'];
        $optinalSubjectSelected = $_POST['optinalSubjectSelected'];
        $optinalSubjectSelectedsem2 = $_POST['optinalSubjectSelectedsem2'];
        $valueAddedSubjectSelected = $_POST['valueAddedSubjectSelected'];
        $valueAddedSubjectSelectedsem2 = $_POST['valueAddedSubjectSelectedsem2'];
        $isMaharashtra = $_POST['isMaharashtra'];
        $sscExam = $_POST['sscExam'];
        $sscBoard = $_POST['sscBoard'];
        $sscClgName = $_POST['sscClgName'];
        $sscPassingYear = $_POST['sscPassingYear'];
        $sscSeatNo = $_POST['sscSeatNo'];
        $sscMarksObtain = $_POST['sscMarksObtain'];
        $sscTotalMarks = $_POST['sscTotalMarks'];
        $sscPercentage = $_POST['sscPercentage'];
        $hscExam = $_POST['hscExam'];
        $hscBoard = $_POST['hscBoard'];
        $hscClgName = $_POST['hscClgName'];
        $hscPassingYear = $_POST['hscPassingYear'];
        $hscSeatNo = $_POST['hscSeatNo'];
        $hscMarksObtain = $_POST['hscMarksObtain'];
        $hscTotalMarks = $_POST['hscTotalMarks'];
        $hscPercentage = $_POST['hscPercentage'];

        $tyBComExam = $_POST['tyBComExam'];
        $tyBComBoard = $_POST['tyBComBoard'];
        $tyBComClgName = $_POST['tyBComClgName'];
        $tyBComPassingYear = $_POST['tyBComPassingYear'];
        $tyBComSeatNo = $_POST['tyBComSeatNo'];
        $tyBComMarksObtain = $_POST['tyBComMarksObtain'];
        $tyBComTotalMarks = $_POST['tyBComTotalMarks'];
        $tyBComPercentage = $_POST['tyBComPercentage'];

        $fyExam = $_POST['fyExam'];
        $fyBoard = $_POST['fyBoard'];
        $fyClgName = $_POST['fyClgName'];
        $fyPassingYear = $_POST['fyPassingYear'];
        $fySeatNo = $_POST['fySeatNo'];
        $fyMarksObtain = $_POST['fyMarksObtain'];
        $fyTotalMarks = $_POST['fyTotalMarks'];
        $fyPercentage  = $_POST['fyPercentage'];
        $syExam = $_POST['syExam'];
        $syBoard = $_POST['syBoard'];
        $syClgName = $_POST['syClgName'];
        $syPassingYear = $_POST['syPassingYear'];
        $sySeatNo = $_POST['sySeatNo'];
        $syMarksObtain = $_POST['syMarksObtain'];
        $syTotalMarks = $_POST['syTotalMarks'];
        $syPercentage = $_POST['syPercentage'];
        
        $fyExamSem2 = $_POST['fyExamSem2'];
        $fyBoardSem2 = $_POST['fyBoardSem2'];
        $fyClgNameSem2 = $_POST['fyClgNameSem2'];
        $fyPassingYearSem2 = $_POST['fyPassingYearSem2'];
        $fySeatNoSem2 = $_POST['fySeatNoSem2'];
        $fyMarksObtainSem2  = $_POST['fyMarksObtainSem2'];
        $fyTotalMarksSem2 = $_POST['fyTotalMarksSem2'];
        $fyPercentageSem2  = $_POST['fyPercentageSem2'];
        $syExamSem2 = $_POST['syExamSem2'];
        $syBoardSem2 = $_POST['syBoardSem2'];
        $syClgNameSem2 = $_POST['syClgNameSem2'];
        $syPassingYearSem2 = $_POST['syPassingYearSem2'];
        $sySeatNoSem2 = $_POST['sySeatNoSem2'];
        $syMarksObtainSem2 = $_POST['syMarksObtainSem2'];
        $syTotalMarksSem2 = $_POST['syTotalMarksSem2'];
        $syPercentageSem2 = $_POST['syPercentageSem2'];
        
        $appearingYear = $_POST['appearingYear'];
        
        $isgapReason = $_POST['isgapReason'];
        $gapReason = $_POST['gapReason'];
        $appliedOtherCollege = $_POST['appliedOtherCollege'];
        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];

        $location = "uploads/".$firstName." ".$lastName;
        if(!is_dir($location)){
            mkdir($location,0755,TRUE);

        }
        if(isset($_FILES['sscMarksheet'])){   
            $sscMarksheet_data = $_FILES['sscMarksheet']['name']; 
            $sscMarksheet_path = $_FILES['sscMarksheet']['tmp_name'];
            $sscMarksheetlocation =$location. "/".$sscMarksheet_data;
            move_uploaded_file($sscMarksheet_path,$sscMarksheetlocation);
            
        } 
        if(isset($_FILES['hscMarksheet'])){   
            $hscMarksheet_data = $_FILES['hscMarksheet']['name']; 
            $hscMarksheet_path = $_FILES['hscMarksheet']['tmp_name'];
            $hscMarksheetlocation =$location. "/".$hscMarksheet_data;
            move_uploaded_file($hscMarksheet_path,$hscMarksheetlocation);
            
        } 

        if(isset($_FILES['tyBComMarksheet'])){   
            $tyBComMarksheet_data = $_FILES['tyBComMarksheet']['name']; 
            $tyBComMarksheet_path = $_FILES['tyBComMarksheet']['tmp_name'];
            $tyBComMarksheetlocation =$location. "/".$tyBComMarksheet_data;
            move_uploaded_file($tyBComMarksheet_path,$tyBComMarksheetlocation);
            
        } 
        
        if(isset($_FILES['fyMarksheet'])){   
            $fyMarksheet_data = $_FILES['fyMarksheet']['name']; 
            $fyMarksheet_path = $_FILES['fyMarksheet']['tmp_name'];
            $fyMarksheetlocation =$location. "/".$fyMarksheet_data;
            move_uploaded_file($fyMarksheet_path,$fyMarksheetlocation);
            
        } 
        if(isset($_FILES['syMarksheet'])){   
            $syMarksheet_data = $_FILES['syMarksheet']['name']; 
            $syMarksheet_path = $_FILES['syMarksheet']['tmp_name'];
            $syMarksheetlocation =$location. "/".$syMarksheet_data;
            move_uploaded_file($syMarksheet_path,$syMarksheetlocation);
            
        } 
        if(isset($_FILES['fyMarksheetSem2'])){   
            $fyMarksheet_datasem2 = $_FILES['fyMarksheetSem2']['name']; 
            $fyMarksheet_pathsem2 = $_FILES['fyMarksheetSem2']['tmp_name'];
            $fyMarksheetlocationsem2 =$location. "/".$fyMarksheet_datasem2;
            move_uploaded_file($fyMarksheet_pathsem2,$fyMarksheetlocationsem2);
            
        } 
        if(isset($_FILES['syMarksheetSem2'])){   
            $syMarksheet_datasem2 = $_FILES['syMarksheetSem2']['name']; 
            $syMarksheet_pathsem2 = $_FILES['syMarksheetSem2']['tmp_name'];
            $syMarksheetlocationsem2 =$location. "/".$syMarksheet_datasem2;
            move_uploaded_file($syMarksheet_pathsem2,$syMarksheetlocationsem2);
            
        } 

    
        $query = $this->db->get_where('student_admission_form_academic_info', array(
            'student_id' => $studentId
        ));
        if ($query->num_rows() == 1) {
            $query1 = $this->db->get_where('student_admission_form_academic_info', array(
                'optinal_subject_selected' => $optinalSubjectSelected,
                'optinal_subject_selected_sem2' => $optinalSubjectSelectedsem2,
                
                'value_added_subject_selected' => $valueAddedSubjectSelected,
                'value_added_subject_selected_sem2' => $valueAddedSubjectSelectedsem2,
                'is_maharashtra' => $isMaharashtra,
                'ssc_exam_name' => $sscExam,
                'ssc_board_university_name' => $sscBoard,
                'ssc_school_clg_name' => $sscClgName,
                'ssc_year_of_passing' => $sscPassingYear,
                'ssc_seat_number' => $sscSeatNo,
                'ssc_marks_obtained' => $sscMarksObtain,
                'ssc_total_marks' => $sscTotalMarks,
                'ssc_percentage' => $sscPercentage,
                'hsc_exam_name' => $hscExam,
                'hsc_board_university_name' => $hscBoard,
                'hsc_school_clg_name' => $hscClgName,
                'hsc_year_of_passing' => $hscPassingYear,
                'hsc_seat_number' => $hscSeatNo,
                'hsc_marks_obtained' => $hscMarksObtain,
                'hsc_total_marks' => $hscTotalMarks,
                'hsc_percentage' => $hscPercentage,
                
                'tybcom_exam_name' => $tyBComExam,
                'tybcom_board_university_name' => $tyBComBoard,
                'tybcom_school_clg_name' => $tyBComClgName,
                'tybcom_year_of_passing' => $tyBComPassingYear,
                'tybcom_seat_number' => $tyBComSeatNo,
                'tybcom_marks_obtained' => $tyBComMarksObtain,
                'tybcom_total_marks' => $tyBComTotalMarks,
                'tybcom_percentage' => $tyBComPercentage,

                'fy_exam_name' => $fyExam,
                'fy_board_university_name' => $fyBoard,
                'fy_school_clg_name' => $fyClgName,
                'fy_year_of_passing' => $fyPassingYear,
                'fy_seat_number' => $fySeatNo,
                'fy_marks_obtained' => $fyMarksObtain,
                'fy_total_marks' => $fyTotalMarks,
                'fy_percentage' => $fyPercentage,
                'sy_exam_name' => $syExam,
                'sy_board_university_name' => $syBoard,
                'sy_school_clg_name' => $syClgName,
                'sy_year_of_passing' => $syPassingYear,
                'sy_seat_number' => $sySeatNo,
                'sy_marks_obtained' => $syMarksObtain,
                'sy_total_marks' => $syTotalMarks,
                'sy_percentage' => $syPercentage,
                
                'fy_exam_name_sem2' => $fyExamSem2,
                'fy_board_university_name_sem2' => $fyBoardSem2,
                'fy_school_clg_name_sem2' => $fyClgNameSem2,
                'fy_year_of_passing_sem2' => $fyPassingYearSem2,
                'fy_seat_number_sem2' => $fySeatNoSem2,
                'fy_marks_obtained_sem2' => $fyMarksObtainSem2,
                'fy_total_marks_sem2' => $fyTotalMarksSem2,
                'fy_percentage_sem2' => $fyPercentageSem2,
                'sy_exam_name_sem2' => $syExamSem2,
                'sy_board_university_name_sem2' => $syBoardSem2,
                'sy_school_clg_name_sem2' => $syClgNameSem2,
                'sy_year_of_passing_sem2' => $syPassingYearSem2,
                'sy_seat_number_sem2' => $sySeatNoSem2,
                'sy_marks_obtained_sem2' => $syMarksObtainSem2,
                'sy_total_marks_sem2' => $syTotalMarksSem2,
                'sy_percentage_sem2' => $syPercentageSem2,
                
                
                'student_repeater_year'=> $appearingYear,
                'is_gap_reason' => $isgapReason,
                'gap_reason' => $gapReason,
                'is_applied_other_clg'=> $appliedOtherCollege,
            ));
            if(isset($_FILES['sscMarksheet'])){
                $query1 = $this->db->get_where('student_admission_form_academic_info', array('ssc_uploaded_documents'=>$sscMarksheetlocation));
            }
            if(isset($_FILES['hscMarksheet'])){
                $query1 = $this->db->get_where('student_admission_form_academic_info',array('hsc_uploaded_documents'=>$hscMarksheetlocation));
                
            }
            if(isset($_FILES['tyBComMarksheet'])){
                $query1 = $this->db->get_where('student_admission_form_academic_info',array('tybcom_uploaded_documents'=>$tyBComMarksheetlocation));
                
            }
            if(isset($_FILES['fyMarksheet'])){
                $query1 = $this->db->get_where('student_admission_form_academic_info',array('fy_uploaded_documents'=>$fyMarksheetlocation));
               
            }
            if(isset($_FILES['syMarksheet'])){
                $query1 = $this->db->get_where('student_admission_form_academic_info',array('sy_uploaded_documents'=>$syMarksheetlocation));
                
            }
             if(isset($_FILES['fyMarksheetSem2'])){
                $query1 = $this->db->get_where('student_admission_form_academic_info',array('fy_uploaded_documents_sem2'=>$fyMarksheetlocationsem2));
               
            }
            if(isset($_FILES['syMarksheetSem2'])){
                $query1 = $this->db->get_where('student_admission_form_academic_info',array('sy_uploaded_documents_sem2'=>$syMarksheetlocationsem2));
                
            }
            if ($query1->num_rows() == 1) {
                return -1;
            }else{
            $data = array(
                'optinal_subject_selected' => $optinalSubjectSelected,
                'optinal_subject_selected_sem2' => $optinalSubjectSelectedsem2,
                'value_added_subject_selected' => $valueAddedSubjectSelected,
                'value_added_subject_selected_sem2' => $valueAddedSubjectSelectedsem2,
                'is_maharashtra' => $isMaharashtra,
                'ssc_exam_name' => $sscExam,
                'ssc_board_university_name' => $sscBoard,
                'ssc_school_clg_name' => $sscClgName,
                'ssc_year_of_passing' => $sscPassingYear,
                'ssc_seat_number' => $sscSeatNo,
                'ssc_marks_obtained' => $sscMarksObtain,
                'ssc_total_marks' => $sscTotalMarks,
                'ssc_percentage' => $sscPercentage,
                'hsc_exam_name' => $hscExam,
                'hsc_board_university_name' => $hscBoard,
                'hsc_school_clg_name' => $hscClgName,
                'hsc_year_of_passing' => $hscPassingYear,
                'hsc_seat_number' => $hscSeatNo,
                'hsc_marks_obtained' => $hscMarksObtain,
                'hsc_total_marks' => $hscTotalMarks,
                'hsc_percentage' => $hscPercentage,
                'tybcom_exam_name' => $tyBComExam,
                'tybcom_board_university_name' => $tyBComBoard,
                'tybcom_school_clg_name' => $tyBComClgName,
                'tybcom_year_of_passing' => $tyBComPassingYear,
                'tybcom_seat_number' => $tyBComSeatNo,
                'tybcom_marks_obtained' => $tyBComMarksObtain,
                'tybcom_total_marks' => $tyBComTotalMarks,
                'tybcom_percentage' => $tyBComPercentage,
                'fy_exam_name' => $fyExam,
                'fy_board_university_name' => $fyBoard,
                'fy_school_clg_name' => $fyClgName,
                'fy_year_of_passing' => $fyPassingYear,
                'fy_seat_number' => $fySeatNo,
                'fy_marks_obtained' => $fyMarksObtain,
                'fy_total_marks' => $fyTotalMarks,
                'fy_percentage' => $fyPercentage,
                'sy_exam_name' => $syExam,
                'sy_board_university_name' => $syBoard,
                'sy_school_clg_name' => $syClgName,
                'sy_year_of_passing' => $syPassingYear,
                'sy_seat_number' => $sySeatNo,
                'sy_marks_obtained' => $syMarksObtain,
                'sy_total_marks' => $syTotalMarks,
                'sy_percentage' => $syPercentage,
                'fy_exam_name_sem2' => $fyExamSem2,
                'fy_board_university_name_sem2' => $fyBoardSem2,
                'fy_school_clg_name_sem2' => $fyClgNameSem2,
                'fy_year_of_passing_sem2' => $fyPassingYearSem2,
                'fy_seat_number_sem2' => $fySeatNoSem2,
                'fy_marks_obtained_sem2' => $fyMarksObtainSem2,
                'fy_total_marks_sem2' => $fyTotalMarksSem2,
                'fy_percentage_sem2' => $fyPercentageSem2,
                'sy_exam_name_sem2' => $syExamSem2,
                'sy_board_university_name_sem2' => $syBoardSem2,
                'sy_school_clg_name_sem2' => $syClgNameSem2,
                'sy_year_of_passing_sem2' => $syPassingYearSem2,
                'sy_seat_number_sem2' => $sySeatNoSem2,
                'sy_marks_obtained_sem2' => $syMarksObtainSem2,
                'sy_total_marks_sem2' => $syTotalMarksSem2,
                'sy_percentage_sem2' => $syPercentageSem2,
                'student_repeater_year'=> $appearingYear,
                'is_gap_reason' => $isgapReason,
                'gap_reason' =>$gapReason,
                'is_applied_other_clg'=> $appliedOtherCollege,
                
            );
            
            $this->db->where('student_id',$studentId);
            $this->db->update('student_admission_form_academic_info', $data);

            if(isset($_FILES['sscMarksheet'])){
                $sscMarksheet =array('ssc_uploaded_documents'=>$sscMarksheetlocation);
                $sscMarksheetArray = array_merge($data,$sscMarksheet);
                
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $sscMarksheetArray);
            }
            if(isset($_FILES['hscMarksheet'])){
                $hscMarksheet =array('hsc_uploaded_documents'=>$hscMarksheetlocation);
                $hscMarksheetArray = array_merge($data,$hscMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $hscMarksheetArray);
            }
            if(isset($_FILES['tyBComMarksheet'])){
                $tyBComMarksheet =array('tybcom_uploaded_documents'=>$tyBComMarksheetlocation);
                $tyBComMarksheetArray = array_merge($data,$tyBComMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $tyBComMarksheetArray);
            }
            
            if(isset($_FILES['fyMarksheet'])){
                $fyMarksheet =array('fy_uploaded_documents'=>$fyMarksheetlocation);
                $fyMarksheetArray = array_merge($data,$fyMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $fyMarksheetArray);
            }
            if(isset($_FILES['syMarksheet'])){
                $syMarksheet =array('sy_uploaded_documents'=>$syMarksheetlocation);
                $syMarksheetArray = array_merge($data,$syMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $syMarksheetArray);
            }
            
            if(isset($_FILES['fyMarksheetSem2'])){
                $fyMarksheetSem2 =array('fy_uploaded_documents_sem2'=>$fyMarksheetlocationsem2);
                $fyMarksheetArraysem2 = array_merge($data,$fyMarksheetSem2);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $fyMarksheetArraysem2);
            }
            if(isset($_FILES['syMarksheetSem2'])){
                $syMarksheetSem2 =array('sy_uploaded_documents_sem2'=>$syMarksheetlocationsem2);
                $syMarksheetArraysem2 = array_merge($data,$syMarksheetSem2);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $syMarksheetArraysem2);
            }

            if($sscExam != '' && $sscBoard && $sscClgName != '' && $sscPassingYear!= '' &&  $sscSeatNo != '' && $sscMarksObtain !='' && $sscTotalMarks !=''&& $sscPercentage != '' && $appliedOtherCollege != '' ){
                $this->db->where('student_id',$studentId);
                $this->db->update('student_registration', array('admission_step_two'=> '1'));
            }
            return 2;
        }
        }else{

            $data = array(
                'student_id' => $studentId,
                'optinal_subject_selected' => $optinalSubjectSelected,
                'optinal_subject_selected_sem2' => $optinalSubjectSelectedsem2,
                'value_added_subject_selected' => $valueAddedSubjectSelected,
                'value_added_subject_selected_sem2' => $valueAddedSubjectSelectedsem2,
                'is_maharashtra' => $isMaharashtra,
                'ssc_exam_name' => $sscExam,
                'ssc_board_university_name' => $sscBoard,
                'ssc_school_clg_name' => $sscClgName,
                'ssc_year_of_passing' => $sscPassingYear,
                'ssc_seat_number' => $sscSeatNo,
                'ssc_marks_obtained' => $sscMarksObtain,
                'ssc_total_marks' => $sscTotalMarks,
                'ssc_percentage' => $sscPercentage,
                'hsc_exam_name' => $hscExam,
                'hsc_board_university_name' => $hscBoard,
                'hsc_school_clg_name' => $hscClgName,
                'hsc_year_of_passing' => $hscPassingYear,
                'hsc_seat_number' => $hscSeatNo,
                'hsc_marks_obtained' => $hscMarksObtain,
                'hsc_total_marks' => $hscTotalMarks,
                'hsc_percentage' => $hscPercentage,
                'tybcom_exam_name' => $tyBComExam,
                'tybcom_board_university_name' => $tyBComBoard,
                'tybcom_school_clg_name' => $tyBComClgName,
                'tybcom_year_of_passing' => $tyBComPassingYear,
                'tybcom_seat_number' => $tyBComSeatNo,
                'tybcom_marks_obtained' => $tyBComMarksObtain,
                'tybcom_total_marks' => $tyBComTotalMarks,
                'tybcom_percentage' => $tyBComPercentage,
                'fy_exam_name' => $fyExam,
                'fy_board_university_name' => $fyBoard,
                'fy_school_clg_name' => $fyClgName,
                'fy_year_of_passing' => $fyPassingYear,
                'fy_seat_number' => $fySeatNo,
                'fy_marks_obtained' => $fyMarksObtain,
                'fy_total_marks' => $fyTotalMarks,
                'fy_percentage' => $fyPercentage,
                'sy_exam_name' => $syExam,
                'sy_board_university_name' => $syBoard,
                'sy_school_clg_name' => $syClgName,
                'sy_year_of_passing' => $syPassingYear,
                'sy_seat_number' => $sySeatNo,
                'sy_marks_obtained' => $syMarksObtain,
                'sy_total_marks' => $syTotalMarks,
                'sy_percentage' => $syPercentage,
                'fy_exam_name_sem2' => $fyExamSem2,
                'fy_board_university_name_sem2' => $fyBoardSem2,
                'fy_school_clg_name_sem2' => $fyClgNameSem2,
                'fy_year_of_passing_sem2' => $fyPassingYearSem2,
                'fy_seat_number_sem2' => $fySeatNoSem2,
                'fy_marks_obtained_sem2' => $fyMarksObtainSem2,
                'fy_total_marks_sem2' => $fyTotalMarksSem2,
                'fy_percentage_sem2' => $fyPercentageSem2,
                'sy_exam_name_sem2' => $syExamSem2,
                'sy_board_university_name_sem2' => $syBoardSem2,
                'sy_school_clg_name_sem2' => $syClgNameSem2,
                'sy_year_of_passing_sem2' => $syPassingYearSem2,
                'sy_seat_number_sem2' => $sySeatNoSem2,
                'sy_marks_obtained_sem2' => $syMarksObtainSem2,
                'sy_total_marks_sem2' => $syTotalMarksSem2,
                'sy_percentage_sem2' => $syPercentageSem2,
                'student_repeater_year'=> $appearingYear,
                'is_gap_reason' => $isgapReason,
                'gap_reason' =>$gapReason,
                'is_applied_other_clg'=> $appliedOtherCollege,
                
            );
            $insert = $this->db->insert('student_admission_form_academic_info', $data);
                
            if(isset($_FILES['sscMarksheet'])){
                $sscMarksheet =array('ssc_uploaded_documents'=>$sscMarksheetlocation);
                $sscMarksheetArray = array_merge($data,$sscMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $sscMarksheetArray);
            }
            if(isset($_FILES['hscMarksheet'])){
                $hscMarksheet =array('hsc_uploaded_documents'=>$hscMarksheetlocation);
                $hscMarksheetArray = array_merge($data,$hscMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $hscMarksheetArray);
            }
            if(isset($_FILES['tyBComMarksheet'])){
                $tyBComMarksheet =array('tybcom_uploaded_documents'=>$tyBComMarksheetlocation);
                $tyBComMarksheetArray = array_merge($data,$tyBComMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $tyBComMarksheetArray);
            }
            if(isset($_FILES['fyMarksheet'])){
                $fyMarksheet =array('fy_uploaded_documents'=>$fyMarksheetlocation);
                $fyMarksheetArray = array_merge($data,$fyMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $fyMarksheetArray);
            }
            if(isset($_FILES['syMarksheet'])){
                $syMarksheet =array('sy_uploaded_documents'=>$syMarksheetlocation);
                $syMarksheetArray = array_merge($data,$syMarksheet);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $syMarksheetArray);
            }
            if(isset($_FILES['fyMarksheetSem2'])){
                $fyMarksheetSem2 =array('fy_uploaded_documents_sem2'=>$fyMarksheetlocationsem2);
                $fyMarksheetArraysem2 = array_merge($data,$fyMarksheetSem2);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $fyMarksheetArraysem2);
            }
            if(isset($_FILES['syMarksheetSem2'])){
                $syMarksheetSem2 =array('sy_uploaded_documents_sem2'=>$syMarksheetlocationsem2);
                $syMarksheetArraysem2 = array_merge($data,$syMarksheetSem2);
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $syMarksheetArraysem2);
            }
            if($sscExam != '' && $sscBoard && $sscClgName != '' && $sscPassingYear!= '' &&  $sscSeatNo != '' && $sscMarksObtain !='' && $sscTotalMarks !=''&& $sscPercentage != '' && $appliedOtherCollege != '' ){
                $this->db->where('student_id',$studentId);
                $this->db->update('student_registration', array('admission_step_two'=> '1'));
            }
            return $this->db->affected_rows() > 0 ? 1 : 0;
        }
        
    }

    function getStudentPersonalInfo(){
        $studentId = $_POST['student_id'];
        $this->db->select("*");
        $this->db->from("student_admission_form_personal_info");
        $this->db->where('student_id', $studentId);
       // $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }
    function getStudentAcademicInfo(){
        $studentId = $_POST['student_id'];
        
        $this->db->select("*");
        $this->db->from("student_admission_form_academic_info");
        $this->db->where('student_id', $studentId);
       // $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }
    function getStudentRegistrationDetails(){
        $studentId = $_POST['student_id'];
        $this->db->select("*");
        $this->db->from("student_registration");
        $this->db->where('student_id', $studentId);
       // $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getAdmissionDate(){
        
        $academicYear = $_POST['academicYear'];
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        
        $this->db->select("*");
        $this->db->from("manage_course");   
        $this->db->where('course_name', $courseName);
        $this->db->where('courses_year', $courseYear);
        $this->db->where('academics_year', $academicYear);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getReligionList(){
        $this->db->select("*");
        $this->db->from("manage_religion");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getCasteList(){

        $religionName = $_POST['religionName'];
        
        $this->db->select("*");
        $this->db->from("manage_caste mc");
        $this->db->where('mr.religion_name', $religionName);
        $this->db->where('mc.delete_bit', '0');
        
        $this->db->where('mr.delete_bit', '0');
        $this->db->join("manage_religion mr", 'mr.religion_id = mc.religion_id');
        return $this->db->get()->result_array();
    }
    function getSubCasteList(){
        $casteName = $_POST['casteName'];
        $this->db->select("*");
        $this->db->from("manage_caste");
        $this->db->where('caste_name', $casteName);
        $this->db->where('delete_bit', '0');
         return $this->db->get()->result_array();
    }
    
    function submitParentInfo(){

        $studentId = $_POST['studentId'];
        $fatherFirstName = $_POST['fatherFirstName'];
        $fatherMiddleName = $_POST['fatherMiddleName'];
        $fatherLastName = $_POST['fatherLastName'];
        $fatherEmail = $_POST['fatherEmail'];
        $fatherMobileNo = $_POST['fatherMobileNo'];
        $fatherOccupation = $_POST['fatherOccupation'];
        $fathercGovtEmp = $_POST['fathercGovtEmp'];
        $fatherAnnualIncome = $_POST['fatherAnnualIncome'];
        $fatherecoBack = $_POST['fatherecoBack'];

        $motherFirstName = $_POST['motherFirstName'];
        $motherMiddleName = $_POST['motherMiddleName'];
        $motherLastName = $_POST['motherLastName'];
        $motherEmail = $_POST['motherEmail'];
        $motherMobileNo = $_POST['motherMobileNo'];
        $motherOccupation = $_POST['motherOccupation'];
        $mothercGovtEmp = $_POST['mothercGovtEmp'];
        $motherAnnualIncome = $_POST['motherAnnualIncome'];
        $motherecoBack = $_POST['motherecoBack'];

        $guaradianFirstName = $_POST['guaradianFirstName'];
        $guaradianMiddleName = $_POST['guaradianMiddleName'];
        $guaradianLastName = $_POST['guaradianLastName'];
        $guaradianEmail = $_POST['guaradianEmail'];
        $guaradianMobileNo = $_POST['guaradianMobileNo'];
        $guaradianOccupation = $_POST['guaradianOccupation'];
        $guaradiancGovtEmp = $_POST['guaradiancGovtEmp'];
        $guaradianAnnualIncome = $_POST['guaradianAnnualIncome'];
        $guaradianecoBack = $_POST['guaradianecoBack'];
        $studentchk1 = $_POST['studentchk1'];

        $query = $this->db->get_where('student_admission_form_parent_info', array(
            'student_id' => $studentId
        ));
        if ($query->num_rows() == 1) {
            $query1 = $this->db->get_where('student_admission_form_parent_info', array(
                'father_first_name' => $fatherFirstName,
               'father_middle_name' => $fatherMiddleName,
               'father_last_name' => $fatherLastName,
               'father_email_address' =>$fatherEmail,
               'father_mobile_number' => $fatherMobileNo,
               'father_occupation' => $fatherOccupation,
               'is_father_central_govern_employee'=>$fathercGovtEmp,
               'father_annual_income' => $fatherAnnualIncome,
               'is_father_economical_backward' => $fatherecoBack,
               'mother_first_name' => $motherFirstName,
               'mother_middle_name' => $motherMiddleName,
               'mother_last_name' => $motherLastName,
               'mother_email_address' => $motherEmail,
               'mother_mobile_number' => $motherMobileNo,
               'mother_occupation' => $motherOccupation,
               'is_mother_central_govern_employee' => $mothercGovtEmp,
               'mother_annual_income' => $motherAnnualIncome,
               'is_mother_economical_backward' => $motherecoBack,
               'guardian_first_name' => $guaradianFirstName,
               'guardian_middle_name' => $guaradianMiddleName,
               'guardian_last_name' => $guaradianLastName,
               'guardian_email_address' => $guaradianEmail,
               'guardian_mobile_number' => $guaradianMobileNo,
               'guardian_occupation' => $guaradianOccupation,
               'is_guardian_central_govern_employee' => $guaradiancGovtEmp,
               'guardian_annual_income' => $guaradianAnnualIncome,
               'is_guardian_economical_backward' => $guaradianecoBack,
               'is_student_agree' => $studentchk1
            ));
                if ($query1->num_rows() == 1) {
                    return -1;
                }
                else{
                
                $data = array(
                    'father_first_name' => $fatherFirstName,
                    'father_middle_name' => $fatherMiddleName,
                    'father_last_name' => $fatherLastName,
                    'father_email_address' =>$fatherEmail,
                    'father_mobile_number' => $fatherMobileNo,
                    'father_occupation' => $fatherOccupation,
                    'is_father_central_govern_employee'=>$fathercGovtEmp,
                    'father_annual_income' => $fatherAnnualIncome,
                    'is_father_economical_backward' => $fatherecoBack,
                    'mother_first_name' => $motherFirstName,
                    'mother_middle_name' => $motherMiddleName,
                    'mother_last_name' => $motherLastName,
                    'mother_email_address' => $motherEmail,
                    'mother_mobile_number' => $motherMobileNo,
                    'mother_occupation' => $motherOccupation,
                    'is_mother_central_govern_employee' => $mothercGovtEmp,
                    'mother_annual_income' => $motherAnnualIncome,
                    'is_mother_economical_backward' => $motherecoBack,
                    'guardian_first_name' => $guaradianFirstName,
                    'guardian_middle_name' => $guaradianMiddleName,
                    'guardian_last_name' => $guaradianLastName,
                    'guardian_email_address' => $guaradianEmail,
                    'guardian_mobile_number' => $guaradianMobileNo,
                    'guardian_occupation' => $guaradianOccupation,
                    'is_guardian_central_govern_employee' => $guaradiancGovtEmp,
                    'guardian_annual_income' => $guaradianAnnualIncome,
                    'is_guardian_economical_backward' => $guaradianecoBack,
                    'is_student_agree' => $studentchk1
                );

            $this->db->where('student_id',$studentId);
            $this->db->update('student_admission_form_parent_info', $data);
            $this->db->where('student_id',$studentId);
            $this->db->update('student_registration', array('admission_step_three'=> '1'));
            print_r($data);
           // return 2;
            }
        }else{
            $data = array(
                'student_id' => $studentId,
                'father_first_name' => $fatherFirstName,
                'father_middle_name' => $fatherMiddleName,
                'father_last_name' => $fatherLastName,
                'father_email_address' =>$fatherEmail,
                'father_mobile_number' => $fatherMobileNo,
                'father_occupation' => $fatherOccupation,
                'is_father_central_govern_employee'=>$fathercGovtEmp,
                'father_annual_income' => $fatherAnnualIncome,
                'is_father_economical_backward' => $fatherecoBack,
                'mother_first_name' => $motherFirstName,
                'mother_middle_name' => $motherMiddleName,
                'mother_last_name' => $motherLastName,
                'mother_email_address' => $motherEmail,
                'mother_mobile_number' => $motherMobileNo,
                'mother_occupation' => $motherOccupation,
                'is_mother_central_govern_employee' => $mothercGovtEmp,
                'mother_annual_income' => $motherAnnualIncome,
                'is_mother_economical_backward' => $motherecoBack,
                'guardian_first_name' => $guaradianFirstName,
                'guardian_middle_name' => $guaradianMiddleName,
                'guardian_last_name' => $guaradianLastName,
                'guardian_email_address' => $guaradianEmail,
                'guardian_mobile_number' => $guaradianMobileNo,
                'guardian_occupation' => $guaradianOccupation,
                'is_guardian_central_govern_employee' => $guaradiancGovtEmp,
                'guardian_annual_income' => $guaradianAnnualIncome,
                'is_guardian_economical_backward' => $guaradianecoBack,
                'is_student_agree' => $studentchk1
            );
 
            $insert = $this->db->insert('student_admission_form_parent_info', $data);
            $this->db->where('student_id',$studentId);
            $this->db->update('student_registration', array('admission_step_three'=> '1'));
            return $this->db->affected_rows() > 0 ? 1 : 0;

        }
   
   
    }
    
    function getStudentParentInfo(){
        $studentId = $_POST['student_id'];
        
        $this->db->select("*");
        $this->db->from("student_admission_form_parent_info");
        $this->db->where('student_id', $studentId);
       // $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array(); 
    }
    function getSubjectList(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $this->db->select("*");
        $this->db->from("manage_subject");
        $this->db->where('course_name', $courseName);
        $this->db->where('course_year', $courseYear);
        $this->db->where('delete_bit', '0');
        
        return $this->db->get()->result_array();
    }

}
?>