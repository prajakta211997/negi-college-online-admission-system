<?php 

class ManageCaste_model extends CI_Model {

    function addCaste(){
        $religionName = $_POST['religionName'];
        $casteName = $_POST['casteName'] ;
        $subCasteName = $_POST['subCasteName'];
        $query = $this->db->get_where('manage_caste', array('religion_id' => $religionName, 'caste_name' => $casteName,'sub_caste_name' => $subCasteName, 'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                        'religion_id' => $religionName,
                        'caste_name' => $casteName,
                        'sub_caste_name' => $subCasteName
                );
                
                $insert = $this->db->insert('manage_caste', $data);

                 return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    }

    function getReligionList(){
        $this->db->select("*");
        $this->db->from("manage_religion");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    

    function getCasteList(){
        $this->db->select("*");
        $this->db->from("manage_caste mc");
        $this->db->where('mc.delete_bit', '0');
        $this->db->where('mr.delete_bit', '0');
        $this->db->join("manage_religion mr", 'mr.religion_id = mc.religion_id');
        return $this->db->get()->result_array();
    }

    function searchByCastes(){
        $CasteSearch = $_POST['CasteSearch'];
        $this->db->select("*");
        $this->db->from("manage_caste mc");
        $this->db->like('caste_name', $CasteSearch);
        $this->db->or_like('sub_caste_name', $CasteSearch);
        $this->db->or_like('mr.religion_name', $CasteSearch);
        $this->db->where('mc.delete_bit', '0');
        $this->db->where('mr.delete_bit', '0');
        $this->db->join("manage_religion mr", 'mr.religion_id = mc.religion_id');
        return $this->db->get()->result_array();
    }

    function updateCaste(){
        $casteId = $_POST['updateCasteId'];
        $religionName = $_POST['religionName'];
        $casteName = $_POST['casteName'] ;
        $subCasteName = $_POST['subCasteName'];
        
        $query = $this->db->get_where('manage_caste', array('religion_id' => $religionName, 'caste_name' => $casteName,'sub_caste_name' => $subCasteName,'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                        'religion_id' => $religionName, 
                        'caste_name' => $casteName,
                        'sub_Caste_name' => $subCasteName
                
                    );
              
                $this->db->where('caste_id', $casteId);
                $this->db->update('manage_caste', $data);
                return true; //return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    
    }
    function deleteCaste(){
        $updateCasteId = $_POST['updateCasteId'];
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('caste_id', $updateCasteId);
        $this->db->update('manage_caste', $data);
       return true;
       
    }
  
    
    
    
}

?>