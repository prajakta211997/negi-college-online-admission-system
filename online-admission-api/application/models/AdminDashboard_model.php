<?php 

class AdminDashboard_model extends CI_Model {
 
   function getCourseWiseAnalysis(){
        $finalReturnArray = [];
        $finalArray = [];
        $this->db->select("*");
        $this->db->from("manage_course ");
        $this->db->where('delete_bit', '0');
        $courseArray =  $this->db->get()->result_array();
       
       foreach($courseArray  as $course){
            
            $admissionConfirmData = array();
            $TotalSeat = array();
           $courseNameArray = array();
          
            $combineYeNoArray = [];
            
            $this->db->select("*");
            $this->db->from("student_registration sr");
            $this->db->where('sr.course_name',$course['course_name']);
            $this->db->where('sr.course_year',$course['courses_year']);
            $this->db->where('sr.stud_academic_year',$course['academics_year']);
            $this->db->where('parentinfo.student_admission_status', '3');
           // $this->db->where('parentinfo.form_fee_submitted_bit', '1');
            $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
            $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
            $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
           // $TotalSeat['Total Seats']= $course['total_seats'];
            $queryOption = $this->db->get();
            $option_Count = $queryOption->num_rows();
            $admissionConfirmData['Confirme Seats'] = $option_Count;
            
            $this->db->select("*");
            $this->db->from("student_registration sr");
            $this->db->where('sr.course_name',$course['course_name']);
            $this->db->where('sr.course_year',$course['courses_year']);
            $this->db->where('sr.stud_academic_year',$course['academics_year']);
            $this->db->where('parentinfo.student_admission_status', '3');
            //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
            $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
            $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
            $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
            $studData=$this->db->get()->result_array();
          //print_r($studData);
          if($studData ){
            $index = 0;
            $courseName = $studData[$index]['course_name'];
            $courseYear = $studData[$index]['course_year'];
            $academicYear = $studData[$index]['stud_academic_year'];
           }
            $this->db->select("*");
            $this->db->from("manage_course ");
            $this->db->where('delete_bit', '0');
            $courseData =  $this->db->get()->result_array();
          
         for($i = 0; $i < count($courseData); $i++)
         {
            
           if($courseData[$i]['course_name']== $courseName && $courseData[$i]['courses_year']== $courseYear && $courseData[$i]['academics_year']== $academicYear )
            { 
               $TotalSeat['Total Seat'] = $courseData[$i]['total_seats'] ;
               $courseNameArray= $courseData[$i]['course_name'] . $courseData[$i]['courses_year'];
            }
         }
         if($studData){  
         $finalArray[$courseNameArray] = array_merge($admissionConfirmData,$TotalSeat);
         }
         // else{
         //    $finalArray['course'] = array_merge($admissionConfirmData,$TotalSeat);
         // }
      }
    
       $finalReturnArray[] =  $finalArray;
       
      return $finalReturnArray; 
        
   }
   

    
}

?>