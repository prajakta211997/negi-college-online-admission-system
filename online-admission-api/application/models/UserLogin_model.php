<?php 

class UserLogin_model extends CI_Model {
 
    function checkLogin(){
        $studEmail = $_POST['studEmail'];
        $studPassword = $_POST['studPassword'];
               
        $query = $this->db->get_where('student_registration', array('student_email' => $studEmail,  'student_password'=>$studPassword,'payment_status' => "success"));
 
        if ($query->num_rows() == 1) 
        {
            $loginArrayFromDB = $query->result_array();
         
            foreach($loginArrayFromDB as $loginArrayv){
              if($loginArrayv['delete_bit'] == '1'){
                return -1;
              }else{
               // $userId = $loginArrayv['student_id'];
                //return $userId; 
                return $loginArrayv;
              }
            } 
        }
        else{
            return 0;
            
        }
        
    }


    function changePassword(){ 
        $student_id = $_POST['student_id'];
        $oldPass = $_POST['oldPass'];
        $newPass = $_POST['newPass'];
        $data = array(
        'student_password' => $newPass,
        );
        $this->db->where('student_id', $student_id);
        $this->db->update('student_registration', $data);
        return true;
        }


    function my_simple_crypt( $string, $action) {
        // you may change these values to your own
        $secret_key = 'whitecode_key';
        $secret_iv = 'whitecode_iv';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    }
    


    
}

?>