<?php 

class ShortlistStudent_model extends CI_Model {

    function getAllCoursesName(){
        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getReligionList(){
        $this->db->select("*");
        $this->db->from("manage_religion");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getCasteList(){
        $this->db->select("*");
        $this->db->from("manage_caste mc");
        $this->db->where('mc.delete_bit', '0');
        $this->db->where('mr.delete_bit', '0');
        $this->db->join("manage_religion mr", 'mr.religion_id = mc.religion_id');
        return $this->db->get()->result_array();
    }

    function getShortlistStudent(){
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
        $this->db->where('parentinfo.student_admission_status', '2');
        //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        return $this->db->get()->result_array();
    }
    
    function getFilterData(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
        $religionName = $_POST['religionName'];
        $casteName = $_POST['casteName'];
        $subCasteName = $_POST['subCasteName'];
        $admissionStatus = $_POST['admissionStatus'];
        $collegeName = $_POST['collegeName'];
        $obtainedMarksFilter = $_POST['obtainedMarksFilter'];
        $leaveDateFr = $_POST['leaveDateFrom'];
        $leaveDateT = $_POST['leaveDateTo'];
         $leaveDateFrom=date("yy-m-d",strtotime($leaveDateFr)).' 00:00'; 
        $leaveDateTo=date("yy-m-d",strtotime($leaveDateT)).' 23:59';
        
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        $this->db->where('parentinfo.student_admission_status', '2');
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
       // $this->db->where('parentinfo.form_fee_submitted_bit', '1');
       if($collegeName){
        $this->db->where('personalinfo.is_jaihind_college', $collegeName);
        }
        if($courseName){
        $this->db->where('sr.course_name', $courseName);
        }
        if($religionName){
        $this->db->where('personalinfo.religion', $religionName);
        }
        if($casteName){
        $this->db->where('personalinfo.caste', $casteName);
        }
        if($subCasteName){
        $this->db->where('personalinfo.sub_caste', $subCasteName);
        }
        if($courseYear){
        $this->db->where('sr.course_year', $courseYear);
        }
        if($academicYear){
        $this->db->where('sr.stud_academic_year', $academicYear);
        }
        if($admissionStatus){
        $this->db->where('parentinfo.student_admission_status', $admissionStatus);
        }
       
        if($courseName != '' && $courseYear !='' &&  $obtainedMarksFilter == 'highestObtainedMarks'){
            if($courseYear == 'First Year' &&  $courseName== 'M.Com'){
                $this->db->order_by('academicinfo.tybcom_marks_obtained', 'desc');
            }
            if($courseYear == 'First Year'){
                $this->db->order_by('academicinfo.hsc_marks_obtained', 'desc');
            }
            if($courseYear == 'Second Year'){
                
                $this->db->order_by('academicinfo.fy_marks_obtained', 'desc');
            }
            if($courseYear == 'Third Year'){
                $this->db->order_by('academicinfo.sy_marks_obtained', 'desc');
            }
        }

        if($courseName != '' && $courseYear !='' &&  $obtainedMarksFilter == 'lowestObtainedMarks'){
            if($courseYear == 'First Year' &&  $courseName== 'M.Com'){
                
                $this->db->order_by('academicinfo.tybcom_marks_obtained', 'asc');
            }
            if($courseYear == 'First Year'){
                $this->db->order_by('academicinfo.hsc_marks_obtained', 'asc');
            }
            if($courseYear == 'Second Year'){
                
                $this->db->order_by('academicinfo.fy_marks_obtained', 'asc');
            }
            if($courseYear == 'Third Year'){
                $this->db->order_by('academicinfo.sy_marks_obtained', 'asc');
            }
        }
        $this->db->where('parentinfo.submitted_by >=', $leaveDateFrom);
        $this->db->where('parentinfo.submitted_by <=', $leaveDateTo);
        
        return $this->db->get()->result_array(); 
    }

     
    function getSingleStudentDetails(){
        $student_id = $_POST['student_id'];
        
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.student_id',$student_id);
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
        $this->db->where('parentinfo.student_admission_status', '2');
        //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        $studentData =  $this->db->get()->result_array();
      
        $this->db->select("*");
        $this->db->from("student_course_fees_payment_details ");
        $this->db->where('student_id', $student_id);
        $paymentData = $this->db->get()->result_array();
        if(empty($paymentData) ){
            $studentData[0]['student_payment_paid'] = 0;
        }
        else{
            $studentData[0]['student_payment_paid'] = 1;
        }
        return $studentData;
    }
      

    

    function addSelectedToSendSMS()
    {
        $studentMobile = $_POST['studentMobile'];
        $studmessage = $_POST['studmessage'];
        foreach($studentMobile as $studMobile){
            
        // $username="mucollege";
        // $password ="Mucollegesms@20";
        // $number=$studMobile;
        // $sender="MUCBLP";
        // $message= $studmessage;
        // $url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3'); 
        // $ch = curl_init($url);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $curl_scraped_page = curl_exec($ch);
        // curl_close($ch);
        }     
        return true;
    }
    

    function getAllDataForExcel(){
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
        $this->db->where('parentinfo.student_admission_status', '2');
        //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        
        $StudentArrayFromDB = $this->db->get()->result_array();
        $returnArray=array();  
        $key = 1;  
        $returnArray[0] = array("Student Name","Student Mobile Number","Student Email","Student Academic Year","Student Course","Student Course Year","Gender", "Date of Birth","Mother Tongue","Address","Religion","Category","Cast","Eligibility Number","SSC Percentage","HSC Percentage","FY Percentage","SY Percentage","TY BCom Percentage");  
        foreach($StudentArrayFromDB as $StudentArray){
            $returnArray[$key] = array($StudentArray['student_name'],$StudentArray['student_mobile'],$StudentArray['student_email'],$StudentArray['stud_academic_year'],$StudentArray['course_name'],$StudentArray['course_year'], $StudentArray['gender'], $StudentArray['date_of_birth'], $StudentArray['mother_tounge'], $StudentArray['local_address'], $StudentArray['religion'], $StudentArray['category'], $StudentArray['caste'], $StudentArray['eligibility_number'],$StudentArray['ssc_percentage'],$StudentArray['hsc_percentage'],$StudentArray['fy_percentage'],$StudentArray['sy_percentage'],$StudentArray['tybcom_percentage']);  
            $key++;   
        }
        return $returnArray;
    }
    
    function getSubjectList(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $this->db->select("*");
        $this->db->from("manage_subject");
        $this->db->where('course_name', $courseName);
        $this->db->where('course_year', $courseYear);
        $this->db->where('delete_bit', '0');
        
        return $this->db->get()->result_array();
    }

    function stopPaymentLink(){
        $studentId = $_POST['studentId'];
        $data = array(
            'stop_payment_link' => "1"
        );
        $this->db->where('student_id', $studentId);
        $this->db->update('student_registration', $data);
        return true;
    }

    function onPaymentLink(){
        $studentId = $_POST['studentId'];
        $data = array(
            'stop_payment_link' => "0"
        );
        $this->db->where('student_id', $studentId);
        $this->db->update('student_registration', $data);
        return true;
    }
}

?>