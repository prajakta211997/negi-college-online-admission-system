<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


function sendUserDetailsMail($result, $template) {
    $CI = get_instance();
    $html = $CI->load->view($template, $result, true);   

    // require_once(APPPATH . "/third_party/sendgrid-php/sendgrid-php.php");
    //     //$sg = new \SendGrid("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");   
    //     $sg = new \SendGrid(SENDGRID_API_KEY);
        
    //     $data = [
    //         'personalizations' => [
    //             [
    //                 'to' => [
    //                     [ 'email' => $result['email']]
    //                 ],
    //                 'subject' => $result['subject']
    //             ]
    //         ],
    //         'from' => [
    //             'email' => 'info@whitecode.co.in',
    //             'name' => 'WhiteCode'
    //         ],
    //         'content' => [
    //             [
    //                 'type' => 'text/html',
    //                 'value' => $html
    //             ]
    //         ],
    //         'track_settings' => [
    //             [
    //                 'click_tracking' => true,
    //                 'open_tracking' => true
    //             ]
    //         ]
    //     ];
    //     $response = $sg->client->mail()->send()->post($data);
    //     return $response;
}


function sendMail($result, $template) {
  
    $CI = get_instance();
    $html = $CI->load->view($template, $result, true);

    $CI->load->library('email');
    $config = array(
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'priority' => '1'
    );
    $CI->email->initialize($config);
    $CI->email->from('admission@tsnegigcreckongpeo.com', 'THAKUR SEN NEGI GOVERNMENT COLLEGE');
    //$CI->email->to('shridhar@whitecode.co.in');
    $CI->email->to($result['email']);
    $CI->email->subject($result['subject']);
    $CI->email->message($html);
    $response = $CI->email->send();

    return $response;
}


?>