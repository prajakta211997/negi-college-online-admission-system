<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminForgotPassword extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('AdminForgotPassword_model');
	}
	
	

	public function ForgotPasswordSendEmail()
	{
		$response = $this->AdminForgotPassword_model->ForgotPasswordSendEmail();
		if($response == 1){
			$userdata['status'] = array('status' => "1", "message" => "Please check your email for password.");
			$userdata['data'] = $response;
		}else if($response == 2){
			$userdata['status'] = array('status' => "0", "message" => "Provided email not register with us");
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
}
