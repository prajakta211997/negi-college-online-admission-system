<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageCaste extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ManageCaste_model');
	}
    
    public function addCaste()
	{
		$response = $this->ManageCaste_model->addCaste();
		if($response != -1){
			$Castdata['status'] = array('status' => "1", "message" => "Caste added successfully.");
			$Castdata['data'] = $response;
		}else if($response == -1){
			$Castdata['status'] = array('status' => "0", "message" => "Caste Name Already Added.");
		}else{
			$Castdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Castdata));
    }
    public function getReligionList()
    {
        $response = $this->ManageCaste_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }


    public function getCasteList()
    {
        $response = $this->ManageCaste_model->getCasteList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
    public function searchByCastes()
    {
        $response = $this->ManageCaste_model->searchByCastes();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function updateCaste()
    {
        $response = $this->ManageCaste_model->updateCaste();
        if($response == 1){
                $Castsdata['status'] = array('status' => "1", "message" => "Caste Name updated successfully.");
                $Castsdata['data'] = $response;
        }else if($response == -1){
                $Castsdata['status'] = array('status' => "0", "message" => "Caste Name Alredy Exist..  Nothing to update.");
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function deleteCaste()
    {
        $response = $this->ManageCaste_model->deleteCaste();
        if($response == 1){
                $Castsdata['status'] = array('status' => "1", "message" => "Caste  deleted successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
    
    
}
