<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShortlistStudent extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ShortlistStudent_model');
	}


    public function getReligionList()
    {
        $response = $this->ShortlistStudent_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }


    public function getCasteList()
    {
        $response = $this->ShortlistStudent_model->getCasteList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function getAllCoursesName()
    {
        $response = $this->ShortlistStudent_model->getAllCoursesName();
        if(is_array($response)){
                $Coursedata['status'] = array('status' => "1", "message" => "Coursedata fetch successfully.");
                $Coursedata['data'] = $response;
        }else{
                $Coursedata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Coursedata));

    }

    public function  getShortlistStudent()
    {
        $response = $this->ShortlistStudent_model-> getShortlistStudent();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    }


    public function searchByDetails()
    {
        $response = $this->ShortlistStudent_model->searchByDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Details fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function getFilterData()
	{
		$response = $this->ShortlistStudent_model->getFilterData();
		if(is_array($response)){
			$Studentdata['status'] = array('status' => "1", "message" => "Fetch Course Fees list successfully.");
			$Studentdata['data'] = $response;
		}else{
			$Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
        
    public function  getSingleStudentDetails()
    {
        $response = $this->ShortlistStudent_model-> getSingleStudentDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 

    public function removeFromShortlist()
    {
        $response = $this->ShortlistStudent_model->removeFromShortlist();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student is Remove From shortlisted List.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function getAllDataForExcel()
    {
        $response = $this->ShortlistStudent_model->getAllDataForExcel();
        if(is_array($response)){
                $userdata['status'] = array('status' => "1", "message" => "Fetch all Student successfully.");
                $userdata['data'] = $response;
        }else{
                $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
    }

    public function addSelectedToConfirm()
    {
        $response = $this->ShortlistStudent_model->addSelectedToConfirm();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student Admissions are confirmed.");
            $Studentdata['data'] = $response;
        }else if($response == -1){
            $Studentdata['status'] = array('status' => "0", "message" => "Sorry ,All seats Are Full.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
    public function addToConfirmed()
    {
        $response = $this->ShortlistStudent_model->addToConfirmed();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student Added in  Confirmed List.");
            $Studentdata['data'] = $response;
        }else if($response == -1){
            $Studentdata['status'] = array('status' => "0", "message" => "Sorry ,All seats Are Full.");
            $Studentdata['data'] = $response;
        }
        else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
    public function addSelectedToSendSMS()
    {
        $response = $this->ShortlistStudent_model->addSelectedToSendSMS();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "SMS send successfully.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
    
    public function getSubjectList()
    {
        $response = $this->ShortlistStudent_model->getSubjectList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

     public function stopPaymentLink()
    {
        $response = $this->ShortlistStudent_model->stopPaymentLink();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student payment link stop successfully.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

     public function onPaymentLink()
    {
        $response = $this->ShortlistStudent_model->onPaymentLink();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student payment link start successfully.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
        
}

    

?>