<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentRegistration extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('StudentRegistration_model');
	}
    
    public function studentRegistration()
	{
		$response = $this->StudentRegistration_model->studentRegistration();
		if($response != -1){
			$studentdata['status'] = array('status' => "1", "message" => "Registered  successfully.");
			$studentdata['data'] = $response;
		}else if($response == -1){
			$studentdata['status'] = array('status' => "0", "message" => "Email address Already registered.");
		}else{
			$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}

	public function updatePaymentStatus()
	{
		$response = $this->StudentRegistration_model->updatePaymentStatus();
		if($response != -1){
			$studentdata['status'] = array('status' => "1", "message" => "Registered  successfully.");
			$studentdata['data'] = $response;
		}else if($response == -1){
			$studentdata['status'] = array('status' => "0", "message" => "Email address Already registered.");
		}else{
			$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}

	

public function testSMS()
	{
$courseName="B-Com";
$studentNumber = "8668555575";
$courseYear="First Year";
$genarateURL = "http://sicescollege.edu.in/sices-online-admission/paidfees.php?FeesDeatils=UEFUSUwgWUFTSCBBTklMfHlhc2hhbXBhdGlsMjdAZ21haWwuY29tfDExfDE=";
 
$username="mucollege";
$password ="Mucollegesms@20";
$number=$studentNumber;
$sender="MUCBLP";
$message="Congrats!You are shortlisted for the ".$courseName ." ". $courseYear." to verification of original documents.  Click this link to pay the fee in 3 days else your stake will be cancelled. 
Payment Link:- ".$genarateURL;
$url="login.bulksmsgateway.in/sendmessage.php?user=".urlencode($username)."&password=".urlencode($password)."&mobile=".urlencode($number)."&sender=".urlencode($sender)."&message=".urlencode($message)."&type=".urlencode('3'); 
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$curl_scraped_page = curl_exec($ch);
curl_close($ch); 
}

	public function getAllCoursesName()
	{
		 $response = $this->StudentRegistration_model->getAllCoursesName();
		 if(is_array($response)){
				 $Formsdata['status'] = array('status' => "1", "message" => "coursename details fetch successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}

	public function getFormFees()
	{	$response = $this->StudentRegistration_model-> getFormFees();
        if($response == NULL){
            $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong."); 
        }else{
            $Formsdata['status'] = array('status' => "1", "message" => "Fees data fetch successfully.");
            $Formsdata['data'] = $response;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
		
	}
	
	public function getAdmissionLinkStatus()
	{	
	   $response = $this->StudentRegistration_model->getAdmissionLinkStatus();
       $Formsdata['status'] = array('status' => "1",'data' => $response, "message" => "Fees data fetch successfully."); 
       $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
	
    
}
