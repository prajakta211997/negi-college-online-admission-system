<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminLogin extends CI_Controller {
	
	
	function __construct() {
		parent::__construct();
               
		$this->load->model('AdminLogin_model');
	}
    
   	public function checkLogin()
	{
		$response = $this->AdminLogin_model->checkLogin();
		if(is_array($response)){
			$userdata['status'] = array('status' => "1", "message" => "login successfull.");
			$userdata['data'] = $response;
		}else if($response==2){
			$userdata['status'] = array('status' => "0", "message" => "Waiting for admin approval.");
		}else if($response == -2){
			$userdata['status'] = array('status' => "0", "message" => "Request sent to admin approval.");
		}else if($response == -4){
			$userdata['status'] = array('status' => "0", "message" => "Admin has denied your request.");
		}else if($response == -3){
			$userdata['status'] = array('status' => "0", "message" => "Invalid login details");
		}else if($response == -5){
			$userdata['status'] = array('status' => "0", "message" => "Previously you are logged in from other machine.your new machine access request sent to admin approval");
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}

	public function changePassword()
	{
		$response = $this->AdminLogin_model->changePassword();
		if($response == 1){
			$passworddata['status'] = array('status' => "1", "message" => "Password Updated successfully.");
			$passworddata['data'] = $response;
		}else{
			$passworddata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($passworddata));
        }
}
