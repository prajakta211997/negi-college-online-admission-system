<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManagePaymentReminder extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ManagePaymentReminder_model');
	}
    
    public function sendPaymentReminder()
	{
		$response = $this->ManagePaymentReminder_model->sendPaymentReminder();
		if($response){
			$PaymentInstallmentdata['status'] = array('status' => "1", "message" => "Payment Reminder Send successfully.");
			$PaymentInstallmentdata['data'] = $response;
		}else if($response == -1){
			$PaymentInstallmentdata['status'] = array('status' => "0", "message" => "Payment Reminder Already Send.");
		}else{
			$PaymentInstallmentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($PaymentInstallmentdata));
    }
    public function getAllCoursesName()
	{
		 $response = $this->ManagePaymentReminder_model->getAllCoursesName();
		 if(is_array($response)){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees details fetch successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
	
	public function updateStudentPaymentStatus()
	{
		 $response = $this->ManagePaymentReminder_model->updateStudentPaymentStatus();
		 if($response){
				 $Formsdata['status'] = array('status' => "1", "message" => "Transaction successfully completed");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
	
	public function checkPayentLinkIsStopOrNot()
	{
		 $response = $this->ManagePaymentReminder_model->checkPayentLinkIsStopOrNot();
		 $Formsdata['status'] = array('status' => "1", "message" => "Transaction successfully completed");
		 $Formsdata['data'] = $response;
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
	
	
    public function sendPaymentReminderIndiusalStudent()
	{
		$response = $this->ManagePaymentReminder_model->sendPaymentReminderIndiusalStudent();
		if($response){
			$PaymentInstallmentdata['status'] = array('status' => "1", "message" => "Payment Reminder Send successfully.");
			$PaymentInstallmentdata['data'] = $response;
		}else if($response == -1){
			$PaymentInstallmentdata['status'] = array('status' => "0", "message" => "Payment Reminder Already Send.");
		}else{
			$PaymentInstallmentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($PaymentInstallmentdata));
    }
    
}
