<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class UserDashboard extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('UserDashboard_model');
	}

    public function getStudentList()
        {
            $response = $this->UserDashboard_model->getStudentList();
            if(is_array($response)){
                $data['status'] = array('status' => "1", "message" => "Student data  fetch successfully.");
                $data['data'] = $response;
            }else{
                $data['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
        public function getSubjectList()
        {
            $response = $this->UserDashboard_model->getSubjectList();
            if(is_array($response)){
                $data['status'] = array('status' => "1", "message" => "Student data  fetch successfully.");
                $data['data'] = $response;
            }else{
                $data['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
        
    
}

