<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddFormFees extends CI_Controller {
	
	function __construct() {
            parent::__construct();
            $this->load->model('AddFormFees_model');
	}
	
	

    public function excelFormFeesUpload() {
        $finalarray = array();
        $retrunBit = 0;
        $returnMessage="";
        $file_data = $_FILES['FormFeesList'];       
        $file_path = $file_data['tmp_name'];

        $this->load->library('EXcel');
        $inputFileName = $file_path;
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		$arrayCount = count($allDataInSheet);
		

        for ($i = 2; $i <= $arrayCount; $i++) {
			
			if(empty($allDataInSheet[$i]["A"]) && empty($allDataInSheet[$i]["B"]) && empty($allDataInSheet[$i]["C"]) && empty($allDataInSheet[$i]["D"]) && empty($allDataInSheet[$i]["E"]) && empty($allDataInSheet[$i]["F"]) && empty($allDataInSheet[$i]["G"]) && empty($allDataInSheet[$i]["H"]) && empty($allDataInSheet[$i]["I"]) && empty($allDataInSheet[$i]["J"]) && empty($allDataInSheet[$i]["K"]) && empty($allDataInSheet[$i]["L"]) && empty($allDataInSheet[$i]["M"]) && empty($allDataInSheet[$i]["N"]) && empty($allDataInSheet[$i]["O"]) && empty($allDataInSheet[$i]["P"]) && empty($allDataInSheet[$i]["Q"]) && empty($allDataInSheet[$i]["R"]) && empty($allDataInSheet[$i]["S"]) && empty($allDataInSheet[$i]["T"]) && empty($allDataInSheet[$i]["U"]) && empty($allDataInSheet[$i]["V"]) && empty($allDataInSheet[$i]["W"]) && empty($allDataInSheet[$i]["X"]) && empty($allDataInSheet[$i]["Y"]) && empty($allDataInSheet[$i]["Z"]) && empty($allDataInSheet[$i]["AA"]) && empty($allDataInSheet[$i]["AB"])&& empty($allDataInSheet[$i]["AC"]) && empty($allDataInSheet[$i]["AD"])&& empty($allDataInSheet[$i]["AE"]) && empty($allDataInSheet[$i]["AF"]) && empty($allDataInSheet[$i]["AG"]) && empty($allDataInSheet[$i]["AH"]) && empty($allDataInSheet[$i]["AI"]) && empty($allDataInSheet[$i]["AJ"]) && empty($allDataInSheet[$i]["AK"]) && empty($allDataInSheet[$i]["AL"]) && empty($allDataInSheet[$i]["AM"]) && empty($allDataInSheet[$i]["AN"])){
				$retrunBit = 1;  
			    break;
			}else{
					if (!empty($allDataInSheet[$i]["A"])) {
						if (!empty($allDataInSheet[$i]["B"])) {
							if (!empty($allDataInSheet[$i]["C"])) {
								if (!empty($allDataInSheet[$i]["D"])) {
									
									$individualArray = array(
											

											'course_name'=> trim($allDataInSheet[$i]["A"]), 
											'academic_year'=> trim($allDataInSheet[$i]["B"]), 
											'course_year' => trim($allDataInSheet[$i]["C"]), 
											'form_fees' => trim($allDataInSheet[$i]["D"])
											
										);
										$finalarray[] = $individualArray;
										$retrunBit = 1;
								
								}else{
								$retrunBit = 2;
								$returnMessage = "Your provided excel sheet contains empty value on column 'Form Fees' & row number ".$i;
								break;
								}
							}else{
							$retrunBit = 2;
							$returnMessage = "Your provided excel sheet contains empty value on column 'Course Year' & row number ".$i;
							break;
							}
						
						}else{
						$retrunBit = 2;
						$returnMessage = "Your provided excel sheet contains empty value on column ' Academic Year ' & row number ".$i;
						break;
						}
					}else{
					$retrunBit = 2;
					$returnMessage = "Your provided excel sheet contains empty value on column 'Course Id '  & row number ".$i;
					break;
					}
				
			}
		}
		if($retrunBit == 1){
                    $response = $this->AddFormFees_model->excelFormFeesUpload($finalarray);
                    if($response == 1){
                        $userdata['status'] = array('status' => "1", "message" => "Course wise Form Fees added successfully.");
                        $userdata['data'] = $response;
                    }else if($response == -1){
                            $userdata['status'] = array('status' => "0", "message" => "Course wise Form Fees already exists.");
                    }else{
                            $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                    }
			//$userdata['status'] = array('status' => "1", "message" => "Student excel uploaded successfully.");
		}else if($retrunBit == 2){
			$userdata['status'] = array('status' => "0", "message" => $returnMessage);
		}else if($retrunBit == -1){
                    $userdata['status'] = array('status' => "0",  "message" => "Course wise Form Fees already exists.");
                }else{
                    $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
	   
	   $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
	
	public function addSingleFormFee()
	{
		$response = $this->AddFormFees_model->addSingleFormFee();
		if($response != -1){
			$Formdata['status'] = array('status' => "1", "message" => "Register successfully.");
			$Formdata['data'] = $response;
		}else if($response == -1){
			$Formdata['status'] = array('status' => "0", "message" => "Form Wise  Fees Details Already Added.");
		}else{
			$Formdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Formdata));
	}

	public function getFilterData()
	{
		$response = $this->AddFormFees_model->getFilterData();
		if(is_array($response)){
			$Formsdata['status'] = array('status' => "1", "message" => "Fetch Form Fees list successfully.");
			$Formsdata['data'] = $response;
		}else{
			$Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	} 

	

	public function getFormFeesList()
	{
		 $response = $this->AddFormFees_model->getFormFeesList();
		 if(is_array($response)){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees details fetch successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}

	public function getAllCoursesName()
	{
		 $response = $this->AddFormFees_model->getAllCoursesName();
		 if(is_array($response)){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees details fetch successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
	

	public function searchByCourses()
	{
		 $response = $this->AddFormFees_model->searchByCourses();
		 if(is_array($response)){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees Details fetch successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
 
	public function updateFormFee()
	{
		 $response = $this->AddFormFees_model->updateFormFee();
		 if($response == 1){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees Details updated successfully.");
				 $Formsdata['data'] = $response;
		 }else if($response == -1){
				 $Formsdata['status'] = array('status' => "0", "message" => "Nothing to update.");
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
 
	public function deleteFormFees()
	{
		 $response = $this->AddFormFees_model->deleteFormFees();
		 if($response == 1){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees details deleted successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
		
	public function getAllDataForExcel()
    {
        $response = $this->AddFormFees_model->getAllDataForExcel();
        if(is_array($response)){
                $coursesdata['status'] = array('status' => "1", "message" => "Fetch all course wise fee details successfully.");
                $coursesdata['data'] = $response;
        }else{
                $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
    }
   


}