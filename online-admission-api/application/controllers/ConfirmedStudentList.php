<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfirmedStudentList extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ConfirmedStudentList_model');
	}


    public function getReligionList()
    {
        $response = $this->ConfirmedStudentList_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }


    public function getCasteList()
    {
        $response = $this->ConfirmedStudentList_model->getCasteList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function getAllCoursesName()
    {
        $response = $this->ConfirmedStudentList_model->getAllCoursesName();
        if(is_array($response)){
                $Coursedata['status'] = array('status' => "1", "message" => "Coursedata fetch successfully.");
                $Coursedata['data'] = $response;
        }else{
                $Coursedata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Coursedata));

    }

    public function  getConfirmedStudentList()
    {
        $response = $this->ConfirmedStudentList_model-> getConfirmedStudentList();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    }


    public function searchByDetails()
    {
        $response = $this->ConfirmedStudentList_model->searchByDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Details fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function searchByPaymentDetails()
    {
        $response = $this->ConfirmedStudentList_model->searchByPaymentDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Details fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
    

    public function getFilterData()
	{
		$response = $this->ConfirmedStudentList_model->getFilterData();
		if(is_array($response)){
			$Studentdata['status'] = array('status' => "1", "message" => "Fatch Course Fees list successfully.");
			$Studentdata['data'] = $response;
		}else{
			$Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
        
    public function  getSingleStudentDetails()
    {
        $response = $this->ConfirmedStudentList_model-> getSingleStudentDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 
    public function  getPaymentList()
    {
        $response = $this->ConfirmedStudentList_model-> getPaymentList();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Paymentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 
    

    public function removeFromShortlist()
    {
        $response = $this->ConfirmedStudentList_model->removeFromShortlist();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student is Removed From shortlisted List.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function removeFromConfirmed()
    {
        $response = $this->ConfirmedStudentList_model->removeFromConfirmed();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student is Remove From Confirmed List.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    
    

    public function addPayment()
	{
		$response = $this->ConfirmedStudentList_model->addPayment();
		if($response != -1){
			$paymentdata['status'] = array('status' => "1", "message" => "Payment added successfully.");
			$paymentdata['data'] = $response;
		}else if($response == -1){
			$paymentdata['status'] = array('status' => "0", "message" => "Payment Already Added.");
		}else{
			$paymentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($paymentdata));
    }

    public function updatePayment()
    {
        $response = $this->ConfirmedStudentList_model->updatePayment();
        if($response == 1){
                $paymentdata['status'] = array('status' => "1", "message" => "Payment updated successfully.");
                $paymentdata['data'] = $response;
        }else if($response == -1){
                $paymentdata['status'] = array('status' => "0", "message" => "Nothing to update.");
        }else{
                $paymentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($paymentdata));
    }

    public function PaymentRecieved()
	{
		$response = $this->ConfirmedStudentList_model->PaymentRecieved();
		if($response == 1){
			$paymentdata['status'] = array('status' => "1", "message" => "Payment Recieved successfully.");
			$paymentdata['data'] = $response;
		}else{
			$paymentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($paymentdata));
    }

    public function deletePayment()
    {
        $response = $this->ConfirmedStudentList_model->deletePayment();
        if($response == 1){
                $paymentdata['status'] = array('status' => "1", "message" => "Payment  deleted successfully.");
                $paymentdata['data'] = $response;
        }else{
                $paymentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($paymentdata));
    }
    
    public function PaymentPaid()
    {
        $response = $this->ConfirmedStudentList_model->PaymentPaid();
            if($response == 1){
                $paymentdata['status'] = array('status' => "1", "message" => "Payment paid successfully.");
                $paymentdata['data'] = $response;
            }else{
                $paymentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($paymentdata));
    }
    public function PaymentUnPaid()
    {
        $response = $this->ConfirmedStudentList_model->PaymentUnPaid();
        if($response == 1){
            $paymentdata['status'] = array('status' => "1", "message" => "Payment Unpaid successfully.");
            $paymentdata['data'] = $response;
        }else{
            $paymentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($paymentdata));
    }
    
    public function  getTotalFees()
    {
        $response = $this->ConfirmedStudentList_model-> getTotalFees();
        if($response == NULL){
            $Studentdata['status'] = array('status' => "", "message" => "Opps! Something went Wrong."); 
        }else{
            $Studentdata['status'] = array('status' => "1", "message" => "Fees data fetch successfully.");
            $Studentdata['data'] = $response;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 

    public function getAllDataForExcel()
    {
        $response = $this->ConfirmedStudentList_model->getAllDataForExcel();
        if(is_array($response)){
                $userdata['status'] = array('status' => "1", "message" => "Fetch all Student successfully.");
                $userdata['data'] = $response;
        }else{
                $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
    }

    public function getStudentRegistrationDetails()
    {
		$response = $this->ConfirmedStudentList_model->getStudentRegistrationDetails();
		if(is_array($response)){
				$studentdata['status'] = array('status' => "1", "message" => "student data fetch successfully.");
				$studentdata['data'] = $response;
		}else{
				$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}
       
    public function saveProfile()
    {
        $response = $this->ConfirmedStudentList_model->saveProfile();
        if($response == 1){
                $coursesdata['status'] = array('status' => "1", "message" => "Profile updated successfully.");
                $coursesdata['data'] = $response;
        }else if($response == -1){
                $coursesdata['status'] = array('status' => "0", "message" => "Nothing to update.");
        }else{
                $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
    }

    public function getSubjectList()
    {
        $response = $this->ConfirmedStudentList_model->getSubjectList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

}
?>