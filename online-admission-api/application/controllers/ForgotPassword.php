<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ForgotPassword extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ForgotPassword_model');
	}
	
	public function index()
	{
		$this->load->view('forgot_password');
	}

	public function ForgotPasswordSendEmail()
	{
		$response = $this->ForgotPassword_model->ForgotPasswordSendEmail();
		if($response == 1){
			$userdata['status'] = array('status' => "1", "message" => "Please check your email for password.");
			$userdata['data'] = $response;
		}else if($response == 2){
			$userdata['status'] = array('status' => "0", "message" => "Provided email not validate ");
		}else{
			$userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
}
