
var csrfHash = $('#csrfHash').val();

app.controller('addCourseFeesServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
        $scope.addCourseFeesDiv = false;
        $scope.addCourseFeesButton = true;
        $scope.updateCourseFeesButton = false;
        $scope.coursesListArray = [];
        $scope.singleCourseArray = [];
        $scope.courseNameArray =[];
        $scope.exportData = [];
        $scope.getCoursesList();
        $scope.getAllCoursesName();
        $scope.getAllDataForExcel();
       $scope.fileName = "AllCourseFeesData";
        $scope.file = "";
        $("#exampleInputFile").val('');
        $scope.disabledCreateCourseFeesButton = false;
        $scope.disabledupdateCourseFeesButton = false;
        $scope.disabledDownloadAllCourseFeesButton= true;
        $scope.addMoreDiv1 = false;
        $scope.addMoreDiv2 = false;
        }
       
        
    }
    var year = 2015;
    var range = [];
    var range1 = [];
   
    for (var i = 2015; i <= new Date().getFullYear()  ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;
    
    $scope.AddMoreFeesDetails = function(){
        if($scope.addMoreDiv1 == '' ){
            $scope.addMoreDiv1 = true;
        }else{
            $scope.addMoreDiv2 = true;
        }
        
    }
    $scope.showaddCourseFeesDiv = function () {
        if($scope.addCourseFeesDiv == true){
          $scope.addCourseFeesDiv = false;
          $scope.updateCourseFeesButton = false;
          $scope.addCourseFeesButton = true;
        }else{
          $scope.addCourseFeesDiv = true;
          $scope.updateCourseFeesButton = false;
          $scope.addCourseFeesButton = true;
        }  
        $scope.academicYear = "";
        $scope.courseName = "";
        $scope.courseYear = "";
        $scope.courseFeesType = "";
        $scope.courseFeesSubType = "";
        $scope.courseDivision ="";
        $scope.tutionFees = "";
        $scope.libraryFees = "";
        $scope.activitiesFees = "";
        $scope.gymkhanaFees = "";
        $scope.eligibilityFees = "";
        $scope.eligibilityFormFees = "";
        $scope.admissionFees = "";
        $scope.studentWelfareFees  = "";
        $scope.computerizationFees = "";
        $scope.developmentFees = "";
        $scope.registrationFees = "";
        $scope.proRataFees = "";
        $scope.disasMgmtFees = "";
        $scope.libraryDeposit  = "";
        $scope.cautionMoney  = "";
        $scope.medicalExam  = "";
        $scope.practicalFees  = "";
        $scope.laboratoryFees  = "";
        $scope.sissFees  = "";
        $scope.personnelFees  = "";
        $scope.shortTermCourseFee = "";
        $scope.securityCharges = "";
        $scope.administrationCharges = "";
        $scope.seminarFees  = "";
        $scope.phyEdnScheme  = "";
        $scope.alumniFees  = "";
        $scope.collegeDevpFees  = "";
        $scope.termendExamFees  = "";
        $scope.computerLabFees  = "";
        $scope.workshopSeminarFees = "";
        $scope.compInternetFees  = "";
        $scope.corpusFund  = "";
        $scope.formFees  = "";
        $scope.nssSectionFees  = "";
        $scope.compLabMaintainanceFees = "";
        $scope.addOnCoursesFees = "";
        $scope.addExtraFees = "";
        $scope.addExtranewFees ="";
        $scope.sum = "";
       
     }
 
    var NumberPattern = /^[0-9]{1,12}$/;

    $scope.addCourseFees= function(){
        if($scope.file){
            $scope.disabledCreateCourseFeesButton = true;
            $("#loadingDiv").css('display','block');
            $http({
                method: 'POST',
                url: window.site_url +'AddCourseFess/excelCoureseFeesUpload',
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();                    
                    formData.append("CourseFeesList", $scope.file);        
                    formData.append("csrf_token_name", csrfHash);                                
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).then((response) => {
                if (response.data.status.status == "1") {
                    toastr.success(response.data.status.message);
                    $scope.init();
                   // $scope.hideAddStudentDiv();
                    $scope.file = "";
                    $("#exampleInputFile").val('');
                    //$scope.getStudentGroup();
                }else {
                    toastr.error(response.data.status.message);
                    $scope.init();
                    $scope.disabledCreateCourseFeesButton = false;
                    $("#loadingDiv").css('display','none');
                }
                $scope.disabledCreateCourseFeesButton = false;
                $("#loadingDiv").css('display','none');
            });
        }else{
            toastr.error('Please select Course Fees excel file');
        }
    }
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
                       
            setTimeout(() => {
                window.location = "index.html";
                },1000)
    }
    $scope.addSingleCourseFees = function(){
     
        if ($scope.academicYear) {
            if ($scope.courseName) {
                if ($scope.courseYear) {
                    if ($scope.courseFeesType) {
                        if($scope.courseDivision){
                            if ($scope.tutionFees) {
                                if(!( $scope.tutionFees.match(NumberPattern))){
                                    $('#tutionFees').focus();
                                    toastr.error("Please enter valid tution Fees.");
                                    return false;   
                                    }
                                    if ($scope.libraryFees) {
                                        if(!( $scope.libraryFees.match(NumberPattern))){
                                            $('#libraryFees').focus();
                                            toastr.error("Please enter valid library Fees.");
                                            return false;   
                                            }
                                            if ($scope.activitiesFees) {
                                                if(!( $scope.activitiesFees.match(NumberPattern))){
                                                    $('#activitiesFees').focus();
                                                    toastr.error("Please enter valid activities Fees.");
                                                    return false;   
                                                    }
                                                    if ($scope.gymkhanaFees) {
                                                        if(!( $scope.gymkhanaFees.match(NumberPattern))){
                                                            $('#gymkhanaFees').focus();
                                                            toastr.error("Please enter valid gymkhana Fees.");
                                                            return false;   
                                                            }
                                                            if ($scope.eligibilityFees) {
                                                                if(!( $scope.eligibilityFees.match(NumberPattern))){
                                                                    $('#eligibilityFees').focus();
                                                                    toastr.error("Please enter valid eligibility Fees.");
                                                                    return false;   
                                                                    }
                                                                    if ($scope.eligibilityFormFees) {
                                                                        if(!( $scope.eligibilityFormFees.match(NumberPattern))){
                                                                            $('#eligibilityFormFees').focus();
                                                                            toastr.error("Please enter valid eligibility Form Fees.");
                                                                            return false;   
                                                                            }
                                                                            if ($scope.admissionFees) {
                                                                                if(!( $scope.admissionFees.match(NumberPattern))){
                                                                                    $('#admissionFees').focus();
                                                                                    toastr.error("Please enter valid admission Fees.");
                                                                                    return false;   
                                                                                    }
                                                                                    if ($scope.studentWelfareFees) {
                                                                                        if(!( $scope.studentWelfareFees.match(NumberPattern))){
                                                                                            $('#studentWelfareFees').focus();
                                                                                            toastr.error("Please enter valid student Welfare Fees.");
                                                                                            return false;   
                                                                                            }
                                                                                            if ($scope.computerizationFees) {
                                                                                                if(!( $scope.computerizationFees.match(NumberPattern))){
                                                                                                    $('#computerizationFees').focus();
                                                                                                    toastr.error("Please enter valid computerization Fees.");
                                                                                                    return false;   
                                                                                                    }
                                                                                                    if ($scope.developmentFees) {
                                                                                                        if(!( $scope.developmentFees.match(NumberPattern))){
                                                                                                            $('#developmentFees').focus();
                                                                                                            toastr.error("Please enter valid development Fees.");
                                                                                                            return false;   
                                                                                                            }
                                                                                                            if ($scope.registrationFees) {
                                                                                                                if(!( $scope.registrationFees.match(NumberPattern))){
                                                                                                                    $('#registrationFees').focus();
                                                                                                                    toastr.error("Please enter valid registration Fees.");
                                                                                                                    return false;   
                                                                                                                    }
                                                                                                                    if ($scope.proRataFees) {
                                                                                                                        if(!( $scope.proRataFees.match(NumberPattern))){
                                                                                                                            $('#proRataFees').focus();
                                                                                                                            toastr.error("Please enter valid proRata Fees.");
                                                                                                                            return false;   
                                                                                                                            }
                                                                                                                            if ($scope.disasMgmtFees) {
                                                                                                                                if(!( $scope.disasMgmtFees.match(NumberPattern))){
                                                                                                                                    $('#disasMgmtFees').focus();
                                                                                                                                    toastr.error("Please enter valid disasMgmt Fees.");
                                                                                                                                    return false;   
                                                                                                                                    }
                                                                                                                                    if ($scope.libraryDeposit) {
                                                                                                                                        if(!( $scope.libraryDeposit.match(NumberPattern))){
                                                                                                                                            $('#libraryDeposit').focus();
                                                                                                                                            toastr.error("Please enter valid library Deposit.");
                                                                                                                                            return false;   
                                                                                                                                            }
                                                                                                                                            if ($scope.cautionMoney) {
                                                                                                                                                if(!( $scope.cautionMoney.match(NumberPattern))){
                                                                                                                                                    $('#cautionMoney').focus();
                                                                                                                                                    toastr.error("Please enter valid caution Money.");
                                                                                                                                                    return false;   
                                                                                                                                                    }
                                                                                                                                                    if ($scope.medicalExam) {
                                                                                                                                                        if(!( $scope.medicalExam.match(NumberPattern))){
                                                                                                                                                            $('#medicalExam').focus();
                                                                                                                                                            toastr.error("Please enter valid medical Exam.");
                                                                                                                                                            return false;   
                                                                                                                                                            }
                                                                                                                                                            if ($scope.practicalFees) {
                                                                                                                                                                if(!( $scope.practicalFees.match(NumberPattern))){
                                                                                                                                                                    $('#practicalFees').focus();
                                                                                                                                                                    toastr.error("Please enter valid practical Fees.");
                                                                                                                                                                    return false;   
                                                                                                                                                                    }
                                                                                                                                                                    if ($scope.laboratoryFees) {
                                                                                                                                                                        if(!( $scope.laboratoryFees.match(NumberPattern))){
                                                                                                                                                                            $('#laboratoryFees').focus();
                                                                                                                                                                            toastr.error("Please enter valid laboratory Fees.");
                                                                                                                                                                            return false;   
                                                                                                                                                                            }
                                                                                                                                                                            if ($scope.sissFees) {
                                                                                                                                                                                if(!( $scope.sissFees.match(NumberPattern))){
                                                                                                                                                                                    $('#sissFees').focus();
                                                                                                                                                                                    toastr.error("Please enter valid siss Fees.");
                                                                                                                                                                                    return false;   
                                                                                                                                                                                    }
                                                                                                                                                                                    if ($scope.personnelFees) {
                                                                                                                                                                                        if(!( $scope.personnelFees.match(NumberPattern))){
                                                                                                                                                                                            $('#personnelFees').focus();
                                                                                                                                                                                            toastr.error("Please enter valid personnel Fees.");
                                                                                                                                                                                            return false;   
                                                                                                                                                                                            }
                                                                                                                                                                                            if ($scope.shortTermCourseFee) {
                                                                                                                                                                                                if(!( $scope.shortTermCourseFee.match(NumberPattern))){
                                                                                                                                                                                                    $('#shortTermCourseFee').focus();
                                                                                                                                                                                                    toastr.error("Please enter valid short Term Course Fee.");
                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                    }
                                                                                                                                                                                                    if ($scope.securityCharges) {
                                                                                                                                                                                                        if(!($scope.securityCharges.match(NumberPattern))){
                                                                                                                                                                                                            $('#securityCharges').focus();
                                                                                                                                                                                                            toastr.error("Please enter valid security Charges.");
                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                            }
                                                                                                                                                                                                            if ($scope.administrationCharges) {
                                                                                                                                                                                                                if(!( $scope.administrationCharges.match(NumberPattern))){
                                                                                                                                                                                                                    $('#administrationCharges').focus();
                                                                                                                                                                                                                    toastr.error("Please enter valid administration Charges.");
                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    if ($scope.seminarFees) {
                                                                                                                                                                                                                        if(!( $scope.seminarFees.match(NumberPattern))){
                                                                                                                                                                                                                            $('#seminarFees').focus();
                                                                                                                                                                                                                            toastr.error("Please enter valid seminar Fees.");
                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                            if ($scope.phyEdnScheme) {
                                                                                                                                                                                                                                if(!($scope.phyEdnScheme.match(NumberPattern))){
                                                                                                                                                                                                                                    $('#phyEdnScheme').focus();
                                                                                                                                                                                                                                    toastr.error("Please enter valid phyEdnScheme.");
                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                    if ($scope.alumniFees) {
                                                                                                                                                                                                                                        if(!($scope.alumniFees.match(NumberPattern))){
                                                                                                                                                                                                                                            $('#alumniFees').focus();
                                                                                                                                                                                                                                            toastr.error("Please enter valid alumni Fees.");
                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                            if ($scope.collegeDevpFees) {
                                                                                                                                                                                                                                                if(!( $scope.collegeDevpFees.match(NumberPattern))){
                                                                                                                                                                                                                                                    $('#collegeDevpFees').focus();
                                                                                                                                                                                                                                                    toastr.error("Please enter valid collegeDevpFees.");
                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                    if ($scope.termendExamFees) {
                                                                                                                                                                                                                                                        if(!( $scope.termendExamFees.match(NumberPattern))){
                                                                                                                                                                                                                                                            $('#termendExamFees').focus();
                                                                                                                                                                                                                                                            toastr.error("Please enter valid termendExamFees.");
                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                            if ($scope.computerLabFees) {
                                                                                                                                                                                                                                                                if(!( $scope.computerLabFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                    $('#computerLabFees').focus();
                                                                                                                                                                                                                                                                    toastr.error("Please enter valid computer Lab Fees.");
                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                    if ($scope.workshopSeminarFees) {
                                                                                                                                                                                                                                                                        if(!( $scope.workshopSeminarFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                            $('#workshopSeminarFees').focus();
                                                                                                                                                                                                                                                                            toastr.error("Please enter valid workshop Seminar Fees.");
                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                            if ($scope.compInternetFees) {
                                                                                                                                                                                                                                                                                if(!( $scope.compInternetFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                    $('#compInternetFees').focus();
                                                                                                                                                                                                                                                                                    toastr.error("Please enter valid computer Internet Fees.");
                                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                    if ($scope.corpusFund) {
                                                                                                                                                                                                                                                                                        if(!( $scope.corpusFund.match(NumberPattern))){
                                                                                                                                                                                                                                                                                            $('#corpusFund').focus();
                                                                                                                                                                                                                                                                                            toastr.error("Please enter valid corpusFund.");
                                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                            if ($scope.formFees) {
                                                                                                                                                                                                                                                                                                if(!( $scope.formFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                    $('#formFees').focus();
                                                                                                                                                                                                                                                                                                    toastr.error("Please enter valid form Fees.");
                                                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                    if ($scope.nssSectionFees) {
                                                                                                                                                                                                                                                                                                        if(!( $scope.nssSectionFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                            $('#nssSectionFees').focus();
                                                                                                                                                                                                                                                                                                            toastr.error("Please enter valid nssSectionFees.");
                                                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                            if ($scope.compLabMaintainanceFees) {
                                                                                                                                                                                                                                                                                                                if(!( $scope.compLabMaintainanceFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                                    $('#compLabMaintainanceFees').focus();
                                                                                                                                                                                                                                                                                                                    toastr.error("Please enter valid number.");
                                                                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                    if($scope.addOnCoursesFees){

                                                                                                                                                                                                                                                                                                                        if(!( $scope.addOnCoursesFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                                            $('#addOnCoursesFees').focus();
                                                                                                                                                                                                                                                                                                                            toastr.error("Please enter valid number.");
                                                                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                    var transform = function(data) {
                                                                                                                                                                                                                                                                                                                        return $.param(data);
                                                                                                                                                                                                                                                                                                                    } 
                                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                    $http.post(window.site_url + 'AddCourseFess/addSingleCourseFee', {
                                                                                                                                                                                                                                                                                                                        'csrf_token_name': csrfHash,
                                                                                                                                                                                                                                                                                                                        academicYear: $scope.academicYear,
                                                                                                                                                                                                                                                                                                                        courseName :$scope.courseName,
                                                                                                                                                                                                                                                                                                                        courseYear:$scope.courseYear,
                                                                                                                                                                                                                                                                                                                        courseFeesType: $scope.courseFeesType,
                                                                                                                                                                                                                                                                                                                        courseFeesSubType : $scope.courseFeesSubType, 
                                                                                                                                                                                                                                                                                                                        courseDivision : $scope.courseDivision ,
                                                                                                                                                                                                                                                                                                                        tutionFees :  $scope.tutionFees,
                                                                                                                                                                                                                                                                                                                        libraryFees : $scope.libraryFees,
                                                                                                                                                                                                                                                                                                                        activitiesFees :  $scope.activitiesFees,
                                                                                                                                                                                                                                                                                                                        gymkhanaFees : $scope.gymkhanaFees,
                                                                                                                                                                                                                                                                                                                        eligibilityFees :  $scope.eligibilityFees ,
                                                                                                                                                                                                                                                                                                                        eligibilityFormFees : $scope.eligibilityFormFees ,
                                                                                                                                                                                                                                                                                                                        admissionFees : $scope.admissionFees,
                                                                                                                                                                                                                                                                                                                        studentWelfareFees : $scope.studentWelfareFees ,
                                                                                                                                                                                                                                                                                                                        computerizationFees :  $scope.computerizationFees,
                                                                                                                                                                                                                                                                                                                        developmentFees: $scope.developmentFees,
                                                                                                                                                                                                                                                                                                                        registrationFees : $scope.registrationFees,
                                                                                                                                                                                                                                                                                                                        proRataFees: $scope.proRataFees,
                                                                                                                                                                                                                                                                                                                        disasMgmtFees: $scope.disasMgmtFees,
                                                                                                                                                                                                                                                                                                                        libraryDeposit: $scope.libraryDeposit ,
                                                                                                                                                                                                                                                                                                                        cautionMoney :  $scope.cautionMoney ,
                                                                                                                                                                                                                                                                                                                        medicalExam: $scope.medicalExam ,
                                                                                                                                                                                                                                                                                                                        practicalFees: $scope.practicalFees ,
                                                                                                                                                                                                                                                                                                                        laboratoryFees: $scope.laboratoryFees ,
                                                                                                                                                                                                                                                                                                                        sissFees:$scope.sissFees ,
                                                                                                                                                                                                                                                                                                                        personnelFees: $scope.personnelFees ,
                                                                                                                                                                                                                                                                                                                        shortTermCourseFee: $scope.shortTermCourseFee,
                                                                                                                                                                                                                                                                                                                        securityCharges: $scope.securityCharges,
                                                                                                                                                                                                                                                                                                                        administrationCharges: $scope.administrationCharges,
                                                                                                                                                                                                                                                                                                                        seminarFees: $scope.seminarFees ,
                                                                                                                                                                                                                                                                                                                        phyEdnScheme: $scope.phyEdnScheme ,
                                                                                                                                                                                                                                                                                                                        alumniFees:  $scope.alumniFees ,
                                                                                                                                                                                                                                                                                                                        collegeDevpFees: $scope.collegeDevpFees ,
                                                                                                                                                                                                                                                                                                                        termendExamFees: $scope.termendExamFees ,
                                                                                                                                                                                                                                                                                                                        computerLabFees: $scope.computerLabFees ,
                                                                                                                                                                                                                                                                                                                        workshopSeminarFees: $scope.workshopSeminarFees,
                                                                                                                                                                                                                                                                                                                        compInternetFees: $scope.compInternetFees ,
                                                                                                                                                                                                                                                                                                                        corpusFund: $scope.corpusFund ,
                                                                                                                                                                                                                                                                                                                        formFees: $scope.formFees ,
                                                                                                                                                                                                                                                                                                                        nssSectionFees: $scope.nssSectionFees ,
                                                                                                                                                                                                                                                                                                                        compLabMaintainanceFees:  $scope.compLabMaintainanceFees,
                                                                                                                                                                                                                                                                                                                        addOnCoursesFees : $scope.addOnCoursesFees,
                                                                                                                                                                                                                                                                                                                        addExtraFees: $scope.addExtraFees,
                                                                                                                                                                                                                                                                                                                        addExtranewFees : $scope.addExtranewFees,
                                                                                                                                                                                                                                                                                                                        totalFees : $scope.sum,
                                                                                                                                                                                                                                                                                                                        
                                                                                                                                                                                                                                                                                                                    }, {
                                                                                                                                                                                                                                                                                                                        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                                                                                                                                                                                                                                                                                                        transformRequest: transform
                                                                                                                                                                                                                                                                                                                    })
                                                                                                                                                                                                                                                                                                                    .then(function(response) {
                                                                                                                                                                                                                                                                                                                        if (response.data.status.status == "1") {
                                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                            toastr.success(response.data.status.message);
                                                                                                                                                                                                                                                                                                                            $scope.academicYear = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseName = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseYear = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseFeesType = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseFeesSubType = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseDivision = "";
                                                                                                                                                                                                                                                                                                                            $scope.tutionFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.libraryFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.activitiesFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.gymkhanaFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.eligibilityFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.eligibilityFormFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.admissionFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.studentWelfareFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.computerizationFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.developmentFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.registrationFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.proRataFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.disasMgmtFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.libraryDeposit  = "";
                                                                                                                                                                                                                                                                                                                            $scope.cautionMoney  = "";
                                                                                                                                                                                                                                                                                                                            $scope.medicalExam  = "";
                                                                                                                                                                                                                                                                                                                            $scope.practicalFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.laboratoryFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.sissFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.personnelFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.shortTermCourseFee = "";
                                                                                                                                                                                                                                                                                                                            $scope.securityCharges = "";
                                                                                                                                                                                                                                                                                                                            $scope.administrationCharges = "";
                                                                                                                                                                                                                                                                                                                            $scope.seminarFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.phyEdnScheme  = "";
                                                                                                                                                                                                                                                                                                                            $scope.alumniFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.collegeDevpFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.termendExamFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.computerLabFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.workshopSeminarFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.compInternetFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.corpusFund  = "";
                                                                                                                                                                                                                                                                                                                            $scope.formFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.nssSectionFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.compLabMaintainanceFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.addOnCoursesFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.addExtraFees ="";
                                                                                                                                                                                                                                                                                                                            $scope.addExtranewFees ="";
                                                                                                                                                                                                                                                                                                                            $scope.sum = "";
                                                                                                                                                                                                                                                                                                                        $scope.init();
                                                                                                                                                                                                                                                                                                                        
                                                                                                                                                                                                                                                                                                                        }else{
                                                                                                                                                                                                                                                                                                                            toastr.error(response.data.status.message);
                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                        $scope.disabledCreateCourseFeesButton = false;
                                                                                                                                                                                                                                                                                                                        $("#loadingDiv").css('display','none');
                                                                                                                                                                                                                                                                                                                    }, function(response) {
                                                                                                                                                                                                                                                                                                                        $scope.disabledCreateCourseFeesButton = false;
                                                                                                                                                                                                                                                                                                                                $("#loadingDiv").css('display','none');
                                                                                                                                                                                                                                                                                                                    })
                                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                                    $('#addOnCoursesFees').focus();
                                                                                                                                                                                                                                                                                                                    toastr.error("Add on course fees is required.");
                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                                $('#compLabMaintainanceFees').focus();
                                                                                                                                                                                                                                                                                                                toastr.error("Computer Lab Maintainance Fees is required.");
                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                                                                        $('#nssSectionFees').focus();
                                                                                                                                                                                                                                                                                                        toastr.error("NSS Section Fees is required.");
                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                $('#formFees').focus();
                                                                                                                                                                                                                                                                                                toastr.error("Form Fees is required.");
                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                                                        $('#corpusFund').focus();
                                                                                                                                                                                                                                                                                        toastr.error("Corpus Fund is required.");
                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                $('#compInternetFees').focus();
                                                                                                                                                                                                                                                                                toastr.error("Computer Internet Fees is required.");
                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                                        $('#workshopSeminarFees').focus();
                                                                                                                                                                                                                                                                        toastr.error("Workshop Seminar Fees is required.");
                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                $('#computerLabFees').focus();
                                                                                                                                                                                                                                                                toastr.error("Computer Lab Fees is required.");
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                        $('#termendExamFees').focus();
                                                                                                                                                                                                                                                        toastr.error("Termend Exam Fees is required.");
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                $('#collegeDevpFees').focus();
                                                                                                                                                                                                                                                toastr.error("College Devlopment  Fees is required.");
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                        $('#alumniFees').focus();
                                                                                                                                                                                                                                        toastr.error("Alumni Fees is required.");
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                $('#phyEdnScheme').focus();
                                                                                                                                                                                                                                toastr.error("Phy. Edn. Scheme is required.");
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                        $('#seminarFees').focus();
                                                                                                                                                                                                                        toastr.error("Seminar Fees is required.");
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                        
                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                $('#administrationCharges').focus();
                                                                                                                                                                                                                toastr.error("Administration Charges is required.");
                                                                                                                                                                                                                }
                                                                                                                                                                                                    } else {
                                                                                                                                                                                                        $('#securityCharges').focus();
                                                                                                                                                                                                        toastr.error("security Charges is required.");
                                                                                                                                                                                                        }
                                                                                                                                                                                            } else {
                                                                                                                                                                                                $('#shortTermCourseFee').focus();
                                                                                                                                                                                                toastr.error("short Term Course Fee is required.");
                                                                                                                                                                                                }
                                                                                                                                                                                    } else {
                                                                                                                                                                                        $('#personnelFees').focus();
                                                                                                                                                                                        toastr.error("personnel Fee is required.");
                                                                                                                                                                                        }
                                                                                                                                                                            } else {
                                                                                                                                                                                $('#sissFees').focus();
                                                                                                                                                                                toastr.error("siss Fee is required.");
                                                                                                                                                                                }
                                                                                                                                                                    } else {
                                                                                                                                                                        $('#laboratoryFees').focus();
                                                                                                                                                                        toastr.error("laboratory Fee is required.");
                                                                                                                                                                        }
                                                                                                                                                            } else {
                                                                                                                                                                $('#practicalFees').focus();
                                                                                                                                                                toastr.error("practical Fee is required.");
                                                                                                                                                                }
                                                                                                                                                    } else {
                                                                                                                                                        $('#medicalExam').focus();
                                                                                                                                                        toastr.error("medicalExam Fee is required.");
                                                                                                                                                        }
                                                                                                                                            } else {
                                                                                                                                                $('#cautionMoney').focus();
                                                                                                                                                toastr.error("cautionMoney is required.");
                                                                                                                                                }
                                                                                                                                    } else {
                                                                                                                                        $('#libraryDeposit').focus();
                                                                                                                                        toastr.error("libraryDeposit Fees is required.");
                                                                                                                                        }
                                                                                                                            } else {
                                                                                                                                $('#disasMgmtFees').focus();
                                                                                                                                toastr.error("disasMgmt Fees is required.");
                                                                                                                                }
                                                                                                                    } else {
                                                                                                                        $('#proRataFees').focus();
                                                                                                                        toastr.error("proRata Fees is required.");
                                                                                                                        }
                                                                                                            } else {
                                                                                                                $('#registrationFees').focus();
                                                                                                                toastr.error("registration Fees is required.");
                                                                                                                }
                                                                                                    } else {
                                                                                                        $('#developmentFees').focus();
                                                                                                        toastr.error("development Fees is required.");
                                                                                                        }
                                                                                            } else {
                                                                                                $('#computerizationFees').focus();
                                                                                                toastr.error("computerization Fees is required.");
                                                                                                }
                                                                                    } else {
                                                                                        $('#studentWelfareFees').focus();
                                                                                        toastr.error("studentWelfare Fees is required.");
                                                                                        }
                                                                            } else {
                                                                                $('#admissionFees').focus();
                                                                                toastr.error("admission Fees is required.");
                                                                                }
                                                                    } else {
                                                                        $('#eligibilityFormFees').focus();
                                                                        toastr.error("eligibility Form Fees is required.");
                                                                        }
                                                            } else {
                                                                $('#eligibilityFees').focus();
                                                                toastr.error("eligibility Fees is required.");
                                                                }        
                                                    } else {
                                                        $('#gymkhanaFees').focus();
                                                        toastr.error("gymkhana Fees is required.");
                                                        }
                                            } else {
                                                $('#activitiesFees').focus();
                                                toastr.error("activities Fees is required.");
                                                }      
                                    } else {
                                        $('#libraryFees').focus();
                                        toastr.error("library Fees is required.");
                                        }
                            } else {
                                $('#tutionFees').focus();
                                toastr.error("tution Fees is required.");
                                }
                        } else {
                            $('#courseDivision').focus();
                            toastr.error("Please select course division is required.");
                            }
                    } else {
                        $('#courseFeesType').focus();
                        toastr.error("course Fees Type is required.");
                        }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course Year is required.");
                    }
            } else {
                $('#courseName').focus();
                toastr.error("course name is required.");
                }
        } else {
            $('#academicYear').focus();
            toastr.error("Academic Year is required.");
            }
        
    }


    $scope.getCoursesList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddCourseFess/getCourseFeesList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }


    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddCourseFess/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

   
    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'AddCourseFess/searchByCourses', {
            'csrf_token_name': csrfHash,
            coursesSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
    }


    $scope.deleteUserClick = function (courses) {
        $scope.updateCoursesId = courses.course_fee_id;
        var transform = function(data) {
            return $.param(data);
        } 
        $scope.disabledDeleteCoursesButton = true;
        $http.post(window.site_url + 'AddCourseFess/deleteCourseFees', {
            'csrf_token_name': csrfHash,
            updateCourseFeesId:courses.course_fee_id
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                toastr.success(response.data.status.message);
                $scope.updateCoursesId = "";
                $scope.init();
            }else {
                toastr.error(response.data.status.message);
            }
            $scope.disabledDeleteCoursesButton = false;
        }, function(response) {
            $scope.disabledDeleteCoursesButton = false;
        })
    }



    $scope.getSingleCourse = function (course) {
        $scope.course_fee_id = course.course_fee_id;
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddCourseFess/getSingleCourseFeesList', {
            'csrf_token_name': csrfHash,
            course_fee_id:course.course_fee_id
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.singleCourseArray = response.data.data;
                console.log(response.data.data);
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })

    }


    $scope.getFilterData = function(){
    
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddCourseFess/getFilterData', {
            'csrf_token_name': csrfHash,
            courseName : $scope.courseName,
            courseYear :$scope.courseYear, 
            academicYear : $scope.academicYear,
            courseFeesType :$scope.courseFeesType,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })              
    }


    $scope.clearfilter = function(){
        $scope.courseName ='';
        $scope.courseYear= ''; 
        $scope.academicYear ='';
        $scope.courseFeesType ="";
        $scope.getCoursesList();
    }


    $scope.uploadedFile = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 5){
                alert("file size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 5 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'xlsx' || file.name.split('.').pop() == 'xls') {
                    var reader = new FileReader();
                    $scope.file = element.files[0];   
                    reader.readAsDataURL(file);
                } else {
                    toastr.error('Please upload correct File , File extension should be (.xls|.xlsx)');
                    return false;
                }
            }
        });
    }

    $scope.AddNumbers = function() {
        $scope.sum =0;
        var tutionFees = Number($scope.tutionFees || 0);
        var libraryFees = Number($scope.libraryFees || 0);
        var activitiesFees = Number($scope.activitiesFees || 0);
        var gymkhanaFees = Number($scope.gymkhanaFees || 0);
        var eligibilityFees = Number($scope.eligibilityFees || 0);
        var eligibilityFormFees = Number($scope.eligibilityFormFees || 0); 
        var admissionFees  = Number($scope.admissionFees || 0);
        var studentWelfareFees = Number($scope.studentWelfareFees || 0); 
        var computerizationFees= Number($scope.computerizationFees || 0); 
        var developmentFees = Number($scope.developmentFees || 0); 
        var registrationFees  = Number($scope.registrationFees || 0); 
        var proRataFees = Number($scope.proRataFees || 0); 
        var disasMgmtFees   = Number($scope.disasMgmtFees || 0); 
        var libraryDeposit  = Number($scope.libraryDeposit || 0); 
        var cautionMoney  = Number($scope.cautionMoney || 0); 
        var medicalExam  = Number($scope.medicalExam || 0); 
        var practicalFees  = Number($scope.practicalFees || 0); 
        var laboratoryFees  = Number($scope.laboratoryFees || 0); 
        var sissFees  = Number($scope.sissFees || 0); 
        var personnelFees = Number($scope.personnelFees || 0); 
        var shortTermCourseFee  = Number($scope.shortTermCourseFee || 0); 
        var securityCharges  = Number($scope.securityCharges || 0); 
        var administrationCharges  = Number($scope.administrationCharges || 0); 
        var seminarFees = Number($scope.seminarFees || 0); 
        var phyEdnScheme  = Number($scope.phyEdnScheme || 0); 
        var alumniFees  = Number($scope.alumniFees || 0); 
        var collegeDevpFees  = Number($scope.collegeDevpFees || 0); 
        var termendExamFees = Number($scope.termendExamFees || 0); 
        var computerLabFees = Number($scope.computerLabFees || 0); 
        var workshopSeminarFees  = Number($scope.workshopSeminarFees || 0); 
        var compInternetFees  = Number($scope.compInternetFees || 0); 
        var corpusFund  = Number($scope.corpusFund || 0); 
        var formFees = Number($scope.formFees || 0); 
        var nssSectionFees  = Number($scope.nssSectionFees || 0); 
        var compLabMaintainanceFees = Number($scope.compLabMaintainanceFees || 0); 
        var addOnCoursesFees = Number($scope.addOnCoursesFees || 0);
        var addExtraFees = Number($scope.addExtraFees || 0);
        var addExtranewFees =  Number($scope.addExtranewFees || 0);

        $scope.sum = tutionFees + libraryFees  + activitiesFees + gymkhanaFees + eligibilityFees + eligibilityFormFees + admissionFees + studentWelfareFees + computerizationFees + developmentFees + registrationFees + proRataFees + disasMgmtFees + libraryDeposit + cautionMoney + medicalExam + practicalFees + laboratoryFees + sissFees + personnelFees + shortTermCourseFee + securityCharges + administrationCharges + seminarFees + phyEdnScheme + alumniFees + collegeDevpFees + termendExamFees + computerLabFees + workshopSeminarFees +  compInternetFees + corpusFund + formFees + nssSectionFees + compLabMaintainanceFees + addOnCoursesFees+ addExtraFees + addExtranewFees;
    }

    $scope.editUserClick = function (courses) {
        console.log(courses);
        $scope.addCourseFeesDiv = true;
        $scope.addCourseFeesButton = false;
        $scope.updateCourseFeesButton = true;
        
        $scope.updateCoursesId =courses.course_fee_id;
       
        $scope.academicYear = courses.academic_year ;
        $scope.courseName = courses.course_name ;
        $scope.courseYear = courses.course_year ;
        $scope.courseFeesType = courses.fees_type;
        $scope.courseFeesSubType = courses.fees_sub_type;
        $scope.courseDivision = courses.course_division;
        $scope.tutionFees = courses.tution_fees;
        $scope.libraryFees = courses.library_fees;
        $scope.activitiesFees = courses.activity_fees;
        $scope.gymkhanaFees = courses.gymkhana_fees;
        $scope.eligibilityFees = courses.eligibility_fees;
        $scope.eligibilityFormFees = courses.eligibility_form_fees;
        $scope.admissionFees = courses.admission_fees;
        $scope.studentWelfareFees  =courses.welfare_fees;
        $scope.computerizationFees = courses.computarization_fees;
        $scope.developmentFees = courses.developement_fees;
        $scope.registrationFees = courses.registration_fees;
        $scope.proRataFees = courses.pro_rata_fees;
        $scope.disasMgmtFees = courses.disas_mgmt_fees;
        $scope.libraryDeposit  = courses.library_deposite_fees;
        $scope.cautionMoney  = courses.caution_money_fees;
        $scope.medicalExam  = courses.medical_exam_fees;
        $scope.practicalFees  = courses.practical_fees;
        $scope.laboratoryFees  = courses.laboratory_fees;
        $scope.sissFees  = courses.siss_fees;
        $scope.personnelFees  = courses.personal_fees;
        $scope.shortTermCourseFee = courses.short_term_fees;
        $scope.securityCharges = courses.security_charges;
        $scope.administrationCharges = courses.adminitration_charges;
        $scope.seminarFees  = courses.seminar_fees;
        $scope.phyEdnScheme  = courses.phy_edn_scheme;
        $scope.alumniFees  = courses.alumni_fees;
        $scope.collegeDevpFees  = courses.college_dev_fees;
        $scope.termendExamFees  = courses.termend_exam_fees;
        $scope.computerLabFees  = courses.computer_lab_fees;
        $scope.workshopSeminarFees = courses.workshop_fees;
        $scope.compInternetFees  = courses.comp_internet_fees;
        $scope.corpusFund  = courses.corpus_fund;
        $scope.formFees  = courses.form_fees;
        $scope.nssSectionFees  = courses.nss_section_fees;
        $scope.compLabMaintainanceFees = courses.comp_lab_maintenace_fee;
        $scope.addOnCoursesFees = courses.add_on_course_fee;
        $scope.addExtraFees = courses.add_extra_fee;
        if($scope.addExtraFees != ''){
            $scope.addMoreDiv1 = true;
        }
        $scope.addExtranewFees = courses.add_extra_new_fee;
        if($scope.addExtranewFees != ''){
            $scope.addMoreDiv2 = true;
        }
        $scope.sum = courses.total_fees;
        $('html, body').animate({scrollTop: '0px'}, 1000);
    }

    $scope.updateCourseFees = function(){
       
        if ($scope.academicYear) {
            if ($scope.courseName) {
                if ($scope.courseYear) {
                    if ($scope.courseFeesType) {
                        if($scope.courseDivision){
                            if ($scope.tutionFees) {
                                if(!( $scope.tutionFees.match(NumberPattern))){
                                    $('#tutionFees').focus();
                                    toastr.error("Please enter valid tution Fees.");
                                    return false;   
                                    }
                                    if ($scope.libraryFees) {
                                        if(!( $scope.libraryFees.match(NumberPattern))){
                                            $('#libraryFees').focus();
                                            toastr.error("Please enter valid library Fees.");
                                            return false;   
                                            }
                                            if ($scope.activitiesFees) {
                                                if(!( $scope.activitiesFees.match(NumberPattern))){
                                                    $('#activitiesFees').focus();
                                                    toastr.error("Please enter valid activities Fees.");
                                                    return false;   
                                                    }
                                                    if ($scope.gymkhanaFees) {
                                                        if(!( $scope.gymkhanaFees.match(NumberPattern))){
                                                            $('#gymkhanaFees').focus();
                                                            toastr.error("Please enter valid gymkhana Fees.");
                                                            return false;   
                                                            }
                                                            if ($scope.eligibilityFees) {
                                                                if(!( $scope.eligibilityFees.match(NumberPattern))){
                                                                    $('#eligibilityFees').focus();
                                                                    toastr.error("Please enter valid eligibility Fees.");
                                                                    return false;   
                                                                    }
                                                                    if ($scope.eligibilityFormFees) {
                                                                        if(!( $scope.eligibilityFormFees.match(NumberPattern))){
                                                                            $('#eligibilityFormFees').focus();
                                                                            toastr.error("Please enter valid eligibility Form Fees.");
                                                                            return false;   
                                                                            }
                                                                            if ($scope.admissionFees) {
                                                                                if(!( $scope.admissionFees.match(NumberPattern))){
                                                                                    $('#admissionFees').focus();
                                                                                    toastr.error("Please enter valid admission Fees.");
                                                                                    return false;   
                                                                                    }
                                                                                    if ($scope.studentWelfareFees) {
                                                                                        if(!( $scope.studentWelfareFees.match(NumberPattern))){
                                                                                            $('#studentWelfareFees').focus();
                                                                                            toastr.error("Please enter valid student Welfare Fees.");
                                                                                            return false;   
                                                                                            }
                                                                                            if ($scope.computerizationFees) {
                                                                                                if(!( $scope.computerizationFees.match(NumberPattern))){
                                                                                                    $('#computerizationFees').focus();
                                                                                                    toastr.error("Please enter valid computerization Fees.");
                                                                                                    return false;   
                                                                                                    }
                                                                                                    if ($scope.developmentFees) {
                                                                                                        if(!( $scope.developmentFees.match(NumberPattern))){
                                                                                                            $('#developmentFees').focus();
                                                                                                            toastr.error("Please enter valid development Fees.");
                                                                                                            return false;   
                                                                                                            }
                                                                                                            if ($scope.registrationFees) {
                                                                                                                if(!( $scope.registrationFees.match(NumberPattern))){
                                                                                                                    $('#registrationFees').focus();
                                                                                                                    toastr.error("Please enter valid registration Fees.");
                                                                                                                    return false;   
                                                                                                                    }
                                                                                                                    if ($scope.proRataFees) {
                                                                                                                        if(!( $scope.proRataFees.match(NumberPattern))){
                                                                                                                            $('#proRataFees').focus();
                                                                                                                            toastr.error("Please enter valid proRata Fees.");
                                                                                                                            return false;   
                                                                                                                            }
                                                                                                                            if ($scope.disasMgmtFees) {
                                                                                                                                if(!( $scope.disasMgmtFees.match(NumberPattern))){
                                                                                                                                    $('#disasMgmtFees').focus();
                                                                                                                                    toastr.error("Please enter valid disasMgmt Fees.");
                                                                                                                                    return false;   
                                                                                                                                    }
                                                                                                                                    if ($scope.libraryDeposit) {
                                                                                                                                        if(!( $scope.libraryDeposit.match(NumberPattern))){
                                                                                                                                            $('#libraryDeposit').focus();
                                                                                                                                            toastr.error("Please enter valid library Deposit.");
                                                                                                                                            return false;   
                                                                                                                                            }
                                                                                                                                            if ($scope.cautionMoney) {
                                                                                                                                                if(!( $scope.cautionMoney.match(NumberPattern))){
                                                                                                                                                    $('#cautionMoney').focus();
                                                                                                                                                    toastr.error("Please enter valid caution Money.");
                                                                                                                                                    return false;   
                                                                                                                                                    }
                                                                                                                                                    if ($scope.medicalExam) {
                                                                                                                                                        if(!( $scope.medicalExam.match(NumberPattern))){
                                                                                                                                                            $('#medicalExam').focus();
                                                                                                                                                            toastr.error("Please enter valid medical Exam.");
                                                                                                                                                            return false;   
                                                                                                                                                            }
                                                                                                                                                            if ($scope.practicalFees) {
                                                                                                                                                                if(!( $scope.practicalFees.match(NumberPattern))){
                                                                                                                                                                    $('#practicalFees').focus();
                                                                                                                                                                    toastr.error("Please enter valid practical Fees.");
                                                                                                                                                                    return false;   
                                                                                                                                                                    }
                                                                                                                                                                    if ($scope.laboratoryFees) {
                                                                                                                                                                        if(!( $scope.laboratoryFees.match(NumberPattern))){
                                                                                                                                                                            $('#laboratoryFees').focus();
                                                                                                                                                                            toastr.error("Please enter valid laboratory Fees.");
                                                                                                                                                                            return false;   
                                                                                                                                                                            }
                                                                                                                                                                            if ($scope.sissFees) {
                                                                                                                                                                                if(!( $scope.sissFees.match(NumberPattern))){
                                                                                                                                                                                    $('#sissFees').focus();
                                                                                                                                                                                    toastr.error("Please enter valid siss Fees.");
                                                                                                                                                                                    return false;   
                                                                                                                                                                                    }
                                                                                                                                                                                    if ($scope.personnelFees) {
                                                                                                                                                                                        if(!( $scope.personnelFees.match(NumberPattern))){
                                                                                                                                                                                            $('#personnelFees').focus();
                                                                                                                                                                                            toastr.error("Please enter valid personnel Fees.");
                                                                                                                                                                                            return false;   
                                                                                                                                                                                            }
                                                                                                                                                                                            if ($scope.shortTermCourseFee) {
                                                                                                                                                                                                if(!( $scope.shortTermCourseFee.match(NumberPattern))){
                                                                                                                                                                                                    $('#shortTermCourseFee').focus();
                                                                                                                                                                                                    toastr.error("Please enter valid short Term Course Fee.");
                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                    }
                                                                                                                                                                                                    if ($scope.securityCharges) {
                                                                                                                                                                                                        if(!($scope.securityCharges.match(NumberPattern))){
                                                                                                                                                                                                            $('#securityCharges').focus();
                                                                                                                                                                                                            toastr.error("Please enter valid security Charges.");
                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                            }
                                                                                                                                                                                                            if ($scope.administrationCharges) {
                                                                                                                                                                                                                if(!( $scope.administrationCharges.match(NumberPattern))){
                                                                                                                                                                                                                    $('#administrationCharges').focus();
                                                                                                                                                                                                                    toastr.error("Please enter valid administration Charges.");
                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                    }
                                                                                                                                                                                                                    if ($scope.seminarFees) {
                                                                                                                                                                                                                        if(!( $scope.seminarFees.match(NumberPattern))){
                                                                                                                                                                                                                            $('#seminarFees').focus();
                                                                                                                                                                                                                            toastr.error("Please enter valid seminar Fees.");
                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                            }
                                                                                                                                                                                                                            if ($scope.phyEdnScheme) {
                                                                                                                                                                                                                                if(!($scope.phyEdnScheme.match(NumberPattern))){
                                                                                                                                                                                                                                    $('#phyEdnScheme').focus();
                                                                                                                                                                                                                                    toastr.error("Please enter valid phyEdnScheme.");
                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                    if ($scope.alumniFees) {
                                                                                                                                                                                                                                        if(!($scope.alumniFees.match(NumberPattern))){
                                                                                                                                                                                                                                            $('#alumniFees').focus();
                                                                                                                                                                                                                                            toastr.error("Please enter valid alumni Fees.");
                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                            if ($scope.collegeDevpFees) {
                                                                                                                                                                                                                                                if(!( $scope.collegeDevpFees.match(NumberPattern))){
                                                                                                                                                                                                                                                    $('#collegeDevpFees').focus();
                                                                                                                                                                                                                                                    toastr.error("Please enter valid collegeDevpFees.");
                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                    if ($scope.termendExamFees) {
                                                                                                                                                                                                                                                        if(!( $scope.termendExamFees.match(NumberPattern))){
                                                                                                                                                                                                                                                            $('#termendExamFees').focus();
                                                                                                                                                                                                                                                            toastr.error("Please enter valid termendExamFees.");
                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                            if ($scope.computerLabFees) {
                                                                                                                                                                                                                                                                if(!( $scope.computerLabFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                    $('#computerLabFees').focus();
                                                                                                                                                                                                                                                                    toastr.error("Please enter valid computer Lab Fees.");
                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                    if ($scope.workshopSeminarFees) {
                                                                                                                                                                                                                                                                        if(!( $scope.workshopSeminarFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                            $('#workshopSeminarFees').focus();
                                                                                                                                                                                                                                                                            toastr.error("Please enter valid workshop Seminar Fees.");
                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                            if ($scope.compInternetFees) {
                                                                                                                                                                                                                                                                                if(!( $scope.compInternetFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                    $('#compInternetFees').focus();
                                                                                                                                                                                                                                                                                    toastr.error("Please enter valid computer Internet Fees.");
                                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                    if ($scope.corpusFund) {
                                                                                                                                                                                                                                                                                        if(!( $scope.corpusFund.match(NumberPattern))){
                                                                                                                                                                                                                                                                                            $('#corpusFund').focus();
                                                                                                                                                                                                                                                                                            toastr.error("Please enter valid corpusFund.");
                                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                            if ($scope.formFees) {
                                                                                                                                                                                                                                                                                                if(!( $scope.formFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                    $('#formFees').focus();
                                                                                                                                                                                                                                                                                                    toastr.error("Please enter valid form Fees.");
                                                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                    if ($scope.nssSectionFees) {
                                                                                                                                                                                                                                                                                                        if(!( $scope.nssSectionFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                            $('#nssSectionFees').focus();
                                                                                                                                                                                                                                                                                                            toastr.error("Please enter valid nssSectionFees.");
                                                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                                                            }
                                                                                                                                                                                                                                                                                                            if ($scope.compLabMaintainanceFees) {
                                                                                                                                                                                                                                                                                                                if(!( $scope.compLabMaintainanceFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                                    $('#compLabMaintainanceFees').focus();
                                                                                                                                                                                                                                                                                                                    toastr.error("Please enter valid number.");
                                                                                                                                                                                                                                                                                                                    return false;   
                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                    if($scope.addOnCoursesFees){

                                                                                                                                                                                                                                                                                                                        if(!( $scope.addOnCoursesFees.match(NumberPattern))){
                                                                                                                                                                                                                                                                                                                            $('#addOnCoursesFees').focus();
                                                                                                                                                                                                                                                                                                                            toastr.error("Please enter valid number.");
                                                                                                                                                                                                                                                                                                                            return false;   
                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                    var transform = function(data) {
                                                                                                                                                                                                                                                                                                                        return $.param(data);
                                                                                                                                                                                                                                                                                                                    } 
                                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                    $http.post(window.site_url + 'AddCourseFess/updateCourseFee', {
                                                                                                                                                                                                                                                                                                                        'csrf_token_name': csrfHash,
                                                                                                                                                                                                                                                                                                                        updateCourseFeesId: $scope.updateCoursesId,
                                                                                                                                                                                                                                                                                                                        academicYear: $scope.academicYear,
                                                                                                                                                                                                                                                                                                                        courseName :$scope.courseName,
                                                                                                                                                                                                                                                                                                                        courseYear:$scope.courseYear,
                                                                                                                                                                                                                                                                                                                        courseFeesType: $scope.courseFeesType,
                                                                                                                                                                                                                                                                                                                        courseFeesSubType : $scope.courseFeesSubType, 
                                                                                                                                                                                                                                                                                                                        courseDivision : $scope.courseDivision ,
                                                                                                                                                                                                                                                                                                                        tutionFees :  $scope.tutionFees,
                                                                                                                                                                                                                                                                                                                        libraryFees : $scope.libraryFees,
                                                                                                                                                                                                                                                                                                                        activitiesFees :  $scope.activitiesFees,
                                                                                                                                                                                                                                                                                                                        gymkhanaFees : $scope.gymkhanaFees,
                                                                                                                                                                                                                                                                                                                        eligibilityFees :  $scope.eligibilityFees ,
                                                                                                                                                                                                                                                                                                                        eligibilityFormFees : $scope.eligibilityFormFees ,
                                                                                                                                                                                                                                                                                                                        admissionFees : $scope.admissionFees,
                                                                                                                                                                                                                                                                                                                        studentWelfareFees : $scope.studentWelfareFees ,
                                                                                                                                                                                                                                                                                                                        computerizationFees :  $scope.computerizationFees,
                                                                                                                                                                                                                                                                                                                        developmentFees: $scope.developmentFees,
                                                                                                                                                                                                                                                                                                                        registrationFees : $scope.registrationFees,
                                                                                                                                                                                                                                                                                                                        proRataFees: $scope.proRataFees,
                                                                                                                                                                                                                                                                                                                        disasMgmtFees: $scope.disasMgmtFees,
                                                                                                                                                                                                                                                                                                                        libraryDeposit: $scope.libraryDeposit ,
                                                                                                                                                                                                                                                                                                                        cautionMoney :  $scope.cautionMoney ,
                                                                                                                                                                                                                                                                                                                        medicalExam: $scope.medicalExam ,
                                                                                                                                                                                                                                                                                                                        practicalFees: $scope.practicalFees ,
                                                                                                                                                                                                                                                                                                                        laboratoryFees: $scope.laboratoryFees ,
                                                                                                                                                                                                                                                                                                                        sissFees:$scope.sissFees ,
                                                                                                                                                                                                                                                                                                                        personnelFees: $scope.personnelFees ,
                                                                                                                                                                                                                                                                                                                        shortTermCourseFee: $scope.shortTermCourseFee,
                                                                                                                                                                                                                                                                                                                        securityCharges: $scope.securityCharges,
                                                                                                                                                                                                                                                                                                                        administrationCharges: $scope.administrationCharges,
                                                                                                                                                                                                                                                                                                                        seminarFees: $scope.seminarFees ,
                                                                                                                                                                                                                                                                                                                        phyEdnScheme: $scope.phyEdnScheme ,
                                                                                                                                                                                                                                                                                                                        alumniFees:  $scope.alumniFees ,
                                                                                                                                                                                                                                                                                                                        collegeDevpFees: $scope.collegeDevpFees ,
                                                                                                                                                                                                                                                                                                                        termendExamFees: $scope.termendExamFees ,
                                                                                                                                                                                                                                                                                                                        computerLabFees: $scope.computerLabFees ,
                                                                                                                                                                                                                                                                                                                        workshopSeminarFees: $scope.workshopSeminarFees,
                                                                                                                                                                                                                                                                                                                        compInternetFees: $scope.compInternetFees ,
                                                                                                                                                                                                                                                                                                                        corpusFund: $scope.corpusFund ,
                                                                                                                                                                                                                                                                                                                        formFees: $scope.formFees ,
                                                                                                                                                                                                                                                                                                                        nssSectionFees: $scope.nssSectionFees ,
                                                                                                                                                                                                                                                                                                                        compLabMaintainanceFees:  $scope.compLabMaintainanceFees,
                                                                                                                                                                                                                                                                                                                        addOnCoursesFees : $scope.addOnCoursesFees,
                                                                                                                                                                                                                                                                                                                        addExtraFees: $scope.addExtraFees,
                                                                                                                                                                                                                                                                                                                        addExtranewFees : $scope.addExtranewFees,
                                                                                                                                                                                                                                                                                                                        totalFees : $scope.sum,
                                                                                                                                                                                                                                                                                                                        
                                                                                                                                                                                                                                                                                                                    }, {
                                                                                                                                                                                                                                                                                                                        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                                                                                                                                                                                                                                                                                                        transformRequest: transform
                                                                                                                                                                                                                                                                                                                    })
                                                                                                                                                                                                                                                                                                                    .then(function(response) {
                                                                                                                                                                                                                                                                                                                        if (response.data.status.status == "1") {
                                                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                            toastr.success(response.data.status.message);
                                                                                                                                                                                                                                                                                                                            $scope.academicYear = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseName = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseYear = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseFeesType = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseFeesSubType = "";
                                                                                                                                                                                                                                                                                                                            $scope.courseDivision = "";
                                                                                                                                                                                                                                                                                                                            $scope.tutionFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.libraryFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.activitiesFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.gymkhanaFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.eligibilityFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.eligibilityFormFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.admissionFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.studentWelfareFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.computerizationFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.developmentFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.registrationFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.proRataFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.disasMgmtFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.libraryDeposit  = "";
                                                                                                                                                                                                                                                                                                                            $scope.cautionMoney  = "";
                                                                                                                                                                                                                                                                                                                            $scope.medicalExam  = "";
                                                                                                                                                                                                                                                                                                                            $scope.practicalFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.laboratoryFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.sissFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.personnelFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.shortTermCourseFee = "";
                                                                                                                                                                                                                                                                                                                            $scope.securityCharges = "";
                                                                                                                                                                                                                                                                                                                            $scope.administrationCharges = "";
                                                                                                                                                                                                                                                                                                                            $scope.seminarFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.phyEdnScheme  = "";
                                                                                                                                                                                                                                                                                                                            $scope.alumniFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.collegeDevpFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.termendExamFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.computerLabFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.workshopSeminarFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.compInternetFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.corpusFund  = "";
                                                                                                                                                                                                                                                                                                                            $scope.formFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.nssSectionFees  = "";
                                                                                                                                                                                                                                                                                                                            $scope.compLabMaintainanceFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.addOnCoursesFees = "";
                                                                                                                                                                                                                                                                                                                            $scope.addExtraFees ="";
                                                                                                                                                                                                                                                                                                                            $scope.addExtranewFees ="";
                                                                                                                                                                                                                                                                                                                            $scope.sum = "";
                                                                                                                                                                                                                                                                                                                        $scope.init();
                                                                                                                                                                                                                                                                                                                        
                                                                                                                                                                                                                                                                                                                        }else{
                                                                                                                                                                                                                                                                                                                            toastr.error(response.data.status.message);
                                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                                                        $scope.disabledCreateCourseFeesButton = false;
                                                                                                                                                                                                                                                                                                                        $("#loadingDiv").css('display','none');
                                                                                                                                                                                                                                                                                                                        $scope.init();
                                                                                                                                                                                                                                                                                                                    }, function(response) {
                                                                                                                                                                                                                                                                                                                        $scope.disabledCreateCourseFeesButton = false;
                                                                                                                                                                                                                                                                                                                        $("#loadingDiv").css('display','none');
                                                                                                                                                                                                                                                                                                                        
                                                                                                                                                                                                                                                                                                                    })
                                                                                                                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                                                                                                                    $('#addOnCoursesFees').focus();
                                                                                                                                                                                                                                                                                                                    toastr.error("Add on course fees is required.");
                                                                                                                                                                                                                                                                                                                    }
                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                                $('#compLabMaintainanceFees').focus();
                                                                                                                                                                                                                                                                                                                toastr.error("Computer Lab Maintainance Fees is required.");
                                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                                                                        $('#nssSectionFees').focus();
                                                                                                                                                                                                                                                                                                        toastr.error("NSS Section Fees is required.");
                                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                                $('#formFees').focus();
                                                                                                                                                                                                                                                                                                toastr.error("Form Fees is required.");
                                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                                                        $('#corpusFund').focus();
                                                                                                                                                                                                                                                                                        toastr.error("Corpus Fund is required.");
                                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                                $('#compInternetFees').focus();
                                                                                                                                                                                                                                                                                toastr.error("Computer Internet Fees is required.");
                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                                        $('#workshopSeminarFees').focus();
                                                                                                                                                                                                                                                                        toastr.error("Workshop Seminar Fees is required.");
                                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                                $('#computerLabFees').focus();
                                                                                                                                                                                                                                                                toastr.error("Computer Lab Fees is required.");
                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                                        $('#termendExamFees').focus();
                                                                                                                                                                                                                                                        toastr.error("Termend Exam Fees is required.");
                                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                                $('#collegeDevpFees').focus();
                                                                                                                                                                                                                                                toastr.error("College Devlopment  Fees is required.");
                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                                        $('#alumniFees').focus();
                                                                                                                                                                                                                                        toastr.error("Alumni Fees is required.");
                                                                                                                                                                                                                                        }
                                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                                $('#phyEdnScheme').focus();
                                                                                                                                                                                                                                toastr.error("Phy. Edn. Scheme is required.");
                                                                                                                                                                                                                                }
                                                                                                                                                                                                                    } else {
                                                                                                                                                                                                                        $('#seminarFees').focus();
                                                                                                                                                                                                                        toastr.error("Seminar Fees is required.");
                                                                                                                                                                                                                        }
                                                                                                                                                                                                                        
                                                                                                                                                                                                            } else {
                                                                                                                                                                                                                $('#administrationCharges').focus();
                                                                                                                                                                                                                toastr.error("Administration Charges is required.");
                                                                                                                                                                                                                }
                                                                                                                                                                                                    } else {
                                                                                                                                                                                                        $('#securityCharges').focus();
                                                                                                                                                                                                        toastr.error("security Charges is required.");
                                                                                                                                                                                                        }
                                                                                                                                                                                            } else {
                                                                                                                                                                                                $('#shortTermCourseFee').focus();
                                                                                                                                                                                                toastr.error("short Term Course Fee is required.");
                                                                                                                                                                                                }
                                                                                                                                                                                    } else {
                                                                                                                                                                                        $('#personnelFees').focus();
                                                                                                                                                                                        toastr.error("personnel Fee is required.");
                                                                                                                                                                                        }
                                                                                                                                                                            } else {
                                                                                                                                                                                $('#sissFees').focus();
                                                                                                                                                                                toastr.error("siss Fee is required.");
                                                                                                                                                                                }
                                                                                                                                                                    } else {
                                                                                                                                                                        $('#laboratoryFees').focus();
                                                                                                                                                                        toastr.error("laboratory Fee is required.");
                                                                                                                                                                        }
                                                                                                                                                            } else {
                                                                                                                                                                $('#practicalFees').focus();
                                                                                                                                                                toastr.error("practical Fee is required.");
                                                                                                                                                                }
                                                                                                                                                    } else {
                                                                                                                                                        $('#medicalExam').focus();
                                                                                                                                                        toastr.error("medicalExam Fee is required.");
                                                                                                                                                        }
                                                                                                                                            } else {
                                                                                                                                                $('#cautionMoney').focus();
                                                                                                                                                toastr.error("cautionMoney is required.");
                                                                                                                                                }
                                                                                                                                    } else {
                                                                                                                                        $('#libraryDeposit').focus();
                                                                                                                                        toastr.error("libraryDeposit Fees is required.");
                                                                                                                                        }
                                                                                                                            } else {
                                                                                                                                $('#disasMgmtFees').focus();
                                                                                                                                toastr.error("disasMgmt Fees is required.");
                                                                                                                                }
                                                                                                                    } else {
                                                                                                                        $('#proRataFees').focus();
                                                                                                                        toastr.error("proRata Fees is required.");
                                                                                                                        }
                                                                                                            } else {
                                                                                                                $('#registrationFees').focus();
                                                                                                                toastr.error("registration Fees is required.");
                                                                                                                }
                                                                                                    } else {
                                                                                                        $('#developmentFees').focus();
                                                                                                        toastr.error("development Fees is required.");
                                                                                                        }
                                                                                            } else {
                                                                                                $('#computerizationFees').focus();
                                                                                                toastr.error("computerization Fees is required.");
                                                                                                }
                                                                                    } else {
                                                                                        $('#studentWelfareFees').focus();
                                                                                        toastr.error("studentWelfare Fees is required.");
                                                                                        }
                                                                            } else {
                                                                                $('#admissionFees').focus();
                                                                                toastr.error("admission Fees is required.");
                                                                                }
                                                                    } else {
                                                                        $('#eligibilityFormFees').focus();
                                                                        toastr.error("eligibility Form Fees is required.");
                                                                        }
                                                            } else {
                                                                $('#eligibilityFees').focus();
                                                                toastr.error("eligibility Fees is required.");
                                                                }        
                                                    } else {
                                                        $('#gymkhanaFees').focus();
                                                        toastr.error("gymkhana Fees is required.");
                                                        }
                                            } else {
                                                $('#activitiesFees').focus();
                                                toastr.error("activities Fees is required.");
                                                }      
                                    } else {
                                        $('#libraryFees').focus();
                                        toastr.error("library Fees is required.");
                                        }
                            } else {
                                $('#tutionFees').focus();
                                toastr.error("tution Fees is required.");
                                }
                        } else {
                            $('#courseDivision').focus();
                            toastr.error("Please select course division is required.");
                            }
                    } else {
                        $('#courseFeesType').focus();
                        toastr.error("course Fees Type is required.");
                        }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course Year is required.");
                    }
            } else {
                $('#courseName').focus();
                toastr.error("course name is required.");
                }
        } else {
            $('#academicYear').focus();
            toastr.error("Academic Year is required.");
            }

    }

    $scope.getAllDataForExcel = function () {
        $scope.disabledDownloadAllStudentButton = true;
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AddCourseFess/getAllDataForExcel', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.exportData = response.data.data;
                console.log("exportData",$scope.exportData);
                $scope.disabledDownloadAllCourseFeesButton = false;
            }else {
                toastr.error(response.data.status.message);
                $scope.disabledDownloadAllCourseFeesButton = false;
             }
        }, function(response) {
            $scope.disabledDownloadAllCourseFeesButton = false;
        })
    }

});


