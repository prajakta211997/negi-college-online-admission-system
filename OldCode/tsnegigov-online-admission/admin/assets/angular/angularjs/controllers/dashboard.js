var csrfHash = $('#csrfHash').val();


app.controller('dashboardCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }
        else{
            $scope.dataArray =[];
            $scope.getCommonData();
        }
    }

    $scope.getCommonData = function(){

        var transform = function(data) {
            return $.param(data);
        } 
            $http.post(window.site_url + 'AdminDashboard/getCourseWiseAnalysis', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
            .then(function(response) {
                console.log("Yes======= ",response);
                if (response.data.status.status == "1") {
                    $scope.dataArray = response.data.data[0];
                 //console.log($scope.dataArray);
                    $incrVal = 1;
                    angular.forEach(response.data.data[0], function (value, key) {
                        var totalSeats = parseInt(value['Total Seat']);
                        var confirmSeats = value['Confirme Seats'];
                        console.log(totalSeats );
                        console.log(confirmSeats);
                    $(document).ready(function(){
                       
                        // Highcharts.chart("dynamicGraphY"+$incrVal, {
                            
                        //         chart: {
                        //             plotBackgroundColor: null,
                        //             plotBorderWidth: null,
                        //             plotShadow: false,
                        //             type: 'pie'
                        //         },
                        //         title: {
                        //             text: key
                        //         },
                        //         tooltip: {
                        //             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                        //         },
                        //         plotOptions: {
                        //             pie: {
                        //                 allowPointSelect: true,
                        //                 cursor: 'pointer',
                        //                 dataLabels: {
                        //                     enabled: true,
                        //                     format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        //                     style: {
                        //                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        //                     }
                        //                 }
                        //             }
                        //         },
                        //         series: [{
                        //             name: key,
                        //             colorByPoint: true,
                        //             data: [{
                        //                 name: 'Confirme Seats',
                        //                 y: value['Confirme Seats']
                        //             }, {
                        //                 name: 'Total Seat',
                        //                 y:parseInt(value['Total Seat'])
                        //             }]
                        //         }]
                        //     });

                        // Highcharts.chart("dynamicGraphY"+$incrVal, {
                        //     chart: {
                        //         type: 'pie',
                        //         options3d: {
                        //             enabled: true,
                        //             alpha: 45
                        //         }
                        //     },
                        //     title: {
                        //         text: key
                        //     },
                            
                        //     plotOptions: {
                        //         pie: {
                        //             innerSize: 100,
                        //             depth: 45
                        //         }
                        //     },
                        //     series: [{
                        //         name: key,
                        //         data: [
                        //             ['Confirme Seats', value['Confirme Seats']],
                        //             ['Total Seat', parseInt(value['Total Seat'])]
                                    
                        //         ]
                        //     }]
                        // });
                        Highcharts.chart("dynamicGraphY"+$incrVal, {
                            chart: {
                                type: 'bar'
                            },
                            title: {
                                text: key,
                            },
                            
                            xAxis: {
                                categories: ['Seats'],
                                title: {
                                    text: null
                                }
                            },
                            yAxis: {
                                min: 0,
                                title: {
                                    text: 'Number of seats',
                                   
                                },
                                labels: {
                                    overflow: 'justify'
                                }
                            },
                            
                            plotOptions: {
                                bar: {
                                    dataLabels: {
                                        enabled: true
                                    }
                                }
                            },
                            legend: {
                                layout: 'vertical',
                                align: 'right',
                                verticalAlign: 'top',
                                x: -40,
                                y: 40,
                                floating: true,
                                borderWidth: 1,
                                backgroundColor:
                                    Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                                shadow: true
                            },
                            credits: {
                                enabled: false
                            },
                        
                                series: [{
                                    name: 'Total Seat',
                                    data: [totalSeats]
                                }, {
                                    name: 'Confirm Seats',
                                    data: [confirmSeats]
                                }]
                                
                        });
                            $incrVal = $incrVal+1;
                        });
                       
                    });
                
                }else {
                    toastr.error(response.data.status.message);
                }
                })
            }


    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    }
});