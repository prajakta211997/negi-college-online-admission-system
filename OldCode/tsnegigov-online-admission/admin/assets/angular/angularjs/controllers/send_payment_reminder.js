
var csrfHash = $('#csrfHash').val();
app.controller('managePaymentReminderServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.addPaymentReminderDiv = false;
            $scope.addPaymentReminderButton = true;
            $scope.updatePaymentReminderButton = false;
            $scope.courseNameArray =[];
            
            $scope.getAllCoursesName();
            
            $scope.courseName = '';
            $scope.courseYear = '' ;
            $scope.academicYear ='';
            
            $scope.disabledCreatePaymentReminderButton = false;
            $scope.disabledupdatePaymentReminderButton = false;
            $scope.disabledDeletePaymentReminderButton = false;
            $scope.disabledResetUserPasswordButton = false;
        }
         
    }
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    } 
    $scope.showaddPaymentReminderDiv = function () {
        if($scope.addPaymentReminderDiv == true){
          $scope.addPaymentReminderDiv = false;
          $scope.updatePaymentReminderButton = false;
          $scope.addPaymentReminderButton = true;
        }else{
          $scope.addPaymentReminderDiv = true;
          $scope.updatePaymentReminderButton = false;
          $scope.addPaymentReminderButton = true;
        }  
        $scope.PaymentReminderName = '';
        $scope.courseName = '';
        $scope.courseYear = '' ;
        $scope.academicYear ='';
       
     }
 
     var year = 2015;
    var range = [];
    var range1 = [];
   
    for (var i = 2015; i <= new Date().getFullYear()  ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;
     
   
    
   
    $scope.sendPaymentReminder = function () {
        
        if($scope.academicYear){
            if ($scope.courseName) {
                if($scope.courseYear){
                   
                
                        var transform = function(data) {
                            return $.param(data);
                        } 

                        $http.post(window.site_url + 'ManagePaymentReminder/sendPaymentReminder', {
                            'csrf_token_name': csrfHash,
                            courseName : $scope.courseName,
                            courseYear: $scope.courseYear,
                            academicYear :  $scope.academicYear,
                            
                            
                        }, {
                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                            transformRequest: transform
                        })
                        .then(function(response) {
                            if (response.data.status.status == "1") {

                                toastr.success(response.data.status.message);
                                $scope.courseName = '';
                                $scope.courseYear = '' ;
                                $scope.academicYear ='';
                                
                                $scope.init();
                            
                            }else{
                                toastr.error(response.data.status.message);
                            }
                            $scope.disabledCreatePaymentReminderButton = false;
                            $("#loadingDiv").css('display','none');
                        }, function(response) {
                            $scope.disabledCreatePaymentReminderButton = false;
                                    $("#loadingDiv").css('display','none');
                        })
                    
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course year is required.");
                }
            } else {
                $('#courseName').focus();
                toastr.error("Coursename is required.");
            }
                                
        } else {
            $('#academicYear').focus();
            toastr.error("Academic Year is required.");
        }
    }

    

    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManagePaymentReminder/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

   






});



