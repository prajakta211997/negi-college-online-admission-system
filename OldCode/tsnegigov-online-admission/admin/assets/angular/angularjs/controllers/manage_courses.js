
var csrfHash = $('#csrfHash').val();
$("#startDate")
  .datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: new Date()
  })
  .val();

  $("#endDate")
  .datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: new Date()
  })
  .val();
 

app.controller('manageCoureseServicesCtrl', function ($scope, $http, toastr) {
    
    $scope.currentPage = 1;
    $scope.perPage = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
      $scope.currentPage = pageNo;
    };

    $scope.init = function () {
        if(localStorage.getItem("AdminLoggedIn") == "False" || localStorage.getItem("AdminLoggedIn") == null){
            window.location = "index.html";
        }else{
            $scope.addCoursesDiv = false;
            $scope.addCoursesButton = true;
            $scope.updateCoursesButton = false;
            $scope.coursesListArray = [];
            $scope.singleCourseArray = [];
            $scope.courseNameArray =[];
            $scope.getCoursesList();
            $scope.getAllCoursesName();
            $scope.disabledCreateCoursesButton = false;
            $scope.disabledupdateCoursesButton = false;
            $scope.disabledDeleteCoursesButton = false;
        }
        
       
        
    }
    var year = 2015;
    var range = [];
    var range1 = [];
   
    for (var i = 2015; i <= new Date().getFullYear()  ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;
    $scope.logOut = function(){
        
        localStorage.setItem("AdminLoggedIn", "False");
        setTimeout(() => {
                window.location = "index.html";
                },1000)
    }
   
    $scope.showaddCoursesDiv = function () {
        if($scope.addCoursesDiv == true){
          $scope.addCoursesDiv = false;
          $scope.updateCoursesButton = false;
          $scope.addCoursesButton = true;
        }else{
          $scope.addCoursesDiv = true;
          $scope.updateCoursesButton = false;
          $scope.addCoursesButton = true;
        }  
        $scope.courseName = '';
        $scope.courseYear = '';
        $scope.totalSeats = '';
        $scope.academicYear = '';
        $("#startDate").val('');
        $("#endDate").val('');
       
     }
 
     $scope.editUserClick = function (courses) {
         console.log(courses);
         $scope.addCoursesDiv = true;
         $scope.addCoursesButton = false;
         $scope.updateCoursesButton = true;
         
         $scope.updateCoursesId =courses.course_id;
        
         $scope.courseName = courses.course_name;
         $scope.courseYear = courses.courses_year;
         $scope.totalSeats = courses.total_seats;
         $scope.academicYear= courses.academics_year;
         $startDate = $("#startDate").val(courses.start_date);
         $endDate = $("#endDate").val(courses.end_date);
         $('html, body').animate({scrollTop: '0px'}, 1000);
     }
    var namePattern = /^[a-zA-z\s][a-zA-Z() ._-\s]+$/;
    
    var NumberPattern = /^[0-9]{1,5}$/;

    $scope.addCourse = function () {
        $startDate= $("#startDate").val();
        $endDate= $("#endDate").val();
        
        if ($scope.courseName) {
            if(!( $scope.courseName.match(namePattern))){
                $('#courseName').focus();
                toastr.error("Please enter valid Course Name.");
                return false;   
                }
                if ($scope.courseYear) {
                    if ($scope.totalSeats) {
                        if(!( $scope.totalSeats.match(NumberPattern))){
                            $('#totalSeats').focus();
                            toastr.error("Please enter valid TotalSeat Number.");
                            return false;   
                        }
                        if($scope.academicYear){
                            if ($startDate) {
                                if ($endDate) {
                                    if($("#startDate").val() > $("#endDate").val()){
                                        toastr.error("Please select valid Start And End Date.");
                                        return false;
                                    } 
                                        var transform = function(data) {
                                            return $.param(data);
                                        } 

                                        $http.post(window.site_url + 'ManageCourse/addCourse', {
                                            'csrf_token_name': csrfHash,
                                            courseName : $scope.courseName ,
                                            courseYear : $scope.courseYear ,
                                            totalSeats : $scope.totalSeats ,
                                            startDate : $startDate,
                                            endDate : $endDate,
                                            academicYear: $scope.academicYear,
                                            
                                            
                                        }, {
                                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                            transformRequest: transform
                                        })
                                        .then(function(response) {
                                            if (response.data.status.status == "1") {

                                                toastr.success(response.data.status.message);
                                                $scope.courseName = '';
                                                $scope.courseYear = '';
                                                $scope.totalSeats = '';
                                                $scope.academicYear = '';
                                                
                                                $("#startDate").val('');
                                                $("#endDate").val('');
                                                $scope.init();
                                            
                                            }else{
                                                toastr.error(response.data.status.message);
                                            }
                                            $scope.disabledCreateCoursesButton = false;
                                            $("#loadingDiv").css('display','none');
                                        }, function(response) {
                                            $scope.disabledCreateCoursesButton = false;
                                                    $("#loadingDiv").css('display','none');
                                        })
                                } else {
                                    $('#endDate').focus();
                                    toastr.error("End Date is required.");
                                    }
                            } else {
                                $('#startDate').focus();
                                toastr.error("Start Date is required.");
                                }
                        } else {
                            $('#academicYear').focus();
                            toastr.error("Academic Years is required.");
                            }        
                                            
                    } else {
                        $('#totalSeats').focus();
                        toastr.error("Total Seats required.");
                        }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course year is required.");
                    }
        } else {
            $('#courseName').focus();
            toastr.error(" Course Name is required.");
        }
    }

    $scope.getCoursesList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageCourse/getCoursesList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    
    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'ManageCourse/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.searchOnDB = function (value) {
        var transform = function(data) {
            return $.param(data);
        } 

        $http.post(window.site_url + 'ManageCourse/searchByCourses', {
            'csrf_token_name': csrfHash,
            coursesSearch:value
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.coursesListArray = response.data.data;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
}

$scope.updateCourse = function(){
    $startDate= $("#startDate").val();
        $endDate= $("#endDate").val();
       
        if ($scope.courseName) {
            if(!( $scope.courseName.match(namePattern))){
                $('#courseName').focus();
                toastr.error("Please enter valid Course Name.");
                return false;   
                }
                if ($scope.courseYear) {
                    if ($scope.totalSeats) {
                        if(!( $scope.totalSeats.match(NumberPattern))){
                            $('#totalSeats').focus();
                            toastr.error("Please enter valid TotalSeat Number.");
                            return false;   
                        }
                        if($scope.academicYear){
                            if ($startDate) {
                                if ($endDate) {
                                    if($("#startDate").val() > $("#endDate").val()){
                                        toastr.error("Please select valid Start And End Date.");
                                        return false;
                                    }
                                    
                                        var transform = function(data) {
                                            return $.param(data);
                                        } 

                                        $http.post(window.site_url + 'ManageCourse/updateCourse', {
                                            'csrf_token_name': csrfHash,
                                            updateCoursesId: $scope.updateCoursesId,
                                            courseName : $scope.courseName ,
                                            courseYear : $scope.courseYear ,
                                            totalSeats : $scope.totalSeats ,
                                            startDate : $startDate,
                                            endDate : $endDate,
                                            academicYear: $scope.academicYear,
                                            
                                        }, {
                                            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                            transformRequest: transform
                                        })
                                        .then(function(response) {
                                            if (response.data.status.status == "1") {

                                                toastr.success(response.data.status.message);
                                                $scope.courseName = '';
                                                $scope.courseYear = '';
                                                $scope.totalSeats = '';
                                                $("#startDate").val('');
                                                $("#endDate").val('');
                                                $scope.init();
                                            
                                            }else{
                                                toastr.error(response.data.status.message);
                                            }
                                            $scope.disabledCreateCoursesButton = false;
                                            $("#loadingDiv").css('display','none');
                                        }, function(response) {
                                            $scope.disabledCreateCoursesButton = false;
                                                    $("#loadingDiv").css('display','none');
                                        })
                                } else {
                                    $('#endDate').focus();
                                    toastr.error("End Date is required.");
                                    }
                            } else {
                                $('#startDate').focus();
                                toastr.error("Start Date is required.");
                                }
                            } else {
                                $('#academicYear').focus();
                                toastr.error("Academic Years is required.");
                                }   
                                            
                    } else {
                        $('#totalSeats').focus();
                        toastr.error("Total Seats required.");
                        }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course year is required.");
                    }
        } else {
            $('#courseName').focus();
            toastr.error(" Course Name is required.");
        }

}

$scope.deleteUserClick = function (courses) {
    $scope.updateCoursesId = courses.course_id;
    var transform = function(data) {
        return $.param(data);
    } 
    $scope.disabledDeleteCoursesButton = true;
    $http.post(window.site_url + 'ManageCourse/deleteCourses', {
        'csrf_token_name': csrfHash,
        updateCoursesId:courses.course_id
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            toastr.success(response.data.status.message);
            $scope.updateCoursesId = "";
            $scope.init();
        }else {
            toastr.error(response.data.status.message);
        }
        $scope.disabledDeleteCoursesButton = false;
    }, function(response) {
        $scope.disabledDeleteCoursesButton = false;
    })
}

$scope.getSingleCourse = function (course) {
    $scope.course_id = course.course_id;
    var transform = function(data) {
        return $.param(data);
    } 
    $http.post(window.site_url + 'ManageCourse/getSingleCourseList', {
        'csrf_token_name': csrfHash,
         course_id:course.course_id
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            $scope.singleCourseArray = response.data.data;
            console.log(response.data.data);
        }else {
            toastr.error(response.data.status.message);
        }
    }, function(response) {
    })

}
$scope.getFilterData = function(){
   
    var transform = function(data) {
        return $.param(data);
    } 
    $http.post(window.site_url + 'ManageCourse/getFilterData', {
        'csrf_token_name': csrfHash,
        courseName : $scope.courseName,
        courseYear :$scope.courseYear, 
        academicYear : $scope.academicYear,
    }, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        transformRequest: transform
    })
    .then(function(response) {
        if (response.data.status.status == "1") {
            $scope.coursesListArray = response.data.data;
        }else {
            toastr.error(response.data.status.message);
        }
    }, function(response) {
    })              
}
$scope.clearfilter = function(){
    $scope.courseName ='';
    $scope.courseYear= ''; 
    $scope.academicYear ='';
    $scope.getCoursesList();
}

});


