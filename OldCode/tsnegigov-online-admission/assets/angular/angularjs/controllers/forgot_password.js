
// $('.menuSection').removeClass('active');
// $('.menu').removeClass('active');
// $('#hotelMenu').addClass('active');       

var csrfHash = $('#csrfHash').val();

var secretKey = "inoxpa";

var app = angular.module('myApp', ['ui.bootstrap', 'toastr']);



app.controller('forgotPasswordServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
       $scope.inputForgotEmail = "";
    }

    $scope.forgotPassword = function () {
        if ($scope.inputForgotEmail) {
                
                var transform = function(data) {
                    return $.param(data);
                } 
                $http.post(window.site_url + 'ForgotPassword/ForgotPasswordSendEmail', {
                    'csrf_token_name': csrfHash,
                    forgotEmail: $scope.inputForgotEmail
                }, {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                    transformRequest: transform
                })
                .then(function(response) {
                    if (response.data.status.status == "1") {
                        toastr.success(response.data.status.message);
                        $scope.init();
                        setTimeout(function(){  
                            window.location = "index.html";
                        }, 1000);
                    }else {
                        toastr.error(response.data.status.message);
                        $scope.inputForgotEmail = "";
                    }
                }, function(response) {
                })
        } else {
            $('#inputForgotEmail').focus();
            toastr.error("Email is required.");
        }
    }

});


