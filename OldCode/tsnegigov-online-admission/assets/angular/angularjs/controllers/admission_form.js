var csrfHash = $('#csrfHash').val();



app.controller('admissionFormServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        if(localStorage.getItem("StudentLoggedIn") == "False" || localStorage.getItem("StudentLoggedIn") == null){
            window.location = "index.html";
        }else{
            var storedNames = JSON.parse(localStorage.getItem("StudentId"));
            
            $scope.studentRegistrationDetails= [];
            $scope.getStudentRegistrationDetails(storedNames);
            $scope.studentPersonalInfoArray = [];
            $scope.getStudentPersonalInfo(storedNames);
            $scope.religionListArray =[];
            $scope.getReligionList();
            $scope.studentAcademicInfoArray = [];
            //$scope.getStudentAcademicInfo(storedNames);
            $scope.studentParentInfoArray = [];
            $scope.getStudentParentInfo(storedNames);
            $scope.new_url = new_url;

           $scope.innerIndex=0;
            $scope.showHscDiv = false;
            $scope.showFyDiv = false;
            $scope.showSyDiv = false;
            $scope.showTyBcomDiv = false;
            $scope.showFyDivSem2 = false;
            $scope.showSyDivSem2 = false;
            $scope.endDate = '';
            $scope.isSindhi = '';
            $scope.fromJaihind = '';
            $scope.firstName = '';
            $scope.middleName = '';
            $scope.lastName = '';
            $scope.motherName = '';
            $scope.emailId = storedNames.student_email;
            $scope.mobileNumber = storedNames.student_mobile;
            $scope.alterMobileNumber = '';
            $scope.aadharNumber = '';
            $scope.voterNumber = '';
            $scope.localAddress = '';
            $scope.nativePlace = '';
            $scope.bloodGroup = '';
            $scope.gender = '';
            $scope.maritalstatus = '';
            $scope.dateOfBirth = '';
            $scope.birthPlace = '';
            $scope.dobInWord = '';
            $scope.district = '';
            $scope.dateOfBirth = '';
            $scope.state = '';
            $scope.nationality = '';
            $scope.Passport = '';
            $scope.religion = '';
            $scope.motherTongue = '';
            $scope.caste = '';
            $scope.category = '';
            $scope.isCastCertificate = '';
            $scope.game = '';
            $scope.hobbies = '';
            $scope.nss = '';
            $scope.event = '';
            $scope.hobbies = '';
            $scope.hobbies = '';
            $scope.photo = '';
            $scope.signature = '';
            $scope.casteCertificate ='';
            $scope.multipleDocument =[];
            $scope.photolink= '';
            $scope.signlink='';
            $scope.certificatelink='';
            $scope.multipleDocumentlink ='';
            $scope.parentSignature ='';
            $scope.parentSignaturelink = '';
            $scope.sscMarksheetlink='';
            $scope.sscExam = '' ;
            $scope.sscBoard = '' ;
            $scope.sscClgName = '' ;
            $scope.sscPassingYear = '' ;
            $scope.sscSeatNo = '' ;
            $scope.sscMarksObtain = '' ;
            $scope.sscTotalMarks = '' ;
            $scope.sscPercentage = '' ;
            $scope.sscMarksheet = '' ;
            $scope.appearingYear = '' ;
            $scope.gapReason = '' ;
            $scope.appliedOtherCollege = '' ;
            $scope.hscMarksheetlink ='';
            $scope.hscExam = '' ;
            $scope.hscBoard = '' ;
            $scope.hscClgName = '' ;
            $scope.hscPassingYear = '' ;
            $scope.hscSeatNo = '';
            $scope.hscMarksObtain = '' ;
            $scope.hscTotalMarks = '' ;
            $scope.hscPercentage = '' ;
            $scope.hscMarksheet ='';
            $scope.tyBComMarksheetlink ='';
            $scope.tyBComExam = '' ;
            $scope.tyBComBoard = '' ;
            $scope.tyBComClgName = '' ;
            $scope.tyBComPassingYear = '' ;
            $scope.tyBComSeatNo = '';
            $scope.tyBComMarksObtain = '' ;
            $scope.tyBComTotalMarks = '' ;
            $scope.tyBComPercentage = '' ;
            $scope.tyBComMarksheet ='';
            $scope.fyMarksheetlink ='';
            $scope.fyExam = '' ;
            $scope.fyBoard = '' ;
            $scope.fyClgName = '' ;
            $scope.fyPassingYear = '' ;
            $scope.fySeatNo = '' ;
            $scope.fyMarksObtain = '' ;
            $scope.fyTotalMarks = '' ;
            $scope.fyPercentage  = '' ;
            $scope.fyMarksheet ='';
            $scope.syMarksheetlink ='';
            $scope.syExam = '' ;
            $scope.syBoard = '';
            $scope.syClgName = '' ;
            $scope.syPassingYear = '';
            $scope.sySeatNo ='';
            $scope.syMarksObtain = '';
            $scope.syTotalMarks = '';
            $scope.syPercentage = '' ;
            $scope.syMarksheet ='';
            $scope.fyExamSem2 ='';
            $scope.fyBoardSem2 ='';
            $scope.fyBoardNamesem2 ='';
            $scope.fyClgNameSem2 ='';
            $scope.fyPassingYearSem2 ='';
            $scope.fySeatNoSem2 ='';
            $scope.fyMarksObtainSem2 ='';
            $scope.fyTotalMarksSem2 ='';
            $scope.fyPercentageSem2 ='';
            $scope.fyMarksheetSem2 ='';
            $scope.syExamSem2 ='';
            $scope.syBoardSem2 ='';
            $scope.syBoardNameSem2 ='';
            $scope.syClgNameSem2 ='';
            $scope.syPassingYearSem2 ='';
            $scope.sySeatNoSem2 ='';
            $scope.syMarksObtainSem2 ='';
            $scope.syTotalMarksSem2 ='';
            $scope.syPercentageSem2 ='';
            $scope.syMarksheetSem2 ='';
            $scope.fyMarksheetSem2link ="";
            $scope.syMarksheetlinkSem2 ="";
            $scope.studentchk1 ='';
            $scope.fatherFirstName ='';
            $scope.fatherMiddleName ='';
            $scope.fatherLastName ='';
            $scope.fatherEmail ='';
            $scope.fatherMobileNo ='';
            $scope.fatherOccupation ='';
            $scope.fathercGovtEmp ='';
            $scope.fatherAnnualIncome ='';
            $scope.fatherecoBack ='';

            $scope.motherFirstName ='';
            $scope.motherMiddleName ='';
            $scope.motherLastName ='';
            $scope.motherEmail ='';
            $scope.motherMobileNo ='';
            $scope.motherOccupation ='';
            $scope.mothercGovtEmp ='';
            $scope.motherAnnualIncome ='';
            $scope.motherecoBack ='';

            $scope.guaradianFirstName ='';
            $scope.guaradianMiddleName ='';
            $scope.guaradianLastName ='';
            $scope.guaradianEmail ='';
            $scope.guaradianMobileNo ='';
            $scope.guaradianOccupation ='';
            $scope.guaradiancGovtEmp ='';
            $scope.guaradianAnnualIncome ='';
            $scope.guaradianecoBack =''; 
            $scope.studentFullName = '';
            $scope.dateyear = "";
	        $scope.month = "";
            $scope.day = "";
            $scope.sscBoardName = "";
            $scope.hscBoardName = "";
            $scope.tyBComBoardName = "";
            $scope.fyBoardName = "";
            $scope.syBoardName = "";
            $scope.disabledSubmitPersonalInfoButton = false;
            $scope.disabledSubmitAcademicInfoButton = false;
            $scope.disabledSubmitParentInfoButton = false;
            $scope.ShowSpinnerStatus = false;
            $scope.sscAaddUniversityName = false;
            $scope.hscAddUniversityName = false;
            $scope.tyBComAddUniversityName = false;
            $scope.fyAddUniversityName = false;
            $scope.syAddUniversityName = false;
            $scope.fyAddUniversityNamesem2 = false;
            $scope.syAddUniversityNameSem2 = false;
            $scope.isgapReason="";
            $scope.isMaharashtra ="";
            $scope.eligibilityNumber ="";
            $scope.gapReason ="";
            $scope.physicallychallengedCertificate="";
            $scope.physicallychallengedCertificatelink ="";
            $scope.physicallyhandi = "";
            $scope.regdNumber ="";
            $scope.admissionType = "";
            $scope.rollNumber = "";
            $scope.correspondanceAddress = "";
            $scope.guardianAddress = "";
           
        } 
        
    }
    

	/*Date Of Birth*/
	
	$scope.days = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
	
    
    $scope.months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
    
	$scope.dateyears = [];
	var d = new Date();
	for (var i = (d.getFullYear() - 13); i > (1947); i--) {
		$scope.dateyears.push(i);
	}
    
    $scope.logOut = function(){
        
        localStorage.setItem("StudentLoggedIn", "False");
        localStorage.setItem("StudentId", "");
        
        setTimeout(() => {
                window.location = new_url+ "online-admission/index.html";
                },0)
    }

    var year = new Date().getFullYear();
    var range = [];
    for (var i = year; i >= 1998 ; i--) {
        range.push(i) ;
        
    }
    $scope.years = range;
    var priYear =[];
    priYear.push((new Date().getFullYear())-1);
    
    $scope.priviousYear = priYear;
    $scope.showDiv = function(){
        if($scope.courseYear == "First Year"){
            if($scope.courseName == 'M.Com'){
                $scope.showHscDiv = true;
                $scope.showTyBcomDiv = true;
            }else{
                $scope.showHscDiv = true;
            }
            
        }
        if($scope.courseYear == "Second Year"){
            
            if($scope.courseName == 'M.Com'){
                $scope.showHscDiv = true;
                $scope.showTyBcomDiv = true;
                $scope.showFyDiv = true;
                $scope.showFyDivSem2 = true;
                
            }else{
                $scope.showHscDiv = true;
                $scope.showFyDiv = true;
                $scope.showFyDivSem2 = true;
            }  
        
        }
        if($scope.courseYear == "Third Year"){
                
            if($scope.courseName == 'M.Com'){
                $scope.showHscDiv = true;
                $scope.showTyBcomDiv = true;
                $scope.showFyDiv = true;
                $scope.showFyDivSem2 = true;
                $scope.showSyDiv = true;
                $scope.showSyDivSem2 = true;
                
            }else{
                $scope.showHscDiv = true;
                $scope.showFyDiv = true;
                $scope.showFyDivSem2 = true;
                $scope.showSyDiv = true;
                $scope.showSyDivSem2 = true;
            }  
        }
    }

    $scope.showEligibilityNoDiv = function(){
        if($scope.courseYear == "First Year"){
            $scope.ifSecondThirdYear = false;
            $scope.ifFirstYear= true;
        }else{
            $scope.ifSecondThirdYear = true;
            $scope.ifFirstYear= false;
        }

    }
    $scope.showSscAddUniversityNameDiv = function(){

        
        if($scope.sscBoard == 'Other'){
        $scope.sscAddUniversityName = true;
        }else{
            $scope.sscAddUniversityName = false;
        }
    }
    $scope.showHscAddUniversityNameDiv = function(){
       
        
        if($scope.hscBoard == 'Other'){
        $scope.hscAddUniversityName = true;
        }else{
            $scope.hscAddUniversityName = false;
        }
    }
    $scope.showFyAddUniversityNameDiv = function(){
        
        if($scope.fyBoard == 'Other'){
        $scope.fyAddUniversityName = true;
        }else{
            $scope.fyAddUniversityName = false;
        }
    }
    $scope.showSyAddUniversityNameDiv = function(){
       
        if($scope.syBoard == 'Other'){
        $scope.syAddUniversityName = true;
        }else{
            $scope.syAddUniversityName = false;
        }
    }
     $scope.showFyAddUniversityNameDivSem2 = function(){
        
        if($scope.fyBoardSem2 == 'Other'){
        $scope.fyAddUniversityNamesem2 = true;
        }else{
            $scope.fyAddUniversityNamesem2 = false;
        }
    }
    $scope.showSyAddUniversityNameDivSem2 = function(){
       
        if($scope.syBoardSem2 == 'Other'){
        $scope.syAddUniversityNameSem2 = true;
        }else{
            $scope.syAddUniversityNameSem2 = false;
        }
    }
    $scope.showtyBComAddUniversityNameDiv = function(){
        if($scope.tyBComBoard == 'Other'){
            $scope.tyBComAddUniversityName = true;
        }else{
            $scope.tyBComAddUniversityName = false;
        }
    }
    var namePattern = /^([a-zA-Z-',.\s]{1,30})$/;
    var castePattern = /^[a-zA-z\s][a-zA-Z .,/_-\s]+$/;
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    var mobileNumberPattern = /^[7-9][0-9]{9}$/;
    var aadharNumberPattern = /^[0-9]{12}$/;
    var eligibilityNumberPattern = /^[0-9]{11}$/;
    var voterNumberPattern = /^[A-Z]{3}[0-9]{7}$/;
    var marksPattern = /^[0-9]{1,6}$/;


    $scope.submitPersonalInfo = function () {
       // $dateOfBirth= $("#dateOfBirth").val();
      
       
        // if ($scope.isSindhi) {
        //     if ($scope.fromJaihind) {
                if($scope.lastName) {
                    if(!($scope.lastName.match(namePattern))){
                        $('#lastName').focus();
                            toastr.error("Please enter valid Last Name.");
                            return false;
                    }
                    if ($scope.firstName) {
                    if(!($scope.firstName.match(namePattern))){
                        $('#firstName').focus();
                        toastr.error("Please enter valid First Name.");
                        return false;
                    }
                    
                        if ($scope.middleName) {
                            if(!($scope.middleName.match(namePattern))){
                                $('#middleName').focus();
                                toastr.error("Please enter valid Middle Name.");
                                return false;
                            }
                            if($scope.motherName){
                                if(!($scope.motherName.match(namePattern))){
                                    $('#motherName').focus();
                                    toastr.error("Please enter valid Mother's Name.");
                                    return false;
                                }
                                if ($scope.emailId) {
                                    if (!($scope.emailId.match(emailPattern))) {
                                        $('#emailId').focus();
                                        toastr.error("Email address is required.");
                                        return false;
                                    }
                                
                                    if ($scope.mobileNumber) {
                                        if (!($scope.mobileNumber.match(mobileNumberPattern))) {
                                            $('#mobileNumber').focus();
                                            toastr.error("Please enter valid Mobile Number.");
                                            return false;
                                        }
                                        if($scope.alterMobileNumber){
                                                if (!($scope.alterMobileNumber.match(mobileNumberPattern))) {
                                                $('#alterMobileNumber').focus();
                                                toastr.error("Please enter valid alternative Mobile Number.");
                                                return false;
                                                }
                                        }
                                        if ($scope.aadharNumber) {
                                            if (!($scope.aadharNumber.match(aadharNumberPattern))) {
                                                $('#aadharNumber').focus();
                                                toastr.error(" Please enter valid Aadhar Number");
                                                return false;
                                            }
                                        
                                        if ($scope.voterNumber) {
                                                if (!($scope.voterNumber.match(voterNumberPattern))) {
                                                    $('#voterNumber').focus();
                                                    toastr.error("Please enter valid voter Number.");
                                                    return false;
                                                }
                                            }
                                            
                                                if ($scope.localAddress) {
                                                    if ($scope.nativePlace) {
                                                        if (!($scope.nativePlace.match(namePattern))) {
                                                            $('#nativePlace').focus();
                                                            toastr.error("Please enter valid Native Place.");
                                                            return false;
                                                        }

                                                    
                                                    if($scope.bloodGroup){
                                                        if ($scope.gender) {
                                                            if ($scope.maritalstatus) {
                                                                if ($scope.day) {
                                                                    if ($scope.month) {
                                                                        if ($scope.dateyear) {
                                                                            $scope.dateOfBirth=  $scope.day+"-"+$scope.month+"-"+$scope.dateyear;
                                                                            
                                                                            if($scope.dobInWord){
                                                                        
                                                                            if ($scope.birthPlace) {
                                                                                if (!($scope.birthPlace.match(namePattern))) {
                                                                                    $('#birthPlace').focus();
                                                                                    toastr.error("Please enter valid Birth Place.");
                                                                                    return false;
                                                                                }
                                                                                if ($scope.district) {
                                                                                    if (!($scope.district.match(namePattern))) {
                                                                                        $('#district').focus();
                                                                                        toastr.error("Please enter valid District Name.");
                                                                                        return false;
                                                                                    }
                                                                    
                                                                                    if($scope.state) {
                                                                                        if (!($scope.state.match(namePattern))) {
                                                                                            $('#state').focus();
                                                                                            toastr.error("Please enter valid State Name.");
                                                                                            return false;
                                                                                        }
                                                                        
                                                                                        if($scope.nationality){
                                                                                            if (!($scope.nationality.match(namePattern))) {
                                                                                                $('#nationality').focus();
                                                                                                toastr.error("Please enter valid Nationality.");
                                                                                                return false;
                                                                                            }
                                                                                          
                                                                                            if ($scope.religion == "") {
                                                                                                $('#religion').focus();
                                                                                                toastr.error("Religion is required.");
                                                                                                return false;
                                                                                            }
                                                                                                if ($scope.category == "") {
                                                                                                    $('#category').focus();
                                                                                                    toastr.error("Category is required.");
                                                                                                    return true;
                                                                                                }
                                                                                                    
                                                                                                    if ($scope.caste) {
                                                                                                        if (!($scope.caste.match(castePattern))) {
                                                                                                            $('#caste').focus();
                                                                                                            toastr.error("Please enter valid Caste Name .");
                                                                                                            return false;
                                                                                                        }
                                                                                                    }else{
                                                                                                        $('#caste').focus();
                                                                                                        toastr.error("caste is required.");
                                                                                                        return false;
                                                                                                    }
                                                                                                
                                                                                                        if ($scope.motherTongue) {
                                                                                                            if (!($scope.motherTongue.match(namePattern))) {
                                                                                                                $('#motherTongue').focus();
                                                                                                                toastr.error("Please enter valid Mother Tongue .");
                                                                                                                return false;
                                                                                                            }
                                                                                
                                                                                                            if ($scope.isCastCertificate) {
                                                                                                                if($scope.physicallyhandi){
                                                                                                                if($scope.courseYear == "Second Year" || $scope.courseYear == "Third Year"){

                                                                                                                    if($scope.eligibilityNumber == ''){
                                                                                                                        $('#eligibilityNumber').focus();
                                                                                                                        toastr.error("Please enter university roll Number.");
                                                                                                                        return false;
                                                                                                                    }
                                                                                                                    // if($scope.eligibilityNumber){
                                                                                                                    //     if (!($scope.eligibilityNumber.match(eligibilityNumberPattern))) {
                                                                                                                    //         $('#eligibilityNumber').focus();
                                                                                                                    //         toastr.error("Please enter valid eligibility Number.");
                                                                                                                    //         return false;
                                                                                                                    //     }
                                                                                                                    // }
                                                                                                                }
                                                                                                                if ($scope.game) {
                                                                                                                    if (!($scope.game.match(namePattern))) {
                                                                                                                        $('#game').focus();
                                                                                                                        toastr.error("Please enter valid games.");
                                                                                                                        return false;
                                                                                                                    }
                                                                                                                }
                                                                                                                if ($scope.hobbies) {
                                                                                                                    if (!($scope.hobbies.match(namePattern))) {
                                                                                                                        $('#hobbies').focus();
                                                                                                                        toastr.error("Please enter valid hobbies.");
                                                                                                                        return false;
                                                                                                                    }
                                                                                                                }
                                                                                                                if($scope.admissionType){
                                                                                                                // if ($scope.nss) {
                                                                                                                //     if ($scope.event) { 
                                                                                                                        if($scope.photolink == ''){
                                                                                                                            if($scope.photo == ''){
                                                                                                                            toastr.error("Please Upload Student Photo.");
                                                                                                                            return false;
                                                                                                                            }
                                                                                                                        }
                                                                                                                            if($scope.signlink == ''){
                                                                                                                                if($scope.signature == ''){
                                                                                                                                toastr.error("Please Upload Student Signature Image.");
                                                                                                                                return false;
                                                                                                                                }
                                                                                                                            }
                                                                                                                            if($scope.certificatelink == ''){
                                                                                                                                if($scope.isCastCertificate == 'Yes'){
                                                                                                                                    if($scope.casteCertificate == ''){
                                                                                                                                        toastr.error("Please Upload Caste Certificate.");
                                                                                                                                        return false;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                            if($scope.parentSignaturelink == ''){
                                                                                                                                if($scope.parentSignature == ''){
                                                                                                                                    toastr.error("Please upload parent signature.");
                                                                                                                                    return false;
                                                                                                                                }
                                                                                                                            }
                                                                                                                            if($scope.physicallychallengedCertificatelink == ''){
                                                                                                                                if($scope.physicallyhandi == 'Yes'){
                                                                                                                                    if($scope.physicallychallengedCertificate == ''){
                                                                                                                                        toastr.error("Please Upload physically challenged/visually or hearing impaired Certificate.");
                                                                                                                                        return false;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                
                                                                                                                    
                                                                                                                            $scope.disabledSubmitPersonalInfoButton = true;
                                                                                                                            $scope.ShowSpinnerStatus = true;
                                                                                                                                $http({
                                                                                                                                    method: 'POST',
                                                                                                                                    url: window.site_url +'AdmissionForm/submitPersonalInfo',
                                                                                                                                    processData: false,
                                                                                                                                    transformRequest: function(data) {
                                                                                                                                        var formData = new FormData();                    
                                                                                                                                            
                                                                                                                                        formData.append("csrf_token_name", csrfHash);
                                                                                                                                        formData.append("studentId", JSON.parse(localStorage.getItem("StudentId")));
                                                                                                                                        formData.append("isSindhi",   $scope.isSindhi);
                                                                                                                                        formData.append("fromJaihind", $scope.fromJaihind);
                                                                                                                                        formData.append("firstName", $scope.firstName);
                                                                                                                                        formData.append("middleName", $scope.middleName);
                                                                                                                                        formData.append("lastName", $scope.lastName);
                                                                                                                                        formData.append("motherName", $scope.motherName);
                                                                                                                                        formData.append("emailId", $scope.emailId );
                                                                                                                                        formData.append("mobileNumber", $scope.mobileNumber);
                                                                                                                                        formData.append("alterMobileNumber", $scope.alterMobileNumber);
                                                                                                                                        formData.append("aadharNumber", $scope.aadharNumber);
                                                                                                                                        formData.append("voterNumber", $scope.voterNumber);
                                                                                                                                        formData.append("localAddress", $scope.localAddress);
                                                                                                                                        formData.append("nativePlace", $scope.nativePlace);
                                                                                                                                        formData.append("bloodGroup", $scope.bloodGroup);
                                                                                                                                        formData.append("gender", $scope.gender);
                                                                                                                                        formData.append("maritalstatus", $scope.maritalstatus);
                                                                                                                                        formData.append("dateOfBirth", $scope.dateOfBirth);
                                                                                                                                        formData.append("birthPlace", $scope.birthPlace);
                                                                                                                                        formData.append("dobInWord",$scope.dobInWord);
                                                                                                                                        formData.append("district", $scope.district);
                                                                                                                                        
                                                                                                                                        formData.append("state", $scope.state);
                                                                                                                                        formData.append("nationality", $scope.nationality);
                                                                                                                                        formData.append("Passport", $scope.Passport);
                                                                                                                                        formData.append("religion", $scope.religion);
                                                                                                                                        formData.append("motherTongue", $scope.motherTongue);
                                                                                                                                        formData.append("caste", $scope.caste);
                                                                                                                                        formData.append("category", $scope.category);
                                                                                                                                        formData.append("isCastCertificate", $scope.isCastCertificate);
                                                                                                                                        formData.append("physicallyhandi", $scope.physicallyhandi);
                                                                                                                                        formData.append("eligibilityNumber", $scope.eligibilityNumber);
                                                                                                                                        formData.append("game", $scope.game );
                                                                                                                                        formData.append("hobbies", $scope.hobbies);
                                                                                                                                        formData.append("admissionType",$scope.admissionType);
                                                                                                                                        formData.append("regdNumber",$scope.regdNumber);
                                                                                                                                        // formData.append("nss", $scope.nss);
                                                                                                                                        // formData.append("event", $scope.event);
                                                                                                                                        formData.append("rollNumber",$scope.rollNumber);
                                                                                                                                        formData.append("correspondanceAddress",$scope.correspondanceAddress);
                                                                                                                                        formData.append("guardianAddress",$scope.guardianAddress);
                                                                                                                                        
                                                                                                                                        formData.append("photo", $scope.photo);
                                                                                                                                        formData.append("signature", $scope.signature);
                                                                                                                                        formData.append("casteCertificate", $scope.casteCertificate);
                                                                                                                                        formData.append("physicallychallengedCertificate", $scope.physicallychallengedCertificate);
                                                                                                                                        formData.append("parentSignature",$scope.parentSignature);
                                                                                                                                        for (i=0; i< $scope.multipleDocument.length; i++){
                                                                                                                                            formData.append('multipleDocument'+[i], $scope.multipleDocument[i]);
                                                                                                                                            
                                                                                                                                        }
                                                                                                                                        
                                                                                                                                        return formData;
                                                                                                                                    },
                                                                                                                                    data: $scope.form,
                                                                                                                                    headers: {
                                                                                                                                        'Content-Type': undefined
                                                                                                                                    }
                                                                                                                                })
                                                                                                                                .then(function(response) {
                                                                                                                                    if (response.data.status.status == "1") {

                                                                                                                                        toastr.success(response.data.status.message);
                                                                                                                                        $('html, body').animate({scrollTop: '0px'}, 3000);
                                                                                                                                        
                                                                                                                                        $scope.init();
                                                                                                                                        
                                                                                                                                    }else{
                                                                                                                                        toastr.error(response.data.status.message);
                                                                                                                                        $scope.disabledSubmitPersonalInfoButton = false;
                                                                                                                                        $("#loadingDiv").css('display','none');
                                                                                                                                        $scope.ShowSpinnerStatus = false;
                                                                                                                                    }
                                                                                                                                }, function(response) {
                                                                                                                                })
                                                                                                                            
                                                                                                                    //     } else {
                                                                                                                    //         $('#eventYes').focus();
                                                                                                                    //         toastr.error("Please select whether participated in any event?");
                                                                                                                    //         }
                                                                                                                    // } else {
                                                                                                                    //     $('#nssYes').focus();
                                                                                                                    //     toastr.error("Please select Do you wish to join college NSS ?");
                                                                                                                    //     }
                                                                                                                    } else {
                                                                                                                        $('#admissionType').focus();
                                                                                                                        toastr.error("Please select admission type.");
                                                                                                                        }
                                                                                                                        
                                                                                                            } else {
                                                                                                                $('#isphysicallyhandiYes').focus();
                                                                                                                toastr.error("Please select Are you physically challenged/visually or hearing impaired ? ");
                                                                                                                }
                                                                                                        } else {
                                                                                                            $('#isCastCertificateYes').focus();
                                                                                                            toastr.error("Please select do you have cast certificate? ");
                                                                                                            }
                                                                                                    } else {
                                                                                                        $('#motherTongue').focus();
                                                                                                        toastr.error("Please enter mother tongue.");
                                                                                                        }
                                                                                        //         } else {
                                                                                        //         $('#caste').focus();
                                                                                        //         toastr.error("caste is required.");
                                                                                        //         }
                                                                            
                                                                                        //     } else {
                                                                                        //         $('#category').focus();
                                                                                        //         toastr.error("Category is required.");
                                                                                        //         }
                                                                                        // } else {
                                                                                        //     $('#religion').focus();
                                                                                        //     toastr.error("Religion is required.");
                                                                                        //     }
                                                                                    } else {
                                                                                        $('#nationality').focus();
                                                                                        toastr.error("Nationality is required.");
                                                                                        }  
                                                                                } else {
                                                                                    $('#state').focus();
                                                                                    toastr.error(" State is required.");
                                                                                    }
                                                                            } else {
                                                                                $('#district').focus();
                                                                                toastr.error(" District is required.");
                                                                                }                                             
                                                                            } else {
                                                                                $('#birthPlace').focus();
                                                                                toastr.error(" Birth place is required.");
                                                                                }
                                                                            } else {
                                                                                $('#dobInWord').focus();
                                                                                toastr.error("Date of birth in word is required.");
                                                                                }
                                                                                
                                                                            } else {
                                                                                    $('#dateyear').focus();
                                                                                    toastr.error("DOB year is required.");
                                                                                    }
                                                                        } else {
                                                                            $('#month').focus();
                                                                            toastr.error("DOB month is required.");
                                                                            }
                                                                } else {
                                                                    $('#day').focus();
                                                                    toastr.error(" DOB day is required.");
                                                                    }
                                                                } else {
                                                                    $('#married').focus();
                                                                    toastr.error("Select marital status is required.");
                                                                    }
                                                        } else {
                                                            $('#male').focus();
                                                            toastr.error("Select gender is required.");
                                                            }
                                                    } else {
                                                        $('#bloodGroup').focus();
                                                        toastr.error("Blood group is required.");
                                                        }
                                                    } else {
                                                    $('#nativePlace').focus();
                                                    toastr.error("Native place is required.");
                                                    }
                                                } else {
                                                $('#localAddress').focus();
                                                toastr.error("Address is required.");
                                                }


                                            // } else {
                                            // $('#voterNumber').focus();
                                            // toastr.error("voter Number is required.");
                                            // }

                                        } else {
                                        $('#aadharNumber').focus();
                                        toastr.error("Aadhar number is required.");
                                        }

                                    } else {
                                    $('#mobileNumber').focus();
                                    toastr.error("Mobile number is required.");
                                    }

                                } else {
                                $('#emailId').focus();
                                toastr.error("Email address is required.");
                                }
                            } else {
                                $('#motherName').focus();
                                toastr.error("Mother's name is required.");
                                }
                        } else {
                        $('#middleName').focus();
                        toastr.error("Middle name is required.");
                        }

                    } else {
                    $('#firstName').focus();
                    toastr.error("First name is required.");
                    }
                } else {
                    $('#lastName').focus();
                    toastr.error("Last name is required.");
                    }
        //     } else {
        //     $('#jaihindInternal').focus();
        //     toastr.error("please select are you from jaihind college ?");
        //     }
        // } else {
        // $('#sindhiYes').focus();
        // toastr.error("please select are you belongs to sindhi category ?");
        // }
}

   


    $scope.getStudentPersonalInfo = function (storedNames) {
        $scope.student_id = storedNames;
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AdmissionForm/getStudentPersonalInfo', {
            'csrf_token_name': csrfHash,
            student_id : $scope.student_id ,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.studentPersonalInfoArray = response.data.data[0];
                $scope.getReligionList();
                $scope.isSindhi = response.data.data[0].is_sindhi_category ;
                $scope.fromJaihind = response.data.data[0].is_jaihind_college ;
                $scope.firstName = response.data.data[0].first_name ;
                $scope.middleName  = response.data.data[0].middle_name ;
                $scope.lastName = response.data.data[0].last_name ;
                $scope.motherName = response.data.data[0].mother_name;
                $scope.alterMobileNumber = response.data.data[0].alter_mobile_number ;
                $scope.aadharNumber  = response.data.data[0].adhar_card_id ;
                $scope.voterNumber  = response.data.data[0].voter_id ;
                $scope.localAddress  = response.data.data[0].local_address ;
                $scope.nativePlace = response.data.data[0].native_place ;
                $scope.bloodGroup  = response.data.data[0].blood_group ;
                $scope.gender = response.data.data[0].gender ;
                $scope.maritalstatus = response.data.data[0].marital_status ;
                $scope.dateOfBirth  = response.data.data[0].date_of_birth.split('-');
                $scope.day= $scope.dateOfBirth[0];
                $scope.month= $scope.dateOfBirth[1];
                $scope.dateyear = $scope.dateOfBirth[2];
                $scope.dobInWord =  response.data.data[0].dob_in_word;
                $scope.birthPlace  = response.data.data[0].birth_place ;
                $scope.district  = response.data.data[0].district ;
                $scope.state  = response.data.data[0].state ;
                $scope.nationality   = response.data.data[0].nationality ;
                $scope.Passport   = response.data.data[0].passport ;
                $scope.religion  = response.data.data[0].religion ;
                $scope.motherTongue  = response.data.data[0].mother_tounge ;
                $scope.caste   = response.data.data[0].caste ;
                $scope.category   = response.data.data[0].category ;
                $scope.isCastCertificate  = response.data.data[0].is_cast_certificate ;
                $scope.physicallyhandi  = response.data.data[0].is_physically_handi ;
                $scope.eligibilityNumber = response.data.data[0].eligibility_number;
                $scope.game  = response.data.data[0].game_played ;
                $scope.hobbies  = response.data.data[0].hobbies ;
                $scope.admissionType = response.data.data[0].admission_type;
                $scope.regdNumber = response.data.data[0].regd_number;
               // $scope.nss   = response.data.data[0].is_join_clg_NNS ;
                $scope.event   = response.data.data[0].is_participate_in_event ;
                $scope.photolink=response.data.data[0].uploaded_photo;
                $scope.signlink   = response.data.data[0].uploaded_signature;
                $scope.certificatelink   = response.data.data[0].uploaded_caste_certificate;
                $scope.physicallychallengedCertificatelink   = response.data.data[0].uploaded_physically_handi_certificate;
                $scope.multipleDocumentlink   = response.data.data[0].uploaded_other_documents.split(',');
                $scope.rollNumber = response.data.data[0].roll_number;
                $scope.correspondanceAddress = response.data.data[0].correspondance_address;
                $scope.guardianAddress = response.data.data[0].guardian_address;
                $scope.parentSignaturelink = response.data.data[0].uploaded_parent_signature;
              // console.log($scope.multipleDocumentlink);
                
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getStudentRegistrationDetails = function (storedNames) {
        $scope.student_id = storedNames;
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AdmissionForm/getStudentRegistrationDetails', {
            'csrf_token_name': csrfHash,
            student_id : $scope.student_id ,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.studentRegistrationDetails=response.data.data[0];
                $scope.emailId  = response.data.data[0].student_email ;
                $scope.mobileNumber = response.data.data[0].student_mobile ;
                $scope.academicYear= response.data.data[0].stud_academic_year;
                $scope.courseName = response.data.data[0].course_name;
                $scope.courseYear = response.data.data[0].course_year;
                $scope.studentFullName =response.data.data[0].student_name.split(" ");
               // alert($scope.studentFullName);
                $scope.lastName = $scope.studentFullName[0] ;
                $scope.firstName  = $scope.studentFullName[1] ;
                $scope.middleName = $scope.studentFullName[2] ;
                $scope.showEligibilityNoDiv();
                $scope.showDiv();
                $scope.SubjectListArray = [];
                $scope.getSubjectList();
                if( response.data.data[0].admission_step_one == 0){
                    $('.CommonClassForActiveDeactive').removeClass('active');
                  // $('#step1-tab').addClass('active');  
                    $('#step1-tab').click();
                    document.getElementById('step1-tab').style.pointerEvents = 'auto';
                    document.getElementById('step2-tab').style.pointerEvents = 'none';
                    document.getElementById('step3-tab').style.pointerEvents = 'none';
                   
    
                    }
                     else if( response.data.data[0].admission_step_two == 0){
                        $('.CommonClassForActiveDeactive').removeClass('active');
                        //$('#step2-tab').addClass('active');  
                        $('#step2-tab').click();
                        document.getElementById('step1-tab').style.pointerEvents = 'auto';
                        document.getElementById('step2-tab').style.pointerEvents = 'auto';
                        document.getElementById('step3-tab').style.pointerEvents = 'none';
                     }else if( response.data.data[0].admission_step_three == 0){
                        $('.CommonClassForActiveDeactive').removeClass('active');
                       //$('#step3-tab').addClass('active');  
                        $('#step3-tab').click();
                        document.getElementById('step1-tab').style.pointerEvents = 'auto';
                        document.getElementById('step2-tab').style.pointerEvents = 'auto';
                        document.getElementById('step3-tab').style.pointerEvents = 'auto';
                    }
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }
    
    $scope.getSubjectList = function(){
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AdmissionForm/getSubjectList', {
            'csrf_token_name': csrfHash,
            courseName : $scope.courseName,
            courseYear : $scope.courseYear,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.optinalSubjectArray = [];
                $scope.optinalSubjectArraysem2 = [];
                $scope.compulsorySubjectArray = [];
                $scope.compulsorySubjectArraysem2 = [];
                $scope.valueAddedSubjectArraysem2 =[];
                $scope.valueAddedSubjectArray = [];
                $scope.SubjectListArray = response.data.data;
                
                var arraylen = $scope.SubjectListArray.length;
                for(i=0;i<arraylen ; i++){
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.optinalSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.optinalSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.compulsorySubjectArray.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.compulsorySubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'  && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.valueAddedSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'&& ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.valueAddedSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                }
               console.log($scope.optinalSubjectArray);
              $scope.optionalLeng = ($scope.optinalSubjectArray).length;
              $scope.valueAddedLeng = ($scope.valueAddedSubjectArray).length;
              $scope.valueAddedsem2Leng = ($scope.valueAddedSubjectArraysem2).length;
                $scope.getStudentAcademicInfo(JSON.parse(localStorage.getItem("StudentId")));
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })      
    }

    $scope.getReligionList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AdmissionForm/getReligionList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.religionListArray = response.data.data;
                //$scope.getCasteList();
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }
    
    $scope.uploadedParentSignature = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024;
            if(fileSize > 2){
                toastr.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png' || file.name.split('.').pop() == 'jpeg') {
                    var reader = new FileReader();
                    $scope.parentSignature = element.files[0];  
                    
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });
    }

    $scope.uploadedPhoto = function(element) {
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$/;
    //$scope.ShowSpinnerStatus = true;
            $scope.$apply(function($scope) { 
               
                var file = element.files[0];
                
                var fileSize = element.files[0].size / 1024 / 1024;
                if(fileSize > 2){
                    toastr.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 2MB.");
                    return false;
                }
                else { 
                    if (file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png'|| file.name.split('.').pop() == 'jpeg') {
                        var reader = new FileReader();
                        $scope.photo = element.files[0];
                       // $scope.ShowSpinnerStatus = false;
                    } else {
                        toastr.error('Please upload correct File , File extension should be (.jpg|.png|.jpeg)');
                       // $scope.ShowSpinnerStatus = false;
                        return false;
                    }
                }
            });
        
    }

    $scope.uploadedSignature = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024;
            if(fileSize > 2){
                toastr.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an image with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png' || file.name.split('.').pop() == 'jpeg') {
                    var reader = new FileReader();
                    $scope.signature = element.files[0];  
                    
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });
    }

    $scope.uploadedCasteCertificate = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf' || file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png' || file.name.split('.').pop() == 'jpeg' ) {
                    var reader = new FileReader();
                    $scope.casteCertificate = element.files[0];  
                    
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });
    }

    $scope.uploadedPhysicallychallengedCertificate = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf' || file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png' || file.name.split('.').pop() == 'jpeg' ) {
                    var reader = new FileReader();
                    $scope.physicallychallengedCertificate = element.files[0];  
                    
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });
    }
    
    $scope.uploadedMultipleDocuments = function(element){
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png)$/;
            $scope.$apply(function($scope) {    
                         
                var filee = element.files;
                for ( var i = 0; i < filee.length; i++)
                {      
                    var file = element.files[i];
                    var fileSize = element.files[0].size / 1024 / 1024; 
                    if(fileSize > 2){
                        toastr.error("size of document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                        return false;
                    }
                    else { 
                        if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                            var reader = new FileReader();
                        
                                
                                $scope.multipleDocument.push(element.files[i]) ;
                    
                            
                          //  $scope.multipleDocument[i]= element.files[i].name;
                            
                           // console.log($scope.multipleDocument);
                            
                        } else {
                            toastr.error('Please upload correct File , File extension should be (.pdf)');
                            return false;
                        }
                    }
                }
           
            });
    }

    $scope.calculatePercentage = function(){
        $scope.sscPercentageValue = 0;
        $scope.hscPercentageValue = 0;
        $scope.tyBComPercentageValue = 0;
        $scope.fyPercentageValue = 0;
        $scope.syPercentageValue = 0;
        fyPercentageValue2 =0;
        syPercentageValueSem2 =0;
        
        
        if($scope.sscMarksObtain != '' || $scope.sscTotalMarks != ''){
            var sscMarksObtain = Number($scope.sscMarksObtain || 0);
            var sscTotalMarks = Number($scope.sscTotalMarks || 0);
            $scope.sscPercentageValue = ((sscMarksObtain / sscTotalMarks)*100).toFixed(2);
        }
        if(isNaN($scope.sscPercentageValue) || $scope.sscPercentageValue == Infinity ){
            $scope.sscPercentageValue = 0;
        }
        if($scope.hscMarksObtain != '' || $scope.hscTotalMarks != ''){
            var hscMarksObtain = Number($scope.hscMarksObtain || 0);
            var hscTotalMarks = Number($scope.hscTotalMarks || 0);
            $scope.hscPercentageValue = ((hscMarksObtain / hscTotalMarks)*100).toFixed(2);
        }
        if(isNaN($scope.hscPercentageValue) || $scope.hscPercentageValue == Infinity ){
            $scope.hscPercentageValue = 0;
        }
        if($scope.tyBComMarksObtain != '' || $scope.tyBComTotalMarks != ''){
            var tyBComMarksObtain = Number($scope.tyBComMarksObtain || 0);
            var tyBComTotalMarks = Number($scope.tyBComTotalMarks || 0);
            $scope.tyBComPercentageValue = ((tyBComMarksObtain / tyBComTotalMarks)*100).toFixed(2);
        }
        if(isNaN($scope.tyBComPercentageValue) || $scope.tyBComPercentageValue == Infinity ){
            $scope.tyBComPercentageValue = 0;
        }
        if($scope.fyMarksObtain != '' || $scope.fyTotalMarks != ''){
            var fyMarksObtain = Number($scope.fyMarksObtain || 0);
            var fyTotalMarks = Number($scope.fyTotalMarks || 0);
            $scope.fyPercentageValue = ((fyMarksObtain / fyTotalMarks)*100).toFixed(2);
        }
        if(isNaN($scope.fyPercentageValue) || $scope.fyPercentageValue == Infinity ){
            $scope.fyPercentageValue = 0;
        }
        
         if($scope.fyMarksObtainSem2 != '' || $scope.fyTotalMarksSem2 != ''){
            var fyMarksObtainSem2 = Number($scope.fyMarksObtainSem2 || 0);
            var fyTotalMarksSem2 = Number($scope.fyTotalMarksSem2 || 0);
            $scope.fyPercentageValue2 = ((fyMarksObtainSem2 / fyTotalMarksSem2)*100).toFixed(2);
        }
        if(isNaN($scope.fyPercentageValue2) || $scope.fyPercentageValue2 == Infinity ){
            $scope.fyPercentageValue2 = 0;
        }
        
        if($scope.syMarksObtainSem2 != '' || $scope.syTotalMarksSem2 != ''){
            var syMarksObtainSem2 = Number($scope.syMarksObtainSem2 || 0);
            var syTotalMarksSem2 = Number($scope.syTotalMarksSem2 || 0);
            $scope.syPercentageValueSem2 = ((syMarksObtainSem2 / syTotalMarksSem2)*100).toFixed(2);
        }
        if(isNaN($scope.syPercentageValueSem2) || $scope.syPercentageValueSem2 == Infinity ){
            $scope.syPercentageValueSem2 = 0;
        }
        
        if($scope.syMarksObtain != '' || $scope.syTotalMarks != ''){
            var syMarksObtain = Number($scope.syMarksObtain || 0);
            var syTotalMarks = Number($scope.syTotalMarks || 0);
            $scope.syPercentageValue = ((syMarksObtain / syTotalMarks)*100).toFixed(2);
        }
        if(isNaN($scope.syPercentageValue) || $scope.syPercentageValue == Infinity ){
            $scope.syPercentageValue = 0;
        }
    }

    $scope.uploadedSSCMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.sscMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedHSCMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.hscMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedtyBComMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.tyBComMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedFYMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png' ) {
                    var reader = new FileReader();
                    $scope.fyMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedSYMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.syMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }
    
     $scope.uploadedFYMarksheetSem2 = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png' ) {
                    var reader = new FileReader();
                    $scope.fyMarksheetSem2 = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedSYMarksheetSem2 = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.syMarksheetSem2 = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.submitAcademicInfo = function(){
        $scope.optinalSubjectSelected = new Array();
        $scope.optinalSubjectSelectedsem2 = new Array();
        for (var i = 0; i < $scope.optinalSubjectArray.length; i++) {
            if ($scope.optinalSubjectArray[i].selected == true) {
                
                $scope.optinalSubjectSelected.push($scope.optinalSubjectArray[i].name);
                
            }
        }
        for (var i = 0; i < $scope.optinalSubjectArraysem2.length; i++) {
            if ($scope.optinalSubjectArraysem2[i].selected == true) {
                //alert($scope.optinalSubjectArraysem2[i].name);
                $scope.optinalSubjectSelectedsem2.push($scope.optinalSubjectArraysem2[i].name);
                
            }
        }
        
        if($scope.courseName == 'B.A.' ){
            var BAsubjectList = document.getElementById("BASubjectSelection");
            var chks = BAsubjectList.getElementsByTagName("INPUT");
            
            for (var i = 0; i < chks.length; i++) {
                if (chks[i].checked) {
                    $scope.optinalSubjectSelected.push(chks[i].value);
                }
            }
            console.log($scope.optinalSubjectSelected);
            
        }

       $scope.valueAddedSubjectSelected = new Array();
       $scope.valueAddedSubjectSelectedsem2 = new Array();
       
       console.log($scope.optinalSubjectSelected);
    //     for (var i = 0; i < $scope.valueAddedLeng; i++) {
            
    //         if ($scope.valueAddedSubjectArray[i].selected  == true) {
                
    //              $scope.valueAddedSubjectSelected.push($scope.valueAddedSubjectArray[i].name);
    //         }
    //     }
    //     for (var i = 0; i < $scope.valueAddedsem2Leng; i++) {
            
    //         if ($scope.valueAddedSubjectArraysem2[i].selected  == true) {
               
    //              $scope.valueAddedSubjectSelectedsem2.push($scope.valueAddedSubjectArraysem2[i].name);
    //         }
    //     }
        
        // if($scope.valueAddedSubjectSelected == ''){
        //     toastr.error("Mandatory to select at least one semesterwise value added subject.");
        //     return false; 
        // }
        // if($scope.valueAddedSubjectSelectedsem2 == ''){
        //     toastr.error("Mandatory to select at least one semesterwise value added subject.");
        //     return false; 
        // }
        if($scope.isMaharashtra == ''){
            $('#maharashtraYes').focus();
            toastr.error("Please select whether if you are come from maharashtra or not?");
            return false; 
        }
        if($scope.courseYear == "First Year"){
            if($scope.sscExam){
                if(!( $scope.sscExam.match(namePattern))){
                    $('#sscExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#sscExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.sscBoard == ''){
                    $('#sscBoard').focus();
                    toastr.error("Please select board name");
                    return false; 
                }
                if($scope.sscBoard == 'Other'){
                    if($scope.sscBoardName == '' || $scope.sscBoardName == undefined){
                        $('#sscBoardName').focus();
                        toastr.error("Please enter board name");
                        return false; 
                    }else{
                        $scope.sscBoard =  $scope.sscBoard +"-"+$scope.sscBoardName;
                    }
                }
                    if($scope.sscClgName){
                        if(!( $scope.sscClgName.match(namePattern))){
                            $('#sscClgName').focus();
                            toastr.error("Please enter valid college Name.");
                            return false; 
                        }  
                    }else{
                        $('#sscClgName').focus();
                        toastr.error("Please enter college Name.");
                        return false;  
                    }

                        if($scope.sscPassingYear == ''){
                            $('#sscPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.sscSeatNo ==''){
                                $('#sscSeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.sscMarksObtain){
                                    if(!( $scope.sscMarksObtain.match(marksPattern))){
                                        $('#sscMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#sscMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.sscTotalMarks){
                                        if(!( $scope.sscTotalMarks.match(marksPattern))){
                                            $('#sscTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#sscTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    
                                    if(Number($scope.sscTotalMarks) <= Number($scope.sscMarksObtain)){
                                        $('#sscMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.sscMarksheetlink == ''){
                                        if($scope.sscMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
                
             
        //}
        
            if($scope.hscExam){
                if(!( $scope.hscExam.match(namePattern))){
                    $('#hscExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#hscExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.hscBoard == ''){
                    $('#hscBoard').focus();
                    toastr.error("Please select board name");
                    return false; 
                }
                if($scope.hscBoard == 'Other'){
                    if($scope.hscBoardName == '' || $scope.hscBoardName == undefined){
                    
                        $('#hscBoardName').focus();
                        toastr.error("Please enter board name");
                        return false; 
                    }else{
                        $scope.hscBoard =  $scope.hscBoard +"-"+$scope.hscBoardName;
                    }
                    
               
                }
                    if($scope.hscClgName){
                        if(!( $scope.hscClgName.match(namePattern))){
                            $('#hscClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#hscClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.hscPassingYear == ''){
                            $('#hscPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.hscSeatNo ==''){
                                $('#hscSeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.hscMarksObtain){
                                    if(!( $scope.hscMarksObtain.match(marksPattern))){
                                        $('#hscMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#hscMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.hscTotalMarks){
                                        if(!( $scope.hscTotalMarks.match(marksPattern))){
                                            $('#hscTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#hscTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.hscTotalMarks) <= Number($scope.hscMarksObtain)){
                                        $('#hscMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.hscMarksheetlink == ''){
                                        if($scope.hscMarksheet == ''){
                                            toastr.error("Please Upload marksheet.");
                                            return false; 
                                        }
                                    }
                    if($scope.courseName == 'M.Com'){
                        if($scope.tyBComExam){
                            if(!( $scope.tyBComExam.match(namePattern))){
                                $('#tyBCom').focus();
                                toastr.error("Please enter valid exam name.");
                                return false;   
                            }
                        }else{
                            $('#tyBComExam').focus();
                            toastr.error("Please enter exam name.");
                            return false;  
                        }
                            if($scope.tyBComBoard == ''){
                                $('#tyBComBoard').focus();
                                toastr.error("Please select university name");
                                return false; 
                            }
                            if($scope.tyBComBoard == 'Other'){
                                if($scope.tyBComBoardName == '' || $scope.tyBComBoardName == undefined){
                                
                                    $('#tyBComBoardName').focus();
                                    toastr.error("Please enter university name");
                                    return false; 
                                }else{
                                    $scope.tyBComBoard =  $scope.tyBComBoard +"-"+$scope.tyBComBoardName;
                                }
                                
                           
                            }
                                if($scope.tyBComClgName){
                                    if(!( $scope.tyBComClgName.match(namePattern))){
                                        $('#tyBComClgName').focus();
                                        toastr.error("Please enter valid college Name.");
                                        return false; 
                                    }  
                                }else{
                                    $('#tyBComClgName').focus();
                                    toastr.error("Please enter college Name.");
                                    return false;  
                                }
            
                                    if($scope.tyBComPassingYear == ''){
                                        $('#tyBComPassingYear').focus();
                                        toastr.error("Please select passing year.");
                                        return false;
                                    }
                                        if($scope.tyBComSeatNo ==''){
                                            $('#tyBComSeatNo').focus();
                                            toastr.error("Please enter exam seat number.");
                                            return false;
                                        }
                                   
                                            if($scope.tyBComMarksObtain){
                                                if(!( $scope.tyBComMarksObtain.match(marksPattern))){
                                                    $('#tyBComMarksObtain').focus();
                                                    toastr.error("Please enter valid obtain marks.");
                                                    return false;   
                                                }
                                            }else{
                                                $('#tyBComMarksObtain').focus();
                                                toastr.error("Please enter obtain marks.");
                                                return false; 
                                            }
                                                if($scope.tyBComTotalMarks){
                                                    if(!( $scope.tyBComTotalMarks.match(marksPattern))){
                                                        $('#tyBComTotalMarks').focus();
                                                        toastr.error("Please enter valid total marks.");
                                                        return false;   
                                                    }
                                                }else{
                                                    $('#tyBComTotalMarks').focus();
                                                        toastr.error("Please enter total marks.");
                                                        return false; 
                                                }
                                                if(Number($scope.tyBComTotalMarks) <= Number($scope.tyBComMarksObtain)){
                                                    $('#tyBComMarksObtain').focus();
                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                        return false;
                                                }
                                                if($scope.tyBComMarksheetlink == ''){
                                                    if($scope.tyBComMarksheet == ''){
                                                        toastr.error("Please upload marksheet.");
                                                        return false; 
                                                    }
                                                }
                    }
                
             
        }

        if($scope.courseYear == "Second Year"){
        
            if($scope.sscExam){
                if(!( $scope.sscExam.match(namePattern))){
                    $('#sscExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#sscExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.sscBoard == ''){
                    $('#sscBoard').focus();
                    toastr.error("Please select board name");
                    return false; 
                }
                if($scope.sscBoard == 'Other'){
                    if($scope.sscBoardName == '' || $scope.sscBoardName == undefined){
                        $('#sscBoardName').focus();
                        toastr.error("Please enter board name");
                        return false; 
                    }else{
                        $scope.sscBoard =  $scope.sscBoard +"-"+$scope.sscBoardName;
                    }
               
                }
                    if($scope.sscClgName){
                        if(!( $scope.sscClgName.match(namePattern))){
                            $('#sscClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#sscClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.sscPassingYear == ''){
                            $('#sscPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.sscSeatNo ==''){
                                $('#sscSeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.sscMarksObtain){
                                    if(!( $scope.sscMarksObtain.match(marksPattern))){
                                        $('#sscMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#sscMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.sscTotalMarks){
                                        if(!( $scope.sscTotalMarks.match(marksPattern))){
                                            $('#sscTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#sscTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }

                                    
                                    if(Number($scope.sscTotalMarks) <= Number($scope.sscMarksObtain)){
                                        $('#sscMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.sscMarksheetlink == ''){
                                        if($scope.sscMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
                
             
        //}
        
            if($scope.hscExam){
                if(!( $scope.hscExam.match(namePattern))){
                    $('#hscExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#hscExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.hscBoard == ''){
                    $('#hscBoard').focus();
                    toastr.error("Please select board name");
                    return false; 
                }
                if($scope.hscBoard == 'Other'){
                    if($scope.hscBoardName == '' || $scope.hscBoardName == undefined){
                        $('#hscBoardName').focus();
                        toastr.error("Please enter board name");
                        return false; 
                    }else{
                        $scope.hscBoard =  $scope.hscBoard +"-"+$scope.hscBoardName;
                    }
               
                }
                    if($scope.hscClgName){
                        if(!( $scope.hscClgName.match(namePattern))){
                            $('#hscClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#hscClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.hscPassingYear == ''){
                            $('#hscPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.hscSeatNo ==''){
                                $('#hscSeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.hscMarksObtain){
                                    if(!( $scope.hscMarksObtain.match(marksPattern))){
                                        $('#hscMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#hscMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.hscTotalMarks){
                                        if(!( $scope.hscTotalMarks.match(marksPattern))){
                                            $('#hscTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#hscTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.hscTotalMarks) <= Number($scope.hscMarksObtain)){
                                        $('#hscMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.hscMarksheetlink == ''){
                                        if($scope.hscMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
                    if($scope.courseName == 'M.Com'){
                        if($scope.tyBComExam){
                            if(!( $scope.tyBComExam.match(namePattern))){
                                $('#tyBCom').focus();
                                toastr.error("Please enter valid exam name.");
                                return false;   
                            }
                        }else{
                            $('#tyBComExam').focus();
                            toastr.error("Please enter exam name.");
                            return false;  
                        }
                            if($scope.tyBComBoard == ''){
                                $('#tyBComBoard').focus();
                                toastr.error("Please select university name");
                                return false; 
                            }
                            if($scope.tyBComBoard == 'Other'){
                                if($scope.tyBComBoardName == '' || $scope.tyBComBoardName == undefined){
                                
                                    $('#tyBComBoardName').focus();
                                    toastr.error("Please enter university name");
                                    return false; 
                                }else{
                                    $scope.tyBComBoard =  $scope.tyBComBoard +"-"+$scope.tyBComBoardName;
                                }
                                
                            
                            }
                                if($scope.tyBComClgName){
                                    if(!( $scope.tyBComClgName.match(namePattern))){
                                        $('#tyBComClgName').focus();
                                        toastr.error("Please enter valid college Name.");
                                        return false; 
                                    }  
                                }else{
                                    $('#tyBComClgName').focus();
                                    toastr.error("Please enter college name.");
                                    return false;  
                                }
            
                                    if($scope.tyBComPassingYear == ''){
                                        $('#tyBComPassingYear').focus();
                                        toastr.error("Please select passing year.");
                                        return false;
                                    }
                                        if($scope.tyBComSeatNo ==''){
                                            $('#tyBComSeatNo').focus();
                                            toastr.error("Please enter exam seat number.");
                                            return false;
                                        }
                                    
                                            if($scope.tyBComMarksObtain){
                                                if(!( $scope.tyBComMarksObtain.match(marksPattern))){
                                                    $('#tyBComMarksObtain').focus();
                                                    toastr.error("Please enter valid obtain marks.");
                                                    return false;   
                                                }
                                            }else{
                                                $('#tyBComMarksObtain').focus();
                                                toastr.error("Please enter obtain marks.");
                                                return false; 
                                            }
                                                if($scope.tyBComTotalMarks){
                                                    if(!( $scope.tyBComTotalMarks.match(marksPattern))){
                                                        $('#tyBComTotalMarks').focus();
                                                        toastr.error("Please enter valid total marks.");
                                                        return false;   
                                                    }
                                                }else{
                                                    $('#tyBComTotalMarks').focus();
                                                        toastr.error("Please enter total marks.");
                                                        return false; 
                                                }
                                                if(Number($scope.tyBComTotalMarks) <= Number($scope.tyBComMarksObtain)){
                                                    $('#tyBComMarksObtain').focus();
                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                        return false;
                                                }
                                                if($scope.tyBComMarksheetlink == ''){
                                                    if($scope.tyBComMarksheet == ''){
                                                        toastr.error("Please upload marksheet.");
                                                        return false; 
                                                    }
                                                }
                    }

            if($scope.fyExam){
                if(!( $scope.fyExam.match(namePattern))){
                    $('#fyExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#fyExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.fyBoard == ''){
                    $('#fyBoard').focus();
                    toastr.error("Please select university name");
                    return false; 
                }
                if($scope.fyBoard == 'Other'){
                    if($scope.fyBoardName == '' || $scope.fyBoardName == undefined){
                        $('#fyBoardName').focus();
                        toastr.error("Please enter university name");
                        return false; 
                    }else{
                        $scope.fyBoard =  $scope.fyBoard +"-"+$scope.fyBoardName;
                    }
                    
               
                }
                    if($scope.fyClgName){
                        if(!( $scope.fyClgName.match(namePattern))){
                            $('#fyClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#fyClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.fyPassingYear == ''){
                            $('#fyPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.fySeatNo ==''){
                                $('#fySeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.fyMarksObtain){
                                    if(!( $scope.fyMarksObtain.match(marksPattern))){
                                        $('#fyMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#fyMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.fyTotalMarks){
                                        if(!( $scope.fyTotalMarks.match(marksPattern))){
                                            $('#fyTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#fyTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.fyTotalMarks) <= Number($scope.fyMarksObtain)){
                                        $('#fyMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                
                                    if($scope.fyMarksheetlink == ''){
                                        if($scope.fyMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
                                    
            if($scope.fyExamSem2){
                if(!( $scope.fyExamSem2.match(namePattern))){
                    $('#fyExamSem2').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#fyExamSem2').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.fyBoardSem2 == ''){
                    $('#fyBoardSem2').focus();
                    toastr.error("Please select university name");
                    return false; 
                }
                if($scope.fyBoardSem2 == 'Other'){
                    if($scope.fyBoardNamesem2 == '' || $scope.fyBoardNamesem2 == undefined){
                        $('#fyBoardNamesem2').focus();
                        toastr.error("Please enter university name");
                        return false; 
                    }else{
                        $scope.fyBoardSem2 =  $scope.fyBoardSem2 +"-"+$scope.fyBoardNamesem2;
                    }
                    
               
                }
                    if($scope.fyClgNameSem2){
                        if(!( $scope.fyClgNameSem2.match(namePattern))){
                            $('#fyClgNameSem2').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#fyClgNameSem2').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.fyPassingYearSem2 == ''){
                            $('#fyPassingYearSem2').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.fySeatNoSem2 ==''){
                                $('#fySeatNoSem2').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.fyMarksObtainSem2){
                                    if(!( $scope.fyMarksObtainSem2.match(marksPattern))){
                                        $('#fyMarksObtainSem2').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#fyMarksObtainSem2').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.fyTotalMarksSem2){
                                        if(!( $scope.fyTotalMarksSem2.match(marksPattern))){
                                            $('#fyTotalMarksSem2').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#fyTotalMarksSem2').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.fyTotalMarksSem2) <= Number($scope.fyMarksObtainSem2)){
                                        $('#fyMarksObtainSem2').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                
                                    if($scope.fyMarksheetSem2link == ''){
                                        if($scope.fyMarksheetSem2 == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
             
        }

        if($scope.courseYear == "Third Year"){
        
            if($scope.sscExam){
                if(!( $scope.sscExam.match(namePattern))){
                    $('#sscExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#sscExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.sscBoard == ''){
                    $('#sscBoard').focus();
                    toastr.error("Please select board name");
                    return false; 
                }
                if($scope.sscBoard == 'Other'){

                    if($scope.sscBoardName == '' || $scope.sscBoardName == undefined){
                        $('#sscBoardName').focus();
                        toastr.error("Please enter board name");
                        return false; 
                    }else{
                        $scope.sscBoard =  $scope.sscBoard +"-"+$scope.sscBoardName;
                    }
               
                }
                    if($scope.sscClgName){
                        if(!( $scope.sscClgName.match(namePattern))){
                            $('#sscClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#sscClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.sscPassingYear == ''){
                            $('#sscPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.sscSeatNo ==''){
                                $('#sscSeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.sscMarksObtain){
                                    if(!( $scope.sscMarksObtain.match(marksPattern))){
                                        $('#sscMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#sscMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.sscTotalMarks){
                                        if(!( $scope.sscTotalMarks.match(marksPattern))){
                                            $('#sscTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#sscTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.sscTotalMarks) <= Number($scope.sscMarksObtain)){
                                        $('#sscMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.sscMarksheetlink == ''){
                                        if($scope.sscMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
                
             
        //}
        
            if($scope.hscExam){
                if(!( $scope.hscExam.match(namePattern))){
                    $('#hscExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#hscExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.hscBoard == ''){
                    $('#hscBoard').focus();
                    toastr.error("Please select board name");
                    return false; 
                }
                if($scope.hscBoard == 'Other'){
                    if($scope.hscBoardName == '' || $scope.hscBoardName == undefined){
                        $('#hscBoardName').focus();
                        toastr.error("Please enter board name");
                        return false; 
                    }else{
                        $scope.hscBoard =  $scope.hscBoard +"-"+$scope.hscBoardName;
                    }
                }
                    if($scope.hscClgName){
                        if(!( $scope.hscClgName.match(namePattern))){
                            $('#hscClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#hscClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.hscPassingYear == ''){
                            $('#hscPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.hscSeatNo ==''){
                                $('#hscSeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.hscMarksObtain){
                                    if(!( $scope.hscMarksObtain.match(marksPattern))){
                                        $('#hscMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#hscMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.hscTotalMarks){
                                        if(!( $scope.hscTotalMarks.match(marksPattern))){
                                            $('#hscTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#hscTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.hscTotalMarks) <= Number($scope.hscMarksObtain)){
                                        $('#hscMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.hscMarksheetlink == ''){
                                        if($scope.hscMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }

                    if($scope.courseName == 'M.Com'){
                        if($scope.tyBComExam){
                            if(!( $scope.tyBComExam.match(namePattern))){
                                $('#tyBCom').focus();
                                toastr.error("Please enter valid exam name.");
                                return false;   
                            }
                        }else{
                            $('#tyBComExam').focus();
                            toastr.error("Please enter exam name.");
                            return false;  
                        }
                            if($scope.tyBComBoard == ''){
                                $('#tyBComBoard').focus();
                                toastr.error("Please select university name");
                                return false; 
                            }
                            if($scope.tyBComBoard == 'Other'){
                                if($scope.tyBComBoardName == '' || $scope.tyBComBoardName == undefined){
                                
                                    $('#tyBComBoardName').focus();
                                    toastr.error("Please enter university name");
                                    return false; 
                                }else{
                                    $scope.tyBComBoard =  $scope.tyBComBoard +"-"+$scope.tyBComBoardName;
                                }
                                
                            
                            }
                                if($scope.tyBComClgName){
                                    if(!( $scope.tyBComClgName.match(namePattern))){
                                        $('#tyBComClgName').focus();
                                        toastr.error("Please enter valid college name.");
                                        return false; 
                                    }  
                                }else{
                                    $('#tyBComClgName').focus();
                                    toastr.error("Please enter college name.");
                                    return false;  
                                }
            
                                    if($scope.tyBComPassingYear == ''){
                                        $('#tyBComPassingYear').focus();
                                        toastr.error("Please select passing year.");
                                        return false;
                                    }
                                        if($scope.tyBComSeatNo ==''){
                                            $('#tyBComSeatNo').focus();
                                            toastr.error("Please enter exam seat number.");
                                            return false;
                                        }
                                    
                                            if($scope.tyBComMarksObtain){
                                                if(!( $scope.tyBComMarksObtain.match(marksPattern))){
                                                    $('#tyBComMarksObtain').focus();
                                                    toastr.error("Please enter valid obtain marks.");
                                                    return false;   
                                                }
                                            }else{
                                                $('#tyBComMarksObtain').focus();
                                                toastr.error("Please enter obtain marks.");
                                                return false; 
                                            }
                                                if($scope.tyBComTotalMarks){
                                                    if(!( $scope.tyBComTotalMarks.match(marksPattern))){
                                                        $('#tyBComTotalMarks').focus();
                                                        toastr.error("Please enter valid total marks.");
                                                        return false;   
                                                    }
                                                }else{
                                                    $('#tyBComTotalMarks').focus();
                                                        toastr.error("Please enter total marks.");
                                                        return false; 
                                                }
                                                if(Number($scope.tyBComTotalMarks) <= Number($scope.tyBComMarksObtain)){
                                                    $('#tyBComMarksObtain').focus();
                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                        return false;
                                                }
                                                if($scope.tyBComMarksheetlink == ''){
                                                    if($scope.tyBComMarksheet == ''){
                                                        toastr.error("Please upload marksheet.");
                                                        return false; 
                                                    }
                                                }
                    }


            if($scope.fyExam){
                if(!( $scope.fyExam.match(namePattern))){
                    $('#fyExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#fyExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.fyBoard == ''){
                    $('#fyBoard').focus();
                    toastr.error("Please select university name");
                    return false; 
                }
                if($scope.fyBoard == 'Other'){
                    if($scope.fyBoardName == '' || $scope.fyBoardName == undefined){
                        $('#fyBoardName').focus();
                        toastr.error("Please enter university name");
                        return false; 
                    }else{
                        $scope.fyBoard =  $scope.fyBoard +"-"+$scope.fyBoardName;
                    }
               
                }
                    if($scope.fyClgName){
                        if(!( $scope.fyClgName.match(namePattern))){
                            $('#fyClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#fyClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.fyPassingYear == ''){
                            $('#fyPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.fySeatNo ==''){
                                $('#fySeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.fyMarksObtain){
                                    if(!( $scope.fyMarksObtain.match(marksPattern))){
                                        $('#fyMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#fyMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.fyTotalMarks){
                                        if(!( $scope.fyTotalMarks.match(marksPattern))){
                                            $('#fyTotalMarks').focus();
                                            toastr.error("Please enter valid Total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#fyTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.fyTotalMarks) <= Number($scope.fyMarksObtain)){
                                        $('#fyMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.fyMarksheetlink == ''){
                                        if($scope.fyMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
                                    
            if($scope.fyExamSem2){
                if(!( $scope.fyExamSem2.match(namePattern))){
                    $('#fyExamSem2').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#fyExamSem2').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.fyBoardSem2 == ''){
                    $('#fyBoardSem2').focus();
                    toastr.error("Please select university name");
                    return false; 
                }
                if($scope.fyBoardSem2 == 'Other'){
                    if($scope.fyBoardNamesem2 == '' || $scope.fyBoardNamesem2 == undefined){
                        $('#fyBoardNamesem2').focus();
                        toastr.error("Please enter university name");
                        return false; 
                    }else{
                        $scope.fyBoardSem2 =  $scope.fyBoardSem2 +"-"+$scope.fyBoardNamesem2;
                    }
                    
               
                }
                    if($scope.fyClgNameSem2){
                        if(!( $scope.fyClgNameSem2.match(namePattern))){
                            $('#fyClgNameSem2').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#fyClgNameSem2').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.fyPassingYearSem2 == ''){
                            $('#fyPassingYearSem2').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.fySeatNoSem2 ==''){
                                $('#fySeatNoSem2').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.fyMarksObtainSem2){
                                    if(!( $scope.fyMarksObtainSem2.match(marksPattern))){
                                        $('#fyMarksObtainSem2').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#fyMarksObtainSem2').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.fyTotalMarksSem2){
                                        if(!( $scope.fyTotalMarksSem2.match(marksPattern))){
                                            $('#fyTotalMarksSem2').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#fyTotalMarksSem2').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.fyTotalMarksSem2) <= Number($scope.fyMarksObtainSem2)){
                                        $('#fyMarksObtainSem2').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                
                                    if($scope.fyMarksheetSem2link == ''){
                                        if($scope.fyMarksheetSem2 == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
             
        
            if($scope.syExam){
                if(!( $scope.syExam.match(namePattern))){
                    $('#syExam').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#syExam').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.syBoard == ''){
                    $('#syBoard').focus();
                    toastr.error("Please select university name");
                    return false; 
                }
                if($scope.syBoard == 'Other'){
                    if($scope.syBoardName== '' || $scope.syBoardName == undefined){
                        $('#syBoardName').focus();
                        toastr.error("Please enter university name");
                        return false; 
                    }else{
                        $scope.syBoard =  $scope.syBoard +"-"+$scope.syBoardName;
                    }
                   
               
                }
                    if($scope.syClgName){
                        if(!( $scope.syClgName.match(namePattern))){
                            $('#syClgName').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#syClgName').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.syPassingYear == ''){
                            $('#syPassingYear').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.sySeatNo ==''){
                                $('#sySeatNo').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.syMarksObtain){
                                    if(!( $scope.syMarksObtain.match(marksPattern))){
                                        $('#syMarksObtain').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#syMarksObtain').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.syTotalMarks){
                                        if(!( $scope.syTotalMarks.match(marksPattern))){
                                            $('#syTotalMarks').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#syTotalMarks').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.syTotalMarks) <= Number($scope.syMarksObtain)){
                                        $('#syMarksObtain').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.syMarksheetlink == ''){
                                        if($scope.syMarksheet == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
                                    
            if($scope.syExamSem2){
                if(!( $scope.syExamSem2.match(namePattern))){
                    $('#syExamSem2').focus();
                    toastr.error("Please enter valid exam name.");
                    return false;   
                }
            }else{
                $('#syExamSem2').focus();
                toastr.error("Please enter exam name.");
                return false;  
            }
                if($scope.syBoardSem2 == ''){
                    $('#syBoardSem2').focus();
                    toastr.error("Please select university name");
                    return false; 
                }
                if($scope.syBoardSem2 == 'Other'){
                    if($scope.syBoardNameSem2== '' || $scope.syBoardNameSem2 == undefined){
                        $('#syBoardNameSem2').focus();
                        toastr.error("Please enter university name");
                        return false; 
                    }else{
                        $scope.syBoardSem2 =  $scope.syBoardSem2 +"-"+$scope.syBoardNameSem2;
                    }
                   
               
                }
                    if($scope.syClgNameSem2){
                        if(!( $scope.syClgNameSem2.match(namePattern))){
                            $('#syClgNameSem2').focus();
                            toastr.error("Please enter valid college name.");
                            return false; 
                        }  
                    }else{
                        $('#syClgNameSem2').focus();
                        toastr.error("Please enter college name.");
                        return false;  
                    }

                        if($scope.syPassingYearSem2 == ''){
                            $('#syPassingYearSem2').focus();
                            toastr.error("Please select passing year.");
                            return false;
                        }
                            if($scope.sySeatNoSem2 ==''){
                                $('#sySeatNoSem2').focus();
                                toastr.error("Please enter exam seat number.");
                                return false;
                            }
                       
                                if($scope.syMarksObtainSem2){
                                    if(!( $scope.syMarksObtainSem2.match(marksPattern))){
                                        $('#syMarksObtainSem2').focus();
                                        toastr.error("Please enter valid obtain marks.");
                                        return false;   
                                    }
                                }else{
                                    $('#syMarksObtainSem2').focus();
                                    toastr.error("Please enter obtain marks.");
                                    return false; 
                                }
                                    if($scope.syTotalMarksSem2){
                                        if(!( $scope.syTotalMarksSem2.match(marksPattern))){
                                            $('#syTotalMarksSem2').focus();
                                            toastr.error("Please enter valid total marks.");
                                            return false;   
                                        }
                                    }else{
                                        $('#syTotalMarksSem2').focus();
                                            toastr.error("Please enter total marks.");
                                            return false; 
                                    }
                                    if(Number($scope.syTotalMarksSem2) <= Number($scope.syMarksObtainSem2)){
                                        $('#syMarksObtainSem2').focus();
                                            toastr.error("Please enter correct total marks and obtain marks.");
                                            return false;
                                    }
                                    if($scope.syMarksheetlinkSem2 == ''){
                                        if($scope.syMarksheetSem2 == ''){
                                            toastr.error("Please upload marksheet.");
                                            return false; 
                                        }
                                    }
        }
        if($scope.isgapReason){
            if($scope.isgapReason == 'gapReasonYes'){
                if($scope.gapReason == ''){
                    $('#gapReason').focus();
                    toastr.error("Please enter gap reason is required.");
                    return false;
                }
            }
        }else{
            $('#gapReasonYes').focus();
            toastr.error("Please Select If there is a gap reason or not.");
            return false;
        }
        if($scope.appliedOtherCollege){
            $scope.disabledSubmitAcademicInfoButton = true;
            $scope.ShowSpinnerStatus = true;
            $http({
                method: 'POST',
                url: window.site_url +'AdmissionForm/submitAcademicInfo',
                processData: false,
                transformRequest: function(data) {
                    var formData = new FormData();                    
                        
                    formData.append("csrf_token_name", csrfHash);
                    formData.append("studentId", JSON.parse(localStorage.getItem("StudentId")));
                    formData.append("optinalSubjectSelected", $scope.optinalSubjectSelected);
                    formData.append("optinalSubjectSelectedsem2", $scope.optinalSubjectSelectedsem2);
                    
                    formData.append("valueAddedSubjectSelected", $scope.valueAddedSubjectSelected);
                    formData.append("valueAddedSubjectSelectedsem2", $scope.valueAddedSubjectSelectedsem2);
                    
                    formData.append("isMaharashtra", $scope.isMaharashtra);
                    formData.append("sscExam",   $scope.sscExam);
                    formData.append("sscBoard", $scope.sscBoard);
                    formData.append("sscClgName", $scope.sscClgName);
                    formData.append("sscPassingYear", $scope.sscPassingYear);
                    formData.append("sscSeatNo", $scope.sscSeatNo);
                    formData.append("sscMarksObtain", $scope.sscMarksObtain );
                    formData.append("sscTotalMarks", $scope.sscTotalMarks);
                    formData.append("sscPercentage", $scope.sscPercentageValue);
                    formData.append("sscMarksheet", $scope.sscMarksheet);
                    formData.append("hscExam", $scope.hscExam);
                    formData.append("hscBoard", $scope.hscBoard);
                    formData.append("hscClgName", $scope.hscClgName);
                    formData.append("hscPassingYear", $scope.hscPassingYear);
                    formData.append("hscSeatNo", $scope.hscSeatNo);
                    formData.append("hscMarksObtain", $scope.hscMarksObtain);
                    formData.append("hscTotalMarks", $scope.hscTotalMarks);
                    formData.append("hscPercentage", $scope.hscPercentageValue);
                    formData.append("hscMarksheet", $scope.hscMarksheet);
                    formData.append("tyBComExam", $scope.tyBComExam);
                    formData.append("tyBComBoard", $scope.tyBComBoard);
                    formData.append("tyBComClgName", $scope.tyBComClgName);
                    formData.append("tyBComPassingYear", $scope.tyBComPassingYear);
                    formData.append("tyBComSeatNo", $scope.tyBComSeatNo);
                    formData.append("tyBComMarksObtain", $scope.tyBComMarksObtain);
                    formData.append("tyBComTotalMarks", $scope.tyBComTotalMarks);
                    formData.append("tyBComPercentage", $scope.tyBComPercentageValue);
                    formData.append("tyBComMarksheet", $scope.tyBComMarksheet);
                    formData.append("fyExam", $scope.fyExam);
                    formData.append("fyBoard", $scope.fyBoard);
                    formData.append("fyClgName", $scope.fyClgName);
                    formData.append("fyPassingYear", $scope.fyPassingYear);
                    formData.append("fySeatNo", $scope.fySeatNo);
                    formData.append("fyMarksObtain", $scope.fyMarksObtain);
                    formData.append("fyTotalMarks", $scope.fyTotalMarks);
                    formData.append("fyPercentage", $scope.fyPercentageValue);
                    formData.append("fyMarksheet", $scope.fyMarksheet );
                    formData.append("syExam", $scope.syExam);
                    formData.append("syBoard", $scope.syBoard);
                    formData.append("syClgName", $scope.syClgName);
                    formData.append("syPassingYear", $scope.syPassingYear);
                    formData.append("sySeatNo", $scope.sySeatNo);
                    formData.append("syMarksObtain", $scope.syMarksObtain);
                    formData.append("syTotalMarks", $scope.syTotalMarks);
                    formData.append("syPercentage", $scope.syPercentageValue);
                    formData.append("syMarksheet", $scope.syMarksheet);
                    
                    formData.append("fyExamSem2", $scope.fyExamSem2);
                    formData.append("fyBoardSem2", $scope.fyBoardSem2);
                    formData.append("fyClgNameSem2", $scope.fyClgNameSem2);
                    formData.append("fyPassingYearSem2", $scope.fyPassingYearSem2);
                    formData.append("fySeatNoSem2", $scope.fySeatNoSem2);
                    formData.append("fyMarksObtainSem2", $scope.fyMarksObtainSem2);
                    formData.append("fyTotalMarksSem2", $scope.fyTotalMarksSem2);
                    formData.append("fyPercentageSem2", $scope.fyPercentageValue2);
                    formData.append("fyMarksheetSem2", $scope.fyMarksheetSem2 );
                    formData.append("syExamSem2", $scope.syExamSem2);
                    formData.append("syBoardSem2", $scope.syBoardSem2);
                    formData.append("syClgNameSem2", $scope.syClgNameSem2);
                    formData.append("syPassingYearSem2", $scope.syPassingYearSem2);
                    formData.append("sySeatNoSem2", $scope.sySeatNoSem2);
                    formData.append("syMarksObtainSem2", $scope.syMarksObtainSem2);
                    formData.append("syTotalMarksSem2", $scope.syTotalMarksSem2);
                    formData.append("syPercentageSem2", $scope.syPercentageValueSem2);
                    formData.append("syMarksheetSem2", $scope.syMarksheetSem2);
                    formData.append("appearingYear", $scope.appearingYear);
                    
                    formData.append("isgapReason", $scope.isgapReason);
                    //if($scope.isgapReason == 'gapReasonYes'){
                    formData.append("gapReason", $scope.gapReason);
                    //}
                    formData.append("appliedOtherCollege", $scope.appliedOtherCollege);
                    formData.append("firstName", $scope.firstName);
                    formData.append("lastName", $scope.lastName);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            })
            .then(function(response) {
                if (response.data.status.status == "1") {

                    toastr.success(response.data.status.message);
                    $('html, body').animate({scrollTop: '0px'}, 1000);
                    $scope.init(); 
                
                }else{
                    toastr.error(response.data.status.message);
                    $scope.disabledSubmitAcademicInfoButton = false;
                    $("#loadingDiv").css('display','none');
                    $scope.ShowSpinnerStatus = false;
                }
            }, function(response) {
            })
        } else {
            $('#appliedOtherCollege').focus();
            toastr.error("please Select if student applied to any other college.");
        }
      

    }

    
    $scope.getStudentAcademicInfo = function (storedNames) {
        $scope.student_id = storedNames;
        
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AdmissionForm/getStudentAcademicInfo', {
            'csrf_token_name': csrfHash,
            student_id : $scope.student_id ,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.studentAcademicInfoArray = response.data.data[0];
                $scope.optionalArray = [];
                $scope.optionalArray = response.data.data[0].optinal_subject_selected.split(',');
                
                if($scope.optionalArray != ''){
                   
                    if($scope.courseName == 'B.A.' ){
                        var BAsubjectList = document.getElementById("BASubjectSelection");
                        var chks = BAsubjectList.getElementsByTagName("INPUT");
                        for (var k = 0; k < chks.length; k++) {
                            for (var l = 0; l < chks.length; l++) {
                                if($scope.optionalArray[k] == chks[l].value ){
                                    chks[l].checked = true;
                               
                                }
                            }
                        }
                       
                        
                    }else{
                    var j=0;
                    var i=0;
                    
                    while(i< $scope.optionalArray.length){
                      
                            if($scope.optionalArray[i] == $scope.optinalSubjectArray[j].name ){
                                $scope.optinalSubjectArray[j].selected = true;
                                
                                i++;
                            }else{
                                j++;
                            }
                        
                    }
                    }
                }
                $scope.optionalArraysem2 = [];
                $scope.optionalArraysem2 = response.data.data[0].optinal_subject_selected_sem2.split(',');
                //console.log($scope.optionalArraysem2);
                if($scope.optionalArraysem2 != ''){
                    
                    var j=0;
                    var i=0;
                    while(i< $scope.optionalArraysem2.length){
                        if($scope.optionalArraysem2[i] == $scope.optinalSubjectArraysem2[j].name ){
                            $scope.optinalSubjectArraysem2[j].selected = true;
                            i++;
                        }else{
                            j++;
                        }
                    }
                }

                // $scope.valueAddedArray = [];
                // $scope.valueAddedArray = response.data.data[0].value_added_subject_selected.split(',');
                
                // if($scope.valueAddedArray != ''){
                    
                //     var j=0;
                //     var i=0;
                //     while(i< $scope.valueAddedArray.length){
                //         if($scope.valueAddedArray[i] == $scope.valueAddedSubjectArray[j].name ){
                //             $scope.valueAddedSubjectArray[j].selected = true;
                //             i++;
                //         }else{
                //             j++;
                //         }
                //     }
                // }
                // $scope.valueAddedArraysem2 = [];
                // $scope.valueAddedArraysem2 = response.data.data[0].value_added_subject_selected_sem2.split(',');
                // if($scope.valueAddedArraysem2 != ''){
                    
                //     var j=0;
                //     var i=0;
                //     while(i< $scope.valueAddedArraysem2.length){
                //         if($scope.valueAddedArraysem2[i] == $scope.valueAddedSubjectArraysem2[j].name ){
                //             $scope.valueAddedSubjectArraysem2[j].selected = true;
                //             i++;
                //         }else{
                //             j++;
                //         }
                //     }
                // }
                $scope.isMaharashtra = response.data.data[0].is_maharashtra ;
                $scope.sscExam = response.data.data[0].ssc_exam_name ;
                $scope.sscBoardOther = response.data.data[0].ssc_board_university_name.split("-");
                $scope.sscBoard = $scope.sscBoardOther[0];
                $scope.sscBoardName = $scope.sscBoardOther[1];
                //alert($scope.sscBoardName);
                $scope.showSscAddUniversityNameDiv();
                $scope.sscClgName = response.data.data[0].ssc_school_clg_name ;
                $scope.sscPassingYear = response.data.data[0].ssc_year_of_passing ;
                $scope.sscSeatNo = response.data.data[0].ssc_seat_number ;
                $scope.sscMarksObtain = response.data.data[0].ssc_marks_obtained ;
                $scope.sscTotalMarks = response.data.data[0].ssc_total_marks ;
                $scope.sscPercentageValue = response.data.data[0].ssc_percentage ;
                $scope.sscMarksheetlink = response.data.data[0].ssc_uploaded_documents ;

                $scope.hscExam = response.data.data[0].hsc_exam_name ;
                $scope.hscBoardOther = response.data.data[0].hsc_board_university_name.split("-");
                $scope.hscBoard = $scope.hscBoardOther[0];
                $scope.hscBoardName = $scope.hscBoardOther[1];
                $scope.showHscAddUniversityNameDiv();
                $scope.hscClgName = response.data.data[0].hsc_school_clg_name ;
                $scope.hscPassingYear = response.data.data[0].hsc_year_of_passing ;
                $scope.hscSeatNo = response.data.data[0].hsc_seat_number ;
                $scope.hscMarksObtain = response.data.data[0].hsc_marks_obtained ;
                $scope.hscTotalMarks = response.data.data[0].hsc_total_marks ;
                $scope.hscPercentageValue = response.data.data[0].hsc_percentage ;
                $scope.hscMarksheetlink  = response.data.data[0].hsc_uploaded_documents ;  

                $scope.tyBComExam = response.data.data[0].tybcom_exam_name ;
                $scope.tyBComBoardOther = response.data.data[0].tybcom_board_university_name.split("-");
                $scope.tyBComBoard = $scope.tyBComBoardOther[0];
                $scope.tyBComBoardName = $scope.tyBComBoardOther[1];
                $scope.showtyBComAddUniversityNameDiv();
                $scope.tyBComClgName = response.data.data[0].tybcom_school_clg_name ;
                $scope.tyBComPassingYear = response.data.data[0].tybcom_year_of_passing ;
                $scope.tyBComSeatNo = response.data.data[0].tybcom_seat_number ;
                $scope.tyBComMarksObtain = response.data.data[0].tybcom_marks_obtained ;
                $scope.tyBComTotalMarks = response.data.data[0].tybcom_total_marks ;
                $scope.tyBComPercentageValue = response.data.data[0].tybcom_percentage ;
                $scope.tyBComMarksheetlink  = response.data.data[0].tybcom_uploaded_documents ;  

                $scope.fyExam = response.data.data[0].fy_exam_name ;
                $scope.fyBoardOther = response.data.data[0].fy_board_university_name.split("-");
                $scope.fyBoard = $scope.fyBoardOther[0];
                $scope.fyBoardName = $scope.fyBoardOther[1];
                $scope.showFyAddUniversityNameDiv();
                $scope.fyClgName = response.data.data[0].fy_school_clg_name ;
                $scope.fyPassingYear = response.data.data[0].fy_year_of_passing ;
                $scope.fySeatNo = response.data.data[0].fy_seat_number ;
                $scope.fyMarksObtain = response.data.data[0].fy_marks_obtained ;
                $scope.fyTotalMarks = response.data.data[0].fy_total_marks ;
                $scope.fyPercentageValue  = response.data.data[0].fy_percentage ;
                $scope.fyMarksheetlink = response.data.data[0].fy_uploaded_documents ;

                $scope.syExam = response.data.data[0].sy_exam_name ;
                $scope.syBoardOther = response.data.data[0].sy_board_university_name.split("-");
                $scope.syBoard = $scope.syBoardOther[0];
                $scope.syBoardName = $scope.syBoardOther[1];
                $scope.showSyAddUniversityNameDiv();
                $scope.syClgName = response.data.data[0].sy_school_clg_name ;
                $scope.syPassingYear = response.data.data[0].sy_year_of_passing ;
                $scope.sySeatNo = response.data.data[0].sy_seat_number ;
                $scope.syMarksObtain = response.data.data[0].sy_marks_obtained ;
                $scope.syTotalMarks = response.data.data[0].sy_total_marks ;
                $scope.syPercentageValue = response.data.data[0].sy_percentage ;
                $scope.syMarksheetlink = response.data.data[0].sy_uploaded_documents ;
                
                $scope.fyExamSem2 = response.data.data[0].fy_exam_name_sem2 ;
                $scope.fyBoardOthersem2 = response.data.data[0].fy_board_university_name_sem2.split("-");
                $scope.fyBoardSem2 = $scope.fyBoardOthersem2[0];
                $scope.fyBoardNamesem2 = $scope.fyBoardOthersem2[1];
                $scope.showFyAddUniversityNameDivSem2();
                $scope.fyClgNameSem2 = response.data.data[0].fy_school_clg_name_sem2 ;
                $scope.fyPassingYearSem2 = response.data.data[0].fy_year_of_passing_sem2 ;
                $scope.fySeatNoSem2 = response.data.data[0].fy_seat_number_sem2 ;
                $scope.fyMarksObtainSem2 = response.data.data[0].fy_marks_obtained_sem2 ;
                $scope.fyTotalMarksSem2 = response.data.data[0].fy_total_marks_sem2 ;
                $scope.fyPercentageValue2  = response.data.data[0].fy_percentage_sem2 ;
                $scope.fyMarksheetSem2link = response.data.data[0].fy_uploaded_documents_sem2 ;
 
                $scope.syExamSem2 = response.data.data[0].sy_exam_name_sem2 ;
                $scope.syBoardOtherSem2 = response.data.data[0].sy_board_university_name_sem2.split("-");
                $scope.syBoardSem2 = $scope.syBoardOtherSem2[0];
                $scope.syBoardNameSem2 = $scope.syBoardOtherSem2[1];
                $scope.showSyAddUniversityNameDivSem2();
                $scope.syClgNameSem2 = response.data.data[0].sy_school_clg_name_sem2 ;
                $scope.syPassingYearSem2 = response.data.data[0].sy_year_of_passing_sem2 ;
                $scope.sySeatNoSem2 = response.data.data[0].sy_seat_number_sem2 ;
                $scope.syMarksObtainSem2 = response.data.data[0].sy_marks_obtained_sem2 ;
                $scope.syTotalMarksSem2 = response.data.data[0].sy_total_marks_sem2 ;
                $scope.syPercentageValueSem2 = response.data.data[0].sy_percentage_sem2 ;
                $scope.syMarksheetlinkSem2 = response.data.data[0].sy_uploaded_documents_sem2 ;

                $scope.appearingYear = response.data.data[0].student_repeater_year ;
                
                $scope.isgapReason = response.data.data[0].is_gap_reason;
                $scope.gapReason = response.data.data[0].gap_reason ;
                $scope.appliedOtherCollege = response.data.data[0].is_applied_other_clg ;
                
                
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.submitParentInfo = function(){
        if($scope.fatherFirstName != '' || $scope.fatherMiddleName != '' || $scope.fatherLastName !='' || $scope.fatherEmail !='' || $scope.fatherMobileNo !='' || $scope.fatherOccupation !='' || $scope.fathercGovtEmp != '' || $scope.fatherAnnualIncome !='' || $scope.fatherecoBack != '' || $scope.motherFirstName != ''|| $scope.motherMiddleName != '' || $scope.motherLastName !='' || $scope.motherEmail != '' || $scope.motherMobileNo != '' || $scope.motherOccupation != '' || $scope.mothercGovtEmp != '' || $scope.motherAnnualIncome != '' || $scope.motherecoBack != '' || $scope.guaradianFirstName !='' || $scope.guaradianMiddleName !='' || $scope.guaradianLastName !='' || $scope.guaradianEmail !='' || $scope.guaradianMobileNo !='' || $scope.guaradianOccupation !='' || $scope.guaradiancGovtEmp  != '' || $scope.guaradianAnnualIncome !='' || $scope.guaradianecoBack != ''){
            
            if($scope.fatherFirstName != '' || $scope.fatherMiddleName != '' || $scope.fatherLastName !='' || $scope.fatherEmail !='' || $scope.fatherMobileNo !='' || $scope.fatherOccupation !='' || $scope.fathercGovtEmp != '' || $scope.fatherAnnualIncome !='' || $scope.fatherecoBack != ''){ 
                if($scope.fatherFirstName){
                    if(!($scope.fatherFirstName.match(namePattern))){
                        $('#fatherFirstName').focus();
                        toastr.error("Please enter valid father first name.");
                        return false;   
                    }
                }else{
                    $('#fatherFirstName').focus();
                    toastr.error("Please enter father first name.");
                    return false;   
                }

                    if($scope.fatherMiddleName){
                        if(!($scope.fatherMiddleName.match(namePattern))){
                            $('#fatherMiddleName').focus();
                            toastr.error("Please enter valid father middle name.");
                            return false;   
                        }
                    }else{
                        $('#fatherMiddleName').focus();
                        toastr.error("Please enter father middle name.");
                        return false;   
                    }
                        if($scope.fatherLastName ){
                            if(!($scope.fatherLastName.match(namePattern))){
                                $('#fatherLastName').focus();
                                toastr.error("Please enter valid Father last name.");
                                return false;   
                            }
                        }else{
                            $('#fatherLastName').focus();
                                toastr.error("Please enter father last name.");
                                return false;  
                        }

                            if($scope.fatherEmail ){
                                if(!($scope.fatherEmail.match(emailPattern))){
                                    $('#fatherEmail').focus();
                                    toastr.error("Please enter valid father email address.");
                                    return false;   
                                }
                            }else{
                                $('#fatherEmail').focus();
                                    toastr.error("Please enter father email address.");
                                    return false; 
                            }

                                if($scope.fatherMobileNo ){
                                    if(!($scope.fatherMobileNo.match(mobileNumberPattern))){
                                        $('#fatherMobileNo').focus();
                                        toastr.error("Please enter valid Father mobile number.");
                                        return false;   
                                    }
                                    if($scope.fatherMobileNo == $scope.mobileNumber){
                                        $('#fatherMobileNo').focus();
                                        toastr.error("please enter different mobile number than registered mobile number.");
                                        return false;  
                                    }
                                }else{
                                    $('#fatherMobileNo').focus();
                                    toastr.error("Please enter father mobile number.");
                                    return false;
                                }

                                    if($scope.fatherOccupation ){
                                        if(!($scope.fatherOccupation.match(namePattern))){
                                            $('#fatherOccupation').focus();
                                            toastr.error("Please enter valid father occupation.");
                                            return false;   
                                        }
                                    }else{
                                        $('#fatherOccupation').focus();
                                            toastr.error("Please enter father occupation.");
                                            return false; 
                                    }
                                    if($scope.fathercGovtEmp == ''){
                                        $('#fathercGovtEmpYes').focus();
                                            toastr.error("Please enter father is govenment employee or not.");
                                            return false;
                                    }

                                        if($scope.fatherAnnualIncome ==''){
                                            $('#fatherAnnualIncome').focus();
                                            toastr.error("Please select father's  annual income.");
                                            return false;
                                        }

                                            if($scope.fatherecoBack== ''){
                                                
                                                $('#fatherecoBackYes').focus();
                                                toastr.error("Please select father is economical backward or not.");
                                                return false;
                                            }
            
            } 
            if($scope.motherFirstName != ''|| $scope.motherMiddleName != '' || $scope.motherLastName !='' || $scope.motherEmail != '' || $scope.motherMobileNo != '' || $scope.motherOccupation != '' || $scope.mothercGovtEmp != '' || $scope.motherAnnualIncome != '' || $scope.motherecoBack != ''){
               
                if($scope.motherFirstName){
                            if(!($scope.motherFirstName.match(namePattern))){
                                $('#motherFirstName').focus();
                                toastr.error("Please enter valid mother first name.");
                                return false;   
                            }
                        }else{
                            $('#motherFirstName').focus();
                            toastr.error("Please enter mother first fame.");
                            return false;   
                        }
        
                            if($scope.motherMiddleName){
                                if(!($scope.motherMiddleName.match(namePattern))){
                                    $('#motherMiddleName').focus();
                                    toastr.error("Please enter valid mother middle name.");
                                    return false;   
                                }
                            }else{
                                $('#motherMiddleName').focus();
                                toastr.error("Please enter mother middle name.");
                                return false;   
                            }
                                if($scope.motherLastName ){
                                    if(!($scope.motherLastName.match(namePattern))){
                                        $('#motherLastName').focus();
                                        toastr.error("Please enter valid mother last name.");
                                        return false;   
                                    }
                                }else{
                                    $('#motherLastName').focus();
                                        toastr.error("Please enter mother last name.");
                                        return false;  
                                }
        
                                    if($scope.motherEmail ){
                                        if(!($scope.motherEmail.match(emailPattern))){
                                            $('#motherEmail').focus();
                                            toastr.error("Please enter valid mother email address.");
                                            return false;   
                                        }
                                    }else{
                                        $('#motherEmail').focus();
                                            toastr.error("Please enter mother email address.");
                                            return false; 
                                    }
        
                                        if($scope.motherMobileNo ){
                                            if(!($scope.motherMobileNo.match(mobileNumberPattern))){
                                                $('#motherMobileNo').focus();
                                                toastr.error("Please enter valid mother mobile number.");
                                                return false;   
                                            }
                                            if($scope.motherMobileNo == $scope.mobileNumber){
                                                $('#motherMobileNo').focus();
                                                toastr.error("please enter different mobile number than registered mobile number.");
                                                return false;  
                                            }
                                        }else{
                                            $('#motherMobileNo').focus();
                                            toastr.error("Please enter mother mobile number.");
                                            return false;
                                        }
        
                                            if($scope.motherOccupation ){
                                                if(!($scope.motherOccupation.match(namePattern))){
                                                    $('#motherOccupation').focus();
                                                    toastr.error("Please enter valid mother occupation.");
                                                    return false;   
                                                }
                                            }else{
                                                $('#motherOccupation').focus();
                                                    toastr.error("Please enter mother occupation.");
                                                    return false; 
                                            }
                                            if($scope.mothercGovtEmp == ''){
                                                $('#mothercGovtEmpYes').focus();
                                                    toastr.error("Please enter mother is govenment employee or not.");
                                                    return false;
                                            }
        
                                                if($scope.motherAnnualIncome ==''){
                                                    $('#motherAnnualIncome').focus();
                                                    toastr.error("Please select mother's  annual income.");
                                                    return false;
                                                }
        
                                                    if($scope.motherecoBack== ''){
                                                        
                                                        $('#motherecoBackYes').focus();
                                                        toastr.error("Please select mother is economical backward or not.");
                                                        return false;
                                                    }
             }
            

            if($scope.guaradianFirstName !='' || $scope.guaradianMiddleName !='' || $scope.guaradianLastName !='' || $scope.guaradianEmail !='' || $scope.guaradianMobileNo !='' || $scope.guaradianOccupation !='' || $scope.guaradiancGovtEmp  != '' || $scope.guaradianAnnualIncome !='' || $scope.guaradianecoBack != ''){ 
             
                if($scope.guaradianFirstName){
                    if(!($scope.guaradianFirstName.match(namePattern))){
                        $('#guaradianFirstName').focus();
                        toastr.error("Please enter valid guaradian first name.");
                        return false;   
                    }
                }else{
                    $('#guaradianFirstName').focus();
                    toastr.error("Please enter guaradian first name.");
                    return false;   
                }

                    if($scope.guaradianMiddleName){
                        if(!($scope.guaradianMiddleName.match(namePattern))){
                            $('#guaradianMiddleName').focus();
                            toastr.error("Please enter valid guaradian middle name.");
                            return false;   
                        }
                    }else{
                        $('#guaradianMiddleName').focus();
                        toastr.error("Please enter guaradian middle name.");
                        return false;   
                    }
                        if($scope.guaradianLastName ){
                            if(!($scope.guaradianLastName.match(namePattern))){
                                $('#guaradianLastName').focus();
                                toastr.error("Please enter valid guaradian last name.");
                                return false;   
                            }
                        }else{
                            $('#guaradianLastName').focus();
                                toastr.error("Please enter guaradian last name.");
                                return false;  
                        }

                            if($scope.guaradianEmail ){
                                if(!($scope.guaradianEmail.match(emailPattern))){
                                    $('#guaradianEmail').focus();
                                    toastr.error("Please enter valid guaradian email address.");
                                    return false;   
                                }
                            }else{
                                $('#guaradianEmail').focus();
                                    toastr.error("Please enter guaradian email address.");
                                    return false; 
                            }

                                if($scope.guaradianMobileNo ){
                                    if(!($scope.guaradianMobileNo.match(mobileNumberPattern))){
                                        $('#guaradianMobileNo').focus();
                                        toastr.error("Please enter valid guaradian mobile number.");
                                        return false;   
                                    }
                                    if($scope.guaradianMobileNo == $scope.mobileNumber){
                                        $('#guaradianMobileNo').focus();
                                        toastr.error("please enter different mobile number than registered mobile number.");
                                        return false;  
                                    }
                                }else{
                                    $('#guaradianMobileNo').focus();
                                    toastr.error("Please enter guaradian mobile number.");
                                    return false;
                                }

                                    if($scope.guaradianOccupation ){
                                        if(!($scope.guaradianOccupation.match(namePattern))){
                                            $('#guaradianOccupation').focus();
                                            toastr.error("Please enter valid guaradian occupation.");
                                            return false;   
                                        }
                                    }else{
                                        $('#guaradianOccupation').focus();
                                            toastr.error("Please enter guaradian occupation.");
                                            return false; 
                                    }
                                    if($scope.guaradiancGovtEmp == ''){
                                        $('#guaradiancGovtEmpYes').focus();
                                            toastr.error("Please enter guaradian is govenment employee or not.");
                                            return false;
                                    }

                                        if($scope.guaradianAnnualIncome ==''){
                                            $('#guaradianAnnualIncome').focus();
                                            toastr.error("Please select guaradian's  annual income.");
                                            return false;
                                        }

                                            if($scope.guaradianecoBack== ''){
                                                
                                                $('#guaradianecoBackYes').focus();
                                                toastr.error("Please select guaradian is economical backward or not.");
                                                return false;
                                            }
                
            } 
            if($scope.fatherFirstName == '' ||  $scope.motherFirstName == ''){
                toastr.error("Manditory to fill Mother's and Father's Details.");
                return false; 
            }
            if($scope.studentchk1){
                    var transform = function(data) {
                        return $.param(data);
                    } 
                    $scope.disabledSubmitParentInfoButton = true;
                    $scope.ShowSpinnerStatus = true;
                    $http.post(window.site_url + 'AdmissionForm/submitParentInfo', {
                        'csrf_token_name': csrfHash,
                        studentId : JSON.parse(localStorage.getItem("StudentId")),
                        fatherFirstName : $scope.fatherFirstName,
                        fatherMiddleName :$scope.fatherMiddleName ,
                        fatherLastName :$scope.fatherLastName ,
                        fatherEmail :$scope.fatherEmail ,
                        fatherMobileNo :$scope.fatherMobileNo ,
                        fatherOccupation :$scope.fatherOccupation ,
                        fathercGovtEmp :$scope.fathercGovtEmp,
                        fatherAnnualIncome :$scope.fatherAnnualIncome ,
                        fatherecoBack :$scope.fatherecoBack ,

                        motherFirstName :$scope.motherFirstName ,
                        motherMiddleName :$scope.motherMiddleName ,
                        motherLastName :$scope.motherLastName ,
                        motherEmail :$scope.motherEmail ,
                        motherMobileNo :$scope.motherMobileNo ,
                        motherOccupation :$scope.motherOccupation ,
                        mothercGovtEmp :$scope.mothercGovtEmp ,
                        motherAnnualIncome :$scope.motherAnnualIncome ,
                        motherecoBack :$scope.motherecoBack,

                        guaradianFirstName :$scope.guaradianFirstName ,
                        guaradianMiddleName :$scope.guaradianMiddleName ,
                        guaradianLastName :$scope.guaradianLastName ,
                        guaradianEmail :$scope.guaradianEmail ,
                        guaradianMobileNo :$scope.guaradianMobileNo ,
                        guaradianOccupation :$scope.guaradianOccupation ,
                        guaradiancGovtEmp :$scope.guaradiancGovtEmp ,
                        guaradianAnnualIncome :$scope.guaradianAnnualIncome ,
                        guaradianecoBack :$scope.guaradianecoBack ,

                        studentchk1 :$scope.studentchk1 ,
                        
                    }, {
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                        transformRequest: transform
                    })
                    .then(function(response) {
                        if (response.data.status.status == "1") {
                            //toastr.success(response.data.status.message);
                            toastr.success("Application has been submitted successfully.");
                            // $scope.getStudentParentInfo(JSON.parse(localStorage.getItem("StudentId")));
                            setTimeout(() => {
                                // window.location = "/online-admission/user/dashboard.html";
                                     window.location = new_url+ "online-admission/index.html"
                                },3000)
                            $scope.init();
                        }else{
                            toastr.error(response.data.status.message);
                            $scope.disabledSubmitParentInfoButton = false;
                            $("#loadingDiv").css('display','none');
                            $scope.ShowSpinnerStatus = false;
                        }
                    
                    }, function(response) {
                    
                    })

                } else {
                    toastr.error("Please marked if you agree to the terms and conditions");
                }


        }else{
            toastr.error("Parents/Guardian information required");
            return false;
        }
    
    }

    
    $scope.getStudentParentInfo = function (storedNames) {
        $scope.student_id = storedNames;
        
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AdmissionForm/getStudentParentInfo', {
            'csrf_token_name': csrfHash,
            student_id : $scope.student_id ,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.studentParentInfoArray = response.data.data[0];
                
                $scope.fatherFirstName = response.data.data[0].father_first_name ;
                $scope.fatherMiddleName = response.data.data[0].father_middle_name ;
                $scope.fatherLastName = response.data.data[0].father_last_name ;
                $scope.fatherEmail = response.data.data[0].father_email_address ;
                $scope.fatherMobileNo = response.data.data[0].father_mobile_number ;
                $scope.fatherOccupation = response.data.data[0].father_occupation ;
                $scope.fathercGovtEmp = response.data.data[0].is_father_central_govern_employee ;
                $scope.fatherAnnualIncome = response.data.data[0].father_annual_income ;
                $scope.fatherecoBack = response.data.data[0].is_father_economical_backward ;

                $scope.motherFirstName = response.data.data[0].mother_first_name ;
                $scope.motherMiddleName = response.data.data[0].mother_middle_name ;
                $scope.motherLastName = response.data.data[0].mother_last_name ;
                $scope.motherEmail = response.data.data[0].mother_email_address ;
                $scope.motherMobileNo = response.data.data[0].mother_mobile_number ;
                $scope.motherOccupation = response.data.data[0].mother_occupation ;
                $scope.mothercGovtEmp = response.data.data[0].is_mother_central_govern_employee ;
                $scope.motherAnnualIncome = response.data.data[0].mother_annual_income ;
                $scope.motherecoBack = response.data.data[0].is_mother_economical_backward ;

                $scope.guaradianFirstName = response.data.data[0].guardian_first_name ;
                $scope.guaradianMiddleName = response.data.data[0].guardian_middle_name ;
                $scope.guaradianLastName = response.data.data[0].guardian_last_name ;
                $scope.guaradianEmail = response.data.data[0].guardian_email_address ;
                $scope.guaradianMobileNo = response.data.data[0].guardian_mobile_number ;
                $scope.guaradianOccupation = response.data.data[0].guardian_occupation ;
                $scope.guaradiancGovtEmp = response.data.data[0].is_guardian_central_govern_employee ;
                $scope.guaradianAnnualIncome = response.data.data[0].guardian_annual_income ;
                $scope.guaradianecoBack = response.data.data[0].is_guardian_economical_backward ;
                //$scope.studentchk1 = response.data.data[0].is_student_agree ;
                //console.log($scope.studentchk1 );
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    const marquees = [...document.querySelectorAll('.marquee')];
    marquees.forEach((marquee) => {
	marquee.innerHTML = marquee.innerHTML + '&nbsp;'.repeat(5);
    marquee.i = 0;
    marquee.step = 3;
	marquee.width = (marquee.clientWidth + 1);
    marquee.style.position = '';
    marquee.innerHTML = `${marquee.innerHTML}&nbsp;`.repeat(6);
    marquee.addEventListener('mouseenter', () => marquee.step = 0, false);
    marquee.addEventListener('mouseleave', () => marquee.step = 3, false);
    });

    setInterval(move, 50);

    function move() {
    marquees
    .forEach((marquee) => {
    marquee.style.marginLeft = `-${marquee.i}px`;;
    marquee.i = marquee.i < marquee.width ? marquee.i + marquee.step : 1;
    }); 
    }

    $scope.getAdmissionDate = function(){
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'AdmissionForm/getAdmissionDate', {
            'csrf_token_name': csrfHash,
            academicYear : $scope.academicYear,
            courseName : $scope.courseName ,
            courseYear : $scope.courseYear
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.endDate = response.data.data[0].end_date;
                
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })
    }

   
   
    
    
    
});


