var csrfHash = $('#csrfHash').val();
var formFees = $('#formFees').val();

app.controller('paidFeedServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        $scope.RazorpayOrderId = '';
        $scope.studName = $("#studName").val();
        $scope.studEmail = $("#studEmail").val();
        $scope.studPayFees = $("#studPayFees").val();
        $scope.studID = $("#studID").val();
        $scope.paymentLinkActiveorNotBit = true;
        $scope.checkPayentLinkIsStopOrNot('jsFile');
        
    }
   
   
    $scope.checkPayentLinkIsStopOrNot = function (checkPage) {
             
            var transform = function(data) {
                return $.param(data);
            } 
            $http.post(window.site_url + 'ManagePaymentReminder/checkPayentLinkIsStopOrNot', {
                'csrf_token_name': csrfHash,
                studID : $scope.studID
            }, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                transformRequest: transform
            })
            .then(function(response) {
                if (response.data.status.status == "1") {
                   console.log(response.data.data);
                   if(response.data.data == 1){
                       $scope.paymentLinkActiveorNotBit = false;
                   }else{
                       $scope.paymentLinkActiveorNotBit = true;
                       $scope.getRazorpayOrderId();
                         if(checkPage == "view"){
                           setTimeout(() => {
                              $scope.payFeesClcik();
                           },1000)
                         }
                   }
                   
                }else {
                   
                }
            }, function(response) {
            })           
       
    }


    $scope.payFeesClcik = function () {
       $scope.paymentclick();  
    }




    $scope.paymentclick = function () {
        //$('#paymentModal').modal('hide');$scope.studPayFees
        var options = {
          "key"               : 'rzp_live_gdv4FrgOSmxKWH',
          "amount"            :  $scope.studPayFees * 100,
          "name"              : "M. U. College of Commerce",
          "description"       : "Pay Admission Fee",
          "image"             : "https://tsnegigcreckongpeo.com//online-admission/images/logo.png",
          "prefill"           : {
          "name"              : $scope.studName,
          "email"             : $scope.studEmail,
          "contact"           : '',
          },
          "notes"             : {
          "address"           : "Pune",
          "merchant_order_id" : "id" + Math.random().toString(16).slice(2),
          },
          "theme"             : {
          "color"             : "#F37254"
          },
          "order_id"          : $scope.RazorpayOrderId,
      };
        
       /**
       * The entire list of Checkout fields is available at
       * https://docs.razorpay.com/docs/checkout-form#checkout-fields
       */
      options.handler = function (transaction){
          if (typeof transaction.razorpay_payment_id == 'undefined' ||  transaction.razorpay_payment_id < 1) {
             window.location.replace("https://tsnegigcreckongpeo.com//online-admission/failure.html");
             $scope.getRazorpayOrderId();
          } else {
              
                  var transform = function(data) {
                      return $.param(data);
                  } 

                  $http.post(window.site_url + 'ManagePaymentReminder/updateStudentPaymentStatus', {
                      'csrf_token_name': csrfHash,
                      studID : $scope.studID ,
                      studName : $scope.studName ,
                      studPayFees : $scope.studPayFees
                  }, {
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                      transformRequest: transform
                  })
                  .then(function(response) {
                      if (response.data.status.status == "1") {

                          toastr.success(response.data.status.message);
                          setTimeout(() => {
                              window.location = "index.html";
                          },2000)
                         
                          
                      }else{
                          toastr.error(response.data.status.message);
                      }
                  }, function(response) {
                  })
          }
      };
      
      // Boolean whether to show image inside a white frame. (default: true)
      options.theme.image_padding = false;
      
      options.modal = {
          ondismiss: function() {
              $scope.getRazorpayOrderId();
          },
          // Boolean indicating whether pressing escape key 
          // should close the checkout form. (default: true)
          escape: true,
          // Boolean indicating whether clicking translucent blank
          // space outside checkout form should close the form. (default: false)
          backdropclose: false
      };
      
      var rzp = new Razorpay(options); 
      rzp.open();
    }
    
    
    
      $scope.getRazorpayOrderId = function () {
           var transform = function(data) {
              return $.param(data);
          } 
          
             $http.post('https://tsnegigcreckongpeo.com//online-admission/pay.php', {
              'csrf_token_name': csrfHash,
              'formFees':$scope.studPayFees,
              'action':'one'
              }, {
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                  transformRequest: transform
              })
              .then(function(response) {
                   console.log(response.data);
                   $scope.RazorpayOrderId = response.data;
              }, function(response) {
              }) 
      }
});


