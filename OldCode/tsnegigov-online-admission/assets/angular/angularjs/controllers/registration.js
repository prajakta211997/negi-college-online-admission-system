var csrfHash = $('#csrfHash').val();
var formFees = $('#formFees').val();

app.controller('registerServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        $scope.courseName = '';
        $scope.courseYear = '';
        $scope.firstName = '';
        $scope.middleName = '';
        $scope.lastName = '';
        $scope.studEmail = '';
        $scope.studMobile = '';
        $scope.studPassword = '';
        $scope.studConfPassword = '';
        $scope.academicYear ='';
        $scope.formFees = "";
        $scope.courseNameArray =[];
        $scope.getAllCoursesName();
        $scope.admissionOpenCloseBit = 0;
    }
    var namePattern = /^[a-zA-z\s][a-zA-Z ._-\s]+$/;
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    var mobileNumberPattern = /^[7-9][0-9]{9}$/;
   
    var year = 2015;
    var range = [];
    for (var i = new Date().getFullYear(); i <= new Date().getFullYear() ; i++) {
        range.push(i+ "-"+(i+1)) ;
        
    }
    $scope.years = range;
    
    $scope.getAdmissionLinkStatus = function (){
        
        if($scope.courseName != '' && $scope.courseYear != '' && $scope.academicYear !=''){
            var transform = function(data) {
                return $.param(data);
            } 
            $http.post(window.site_url + 'StudentRegistration/getAdmissionLinkStatus', {
                'csrf_token_name': csrfHash,
                courseName : $scope.courseName,
                courseYear : $scope.courseYear,
                academicYear: $scope.academicYear
            }, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                transformRequest: transform
            })
            .then(function(response) {
                if (response.data.status.status == "1") {
                    $scope.admissionOpenCloseBit = response.data.status.data
                    if($scope.admissionOpenCloseBit == 1){
                        var errorMessage = "Admissions for "+ $scope.courseName + "  " + $scope.courseYear + " are closed temporarily. Please check the website for update.";
                        toastr.error(errorMessage);
                        console.log("link Status",errorMessage);
                    }
                }else {
                    //toastr.error(response.data.status.message);
                }
            }, function(response) {
            })           
        }
        
    }
    
    $scope.getAllCoursesName = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'StudentRegistration/getAllCoursesName', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.courseNameArray = response.data.data;
                
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }
    
    $scope.registerService = function () {
        
        if($scope.academicYear){
            if ($scope.courseName) {
                if ($scope.courseYear) {
                    if($scope.admissionOpenCloseBit == 0){
                        if ($scope.firstName) {
                            if(!($scope.firstName.match(namePattern))){
                             $('#firstName').focus();
                             toastr.error("Please enter valid First Name.");
                             return false;
                            }
                         
                            if ($scope.middleName) {
                                 if(!($scope.middleName.match(namePattern))){
                                     $('#middleName').focus();
                                     toastr.error("Please enter valid Middle Name.");
                                     return false;
                                 }
                             
                                if($scope.lastName) {
                                     if(!($scope.lastName.match(namePattern))){
                                         $('#lastName').focus();
                                             toastr.error("Please enter valid Last Name.");
                                             return false;
                                     }
                                if ($scope.studEmail) {
                                    if(!( $scope.studEmail.match(emailPattern))){
                                        $('#studEmail').focus();
                                        toastr.error("Please enter valid Email Address.");
                                        return false;   
                                    }
                                        if ($scope.studMobile) {
                                            if(!( $scope.studMobile.match(mobileNumberPattern))){
                                                $('#studMobile').focus();
                                                toastr.error("Please enter valid Mobile Number.");
                                                return false;   
                                            }
                                                if ($scope.studPassword) {
                                                    if ($scope.studConfPassword) {
                                                        var password = document.getElementById("studPassword").value;
                                                        var confirmPassword = document.getElementById("studConfPassword").value;
                                                        if (password != confirmPassword) {
                                                            toastr.error("Passwords do not match.");
                                                            return false;
                                                        }
                                                        $scope.paymentclick();  
                                                    //   var transform = function(data) {
                                                    //     return $.param(data);
                                                    // } 

                                                    // $http.post(window.site_url + 'StudentRegistration/studentRegistration', {
                                                    //     'csrf_token_name': csrfHash,
                                                    //     courseName : $scope.courseName ,
                                                    //     courseYear : $scope.courseYear ,
                                                    //     firstName : $scope.firstName ,
                                                    //     middleName : $scope.middleName ,
                                                    //     lastName : $scope.lastName,
                                                    //     studEmail : $scope.studEmail ,
                                                    //     studMobile : $scope.studMobile ,
                                                    //     studPassword : $scope.studPassword ,
                                                    //     academicYear :$scope.academicYear,
                                                        
                                                    // }, {
                                                    //     headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                                                    //     transformRequest: transform
                                                    // })
                                                    // .then(function(response) {
                                                    //     if (response.data.status.status == "1") {

                                                    //         toastr.success(response.data.status.message);
                                                    //          setTimeout(() => {
                                                    //             window.location = "index.html";
                                                    //          },1000)
                                                           
                                                            
                                                    //     }else{
                                                    //         toastr.error(response.data.status.message);
                                                    //     }
                                                    // }, function(response) {
                                                    // })
                                    
                                                    } else {
                                                        $('#studConfPassword').focus();
                                                        toastr.error("Confirmed Password is required.");
                                                    }
                                                } else {
                                                    $('#studPassword').focus();
                                                    toastr.error("Password is required.");
                                                }
    
                                        } else {
                                            $('#studMobile').focus();
                                            toastr.error("Student Mobile Number is required.");
                                        }
                                } else {
                                    $('#studEmail').focus();
                                    toastr.error("Student Email Address is required.");
                                }
                                } else {
                                    $('#lastName').focus();
                                    toastr.error("Last Name is required.");
                                    }
        
                            } else {
                            $('#middleName').focus();
                            toastr.error("Middle Name is required.");
                            }
        
                        } else {
                        $('#firstName').focus();
                        toastr.error("First Name is required.");
                        }
                    }else{
                        var errorMessage = "Admissions for "+ $scope.courseName + "  " + $scope.courseYear + " are closed temporarily. Please check the website for update.";
                        toastr.error(errorMessage);
                    }
                } else {
                    $('#courseYear').focus();
                    toastr.error("Course Year is required.");
                }
            } else {
                $('#courseName').focus();
                toastr.error(" Course is required.");
            }
        }else {
            $('#academicYear').focus();
            toastr.error(" Academic Year is Required.")
        }
    }

    $scope.getFormFees = function (){
        if($scope.courseName != '' && $scope.courseYear != '' && $scope.academicYear !=''){
            var transform = function(data) {
                return $.param(data);
            } 
            $http.post(window.site_url + 'StudentRegistration/getFormFees', {
                'csrf_token_name': csrfHash,
                courseName : $scope.courseName,
                courseYear : $scope.courseYear,
                academicYear: $scope.academicYear
            }, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                transformRequest: transform
            })
            .then(function(response) {
                if (response.data.status.status == "1") {
                   $scope.formFees = response.data.data;
                  // $scope.formFees = 1;
                    $scope.getAdmissionLinkStatus();
                     setTimeout(() => {
                          $scope.getRazorpayOrderId();
                       },1000);
                }else {
                    toastr.error(response.data.status.message);
                }
            }, function(response) {
            })  
            
        }
    }


    $scope.paymentclick = function () {
        //$('#paymentModal').modal('hide');
        var options = {
          "key"               : 'rzp_live_qkiwV0Fl18zxVt',
          "amount"            :  $scope.formFees * 100,
          "name"              : "THAKUR SEN NEGI GOVERNMENT COLLEGE",
          "description"       : "Apply For Online Admission",
          "image"             : "https://tsnegigcreckongpeo.com/online-admission/images/logo.png",
          "prefill"           : {
          "name"              : $scope.firstName+' '+$scope.middleName+' '+$scope.lastName,
          "email"             : $scope.studEmail,
          "contact"           : $scope.studMobile,
          },
          "notes"             : {
          "address"           : "Pune",
          "merchant_order_id" : "id" + Math.random().toString(16).slice(2),
          },
          "theme"             : {
          "color"             : "#F37254"
          },
          "order_id"          : $scope.RazorpayOrderId,
      };
        
       /**
       * The entire list of Checkout fields is available at
       * https://docs.razorpay.com/docs/checkout-form#checkout-fields
       */
      options.handler = function (transaction){
          console.log("transaction --> ",transaction);
          console.log("response.razorpay_order_id --> ",transaction.razorpay_order_id);
          console.log("response.razorpay_payment_id ---> ",transaction.razorpay_payment_id);
          console.log("response.razorpay_signature --->",transaction.razorpay_signature);
          
          if (typeof transaction.razorpay_payment_id == 'undefined' ||  transaction.razorpay_payment_id < 1) {
             window.location.replace("https://tsnegigcreckongpeo.com//online-admission/failure.html");
             $scope.getRazorpayOrderId();
          } else {
              
                var transform = function(data) {
                      return $.param(data);
                  } 

                  $http.post(window.site_url + 'StudentRegistration/studentRegistration', {
                      'csrf_token_name': csrfHash,
                      courseName : $scope.courseName ,
                      courseYear : $scope.courseYear ,
                      firstName : $scope.firstName ,
                      middleName : $scope.middleName ,
                      lastName : $scope.lastName,
                      studEmail : $scope.studEmail ,
                      studMobile : $scope.studMobile ,
                      studPassword : $scope.studPassword ,
                      academicYear :$scope.academicYear,
                      
                  }, {
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                      transformRequest: transform
                  })
                  .then(function(response) {
                      if (response.data.status.status == "1") {

                          toastr.success(response.data.status.message);
                           setTimeout(() => {
                              window.location = "index.html";
                           },1000)
                         
                          
                      }else{
                          toastr.error(response.data.status.message);
                      }
                  }, function(response) {
                  })
          }
      };
      
      // Boolean whether to show image inside a white frame. (default: true)
      options.theme.image_padding = false;
      
      options.modal = {
          ondismiss: function() {
              $scope.getRazorpayOrderId();
          },
          // Boolean indicating whether pressing escape key 
          // should close the checkout form. (default: true)
          escape: true,
          // Boolean indicating whether clicking translucent blank
          // space outside checkout form should close the form. (default: false)
          backdropclose: false
      };
      
      var rzp = new Razorpay(options); 
      rzp.open();
    }
    
    
    
      $scope.getRazorpayOrderId = function () {
           var transform = function(data) {
              return $.param(data);
          } 
          console.log(window.current_url);
             $http.post('https://tsnegigcreckongpeo.com/online-admission/pay.php', {
              'csrf_token_name': csrfHash,
              'formFees':$scope.formFees,
              'action':'one'
              }, {
                  headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                  transformRequest: transform
              })
              .then(function(response) {
                   console.log(response.data);
                   $scope.RazorpayOrderId = response.data;
              }, function(response) {
              }) 
      }
});


