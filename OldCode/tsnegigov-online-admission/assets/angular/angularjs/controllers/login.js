var csrfHash = $('#csrfHash').val();


app.controller('loginServicesCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        $scope.studEmail = '';
        $scope.studPassword = '';
        $scope.new_url = new_url;
    }
    

    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/; 

    $scope.loginService = function () {
        if ($scope.studEmail) {
            if(!( $scope.studEmail.match(emailPattern))){
                toastr.error("Please enter valid Email Address.");
                return false;   
            }
            if ($scope.studPassword) {
                                
                var transform = function(data) {
                    return $.param(data);
                } 
                $http.post(window.site_url + 'UserLogin/checkLogin', {
                    'csrf_token_name': csrfHash,
                    studEmail : $scope.studEmail ,
                    studPassword : $scope.studPassword ,

                }, {
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
                    transformRequest: transform
                })
                .then(function(response) {
                    if (response.data.status.status == "1") {
                        localStorage.setItem("StudentId",JSON.stringify(response.data.data.student_id));
                        localStorage.setItem("StudentLoggedIn", "True");
                        
                        toastr.success(response.data.status.message);
                        if(response.data.data.admission_step_one == '1' && response.data.data.admission_step_two == '1' && response.data.data.admission_step_three == '1'){
                 
                            setTimeout(() => {
                               window.location =new_url+ "online-admission/user/dashboard.html";
                           },1000)
                          }
                          else{
                           setTimeout(() => {
                                   window.location = "admissionForm.html";
                               },1000)
                          }
                    }else{
                        toastr.error(response.data.status.message);
                    }
                }, function(response) {
                })              
            } else {
                $('#studPassword').focus();
                toastr.error("Password is required.");
            }
        } else {
            $('#studEmail').focus();
            toastr.error("EmailId is required.");
        }
               
    }

    
});


