var csrfHash = $('#csrfHash').val();
$("#dateOfBirth")
  .datepicker({
    format: "dd-mm-yyyy",
    autoclose: true,
    //startDate: new Date()
  })
  .val();

 
app.controller('editProfileCtrl', function ($scope, $http, toastr) {

    $scope.init = function () {
        
        if(localStorage.getItem("StudentLoggedIn") == "False" || localStorage.getItem("StudentLoggedIn") == null){
            window.location = "/online-admission/index.html";
        }
        else{
            
            var storedNames = JSON.parse(localStorage.getItem("StudentId"));
            $scope.studentPersonalInfoArray = [];
            $scope.getStudentRegistrationDetails(storedNames);
            $scope.singleStudentArray =[];
            //$scope.getStudentList();
            $scope.religionListArray =[];
            
            $scope.new_url = new_url;
            $scope.showHscDiv = false;
            $scope.showTyBcomDiv = false;
            $scope.showFyDiv = false;
            $scope.showSyDiv = false;
            $scope.showEditDiv = false;
            $scope.showProfileDiv = true;
            $scope.endDate = '';
            $scope.isSindhi = '';
            $scope.fromJaihind = '';
            $scope.firstName = '';
            $scope.middleName = '';
            $scope.lastName = '';
            $scope.motherName = '';
            $scope.emailId = '';
            $scope.mobileNumber = '';
            $scope.alterMobileNumber = '';
            $scope.aadharNumber = '';
            $scope.voterNumber = '';
            $scope.localAddress = '';
            $scope.nativePlace = '';
            $scope.bloodGroup = '';
            $scope.gender = '';
            $scope.maritalstatus = '';
            $scope.dateOfBirth = '';
            $scope.birthPlace = '';
            $scope.district = '';
            $scope.dateOfBirth = '';
            $scope.dateyear = "";
	        $scope.month = "";
	        $scope.day = "";
            $scope.state = '';
            $scope.nationality = '';
            $scope.Passport = '';
            $scope.religion = '';
            $scope.motherTongue = '';
            $scope.caste = '';
            $scope.category = '';
            $scope.isCastCertificate = '';
            $scope.game = '';
            $scope.hobbies = '';
            $scope.nss = '';
            $scope.event = '';
            $scope.hobbies = '';
            $scope.hobbies = '';
            $scope.photo = '';
            $scope.signature = '';
            $scope.casteCertificate ='';
            $scope.multipleDocument =[];
            $scope.photolink= '';
            $scope.signlink='';
            $scope.certificatelink='';
            $scope.multipleDocumentlink ='';
            $scope.sscMarksheetlink='';
            $scope.sscExam = '' ;
            $scope.sscBoard = '' ;
            $scope.sscClgName = '' ;
            $scope.sscPassingYear = '' ;
            $scope.sscSeatNo = '' ;
            $scope.sscMarksObtain = '' ;
            $scope.sscTotalMarks = '' ;
            $scope.sscPercentage = '' ;
            $scope.sscMarksheet = '' ;
            $scope.appearingYear = '' ;
            $scope.gapReason = '' ;
            $scope.appliedOtherCollege = '' ;
            $scope.hscMarksheetlink ='';
            $scope.hscExam = '' ;
            $scope.hscBoard = '' ;
            $scope.hscClgName = '' ;
            $scope.hscPassingYear = '' ;
            $scope.hscSeatNo = '';
            $scope.hscMarksObtain = '' ;
            $scope.hscTotalMarks = '' ;
            $scope.hscPercentage = '' ;
            $scope.hscMarksheet ='';
            $scope.tyBComMarksheetlink ='';
            $scope.tyBComExam = '' ;
            $scope.tyBComBoard = '' ;
            $scope.tyBComClgName = '' ;
            $scope.tyBComPassingYear = '' ;
            $scope.tyBComSeatNo = '';
            $scope.tyBComMarksObtain = '' ;
            $scope.tyBComTotalMarks = '' ;
            $scope.tyBComPercentage = '' ;
            $scope.tyBComMarksheet ='';
            $scope.fyMarksheetlink ='';
            $scope.fyExam = '' ;
            $scope.fyBoard = '' ;
            $scope.fyClgName = '' ;
            $scope.fyPassingYear = '' ;
            $scope.fySeatNo = '' ;
            $scope.fyMarksObtain = '' ;
            $scope.fyTotalMarks = '' ;
            $scope.fyPercentage  = '' ;
            $scope.fyMarksheet ='';
            $scope.syMarksheetlink ='';
            $scope.syExam = '' ;
            $scope.syBoard = '';
            $scope.syClgName = '' ;
            $scope.syPassingYear = '';
            $scope.sySeatNo ='';
            $scope.syMarksObtain = '';
            $scope.syTotalMarks = '';
            $scope.syPercentage = '' ;
            $scope.syMarksheet ='';
            $scope.studentchk1 ='';
            $scope.fatherFirstName ='';
            $scope.fatherMiddleName ='';
            $scope.fatherLastName ='';
            $scope.fatherEmail ='';
            $scope.fatherMobileNo ='';
            $scope.fatherOccupation ='';
            $scope.fathercGovtEmp ='';
            $scope.fatherAnnualIncome ='';
            $scope.fatherecoBack ='';

            $scope.motherFirstName ='';
            $scope.motherMiddleName ='';
            $scope.motherLastName ='';
            $scope.motherEmail ='';
            $scope.motherMobileNo ='';
            $scope.motherOccupation ='';
            $scope.mothercGovtEmp ='';
            $scope.motherAnnualIncome ='';
            $scope.motherecoBack ='';

            $scope.guaradianFirstName ='';
            $scope.guaradianMiddleName ='';
            $scope.guaradianLastName ='';
            $scope.guaradianEmail ='';
            $scope.guaradianMobileNo ='';
            $scope.guaradianOccupation ='';
            $scope.guaradiancGovtEmp ='';
            $scope.guaradianAnnualIncome ='';
            $scope.guaradianecoBack ='';
            $scope.sscBoardName = "";
            $scope.hscBoardName = "";
            $scope.tyBComBoardName = "";
            $scope.fyBoardName = "";
            $scope.syBoardName = "";
            $scope.disabledUpdateProfileInfoButton = false; 
            $scope.ShowSpinnerStatus = false;
            $scope.ShowSpinnerStatus = false;
            $scope.sscAaddUniversityName = false;
            $scope.hscAddUniversityName = false;
            $scope.fyAddUniversityName = false;
            $scope.tyBComAddUniversityName = false;
            $scope.syAddUniversityName = false;
            $scope.studentAdmissionStatus = "";
            $scope.isgapReason="";
            $scope.isMaharashtra ="";
            $scope.eligibilityNumber ="";
            $scope.physicallychallengedCertificate="";
            $scope.physicallychallengedCertificatelink ="";
            $scope.physicallyhandi = "";
            $scope.paymentid ="";
        }
    }
    
    /*Date Of Birth*/
	
	$scope.days = ['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
	
    $scope.months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
	$scope.dateyears = [];
	var d = new Date();
	for (var i = (d.getFullYear() - 18); i > (1947); i--) {
		$scope.dateyears.push(i);
	}

    var year = new Date().getFullYear();
    var range = [];
    for (var i = year; i >= 2010 ; i--) {
        range.push(i) ;
        
    }
    $scope.years = range;

    $scope.showDiv = function(){
        if($scope.courseYear == "First Year"){
            if($scope.courseName == 'M.Com'){
                $scope.showHscDiv = true;
                $scope.showTyBcomDiv = true;
            }else{
                $scope.showHscDiv = true;
            }
            
        }
        if($scope.courseYear == "Second Year"){
            
            if($scope.courseName == 'M.Com'){
                $scope.showHscDiv = true;
                $scope.showTyBcomDiv = true;
                $scope.showFyDiv = true;
                
            }else{
                $scope.showHscDiv = true;
                $scope.showFyDiv = true;
            }  
        
        }
        if($scope.courseYear == "Third Year"){
                
            if($scope.courseName == 'M.Com'){
                $scope.showHscDiv = true;
                $scope.showTyBcomDiv = true;
                $scope.showFyDiv = true;
                $scope.showSyDiv = true;
                
            }else{
                $scope.showHscDiv = true;
                $scope.showFyDiv = true;
                $scope.showSyDiv = true;
            }  
        }
    }
    $scope.showEditPrfileDiv = function(){
        if($scope.studentAdmissionStatus == '3'){
            $scope.showEditDiv = false;
            $scope.showProfileDiv = true;
        }else{
            $scope.showEditDiv = true;
            $scope.showProfileDiv = false;
        }
    }

    $scope.showSscAddUniversityNameDiv = function(){

        
        if($scope.sscBoard == 'Other'){
        $scope.sscAddUniversityName = true;
        }else{
            $scope.sscAddUniversityName = false;
        }
    }
    $scope.showHscAddUniversityNameDiv = function(){
       
        
        if($scope.hscBoard == 'Other'){
        $scope.hscAddUniversityName = true;
        }else{
            $scope.hscAddUniversityName = false;
        }
    }
    $scope.showFyAddUniversityNameDiv = function(){
        
        if($scope.fyBoard == 'Other'){
        $scope.fyAddUniversityName = true;
        }else{
            $scope.fyAddUniversityName = false;
        }
    }
    $scope.showSyAddUniversityNameDiv = function(){
       
        if($scope.syBoard == 'Other'){
        $scope.syAddUniversityName = true;
        }else{
            $scope.syAddUniversityName = false;
        }
    }

    $scope.showtyBComAddUniversityNameDiv = function(){
        if($scope.tyBComBoard == 'Other'){
            $scope.tyBComAddUniversityName = true;
        }else{
            $scope.tyBComAddUniversityName = false;
        }
    }

    $scope.showEligibilityNoDiv = function(){
        if($scope.courseYear == "First Year"){
            $scope.ifSecondThirdYear = false;
            $scope.ifFirstYear= true;
        }else{
            $scope.ifSecondThirdYear = true;
            $scope.ifFirstYear= false;
        }

    }

    $scope.getStudentList = function () {
        
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'EditProfile/getStudentList', {
            'csrf_token_name': csrfHash,
            student_id : JSON.parse(localStorage.getItem("StudentId")),
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.singleStudentArray = response.data.data;
                
                
                console.log("getsinglestudentlist",response.data.data);
                $scope.leng = (response.data.data).length;
                $scope.studentAdmissionStatus = response.data.data[0].student_admission_status;
                $scope.isSindhi = response.data.data[0].is_sindhi_category;
                $scope.fromJaihind = response.data.data[0].is_jaihind_college ;
                $scope.firstName = response.data.data[0].first_name ;
                $scope.middleName  = response.data.data[0].middle_name ;
                $scope.lastName = response.data.data[0].last_name ;
                $scope.motherName = response.data.data[0].mother_name ;
                $scope.emailId = response.data.data[0].email_address;
                $scope.mobileNumber = response.data.data[0].mobile_number;
                $scope.alterMobileNumber = response.data.data[0].alter_mobile_number ;
                $scope.aadharNumber  = response.data.data[0].adhar_card_id ;
                $scope.voterNumber  = response.data.data[0].voter_id ;
                $scope.localAddress  = response.data.data[0].local_address ;
                $scope.nativePlace = response.data.data[0].native_place ;
                $scope.bloodGroup  = response.data.data[0].blood_group ;
                $scope.gender = response.data.data[0].gender ;
                $scope.maritalstatus = response.data.data[0].marital_status ;
                $scope.dateOfBirth  = response.data.data[0].date_of_birth.split('-');
                $scope.day= $scope.dateOfBirth[0];
                $scope.month= $scope.dateOfBirth[1];
                $scope.dateyear = $scope.dateOfBirth[2];
                $scope.birthPlace  = response.data.data[0].birth_place ;
                $scope.district  = response.data.data[0].district ;
                $scope.state  = response.data.data[0].state ;
                $scope.nationality   = response.data.data[0].nationality ;
                $scope.Passport   = response.data.data[0].passport ;
                $scope.religion  = response.data.data[0].religion ;
                $scope.motherTongue  = response.data.data[0].mother_tounge ;
                $scope.caste   = response.data.data[0].caste ;
                $scope.category   = response.data.data[0].category ;
                $scope.isCastCertificate  = response.data.data[0].is_cast_certificate ;
                $scope.physicallyhandi  = response.data.data[0].is_physically_handi ;
                $scope.eligibilityNumber = response.data.data[0].eligibility_number;
                $scope.game  = response.data.data[0].game_played ;
                $scope.hobbies  = response.data.data[0].hobbies ;
                $scope.nss   = response.data.data[0].is_join_clg_NNS ;
                $scope.event   = response.data.data[0].is_participate_in_event ;
                $scope.photolink=response.data.data[0].uploaded_photo;
                $scope.signlink   = response.data.data[0].uploaded_signature;
                $scope.certificatelink   = response.data.data[0].uploaded_caste_certificate;
                $scope.physicallychallengedCertificatelink   = response.data.data[0].uploaded_physically_handi_certificate;
                $scope.multipleDocumentlink   = response.data.data[0].uploaded_other_documents.split(',');
               
                $scope.optionalArray = [];
                $scope.optionalArray = response.data.data[0].optinal_subject_selected.split(',');
                
                if($scope.optionalArray != ''){
                   
                    var j=0;
                    var i=0;
                    while(i< $scope.optionalArray.length){
                        if($scope.optionalArray[i] == $scope.optinalSubjectArray[j].name ){
                            $scope.optinalSubjectArray[j].selected = true;
                            
                            i++;
                        }else{
                            j++;
                        }
                    }
                }
                $scope.optionalArraysem2 = [];
                $scope.optionalArraysem2 = response.data.data[0].optinal_subject_selected_sem2.split(',');
                //console.log($scope.optionalArraysem2);
                if($scope.optionalArraysem2 != ''){
                    
                    var j=0;
                    var i=0;
                    while(i< $scope.optionalArraysem2.length){
                        if($scope.optionalArraysem2[i] == $scope.optinalSubjectArraysem2[j].name ){
                            $scope.optinalSubjectArraysem2[j].selected = true;
                            i++;
                        }else{
                            j++;
                        }
                    }
                }

                // $scope.valueAddedArray = [];
                // $scope.valueAddedArray = response.data.data[0].value_added_subject_selected.split(',');
                
                // if($scope.valueAddedArray != ''){
                    
                //     var j=0;
                //     var i=0;
                //     while(i< $scope.valueAddedArray.length){
                //         if($scope.valueAddedArray[i] == $scope.valueAddedSubjectArray[j].name ){
                //             $scope.valueAddedSubjectArray[j].selected = true;
                //             i++;
                //         }else{
                //             j++;
                //         }
                //     }
                // }
                // $scope.valueAddedArraysem2 = [];
                // $scope.valueAddedArraysem2 = response.data.data[0].value_added_subject_selected_sem2.split(',');
                // if($scope.valueAddedArraysem2 != ''){
                    
                //     var j=0;
                //     var i=0;
                //     while(i< $scope.valueAddedArraysem2.length){
                //         if($scope.valueAddedArraysem2[i] == $scope.valueAddedSubjectArraysem2[j].name ){
                //             $scope.valueAddedSubjectArraysem2[j].selected = true;
                //             i++;
                //         }else{
                //             j++;
                //         }
                //     }
                // }
                
                $scope.isMaharashtra = response.data.data[0].is_maharashtra ;
                $scope.sscExam = response.data.data[0].ssc_exam_name ;
                //$scope.sscBoard = response.data.data[0].ssc_board_university_name ;
                $scope.sscBoardOther = response.data.data[0].ssc_board_university_name.split("-");
                $scope.sscBoard = $scope.sscBoardOther[0];
                $scope.sscBoardName = $scope.sscBoardOther[1];
                //alert($scope.sscBoardName);
                $scope.showSscAddUniversityNameDiv();
                $scope.sscClgName = response.data.data[0].ssc_school_clg_name ;
                $scope.sscPassingYear = response.data.data[0].ssc_year_of_passing ;
                $scope.sscSeatNo = response.data.data[0].ssc_seat_number ;
                $scope.sscMarksObtain = response.data.data[0].ssc_marks_obtained ;
                $scope.sscTotalMarks = response.data.data[0].ssc_total_marks ;
                $scope.sscPercentageValue = response.data.data[0].ssc_percentage ;
                $scope.sscMarksheetlink = response.data.data[0].ssc_uploaded_documents ;

                $scope.hscExam = response.data.data[0].hsc_exam_name ;
                $scope.hscBoardOther = response.data.data[0].hsc_board_university_name.split("-");
                $scope.hscBoard = $scope.hscBoardOther[0];
                $scope.hscBoardName = $scope.hscBoardOther[1];
                $scope.showHscAddUniversityNameDiv();
                $scope.hscClgName = response.data.data[0].hsc_school_clg_name ;
                $scope.hscPassingYear = response.data.data[0].hsc_year_of_passing ;
                $scope.hscSeatNo = response.data.data[0].hsc_seat_number ;
                $scope.hscMarksObtain = response.data.data[0].hsc_marks_obtained ;
                $scope.hscTotalMarks = response.data.data[0].hsc_total_marks ;
                $scope.hscPercentageValue = response.data.data[0].hsc_percentage ;
                $scope.hscMarksheetlink  = response.data.data[0].hsc_uploaded_documents ;
                
                $scope.tyBComExam = response.data.data[0].tybcom_exam_name ;
                $scope.tyBComBoardOther = response.data.data[0].tybcom_board_university_name.split("-");
                $scope.tyBComBoard = $scope.tyBComBoardOther[0];
                $scope.tyBComBoardName = $scope.tyBComBoardOther[1];
                $scope.showtyBComAddUniversityNameDiv();
                $scope.tyBComClgName = response.data.data[0].tybcom_school_clg_name ;
                $scope.tyBComPassingYear = response.data.data[0].tybcom_year_of_passing ;
                $scope.tyBComSeatNo = response.data.data[0].tybcom_seat_number ;
                $scope.tyBComMarksObtain = response.data.data[0].tybcom_marks_obtained ;
                $scope.tyBComTotalMarks = response.data.data[0].tybcom_total_marks ;
                $scope.tyBComPercentageValue = response.data.data[0].tybcom_percentage ;
                $scope.tyBComMarksheetlink  = response.data.data[0].tybcom_uploaded_documents ;  

                $scope.fyExam = response.data.data[0].fy_exam_name ;
                $scope.fyBoardOther = response.data.data[0].fy_board_university_name.split("-");
                $scope.fyBoard = $scope.fyBoardOther[0];
                $scope.fyBoardName = $scope.fyBoardOther[1];
                $scope.showFyAddUniversityNameDiv();
                $scope.fyClgName = response.data.data[0].fy_school_clg_name ;
                $scope.fyPassingYear = response.data.data[0].fy_year_of_passing ;
                $scope.fySeatNo = response.data.data[0].fy_seat_number ;
                $scope.fyMarksObtain = response.data.data[0].fy_marks_obtained ;
                $scope.fyTotalMarks = response.data.data[0].fy_total_marks ;
                $scope.fyPercentageValue  = response.data.data[0].fy_percentage ;
                $scope.fyMarksheetlink = response.data.data[0].fy_uploaded_documents ;

                $scope.syExam = response.data.data[0].sy_exam_name ;
                $scope.syBoardOther = response.data.data[0].sy_board_university_name.split("-");
                $scope.syBoard = $scope.syBoardOther[0];
                $scope.syBoardName = $scope.syBoardOther[1];
                $scope.showSyAddUniversityNameDiv();
                $scope.syClgName = response.data.data[0].sy_school_clg_name ;
                $scope.syPassingYear = response.data.data[0].sy_year_of_passing ;
                $scope.sySeatNo = response.data.data[0].sy_seat_number ;
                $scope.syMarksObtain = response.data.data[0].sy_marks_obtained ;
                $scope.syTotalMarks = response.data.data[0].sy_total_marks ;
                $scope.syPercentageValue = response.data.data[0].sy_percentage ;
                $scope.syMarksheetlink = response.data.data[0].sy_uploaded_documents ;
                
                $scope.isgapReason = response.data.data[0].is_gap_reason;
                $scope.gapReason = response.data.data[0].gap_reason ;
                $scope.fatherFirstName = response.data.data[0].father_first_name ;
                $scope.fatherMiddleName = response.data.data[0].father_middle_name ;
                $scope.fatherLastName = response.data.data[0].father_last_name ;
                $scope.fatherEmail = response.data.data[0].father_email_address ;
                $scope.fatherMobileNo = response.data.data[0].father_mobile_number ;
                $scope.fatherOccupation = response.data.data[0].father_occupation ;
                $scope.fathercGovtEmp = response.data.data[0].is_father_central_govern_employee ;
                $scope.fatherAnnualIncome = response.data.data[0].father_annual_income ;
                $scope.fatherecoBack = response.data.data[0].is_father_economical_backward ;

                $scope.motherFirstName = response.data.data[0].mother_first_name ;
                $scope.motherMiddleName = response.data.data[0].mother_middle_name ;
                $scope.motherLastName = response.data.data[0].mother_last_name ;
                $scope.motherEmail = response.data.data[0].mother_email_address ;
                $scope.motherMobileNo = response.data.data[0].mother_mobile_number ;
                $scope.motherOccupation = response.data.data[0].mother_occupation ;
                $scope.mothercGovtEmp = response.data.data[0].is_mother_central_govern_employee ;
                $scope.motherAnnualIncome = response.data.data[0].mother_annual_income ;
                $scope.motherecoBack = response.data.data[0].is_mother_economical_backward ;

                $scope.guaradianFirstName = response.data.data[0].guardian_first_name ;
                $scope.guaradianMiddleName = response.data.data[0].guardian_middle_name ;
                $scope.guaradianLastName = response.data.data[0].guardian_last_name ;
                $scope.guaradianEmail = response.data.data[0].guardian_email_address ;
                $scope.guaradianMobileNo = response.data.data[0].guardian_mobile_number ;
                $scope.guaradianOccupation = response.data.data[0].guardian_occupation ;
                $scope.guaradiancGovtEmp = response.data.data[0].is_guardian_central_govern_employee ;
                $scope.guaradianAnnualIncome = response.data.data[0].guardian_annual_income ;
                $scope.guaradianecoBack = response.data.data[0].is_guardian_economical_backward ;
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }

    $scope.getReligionList = function () {
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'EditProfile/getReligionList', {
            'csrf_token_name': csrfHash
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.religionListArray = response.data.data;
                //$scope.getCasteList();
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }
    // $scope.getCasteList = function () {
        
    //     var transform = function(data) {
    //         return $.param(data);
    //     } 
    //     $http.post(window.site_url + 'AdmissionForm/getCasteList', {
    //         'csrf_token_name': csrfHash,
    //         religionName : $scope.religion,
    //     }, {
    //         headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
    //         transformRequest: transform
    //     })
    //     .then(function(response) {
    //         if (response.data.status.status == "1") {
    //             $scope.CasteListArray = response.data.data;
    //            // $scope.getSubCasteList();
    //         }else {
    //             toastr.error(response.data.status.message);
    //         }
    //     }, function(response) {
    //     })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    // }

    // $scope.getSubCasteList = function(){
       
    //     var transform = function(data) {
    //         return $.param(data);
    //     } 
    //     $http.post(window.site_url + 'AdmissionForm/getSubCasteList', {
    //         'csrf_token_name': csrfHash,
    //         casteName : $scope.cast,
    //     }, {
    //         headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
    //         transformRequest: transform
    //     })
    //     .then(function(response) {
    //         if (response.data.status.status == "1") {
    //             $scope.subCasteListArray = response.data.data;
               
                
    //         }else {
    //             toastr.error(response.data.status.message);
    //         }
    //     }, function(response) {
    //     })    
    // }
   
    $scope.logOut = function(){
        
        localStorage.setItem("StudentLoggedIn", "False");
        localStorage.setItem("StudentId", "");
        setTimeout(() => {
                window.location = new_url+ "online-admission/index.html";
                },0)
    }

    $scope.getStudentRegistrationDetails = function (storedNames) {
        $scope.student_id = storedNames;
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'EditProfile/getStudentRegistrationDetails', {
            'csrf_token_name': csrfHash,
            student_id : $scope.student_id ,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.studentRegistrationDetails=response.data.data[0];
                $scope.emailId  = response.data.data[0].student_email ;
                $scope.mobileNumber = response.data.data[0].student_mobile ;
                $scope.academicYear= response.data.data[0].stud_academic_year;
                $scope.courseName = response.data.data[0].course_name;
                $scope.courseYear = response.data.data[0].course_year;
                $scope.showEligibilityNoDiv();
                $scope.showDiv();
                $scope.SubjectListArray = [];
                $scope.getSubjectList();
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
    }
    $scope.getSubjectList = function(){
        var transform = function(data) {
            return $.param(data);
        } 
        $http.post(window.site_url + 'EditProfile/getSubjectList', {
            'csrf_token_name': csrfHash,
            courseName : $scope.courseName,
            courseYear : $scope.courseYear,
        }, {
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            transformRequest: transform
        })
        .then(function(response) {
            if (response.data.status.status == "1") {
                $scope.optinalSubjectArray = [];
                $scope.optinalSubjectArraysem2 = [];
                $scope.compulsorySubjectArray = [];
                $scope.compulsorySubjectArraysem2 = [];
                $scope.valueAddedSubjectArraysem2 =[];
                $scope.valueAddedSubjectArray = [];
                $scope.SubjectListArray = response.data.data;
                
                var arraylen = $scope.SubjectListArray.length;
                for(i=0;i<arraylen ; i++){
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.optinalSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Optional' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.optinalSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester,optionalsubtype : $scope.SubjectListArray[i].optional_sub_type, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.compulsorySubjectArray.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Compulsory' && ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.compulsorySubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name, semester : $scope.SubjectListArray[i].course_semester} );
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'  && ($scope.SubjectListArray[i].course_semester == 'Semester I'|| $scope.SubjectListArray[i].course_semester == 'Semester III' || $scope.SubjectListArray[i].course_semester == 'Semester V')){
                        $scope.valueAddedSubjectArray.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                    if($scope.SubjectListArray[i].subject_type == 'Value Added'&& ($scope.SubjectListArray[i].course_semester == 'Semester II'|| $scope.SubjectListArray[i].course_semester == 'Semester IV' || $scope.SubjectListArray[i].course_semester == 'Semester VI')){
                        $scope.valueAddedSubjectArraysem2.push({name : $scope.SubjectListArray[i].subject_name,semester : $scope.SubjectListArray[i].course_semester, selected: false});
                    }
                }
                
                $scope.optionalLeng = ($scope.optinalSubjectArray).length;
                $scope.valueAddedLeng = ($scope.valueAddedSubjectArray).length;
                $scope.valueAddedsem2Leng = ($scope.valueAddedSubjectArraysem2).length;
              $scope.getStudentList(); 
              $scope.getReligionList();
            }else {
                toastr.error(response.data.status.message);
            }
        }, function(response) {
        })      
    }


    var namePattern = /^([a-zA-Z-',.\s]{1,30})$/;
    var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    var castePattern = /^[a-zA-z\s][a-zA-Z .,/_-\s]+$/;
    var mobileNumberPattern = /^[7-9][0-9]{9}$/;
    var aadharNumberPattern =  /^[0-9]{12}$/;
    var eligibilityNumberPattern = /^[0-9]{11}$/;
    var voterNumberPattern = /^[A-Z]{3}[0-9]{7}$/;
    var marksPattern = /^[0-9]{1,6}$/;

    $scope.saveProfile = function(){
        
        $dateOfBirth= $("#dateOfBirth").val();
        // if ($scope.isSindhi) {
        //     if ($scope.fromJaihind) {
                if($scope.lastName) {
                    if(!($scope.lastName.match(namePattern))){
                        $('#lastName').focus();
                            toastr.error("Please enter valid Last Name.");
                            return false;
                    }
                    if ($scope.firstName) {
                        if(!($scope.firstName.match(namePattern))){
                        $('#firstName').focus();
                        toastr.error("Please enter valid First Name.");
                        return false;
                        }
                    
                        if ($scope.middleName) {
                            if(!($scope.middleName.match(namePattern))){
                                $('#middleName').focus();
                                toastr.error("Please enter valid Middle Name.");
                                return false;
                            }
                            if($scope.motherName){
                                if(!($scope.motherName.match(namePattern))){
                                    $('#motherName').focus();
                                    toastr.error("Please enter valid Mother's Name.");
                                    return false;
                                }
                                if ($scope.emailId) {
                                    if (!($scope.emailId.match(emailPattern))) {
                                        $('#emailId').focus();
                                        toastr.error("Email address is required.");
                                        return false;
                                    }
                                
                                    if ($scope.mobileNumber) {
                                        if (!($scope.mobileNumber.match(mobileNumberPattern))) {
                                            $('#mobileNumber').focus();
                                            toastr.error("Please enter valid Mobile Number.");
                                            return false;
                                        }
                                    
                                        if ($scope.aadharNumber) {
                                            if (!($scope.aadharNumber.match(aadharNumberPattern))) {
                                                $('#aadharNumber').focus();
                                                toastr.error(" Please enter valid Aadhar Number");
                                                return false;
                                            }
                                        
                                            // if ($scope.voterNumber) {
                                                // if (!($scope.voterNumber.match(voterNumberPattern))) {
                                                //     $('#voterNumber').focus();
                                                //     toastr.error("Please enter valid voter Number.");
                                                //     return false;
                                                // }
                                            
                                                if ($scope.localAddress) {
                                                    if ($scope.nativePlace) {
                                                        if (!($scope.nativePlace.match(namePattern))) {
                                                            $('#nativePlace').focus();
                                                            toastr.error("Please enter valid Native Place.");
                                                            return false;
                                                        }

                                                    
                                                    if($scope.bloodGroup){
                                                        if ($scope.gender) {
                                                            if ($scope.maritalstatus) {
                                                                if ($scope.day) {
                                                                    if ($scope.month) {
                                                                        if ($scope.dateyear) {
                                                                            $scope.dateOfBirth=  $scope.day+"-"+$scope.month+"-"+$scope.dateyear;
                                                                    if ($scope.birthPlace) {
                                                                        if (!($scope.birthPlace.match(namePattern))) {
                                                                            $('#birthPlace').focus();
                                                                            toastr.error("Please enter valid Birth Place.");
                                                                            return false;
                                                                        }
                                                                        if ($scope.district) {
                                                                            if (!($scope.district.match(namePattern))) {
                                                                                $('#district').focus();
                                                                                toastr.error("Please enter valid District Name.");
                                                                                return false;
                                                                            }
                                                            
                                                                            if($scope.state) {
                                                                                if (!($scope.state.match(namePattern))) {
                                                                                    $('#state').focus();
                                                                                    toastr.error("Please enter valid State Name.");
                                                                                    return false;
                                                                                }
                                                                                if($scope.nationality){
                                                                                    if (!($scope.nationality.match(namePattern))) {
                                                                                        $('#nationality').focus();
                                                                                        toastr.error("Please enter valid Nationality.");
                                                                                        return false;
                                                                                    }
                                                                    
                                                                                    if($scope.isSindhi == 'sindhiNo'){
                                                                                        if ($scope.religion == "") {
                                                                                            $('#religion').focus();
                                                                                            toastr.error("Religion is required.");
                                                                                            return false;
                                                                                        }
                                                                                            if ($scope.category == "") {
                                                                                                $('#category').focus();
                                                                                                toastr.error("Category is required.");
                                                                                                return true;
                                                                                            }
                                                                                                
                                                                                                if ($scope.caste) {
                                                                                                    if (!($scope.caste.match(castePattern))) {
                                                                                                        $('#caste').focus();
                                                                                                        toastr.error("Please enter valid Caste Name .");
                                                                                                        return false;
                                                                                                    }
                                                                                                }else{
                                                                                                    $('#caste').focus();
                                                                                                    toastr.error("caste is required.");
                                                                                                    return false;
                                                                                                }
                                                                                            }
                                                                                                if ($scope.motherTongue) {
                                                                                                    if (!($scope.motherTongue.match(namePattern))) {
                                                                                                        $('#motherTongue').focus();
                                                                                                        toastr.error("Please enter valid Mother Tongue .");
                                                                                                        return false;
                                                                                                    }
                                                                        
                                                                                                    if ($scope.isCastCertificate) {
                                                                                                        if($scope.physicallyhandi){
                                                                                                        if($scope.courseYear == "Second Year" || $scope.courseYear == "Third Year"){

                                                                                                            if($scope.eligibilityNumber == ''){
                                                                                                                $('#eligibilityNumber').focus();
                                                                                                                toastr.error("Please enter Eligibility Number.");
                                                                                                                return false;
                                                                                                            }
                                                                                                            if($scope.eligibilityNumber){
                                                                                                                if (!($scope.eligibilityNumber.match(eligibilityNumberPattern))) {
                                                                                                                    $('#eligibilityNumber').focus();
                                                                                                                    toastr.error("Please enter valid eligibility Number.");
                                                                                                                    return false;
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                        //if ($scope.nss) {
                                                                                                            //if ($scope.event) { 
                                                                                                                if($scope.certificatelink == ''){
                                                                                                                    if($scope.isCastCertificate == 'Yes'){
                                                                                                                        if($scope.casteCertificate == ''){
                                                                                                                            toastr.error("Please Upload Caste Certificate.");
                                                                                                                            return false;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                                if($scope.physicallychallengedCertificatelink == ''){
                                                                                                                    if($scope.physicallyhandi == 'Yes'){
                                                                                                                        if($scope.physicallychallengedCertificate == ''){
                                                                                                                            toastr.error("Please Upload physically challenged/visually or hearing impaired Certificate.");
                                                                                                                            return false;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                

                                                                                                                $scope.optinalSubjectSelected = new Array();
                                                                                                                $scope.optinalSubjectSelectedsem2 = new Array();
                                                                                                                for (var i = 0; i < $scope.optinalSubjectArray.length; i++) {
                                                                                                                    if ($scope.optinalSubjectArray[i].selected == true) {
                                                                                                                        
                                                                                                                        $scope.optinalSubjectSelected.push($scope.optinalSubjectArray[i].name);
                                                                                                                        
                                                                                                                    }
                                                                                                                }
                                                                                                                for (var i = 0; i < $scope.optinalSubjectArraysem2.length; i++) {
                                                                                                                    if ($scope.optinalSubjectArraysem2[i].selected == true) {
                                                                                                                        //alert($scope.optinalSubjectArraysem2[i].name);
                                                                                                                        $scope.optinalSubjectSelectedsem2.push($scope.optinalSubjectArraysem2[i].name);
                                                                                                                        
                                                                                                                    }
                                                                                                                }
                                                                                                        
                                                                                                               $scope.valueAddedSubjectSelected = new Array();
                                                                                                               $scope.valueAddedSubjectSelectedsem2 = new Array();
                                                                                                                // for (var i = 0; i < $scope.valueAddedLeng; i++) {
                                                                                                                    
                                                                                                                //     if ($scope.valueAddedSubjectArray[i].selected  == true) {
                                                                                                                        
                                                                                                                //          $scope.valueAddedSubjectSelected.push($scope.valueAddedSubjectArray[i].name);
                                                                                                                //     }
                                                                                                                // }
                                                                                                                // for (var i = 0; i < $scope.valueAddedsem2Leng; i++) {
                                                                                                                    
                                                                                                                //     if ($scope.valueAddedSubjectArraysem2[i].selected  == true) {
                                                                                                                       
                                                                                                                //          $scope.valueAddedSubjectSelectedsem2.push($scope.valueAddedSubjectArraysem2[i].name);
                                                                                                                //     }
                                                                                                                // }
                                                                                                                // if($scope.valueAddedSubjectSelected == ''){
                                                                                                                //     toastr.error("Mandatory to select at least one semesterwise value added subject.");
                                                                                                                //     return false; 
                                                                                                                // }
                                                                                                                // if($scope.valueAddedSubjectSelectedsem2 == ''){
                                                                                                                //     toastr.error("Mandatory to select at least one semesterwise value added subject.");
                                                                                                                //     return false; 
                                                                                                                // }
                                                                                                                if($scope.isMaharashtra == ''){
                                                                                                                    $('#maharashtraYes').focus();
                                                                                                                    toastr.error("Please select whether if you are come from maharashtra or not?");
                                                                                                                    return false; 
                                                                                                                }
                                                                                                                
                                                                                    if($scope.courseYear == "First Year"){
                                                                                        if($scope.sscExam){
                                                                                            if(!( $scope.sscExam.match(namePattern))){
                                                                                                $('#sscExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#sscExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.sscBoard == ''){
                                                                                                $('#sscBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.sscBoard == 'Other'){
                                                                                                if($scope.sscBoardName == '' || $scope.sscBoardName == undefined){
                                                                                                    $('#sscBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.sscBoard =  $scope.sscBoard +"-"+$scope.sscBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.sscClgName){
                                                                                                    if(!( $scope.sscClgName.match(namePattern))){
                                                                                                        $('#sscClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#sscClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.sscPassingYear == ''){
                                                                                                        $('#sscPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.sscSeatNo ==''){
                                                                                                            $('#sscSeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.sscMarksObtain){
                                                                                                                if(!( $scope.sscMarksObtain.match(marksPattern))){
                                                                                                                    $('#sscMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#sscMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.sscTotalMarks){
                                                                                                                    if(!( $scope.sscTotalMarks.match(marksPattern))){
                                                                                                                        $('#sscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#sscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                                                if(Number($scope.sscTotalMarks) <= Number($scope.sscMarksObtain)){
                                                                                                                    $('#sscMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.sscMarksheetlink == ''){
                                                                                                                    if($scope.sscMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }
                                                                                            
                                                                                        
                                                                                    //}
                                                                                    
                                                                                        if($scope.hscExam){
                                                                                            if(!( $scope.hscExam.match(namePattern))){
                                                                                                $('#hscExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#hscExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.hscBoard == ''){
                                                                                                $('#hscBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.hscBoard == 'Other'){
                                                                                                if($scope.hscBoardName == '' || $scope.hscBoardName == undefined){
                                                                                                    $('#hscBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.hscBoard =  $scope.hscBoard +"-"+$scope.hscBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.hscClgName){
                                                                                                    if(!( $scope.hscClgName.match(namePattern))){
                                                                                                        $('#hscClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#hscClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.hscPassingYear == ''){
                                                                                                        $('#hscPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.hscSeatNo ==''){
                                                                                                            $('#hscSeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.hscMarksObtain){
                                                                                                                if(!( $scope.hscMarksObtain.match(marksPattern))){
                                                                                                                    $('#hscMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#hscMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.hscTotalMarks){
                                                                                                                    if(!( $scope.hscTotalMarks.match(marksPattern))){
                                                                                                                        $('#hscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#hscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                                                if(Number($scope.hscTotalMarks) <= Number($scope.hscMarksObtain)){
                                                                                                                    $('#hscMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.hscMarksheetlink == ''){
                                                                                                                    if($scope.hscMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }
                                                                                        if($scope.courseName == 'M.Com'){
                                                                                            if($scope.tyBComExam){
                                                                                                if(!( $scope.tyBComExam.match(namePattern))){
                                                                                                    $('#tyBCom').focus();
                                                                                                    toastr.error("Please enter valid Exam Name.");
                                                                                                    return false;   
                                                                                                }
                                                                                            }else{
                                                                                                $('#tyBComExam').focus();
                                                                                                toastr.error("Please enter Exam Name.");
                                                                                                return false;  
                                                                                            }
                                                                                                if($scope.tyBComBoard == ''){
                                                                                                    $('#tyBComBoard').focus();
                                                                                                    toastr.error("Please Select University Name");
                                                                                                    return false; 
                                                                                                }
                                                                                                if($scope.tyBComBoard == 'Other'){
                                                                                                    if($scope.tyBComBoardName == '' || $scope.tyBComBoardName == undefined){
                                                                                                    
                                                                                                        $('#tyBComBoardName').focus();
                                                                                                        toastr.error("Please Enter University Name");
                                                                                                        return false; 
                                                                                                    }else{
                                                                                                        $scope.tyBComBoard =  $scope.tyBComBoard +"-"+$scope.tyBComBoardName;
                                                                                                    }
                                                                                                    
                                                                                                
                                                                                                }
                                                                                                    if($scope.tyBComClgName){
                                                                                                        if(!( $scope.tyBComClgName.match(namePattern))){
                                                                                                            $('#tyBComClgName').focus();
                                                                                                            toastr.error("Please enter valid college Name.");
                                                                                                            return false; 
                                                                                                        }  
                                                                                                    }else{
                                                                                                        $('#tyBComClgName').focus();
                                                                                                        toastr.error("Please enter college Name.");
                                                                                                        return false;  
                                                                                                    }
                                                                                
                                                                                                        if($scope.tyBComPassingYear == ''){
                                                                                                            $('#tyBComPassingYear').focus();
                                                                                                            toastr.error("Please Select Passing Year.");
                                                                                                            return false;
                                                                                                        }
                                                                                                            if($scope.tyBComSeatNo ==''){
                                                                                                                $('#tyBComSeatNo').focus();
                                                                                                                toastr.error("Please Enter Exam Seat Number.");
                                                                                                                return false;
                                                                                                            }
                                                                                                        
                                                                                                                if($scope.tyBComMarksObtain){
                                                                                                                    if(!( $scope.tyBComMarksObtain.match(marksPattern))){
                                                                                                                        $('#tyBComMarksObtain').focus();
                                                                                                                        toastr.error("Please enter valid Obtain Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#tyBComMarksObtain').focus();
                                                                                                                    toastr.error("Please enter Obtain Marks.");
                                                                                                                    return false; 
                                                                                                                }
                                                                                                                    if($scope.tyBComTotalMarks){
                                                                                                                        if(!( $scope.tyBComTotalMarks.match(marksPattern))){
                                                                                                                            $('#tyBComTotalMarks').focus();
                                                                                                                            toastr.error("Please enter valid Total Marks.");
                                                                                                                            return false;   
                                                                                                                        }
                                                                                                                    }else{
                                                                                                                        $('#tyBComTotalMarks').focus();
                                                                                                                            toastr.error("Please enter  Total Marks.");
                                                                                                                            return false; 
                                                                                                                    }
                                                                                                                    if(Number($scope.tyBComTotalMarks) <= Number($scope.tyBComMarksObtain)){
                                                                                                                        $('#tyBComMarksObtain').focus();
                                                                                                                            toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                            return false;
                                                                                                                    }
                                                                                                                    if($scope.tyBComMarksheetlink == ''){
                                                                                                                        if($scope.tyBComMarksheet == ''){
                                                                                                                            toastr.error("Please Upload Marksheet.");
                                                                                                                            return false; 
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                            
                                                                                        
                                                                                    }
                                                                            
                                                                                    if($scope.courseYear == "Second Year"){
                                                                                    
                                                                                        if($scope.sscExam){
                                                                                            if(!( $scope.sscExam.match(namePattern))){
                                                                                                $('#sscExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#sscExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.sscBoard == ''){
                                                                                                $('#sscBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.sscBoard == 'Other'){
                                                                                                if($scope.sscBoardName == '' || $scope.sscBoardName == undefined){
                                                                                                    $('#sscBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.sscBoard =  $scope.sscBoard +"-"+$scope.sscBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.sscClgName){
                                                                                                    if(!( $scope.sscClgName.match(namePattern))){
                                                                                                        $('#sscClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#sscClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.sscPassingYear == ''){
                                                                                                        $('#sscPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.sscSeatNo ==''){
                                                                                                            $('#sscSeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.sscMarksObtain){
                                                                                                                if(!( $scope.sscMarksObtain.match(marksPattern))){
                                                                                                                    $('#sscMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#sscMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.sscTotalMarks){
                                                                                                                    if(!( $scope.sscTotalMarks.match(marksPattern))){
                                                                                                                        $('#sscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#sscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                                                if(Number($scope.sscTotalMarks) <= Number($scope.sscMarksObtain)){
                                                                                                                    $('#sscMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.sscMarksheetlink == ''){
                                                                                                                    if($scope.sscMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }
                                                                                            
                                                                                        
                                                                                    //}
                                                                                    
                                                                                        if($scope.hscExam){
                                                                                            if(!( $scope.hscExam.match(namePattern))){
                                                                                                $('#hscExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#hscExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.hscBoard == ''){
                                                                                                $('#hscBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.hscBoard == 'Other'){
                                                                                                if($scope.hscBoardName == '' || $scope.hscBoardName == undefined){
                                                                                                    $('#hscBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.hscBoard =  $scope.hscBoard +"-"+$scope.hscBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.hscClgName){
                                                                                                    if(!( $scope.hscClgName.match(namePattern))){
                                                                                                        $('#hscClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#hscClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.hscPassingYear == ''){
                                                                                                        $('#hscPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.hscSeatNo ==''){
                                                                                                            $('#hscSeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.hscMarksObtain){
                                                                                                                if(!( $scope.hscMarksObtain.match(marksPattern))){
                                                                                                                    $('#hscMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#hscMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.hscTotalMarks){
                                                                                                                    if(!( $scope.hscTotalMarks.match(marksPattern))){
                                                                                                                        $('#hscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#hscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                                                if(Number($scope.hscTotalMarks) <= Number($scope.hscMarksObtain)){
                                                                                                                    $('#hscMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.hscMarksheetlink == ''){
                                                                                                                    if($scope.hscMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }

                                                                                        if($scope.courseName == 'M.Com'){
                                                                                            if($scope.tyBComExam){
                                                                                                if(!( $scope.tyBComExam.match(namePattern))){
                                                                                                    $('#tyBCom').focus();
                                                                                                    toastr.error("Please enter valid Exam Name.");
                                                                                                    return false;   
                                                                                                }
                                                                                            }else{
                                                                                                $('#tyBComExam').focus();
                                                                                                toastr.error("Please enter Exam Name.");
                                                                                                return false;  
                                                                                            }
                                                                                                if($scope.tyBComBoard == ''){
                                                                                                    $('#tyBComBoard').focus();
                                                                                                    toastr.error("Please Select University Name");
                                                                                                    return false; 
                                                                                                }
                                                                                                if($scope.tyBComBoard == 'Other'){
                                                                                                    if($scope.tyBComBoardName == '' || $scope.tyBComBoardName == undefined){
                                                                                                    
                                                                                                        $('#tyBComBoardName').focus();
                                                                                                        toastr.error("Please Enter University Name");
                                                                                                        return false; 
                                                                                                    }else{
                                                                                                        $scope.tyBComBoard =  $scope.tyBComBoard +"-"+$scope.tyBComBoardName;
                                                                                                    }
                                                                                                    
                                                                                                
                                                                                                }
                                                                                                    if($scope.tyBComClgName){
                                                                                                        if(!( $scope.tyBComClgName.match(namePattern))){
                                                                                                            $('#tyBComClgName').focus();
                                                                                                            toastr.error("Please enter valid college Name.");
                                                                                                            return false; 
                                                                                                        }  
                                                                                                    }else{
                                                                                                        $('#tyBComClgName').focus();
                                                                                                        toastr.error("Please enter college Name.");
                                                                                                        return false;  
                                                                                                    }
                                                                                
                                                                                                        if($scope.tyBComPassingYear == ''){
                                                                                                            $('#tyBComPassingYear').focus();
                                                                                                            toastr.error("Please Select Passing Year.");
                                                                                                            return false;
                                                                                                        }
                                                                                                            if($scope.tyBComSeatNo ==''){
                                                                                                                $('#tyBComSeatNo').focus();
                                                                                                                toastr.error("Please Enter Exam Seat Number.");
                                                                                                                return false;
                                                                                                            }
                                                                                                        
                                                                                                                if($scope.tyBComMarksObtain){
                                                                                                                    if(!( $scope.tyBComMarksObtain.match(marksPattern))){
                                                                                                                        $('#tyBComMarksObtain').focus();
                                                                                                                        toastr.error("Please enter valid Obtain Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#tyBComMarksObtain').focus();
                                                                                                                    toastr.error("Please enter Obtain Marks.");
                                                                                                                    return false; 
                                                                                                                }
                                                                                                                    if($scope.tyBComTotalMarks){
                                                                                                                        if(!( $scope.tyBComTotalMarks.match(marksPattern))){
                                                                                                                            $('#tyBComTotalMarks').focus();
                                                                                                                            toastr.error("Please enter valid Total Marks.");
                                                                                                                            return false;   
                                                                                                                        }
                                                                                                                    }else{
                                                                                                                        $('#tyBComTotalMarks').focus();
                                                                                                                            toastr.error("Please enter  Total Marks.");
                                                                                                                            return false; 
                                                                                                                    }
                                                                                                                    if(Number($scope.tyBComTotalMarks) <= Number($scope.tyBComMarksObtain)){
                                                                                                                        $('#tyBComMarksObtain').focus();
                                                                                                                            toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                            return false;
                                                                                                                    }
                                                                                                                    if($scope.tyBComMarksheetlink == ''){
                                                                                                                        if($scope.tyBComMarksheet == ''){
                                                                                                                            toastr.error("Please Upload Marksheet.");
                                                                                                                            return false; 
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                            
                                                                            
                                                                                        if($scope.fyExam){
                                                                                            if(!( $scope.fyExam.match(namePattern))){
                                                                                                $('#fyExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#fyExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.fyBoard == ''){
                                                                                                $('#fyBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.fyBoard == 'Other'){
                                                                                                if($scope.fyBoardName == '' || $scope.fyBoardName == undefined){
                                                                                                    $('#fyBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.fyBoard =  $scope.fyBoard +"-"+$scope.fyBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.fyClgName){
                                                                                                    if(!( $scope.fyClgName.match(namePattern))){
                                                                                                        $('#fyClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#fyClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.fyPassingYear == ''){
                                                                                                        $('#fyPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.fySeatNo ==''){
                                                                                                            $('#fySeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.fyMarksObtain){
                                                                                                                if(!( $scope.fyMarksObtain.match(marksPattern))){
                                                                                                                    $('#fyMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#fyMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.fyTotalMarks){
                                                                                                                    if(!( $scope.fyTotalMarks.match(marksPattern))){
                                                                                                                        $('#fyTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#fyTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                            
                                                                                                                if(Number($scope.fyTotalMarks) <= Number($scope.fyMarksObtain)){
                                                                                                                    $('#fyMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.fyMarksheetlink == ''){
                                                                                                                    if($scope.fyMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }
                                                                                        
                                                                                    }
                                                                            
                                                                                    if($scope.courseYear == "Third Year"){
                                                                                    
                                                                                        if($scope.sscExam){
                                                                                            if(!( $scope.sscExam.match(namePattern))){
                                                                                                $('#sscExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#sscExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.sscBoard == ''){
                                                                                                $('#sscBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.sscBoard == 'Other'){
                                                                                                if($scope.sscBoardName == '' || $scope.sscBoardName == undefined){
                                                                                                    $('#sscBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.sscBoard =  $scope.sscBoard +"-"+$scope.sscBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.sscClgName){
                                                                                                    if(!( $scope.sscClgName.match(namePattern))){
                                                                                                        $('#sscClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#sscClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.sscPassingYear == ''){
                                                                                                        $('#sscPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.sscSeatNo ==''){
                                                                                                            $('#sscSeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.sscMarksObtain){
                                                                                                                if(!( $scope.sscMarksObtain.match(marksPattern))){
                                                                                                                    $('#sscMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#sscMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.sscTotalMarks){
                                                                                                                    if(!( $scope.sscTotalMarks.match(marksPattern))){
                                                                                                                        $('#sscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#sscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                                                if(Number($scope.sscTotalMarks) <= Number($scope.sscMarksObtain)){
                                                                                                                    $('#sscMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.sscMarksheetlink == ''){
                                                                                                                    if($scope.sscMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }
                                                                                            
                                                                                        
                                                                                    //}
                                                                                    
                                                                                        if($scope.hscExam){
                                                                                            if(!( $scope.hscExam.match(namePattern))){
                                                                                                $('#hscExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#hscExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.hscBoard == ''){
                                                                                                $('#hscBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.hscBoard == 'Other'){
                                                                                                if($scope.hscBoardName == '' || $scope.hscBoardName == undefined){
                                                                                                    $('#hscBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.hscBoard =  $scope.hscBoard +"-"+$scope.hscBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.hscClgName){
                                                                                                    if(!( $scope.hscClgName.match(namePattern))){
                                                                                                        $('#hscClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#hscClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.hscPassingYear == ''){
                                                                                                        $('#hscPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.hscSeatNo ==''){
                                                                                                            $('#hscSeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.hscMarksObtain){
                                                                                                                if(!( $scope.hscMarksObtain.match(marksPattern))){
                                                                                                                    $('#hscMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#hscMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.hscTotalMarks){
                                                                                                                    if(!( $scope.hscTotalMarks.match(marksPattern))){
                                                                                                                        $('#hscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#hscTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                                                if(Number($scope.hscTotalMarks) <= Number($scope.hscMarksObtain)){
                                                                                                                    $('#hscMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.hscMarksheetlink == ''){
                                                                                                                    if($scope.hscMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }

                                                                                        if($scope.courseName == 'M.Com'){
                                                                                            if($scope.tyBComExam){
                                                                                                if(!( $scope.tyBComExam.match(namePattern))){
                                                                                                    $('#tyBCom').focus();
                                                                                                    toastr.error("Please enter valid Exam Name.");
                                                                                                    return false;   
                                                                                                }
                                                                                            }else{
                                                                                                $('#tyBComExam').focus();
                                                                                                toastr.error("Please enter Exam Name.");
                                                                                                return false;  
                                                                                            }
                                                                                                if($scope.tyBComBoard == ''){
                                                                                                    $('#tyBComBoard').focus();
                                                                                                    toastr.error("Please Select University Name");
                                                                                                    return false; 
                                                                                                }
                                                                                                if($scope.tyBComBoard == 'Other'){
                                                                                                    if($scope.tyBComBoardName == '' || $scope.tyBComBoardName == undefined){
                                                                                                    
                                                                                                        $('#tyBComBoardName').focus();
                                                                                                        toastr.error("Please Enter University Name");
                                                                                                        return false; 
                                                                                                    }else{
                                                                                                        $scope.tyBComBoard =  $scope.tyBComBoard +"-"+$scope.tyBComBoardName;
                                                                                                    }
                                                                                                    
                                                                                                
                                                                                                }
                                                                                                    if($scope.tyBComClgName){
                                                                                                        if(!( $scope.tyBComClgName.match(namePattern))){
                                                                                                            $('#tyBComClgName').focus();
                                                                                                            toastr.error("Please enter valid college Name.");
                                                                                                            return false; 
                                                                                                        }  
                                                                                                    }else{
                                                                                                        $('#tyBComClgName').focus();
                                                                                                        toastr.error("Please enter college Name.");
                                                                                                        return false;  
                                                                                                    }
                                                                                
                                                                                                        if($scope.tyBComPassingYear == ''){
                                                                                                            $('#tyBComPassingYear').focus();
                                                                                                            toastr.error("Please Select Passing Year.");
                                                                                                            return false;
                                                                                                        }
                                                                                                            if($scope.tyBComSeatNo ==''){
                                                                                                                $('#tyBComSeatNo').focus();
                                                                                                                toastr.error("Please Enter Exam Seat Number.");
                                                                                                                return false;
                                                                                                            }
                                                                                                        
                                                                                                                if($scope.tyBComMarksObtain){
                                                                                                                    if(!( $scope.tyBComMarksObtain.match(marksPattern))){
                                                                                                                        $('#tyBComMarksObtain').focus();
                                                                                                                        toastr.error("Please enter valid Obtain Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#tyBComMarksObtain').focus();
                                                                                                                    toastr.error("Please enter Obtain Marks.");
                                                                                                                    return false; 
                                                                                                                }
                                                                                                                    if($scope.tyBComTotalMarks){
                                                                                                                        if(!( $scope.tyBComTotalMarks.match(marksPattern))){
                                                                                                                            $('#tyBComTotalMarks').focus();
                                                                                                                            toastr.error("Please enter valid Total Marks.");
                                                                                                                            return false;   
                                                                                                                        }
                                                                                                                    }else{
                                                                                                                        $('#tyBComTotalMarks').focus();
                                                                                                                            toastr.error("Please enter  Total Marks.");
                                                                                                                            return false; 
                                                                                                                    }
                                                                                                                    if(Number($scope.tyBComTotalMarks) <= Number($scope.tyBComMarksObtain)){
                                                                                                                        $('#tyBComMarksObtain').focus();
                                                                                                                            toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                            return false;
                                                                                                                    }
                                                                                                                    if($scope.tyBComMarksheetlink == ''){
                                                                                                                        if($scope.tyBComMarksheet == ''){
                                                                                                                            toastr.error("Please Upload Marksheet.");
                                                                                                                            return false; 
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                            
                                                                            
                                                                                        if($scope.fyExam){
                                                                                            if(!( $scope.fyExam.match(namePattern))){
                                                                                                $('#fyExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#fyExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.fyBoard == ''){
                                                                                                $('#fyBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.fyBoard == 'Other'){
                                                                                                if($scope.fyBoardName == '' || $scope.fyBoardName == undefined){
                                                                                                    $('#fyBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.fyBoard =  $scope.fyBoard +"-"+$scope.fyBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.fyClgName){
                                                                                                    if(!( $scope.fyClgName.match(namePattern))){
                                                                                                        $('#fyClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#fyClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.fyPassingYear == ''){
                                                                                                        $('#fyPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.fySeatNo ==''){
                                                                                                            $('#fySeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.fyMarksObtain){
                                                                                                                if(!( $scope.fyMarksObtain.match(marksPattern))){
                                                                                                                    $('#fyMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#fyMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.fyTotalMarks){
                                                                                                                    if(!( $scope.fyTotalMarks.match(marksPattern))){
                                                                                                                        $('#fyTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#fyTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }

                                                                                                                if(Number($scope.fyTotalMarks) <= Number($scope.fyMarksObtain)){
                                                                                                                    $('#fyMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                            
                                                                                                                if($scope.fyMarksheetlink == ''){
                                                                                                                    if($scope.fyMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }
                                                                                        
                                                                                    
                                                                                        if($scope.syExam){
                                                                                            if(!( $scope.syExam.match(namePattern))){
                                                                                                $('#syExam').focus();
                                                                                                toastr.error("Please enter valid Exam Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#syExam').focus();
                                                                                            toastr.error("Please enter Exam Name.");
                                                                                            return false;  
                                                                                        }
                                                                                            if($scope.syBoard == ''){
                                                                                                $('#syBoard').focus();
                                                                                                toastr.error("Please Select Board Name");
                                                                                                return false; 
                                                                                            }
                                                                                            if($scope.syBoard == 'Other'){
                                                                                                if($scope.syBoardName== '' || $scope.syBoardName == undefined){
                                                                                                    $('#syBoardName').focus();
                                                                                                    toastr.error("Please Enter Board Name");
                                                                                                    return false; 
                                                                                                }else{
                                                                                                    $scope.syBoard =  $scope.syBoard +"-"+$scope.syBoardName;
                                                                                                }
                                                                                           
                                                                                            }
                                                                                                if($scope.syClgName){
                                                                                                    if(!( $scope.syClgName.match(namePattern))){
                                                                                                        $('#syClgName').focus();
                                                                                                        toastr.error("Please enter valid college Name.");
                                                                                                        return false; 
                                                                                                    }  
                                                                                                }else{
                                                                                                    $('#syClgName').focus();
                                                                                                    toastr.error("Please enter college Name.");
                                                                                                    return false;  
                                                                                                }
                                                                            
                                                                                                    if($scope.syPassingYear == ''){
                                                                                                        $('#syPassingYear').focus();
                                                                                                        toastr.error("Please Select Passing Year.");
                                                                                                        return false;
                                                                                                    }
                                                                                                        if($scope.sySeatNo ==''){
                                                                                                            $('#sySeatNo').focus();
                                                                                                            toastr.error("Please Enter Exam Seat Number.");
                                                                                                            return false;
                                                                                                        }
                                                                                                
                                                                                                            if($scope.syMarksObtain){
                                                                                                                if(!( $scope.syMarksObtain.match(marksPattern))){
                                                                                                                    $('#syMarksObtain').focus();
                                                                                                                    toastr.error("Please enter valid Obtain Marks.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#syMarksObtain').focus();
                                                                                                                toastr.error("Please enter Obtain Marks.");
                                                                                                                return false; 
                                                                                                            }
                                                                                                                if($scope.syTotalMarks){
                                                                                                                    if(!( $scope.syTotalMarks.match(marksPattern))){
                                                                                                                        $('#syTotalMarks').focus();
                                                                                                                        toastr.error("Please enter valid Total Marks.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#syTotalMarks').focus();
                                                                                                                        toastr.error("Please enter  Total Marks.");
                                                                                                                        return false; 
                                                                                                                }
                                                                                                                if(Number($scope.syTotalMarks) <= Number($scope.syMarksObtain)){
                                                                                                                    $('#syMarksObtain').focus();
                                                                                                                        toastr.error("Please enter correct total marks and obtain marks.");
                                                                                                                        return false;
                                                                                                                }
                                                                                                                if($scope.syMarksheetlink == ''){
                                                                                                                    if($scope.syMarksheet == ''){
                                                                                                                        toastr.error("Please Upload Marksheet.");
                                                                                                                        return false; 
                                                                                                                    }
                                                                                                                }
                                                                                    }
                                                                                    if($scope.isgapReason){
                                                                                        if($scope.isgapReason == 'gapReasonYes'){
                                                                                            if($scope.gapReason == ''){
                                                                                                $('#gapReason').focus();
                                                                                                toastr.error("Please enter gap reason is required.");
                                                                                                return false;
                                                                                            }
                                                                                        }
                                                                                    }else{
                                                                                        $('#gapReasonYes').focus();
                                                                                        toastr.error("Please Select If there is a gap reason or not.");
                                                                                        return false;
                                                                                    }

                                                                                    if($scope.fatherFirstName != '' || $scope.fatherMiddleName != '' || $scope.fatherLastName !='' || $scope.fatherEmail !='' || $scope.fatherMobileNo !='' || $scope.fatherOccupation !='' || $scope.fathercGovtEmp != '' || $scope.fatherAnnualIncome !='' || $scope.fatherecoBack != ''){ 
                                                                                        if($scope.fatherFirstName){
                                                                                            if(!($scope.fatherFirstName.match(namePattern))){
                                                                                                $('#fatherFirstName').focus();
                                                                                                toastr.error("Please enter valid Father First Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#fatherFirstName').focus();
                                                                                            toastr.error("Please enter Father First Name.");
                                                                                            return false;   
                                                                                        }
                                                                        
                                                                                            if($scope.fatherMiddleName){
                                                                                                if(!($scope.fatherMiddleName.match(namePattern))){
                                                                                                    $('#fatherMiddleName').focus();
                                                                                                    toastr.error("Please enter valid Father Middle Name.");
                                                                                                    return false;   
                                                                                                }
                                                                                            }else{
                                                                                                $('#fatherMiddleName').focus();
                                                                                                toastr.error("Please enter Father Middle Name.");
                                                                                                return false;   
                                                                                            }
                                                                                                if($scope.fatherLastName ){
                                                                                                    if(!($scope.fatherLastName.match(namePattern))){
                                                                                                        $('#fatherLastName').focus();
                                                                                                        toastr.error("Please enter valid Father Last Name.");
                                                                                                        return false;   
                                                                                                    }
                                                                                                }else{
                                                                                                    $('#fatherLastName').focus();
                                                                                                        toastr.error("Please enter Father Last Name.");
                                                                                                        return false;  
                                                                                                }
                                                                        
                                                                                                    if($scope.fatherEmail ){
                                                                                                        if(!($scope.fatherEmail.match(emailPattern))){
                                                                                                            $('#fatherEmail').focus();
                                                                                                            toastr.error("Please enter valid Father Email Id.");
                                                                                                            return false;   
                                                                                                        }
                                                                                                    }else{
                                                                                                        $('#fatherEmail').focus();
                                                                                                            toastr.error("Please enter Father Email Id.");
                                                                                                            return false; 
                                                                                                    }
                                                                        
                                                                                                        if($scope.fatherMobileNo ){
                                                                                                            if(!($scope.fatherMobileNo.match(mobileNumberPattern))){
                                                                                                                $('#fatherMobileNo').focus();
                                                                                                                toastr.error("Please enter valid Father Mobile Number.");
                                                                                                                return false;   
                                                                                                            }
                                                                                                            if($scope.fatherMobileNo == $scope.mobileNumber){
                                                                                                                $('#fatherMobileNo').focus();
                                                                                                                toastr.error("please enter different Mobile Number than Registered Mobile Number.");
                                                                                                                return false;  
                                                                                                            }
                                                                                                        }else{
                                                                                                            $('#fatherMobileNo').focus();
                                                                                                            toastr.error("Please enter Father Mobile Number.");
                                                                                                            return false;
                                                                                                        }
                                                                        
                                                                                                            if($scope.fatherOccupation ){
                                                                                                                if(!($scope.fatherOccupation.match(namePattern))){
                                                                                                                    $('#fatherOccupation').focus();
                                                                                                                    toastr.error("Please enter valid Father Occupation.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#fatherOccupation').focus();
                                                                                                                    toastr.error("Please enter Father Occupation.");
                                                                                                                    return false; 
                                                                                                            }
                                                                                                            if($scope.fathercGovtEmp == ''){
                                                                                                                $('#fathercGovtEmpYes').focus();
                                                                                                                    toastr.error("Please enter Father is Govenment Employee or Not.");
                                                                                                                    return false;
                                                                                                            }
                                                                        
                                                                                                                if($scope.fatherAnnualIncome ==''){
                                                                                                                    $('#fatherAnnualIncome').focus();
                                                                                                                    toastr.error("Please Select Father's  Annual Income.");
                                                                                                                    return false;
                                                                                                                }
                                                                        
                                                                                                                    if($scope.fatherecoBack== ''){
                                                                                                                        
                                                                                                                        $('#fatherecoBackYes').focus();
                                                                                                                        toastr.error("Please Select Father is Economical Backward or not.");
                                                                                                                        return false;
                                                                                                                    }
                                                                                    
                                                                                    } 
                                                                                    if($scope.motherFirstName != ''|| $scope.motherMiddleName != '' || $scope.motherLastName !='' || $scope.motherEmail != '' || $scope.motherMobileNo != '' || $scope.motherOccupation != '' || $scope.mothercGovtEmp != '' || $scope.motherAnnualIncome != '' || $scope.motherecoBack != ''){
                                                                                    
                                                                                        if($scope.motherFirstName){
                                                                                                    if(!($scope.motherFirstName.match(namePattern))){
                                                                                                        $('#motherFirstName').focus();
                                                                                                        toastr.error("Please enter valid Mother First Name.");
                                                                                                        return false;   
                                                                                                    }
                                                                                                }else{
                                                                                                    $('#motherFirstName').focus();
                                                                                                    toastr.error("Please enter Mother First Name.");
                                                                                                    return false;   
                                                                                                }
                                                                                
                                                                                                    if($scope.motherMiddleName){
                                                                                                        if(!($scope.motherMiddleName.match(namePattern))){
                                                                                                            $('#motherMiddleName').focus();
                                                                                                            toastr.error("Please enter valid Mother Middle Name.");
                                                                                                            return false;   
                                                                                                        }
                                                                                                    }else{
                                                                                                        $('#motherMiddleName').focus();
                                                                                                        toastr.error("Please enter Mother Middle Name.");
                                                                                                        return false;   
                                                                                                    }
                                                                                                        if($scope.motherLastName ){
                                                                                                            if(!($scope.motherLastName.match(namePattern))){
                                                                                                                $('#motherLastName').focus();
                                                                                                                toastr.error("Please enter Valid Mother Last Name.");
                                                                                                                return false;   
                                                                                                            }
                                                                                                        }else{
                                                                                                            $('#motherLastName').focus();
                                                                                                                toastr.error("Please enter Mother Last Name.");
                                                                                                                return false;  
                                                                                                        }
                                                                                
                                                                                                            if($scope.motherEmail ){
                                                                                                                if(!($scope.motherEmail.match(emailPattern))){
                                                                                                                    $('#motherEmail').focus();
                                                                                                                    toastr.error("Please enter valid Mother Email Id.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#motherEmail').focus();
                                                                                                                    toastr.error("Please enter Mother Email Id.");
                                                                                                                    return false; 
                                                                                                            }
                                                                                
                                                                                                                if($scope.motherMobileNo ){
                                                                                                                    if(!($scope.motherMobileNo.match(mobileNumberPattern))){
                                                                                                                        $('#motherMobileNo').focus();
                                                                                                                        toastr.error("Please enter valid Mother Mobile Number.");
                                                                                                                        return false;   
                                                                                                                    }
                                                                                                                    if($scope.motherMobileNo == $scope.mobileNumber){
                                                                                                                        $('#motherMobileNo').focus();
                                                                                                                        toastr.error("please enter different Mobile Number than Registered Mobile Number.");
                                                                                                                        return false;  
                                                                                                                    }
                                                                                                                }else{
                                                                                                                    $('#motherMobileNo').focus();
                                                                                                                    toastr.error("Please enter Mother Mobile Number.");
                                                                                                                    return false;
                                                                                                                }
                                                                                
                                                                                                                    if($scope.motherOccupation ){
                                                                                                                        if(!($scope.motherOccupation.match(namePattern))){
                                                                                                                            $('#motherOccupation').focus();
                                                                                                                            toastr.error("Please enter valid Mother Occupation.");
                                                                                                                            return false;   
                                                                                                                        }
                                                                                                                    }else{
                                                                                                                        $('#motherOccupation').focus();
                                                                                                                            toastr.error("Please enter Mother Occupation.");
                                                                                                                            return false; 
                                                                                                                    }
                                                                                                                    if($scope.mothercGovtEmp == ''){
                                                                                                                        $('#mothercGovtEmpYes').focus();
                                                                                                                            toastr.error("Please enter Mother is Govenment Employee or Not.");
                                                                                                                            return false;
                                                                                                                    }
                                                                                
                                                                                                                        if($scope.motherAnnualIncome ==''){
                                                                                                                            $('#motherAnnualIncome').focus();
                                                                                                                            toastr.error("Please Select Mother's  Annual Income.");
                                                                                                                            return false;
                                                                                                                        }
                                                                                
                                                                                                                            if($scope.motherecoBack== ''){
                                                                                                                                
                                                                                                                                $('#motherecoBackYes').focus();
                                                                                                                                toastr.error("Please Select Mother is Economical Backward or not.");
                                                                                                                                return false;
                                                                                                                            }
                                                                                    }
                                                                                    
                                                                        
                                                                                    if($scope.guaradianFirstName !='' || $scope.guaradianMiddleName !='' || $scope.guaradianLastName !='' || $scope.guaradianEmail !='' || $scope.guaradianMobileNo !='' || $scope.guaradianOccupation !='' || $scope.guaradiancGovtEmp  != '' || $scope.guaradianAnnualIncome !='' || $scope.guaradianecoBack != ''){ 
                                                                                    
                                                                                        if($scope.guaradianFirstName){
                                                                                            if(!($scope.guaradianFirstName.match(namePattern))){
                                                                                                $('#guaradianFirstName').focus();
                                                                                                toastr.error("Please enter valid guaradian First Name.");
                                                                                                return false;   
                                                                                            }
                                                                                        }else{
                                                                                            $('#guaradianFirstName').focus();
                                                                                            toastr.error("Please enter guaradian First Name.");
                                                                                            return false;   
                                                                                        }
                                                                        
                                                                                            if($scope.guaradianMiddleName){
                                                                                                if(!($scope.guaradianMiddleName.match(namePattern))){
                                                                                                    $('#guaradianMiddleName').focus();
                                                                                                    toastr.error("Please enter valid guaradian Middle Name.");
                                                                                                    return false;   
                                                                                                }
                                                                                            }else{
                                                                                                $('#guaradianMiddleName').focus();
                                                                                                toastr.error("Please enter guaradian Middle Name.");
                                                                                                return false;   
                                                                                            }
                                                                                                if($scope.guaradianLastName ){
                                                                                                    if(!($scope.guaradianLastName.match(namePattern))){
                                                                                                        $('#guaradianLastName').focus();
                                                                                                        toastr.error("Please enter valid guaradian Last Name.");
                                                                                                        return false;   
                                                                                                    }
                                                                                                }else{
                                                                                                    $('#guaradianLastName').focus();
                                                                                                        toastr.error("Please enter guaradian Last Name.");
                                                                                                        return false;  
                                                                                                }
                                                                        
                                                                                                    if($scope.guaradianEmail ){
                                                                                                        if(!($scope.guaradianEmail.match(emailPattern))){
                                                                                                            $('#guaradianEmail').focus();
                                                                                                            toastr.error("Please enter valid guaradian Email Id.");
                                                                                                            return false;   
                                                                                                        }
                                                                                                    }else{
                                                                                                        $('#guaradianEmail').focus();
                                                                                                            toastr.error("Please enter guaradian Email Id.");
                                                                                                            return false; 
                                                                                                    }
                                                                        
                                                                                                        if($scope.guaradianMobileNo ){
                                                                                                            if(!($scope.guaradianMobileNo.match(mobileNumberPattern))){
                                                                                                                $('#guaradianMobileNo').focus();
                                                                                                                toastr.error("Please enter valid guaradian Mobile Number.");
                                                                                                                return false;   
                                                                                                            }
                                                                                                            if($scope.guaradianMobileNo == $scope.mobileNumber){
                                                                                                                $('#guaradianMobileNo').focus();
                                                                                                                toastr.error("please enter Different Mobile Number than Registered Mobile Number.");
                                                                                                                return false;  
                                                                                                            }
                                                                                                        }else{
                                                                                                            $('#guaradianMobileNo').focus();
                                                                                                            toastr.error("Please enter guaradian Mobile Number.");
                                                                                                            return false;
                                                                                                        }
                                                                        
                                                                                                            if($scope.guaradianOccupation ){
                                                                                                                if(!($scope.guaradianOccupation.match(namePattern))){
                                                                                                                    $('#guaradianOccupation').focus();
                                                                                                                    toastr.error("Please enter valid guaradian Occupation.");
                                                                                                                    return false;   
                                                                                                                }
                                                                                                            }else{
                                                                                                                $('#guaradianOccupation').focus();
                                                                                                                    toastr.error("Please enter guaradian Occupation.");
                                                                                                                    return false; 
                                                                                                            }
                                                                                                            if($scope.guaradiancGovtEmp == ''){
                                                                                                                $('#guaradiancGovtEmpYes').focus();
                                                                                                                    toastr.error("Please enter guaradian is Govenment Employee or Not.");
                                                                                                                    return false;
                                                                                                            }
                                                                        
                                                                                                                if($scope.guaradianAnnualIncome ==''){
                                                                                                                    $('#guaradianAnnualIncome').focus();
                                                                                                                    toastr.error("Please Select guaradian's  Annual Income.");
                                                                                                                    return false;
                                                                                                                }
                                                                        
                                                                                                                    if($scope.guaradianecoBack== ''){
                                                                                                                        
                                                                                                                        $('#guaradianecoBackYes').focus();
                                                                                                                        toastr.error("Please Select guaradian is Economical Backward or not.");
                                                                                                                        return false;
                                                                                                                    }
                                                                                    
                                                                                    } 
                                                                                    if($scope.fatherFirstName == '' ||  $scope.motherFirstName == ''){
                                                                                        toastr.error("Manditory to fill Mother's and Father's Details.");
                                                                                        return false; 
                                                                                    }
                                                                                                                        $scope.disabledUpdateProfileInfoButton =true;
                                                                                                                        $scope.ShowSpinnerStatus = true;
                                                                                                                        $http({
                                                                                                                            method: 'POST',
                                                                                                                            url: window.site_url +'EditProfile/saveProfile',
                                                                                                                            processData: false,
                                                                                                                            transformRequest: function(data) {
                                                                                                                                var formData = new FormData();                    
                                                                                                                                    
                                                                                                                                formData.append("csrf_token_name", csrfHash);
                                                                                                                                formData.append("studentId", JSON.parse(localStorage.getItem("StudentId")));
                                                                                                                                formData.append("isSindhi",   $scope.isSindhi);
                                                                                                                                formData.append("fromJaihind", $scope.fromJaihind);
                                                                                                                                formData.append("firstName", $scope.firstName);
                                                                                                                                formData.append("middleName", $scope.middleName);
                                                                                                                                formData.append("lastName", $scope.lastName);
                                                                                                                                formData.append("motherName", $scope.motherName);
                                                                                                                                formData.append("emailId", $scope.emailId );
                                                                                                                                formData.append(" mobileNumber", $scope.mobileNumber);
                                                                                                                                formData.append(" alterMobileNumber", $scope.alterMobileNumber);
                                                                                                                                formData.append("aadharNumber", $scope.aadharNumber);
                                                                                                                                formData.append("voterNumber", $scope.voterNumber);
                                                                                                                                formData.append("localAddress", $scope.localAddress);
                                                                                                                                formData.append("nativePlace", $scope.nativePlace);
                                                                                                                                formData.append("bloodGroup", $scope.bloodGroup);
                                                                                                                                formData.append("gender", $scope.gender);
                                                                                                                                formData.append("maritalstatus", $scope.maritalstatus);
                                                                                                                                formData.append("dateOfBirth", $scope.dateOfBirth);
                                                                                                                                formData.append("birthPlace", $scope.birthPlace);
                                                                                                                                formData.append("district", $scope.district);
                                                                                                                                
                                                                                                                                formData.append("state", $scope.state);
                                                                                                                                formData.append("nationality", $scope.nationality);
                                                                                                                                formData.append("Passport", $scope.Passport);
                                                                                                                                formData.append("religion", $scope.religion);
                                                                                                                                formData.append("motherTongue", $scope.motherTongue);
                                                                                                                                formData.append("caste", $scope.caste);
                                                                                                                                formData.append("category", $scope.category);
                                                                                                                                formData.append("isCastCertificate", $scope.isCastCertificate);
                                                                                                                                formData.append("physicallyhandi", $scope.physicallyhandi);
                                                                                                                                formData.append("eligibilityNumber", $scope.eligibilityNumber);
                                                                                                                                formData.append("game", $scope.game );
                                                                                                                                formData.append("hobbies", $scope.hobbies);
                                                                                                                                formData.append("nss", $scope.nss);
                                                                                                                                formData.append("event", $scope.event);
                                                                                                                                formData.append("casteCertificate", $scope.casteCertificate);
                                                                                                                                formData.append("casteCertificate", $scope.casteCertificate);
                                                                                                                                formData.append("physicallychallengedCertificate", $scope.physicallychallengedCertificate);
                                                                                                                                formData.append("optinalSubjectSelected", $scope.optinalSubjectSelected);
                                                                                                                                formData.append("optinalSubjectSelectedsem2", $scope.optinalSubjectSelectedsem2);
                                                                                                                                
                                                                                                                                formData.append("valueAddedSubjectSelected", $scope.valueAddedSubjectSelected);
                                                                                                                                formData.append("valueAddedSubjectSelectedsem2", $scope.valueAddedSubjectSelectedsem2);
                                                                                                                                formData.append("isMaharashtra", $scope.isMaharashtra);
                                                                                                                                formData.append("sscExam",   $scope.sscExam);
                                                                                                                                formData.append("sscBoard", $scope.sscBoard);
                                                                                                                                formData.append("sscClgName", $scope.sscClgName);
                                                                                                                                formData.append("sscPassingYear", $scope.sscPassingYear);
                                                                                                                                formData.append("sscSeatNo", $scope.sscSeatNo);
                                                                                                                                formData.append("sscMarksObtain", $scope.sscMarksObtain );
                                                                                                                                formData.append("sscTotalMarks", $scope.sscTotalMarks);
                                                                                                                                formData.append("sscPercentage", $scope.sscPercentageValue);
                                                                                                                                formData.append("sscMarksheet", $scope.sscMarksheet);
                                                                                                                                formData.append("hscExam", $scope.hscExam);
                                                                                                                                formData.append("hscBoard", $scope.hscBoard);
                                                                                                                                formData.append("hscClgName", $scope.hscClgName);
                                                                                                                                formData.append("hscPassingYear", $scope.hscPassingYear);
                                                                                                                                formData.append("hscSeatNo", $scope.hscSeatNo);
                                                                                                                                formData.append("hscMarksObtain", $scope.hscMarksObtain);
                                                                                                                                formData.append("hscTotalMarks", $scope.hscTotalMarks);
                                                                                                                                formData.append("hscPercentage", $scope.hscPercentageValue);
                                                                                                                                formData.append("hscMarksheet", $scope.hscMarksheet);
                                                                                                                                formData.append("tyBComExam", $scope.tyBComExam);
                                                                                                                                formData.append("tyBComBoard", $scope.tyBComBoard);
                                                                                                                                formData.append("tyBComClgName", $scope.tyBComClgName);
                                                                                                                                formData.append("tyBComPassingYear", $scope.tyBComPassingYear);
                                                                                                                                formData.append("tyBComSeatNo", $scope.tyBComSeatNo);
                                                                                                                                formData.append("tyBComMarksObtain", $scope.tyBComMarksObtain);
                                                                                                                                formData.append("tyBComTotalMarks", $scope.tyBComTotalMarks);
                                                                                                                                formData.append("tyBComPercentage", $scope.tyBComPercentageValue);
                                                                                                                                formData.append("tyBComMarksheet", $scope.tyBComMarksheet);
                                                                                                                                formData.append("fyExam", $scope.fyExam);
                                                                                                                                formData.append("fyBoard", $scope.fyBoard);
                                                                                                                                formData.append("fyClgName", $scope.fyClgName);
                                                                                                                                formData.append("fyPassingYear", $scope.fyPassingYear);
                                                                                                                                formData.append("fySeatNo", $scope.fySeatNo);
                                                                                                                                formData.append("fyMarksObtain", $scope.fyMarksObtain);
                                                                                                                                formData.append("fyTotalMarks", $scope.fyTotalMarks);
                                                                                                                                formData.append("fyPercentage", $scope.fyPercentageValue);
                                                                                                                                formData.append("fyMarksheet", $scope.fyMarksheet );
                                                                                                                                formData.append("syExam", $scope.syExam);
                                                                                                                                formData.append("syBoard", $scope.syBoard);
                                                                                                                                formData.append("syClgName", $scope.syClgName);
                                                                                                                                formData.append("syPassingYear", $scope.syPassingYear);
                                                                                                                                formData.append("sySeatNo", $scope.sySeatNo);
                                                                                                                                formData.append("syMarksObtain", $scope.syMarksObtain);
                                                                                                                                formData.append("syTotalMarks", $scope.syTotalMarks);
                                                                                                                                formData.append("syPercentage", $scope.syPercentageValue);
                                                                                                                                formData.append("syMarksheet", $scope.syMarksheet);
                                                                                                                                formData.append("isgapReason", $scope.isgapReason);
                                                                                                                                
                                                                                                                                formData.append("gapReason", $scope.gapReason);
                                                                                                                               
                                                                                                                                
                                                                                                                                formData.append("fatherFirstName",  $scope.fatherFirstName);
                                                                                                                                formData.append("fatherMiddleName", $scope.fatherMiddleName );
                                                                                                                                formData.append("fatherLastName", $scope.fatherLastName );
                                                                                                                                formData.append("fatherEmail", $scope.fatherEmail );
                                                                                                                                formData.append("fatherMobileNo", $scope.fatherMobileNo );
                                                                                                                                formData.append("fatherOccupation", $scope.fatherOccupation );
                                                                                                                                formData.append("fathercGovtEmp", $scope.fathercGovtEmp);
                                                                                                                                formData.append("fatherAnnualIncome", $scope.fatherAnnualIncome );
                                                                                                                                formData.append("fatherecoBack", $scope.fatherecoBack );

                                                                                                                                formData.append("motherFirstName", $scope.motherFirstName );
                                                                                                                                formData.append("motherMiddleName", $scope.motherMiddleName );
                                                                                                                                formData.append("motherLastName", $scope.motherLastName );
                                                                                                                                formData.append("motherEmail", $scope.motherEmail );
                                                                                                                                formData.append("motherMobileNo", $scope.motherMobileNo );
                                                                                                                                formData.append("motherOccupation", $scope.motherOccupation );
                                                                                                                                formData.append("mothercGovtEmp", $scope.mothercGovtEmp );
                                                                                                                                formData.append("motherAnnualIncome", $scope.motherAnnualIncome );
                                                                                                                                formData.append("motherecoBack", $scope.motherecoBack);

                                                                                                                                formData.append("guaradianFirstName", $scope.guaradianFirstName );
                                                                                                                                formData.append("guaradianMiddleName", $scope.guaradianMiddleName );
                                                                                                                                formData.append("guaradianLastName", $scope.guaradianLastName );
                                                                                                                                formData.append("guaradianEmail", $scope.guaradianEmail );
                                                                                                                                formData.append("guaradianMobileNo", $scope.guaradianMobileNo );
                                                                                                                                formData.append("guaradianOccupation", $scope.guaradianOccupation );
                                                                                                                                formData.append("guaradiancGovtEmp", $scope.guaradiancGovtEmp );
                                                                                                                                formData.append("guaradianAnnualIncome", $scope.guaradianAnnualIncome );
                                                                                                                                formData.append("guaradianecoBack", $scope.guaradianecoBack );
                                                                                                                                
                                                                                                                                return formData;
                                                                                                                            },
                                                                                                                            data: $scope.form,
                                                                                                                            headers: {
                                                                                                                                'Content-Type': undefined
                                                                                                                            }
                                                                                                                        })
                                                                                                                        .then(function(response) {
                                                                                                                            if (response.data.status.status == "1") {

                                                                                                                                toastr.success(response.data.status.message);
                                                                                                                                $scope.init();
                                                                                                                                
                                                                                                    
                                                                                                                            }else{
                                                                                                                                toastr.error(response.data.status.message);
                                                                                                                                $scope.disabledUpdateProfileInfoButton =false;
                                                                                                                                $("#loadingDiv").css('display','none');
                                                                                                                                $scope.ShowSpinnerStatus = false;
                                                                                                                                
                                                                                                                            }
                                                                                                                        }, function(response) {
                                                                                                                        })

                                                                                                                // } else {
                                                                                                                //     $('#eventYes').focus();
                                                                                                                //     toastr.error("Please select whether participated in any event?");
                                                                                                                //     }
                                                                                                            // } else {
                                                                                                            //     $('#nssYes').focus();
                                                                                                            //     toastr.error("Please select Do you wish to join college N.S.S. ?");
                                                                                                            //     }
                                                                                                            } else {
                                                                                                                $('#isphysicallyhandiYes').focus();
                                                                                                                toastr.error("Please select Are you physically challenged/visually or hearing impaired ? ");
                                                                                                                }
                                                                                                        } else {
                                                                                                            $('#isCastCertificateYes').focus();
                                                                                                            toastr.error("Please select do you have cast certificate? ");
                                                                                                            }
                                                                                                    } else {
                                                                                                        $('#motherTongue').focus();
                                                                                                        toastr.error("Please enter  Mother Tongue ");
                                                                                                        }
                                                                                        //         } else {
                                                                                        //             $('#caste').focus();
                                                                                        //             toastr.error("caste is required.");
                                                                                        //             }
                                                                                    
                                                                                        //     } else {
                                                                                        //         $('#category').focus();
                                                                                        //         toastr.error("Category is required.");
                                                                                        //         }
                                                                                        // } else {
                                                                                        //     $('#religion').focus();
                                                                                        //     toastr.error("Religion is required.");
                                                                                        //     }
                                                                                    } else {
                                                                                        $('#nationality').focus();
                                                                                        toastr.error("Nationality is required.");
                                                                                        }  
                                                                                } else {
                                                                                    $('#state').focus();
                                                                                    toastr.error(" State is required.");
                                                                                    }
                                                                            } else {
                                                                                $('#district').focus();
                                                                                toastr.error(" District is required.");
                                                                                }                                              
                                                                        } else {
                                                                            $('#birthPlace').focus();
                                                                            toastr.error(" BirthPlace is required.");
                                                                            }
                                                                        } else {
                                                                            $('#dateyear').focus();
                                                                            toastr.error("DOB Year is required.");
                                                                            }
                                                                    } else {
                                                                        $('#month').focus();
                                                                        toastr.error("DOB Month is required.");
                                                                        }
                                                                } else {
                                                                    $('#day').focus();
                                                                    toastr.error(" DOB Day is required.");
                                                                    }
                                                            } else {
                                                                $('#married').focus();
                                                                toastr.error("Select Marital Status is required.");
                                                                }
                                                        } else {
                                                            $('#male').focus();
                                                            toastr.error("Select Gender is required.");
                                                            }
                                                    } else {
                                                        $('#bloodGroup').focus();
                                                        toastr.error("bloodGroup is required.");
                                                        }
                                                    } else {
                                                    $('#nativePlace').focus();
                                                    toastr.error("Native Place is required.");
                                                    }
                                                } else {
                                                $('#localAddress').focus();
                                                toastr.error("local Address is required.");
                                                }


                                            // } else {
                                            // $('#voterNumber').focus();
                                            // toastr.error("voter Number is required.");
                                            // }

                                        } else {
                                        $('#aadharNumber').focus();
                                        toastr.error("Aadhar Number is required.");
                                        }

                                    } else {
                                    $('#mobileNumber').focus();
                                    toastr.error("Mobile Number is required.");
                                    }

                                } else {
                                $('#emailId').focus();
                                toastr.error("Email address is required.");
                                }
                            } else {
                                $('#motherName').focus();
                                toastr.error("Mother's Name is required.");
                                }
                        } else {
                            $('#middleName').focus();
                            toastr.error("Middle Name is required.");
                            }

                    } else {
                    $('#firstName').focus();
                    toastr.error("First Name is required.");
                    }
            } else {
                $('#lastName').focus();
                toastr.error("Last Name is required.");
                }
    //     } else {
    //         $('#jaihindInternal').focus();
    //         toastr.error("please select are you from Jaihind college ?");
    //         }
    // } else {
    // $('#sindhiYes').focus();
    // toastr.error("please select are you Belongs to Sindhi Category ?");
    // }
                                                        
    }

    $scope.calculatePercentage = function(){
        $scope.sscPercentageValue = 0;
        $scope.hscPercentageValue = 0;
        $scope.tyBComPercentageValue = 0;
        $scope.fyPercentageValue = 0;
        $scope.syPercentageValue = 0;
        
        var sscMarksObtain = Number($scope.sscMarksObtain || 0);
        var sscTotalMarks = Number($scope.sscTotalMarks || 0);
        $scope.sscPercentageValue = ((sscMarksObtain / sscTotalMarks)*100).toFixed(2);
        
        var hscMarksObtain = Number($scope.hscMarksObtain || 0);
        var hscTotalMarks = Number($scope.hscTotalMarks || 0);
        $scope.hscPercentageValue = ((hscMarksObtain / hscTotalMarks)*100).toFixed(2);

        var tyBComMarksObtain = Number($scope.tyBComMarksObtain || 0);
        var tyBComTotalMarks = Number($scope.tyBComTotalMarks || 0);
        $scope.tyBComPercentageValue = ((tyBComMarksObtain / tyBComTotalMarks)*100).toFixed(2);
        
        var fyMarksObtain = Number($scope.fyMarksObtain || 0);
        var fyTotalMarks = Number($scope.fyTotalMarks || 0);
        $scope.fyPercentageValue = ((fyMarksObtain / fyTotalMarks)*100).toFixed(2);

        var syMarksObtain = Number($scope.syMarksObtain || 0);
        var syTotalMarks = Number($scope.syTotalMarks || 0);
        $scope.syPercentageValue = ((syMarksObtain / syTotalMarks)*100).toFixed(2);
    }
    $scope.uploadedCasteCertificate = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf' || file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png' || file.name.split('.').pop() == 'jpeg' ) {
                    var reader = new FileReader();
                    $scope.casteCertificate = element.files[0];  
                    
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });
    }
    $scope.uploadedPhysicallychallengedCertificate = function(element) {
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of image is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf' || file.name.split('.').pop() == 'jpg' || file.name.split('.').pop() == 'png' || file.name.split('.').pop() == 'jpeg' ) {
                    var reader = new FileReader();
                    $scope.physicallychallengedCertificate = element.files[0];  
                    
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });
    }
    $scope.uploadedSSCMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.sscMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedHSCMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.hscMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedFYMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png' ) {
                    var reader = new FileReader();
                    $scope.fyMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedSYMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.syMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.uploadedtyBComMarksheet = function(element){
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.pdf|.jpg|.png|.jpeg)$/;
        $scope.$apply(function($scope) {                
            var file = element.files[0];
            var fileSize = element.files[0].size / 1024 / 1024; 
            if(fileSize > 2){
                toastr.error("size of Document is "+ Math.round(fileSize) + "MB. Please upload an file with size below than 2 MB.");
                return false;
            }
            else { 
                if (file.name.split('.').pop() == 'pdf'|| file.name.split('.').pop() == 'jpg'  || file.name.split('.').pop() == 'png') {
                    var reader = new FileReader();
                    $scope.tyBComMarksheet = element.files[0];  
                   
                } else {
                    toastr.error('Please upload correct File , File extension should be (.pdf|.jpg|.png|.jpeg)');
                    return false;
                }
            }
        });  
    }

    $scope.printToCart = function(viewStudentDetails) {
        var innerContents = document.getElementById(viewStudentDetails).innerHTML;
        var popupWin; 
        popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
        popupWin.document.open();
        popupWin.document.write(`
        <html>
            <head>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/styles/style.css"/>
                <link rel="stylesheet" href="assets/fonts/themify-icons/themify-icons.css"/>
                <link rel="stylesheet" href="assets/plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css"/>
                <link rel="stylesheet" href="assets/plugin/waves/waves.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/media/css/dataTables.bootstrap.min.css"/>
                <link rel="stylesheet" href="assets/plugin/datatables/extensions/Responsive/css/responsive.bootstrap.min.css"/>
                <script src="assets/scripts/modernizr.min.js"></script>
                <script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/scripts/main.min.js"></script>
                <script src="assets/angular/angular.min.js"></script>
                <script src="assets/angular/ui-bootstrap-tpls-1.0.3.js"></script>
                <script src="assets/scripts/jquery.min.js"></script>
                <script src="assets/angular/angularjs/app.js"></script>
            </head>
            
            <body onload="window.print();window.close()">${innerContents}
            
            </body>
        </html>`
        );
        popupWin.document.close();
      }


});