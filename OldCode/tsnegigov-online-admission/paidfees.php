<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Admission : Application Fees</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/online-admission-style.css">
    <link rel="stylesheet" type="text/css" href="css/online-admission-theme25.css">
</head>
<body>
    <div class="form-body" ng-app="myApp" ng-controller="paidFeedServicesCtrl" >
        <div class="website-logo" >
            <center><a href="index.html">
                <div class="logo">
                    <img class="logo-size" src="images/logo.png" alt="">
                </div>
                
            </a></center>
            <br/><br/>
                 <div >
                    <h4 style="color: black;">THAKUR SEN NEGI GOVERNMENT COLLEGE</h4>
                </div>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                    <img class="md-size" src="images/graphic7.svg" alt="">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <?php  
                               $getDetails = base64_decode($_GET['FeesDeatils']);
                               $splitArrayValues = explode("|",$getDetails);
                        ?>
                        <form>
                            <label><b>Student Name</b></label>
                            <input class="form-control" type="text" name="studName"  id="studName" placeholder="Student Name" ng-required="required" value="<?php echo $splitArrayValues[0] ?>" readonly/>
                            <label><b>Student Email</b></label>
                            <input class="form-control" type="text" name="studEmail"  id="studEmail" placeholder="Student Email Address" value="<?php echo $splitArrayValues[1] ?>" ng-required="required" readonly/>
                            <label><b>Total Fee</b></label>
                            <input class="form-control" type="text" name="studPayFees"  id="studPayFees" placeholder="Fee Paid Amount" value="<?php echo $splitArrayValues[3] ?>" ng-required="required" readonly/>
                            <input class="form-control" type="hidden" name="studID"  id="studID" value="<?php echo $splitArrayValues[2] ?>"  />
                            
                            <div class="form-button" ng-init="init()">
                                <a  id="submit" type="button" ng-click="checkPayentLinkIsStopOrNot('view')" class="ibtn" ng-show="paymentLinkActiveorNotBit">Pay Now</a>
                                <a  class="ibtn" ng-show="!paymentLinkActiveorNotBit" style="background-color: #c94444;">Payment Link Is Expire</a>
                            </div>
                        </form>
                       
                   </div>
               </div>
           </div>
       </div>
   </div>
   <script src="js/jquery.min.js"></script>
   <script src="js/popper.min.js"></script>
   <script src="js/bootstrap.min.js"></script>
   <script src="js/main.js"></script>
       <script src="assets/angular/angular.min.js"></script>
    <script src="assets/angular/ui-bootstrap-tpls-1.0.3.js"></script>
    <!-- toastr -->
    <script src="assets/angular/angular-toastr.tpls.js"></script>
    <link rel="stylesheet" href="assets/angular/angular-toastr.css" />

    <script src="assets/angular/angularjs/app.js"></script>
    <!-- app.js -->

    <script src="assets/angular/angularjs/controllers/paidfees.js"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
</body>
</html>