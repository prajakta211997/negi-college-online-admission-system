<?php 

class ManagePaymentInstallment_model extends CI_Model {

    function addPaymentInstallment(){
        
        
        $courseName = $_POST['courseName'] ;
        $courseYear = $_POST['courseYear'] ;
        $academicYear = $_POST['academicYear'] ;
        $totalFee = $_POST['totalFee'] ;
        $firstInstallment = $_POST['firstInstallment'] ;
        $secondInstallment = $_POST['secondInstallment'] ;
        $thirdInstallment = $_POST['thirdInstallment'] ;
        $courseFeesType = $_POST['courseFeesType'];
        $query = $this->db->get_where('manage_payment_installment', array(
            'academic_year' => $academicYear,
            'course_name' => $courseName,
            'course_year' => $courseYear,
            'fees_type' => $courseFeesType,
            'total_fee' => $totalFee,
            'first_installment' => $firstInstallment,
            'second_installment' => $secondInstallment,
            'third_installment' => $thirdInstallment,
            'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                    'academic_year' => $academicYear,
                    'course_name' => $courseName,
                    'course_year' => $courseYear,
                    'fees_type' => $courseFeesType,
                    'total_fee' => $totalFee,
                    'first_installment' => $firstInstallment,
                    'second_installment' => $secondInstallment,
                    'third_installment' => $thirdInstallment
                );
                
                $insert = $this->db->insert('manage_payment_installment', $data);

                 return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    }

    

    function getPaymentInstallmentList(){
        $this->db->select("*");
        $this->db->from("manage_payment_installment");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function searchByPaymentInstallments(){
        $PaymentInstallmentSearch = $_POST['PaymentInstallmentSearch'];
        $this->db->select("*");
        $this->db->from("manage_payment_installment");
        $this->db->like('course_name', $PaymentInstallmentSearch);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getAllCoursesName(){
        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getCourseFees(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
        $courseFeesType = $_POST['courseFeesType'];
            
            $this->db->select("*");
            $this->db->from("manage_course_fees");
            $this->db->where('delete_bit', '0');
            $totalfeeArray= $this->db->get()->result_array();
        
            foreach($totalfeeArray as $totalfee) {
                if(($totalfee['course_name']== $courseName)&& ($totalfee['course_year'] == $courseYear) && ( $totalfee['academic_year']==$academicYear)  ){
                    if($courseFeesType != ''){
                        if($totalfee['fees_sub_type'] == $courseFeesType){
                            $totalFees = $totalfee['total_fees'];
                            return $totalFees;
                        }
                    }else{
                        $totalFees = $totalfee['total_fees'];
                        return $totalFees;
                    }
                    
                   
                      
                }
            }
    }
    function updatePaymentInstallment(){
        $PaymentInstallmentId = $_POST['updatePaymentInstallmentId'];
        $courseName = $_POST['courseName'] ;
        $courseYear = $_POST['courseYear'] ;
        $academicYear = $_POST['academicYear'] ;
        $totalFee = $_POST['totalFee'] ;
        $firstInstallment = $_POST['firstInstallment'] ;
        $secondInstallment = $_POST['secondInstallment'] ;
        $thirdInstallment = $_POST['thirdInstallment'] ;
        $courseFeesType = $_POST['courseFeesType'];
        $query = $this->db->get_where('manage_payment_installment', array(
            'academic_year' => $academicYear,
            'course_name' => $courseName,
            'course_year' => $courseYear,
            'fees_type' => $courseFeesType,
            'total_fee' => $totalFee,
            'first_installment' => $firstInstallment,
            'second_installment' => $secondInstallment,
            'third_installment' => $thirdInstallment,
            'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                    'academic_year' => $academicYear,
                    'course_name' => $courseName,
                    'course_year' => $courseYear,
                    'fees_type' => $courseFeesType,
                    'total_fee' => $totalFee,
                    'first_installment' => $firstInstallment,
                    'second_installment' => $secondInstallment,
                    'third_installment' => $thirdInstallment
                );
              
                $this->db->where('payment_install_id', $PaymentInstallmentId);
                $this->db->update('manage_payment_installment', $data);
                return true; //return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    
    }
    function deletePaymentInstallment(){
        $updatePaymentInstallmentId = $_POST['updatePaymentInstallmentId'];
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('payment_install_id', $updatePaymentInstallmentId);
        $this->db->update('manage_payment_installment', $data);
        
       return true;
       
    }
  
    
    
    
}

?>