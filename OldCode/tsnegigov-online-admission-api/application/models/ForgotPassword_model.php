<?php 

class ForgotPassword_model extends CI_Model {

    function ForgotPasswordSendEmail(){
        $forgotEmail = $_POST['forgotEmail'];
        $query = $this->db->get_where('student_registration', array('student_email' => $forgotEmail));
        if ($query->num_rows() == 1) {
            $loginArrayFromDB = $query->result_array();
            foreach($loginArrayFromDB as $loginArrayv){
                $result['name']= $loginArrayv['student_name'];
                $result['userPassword']= $loginArrayv['student_password'];
                $result['email']= $forgotEmail;
                $result['subject'] = 'Forgot Password Request';
                $template = 'email-template/user-forgot-password';
                $sendMail = sendMail($result,$template);

                return 1;
            }
        } 
        else {
            return 2;
        }
        //return $userName;
    }
}

?>