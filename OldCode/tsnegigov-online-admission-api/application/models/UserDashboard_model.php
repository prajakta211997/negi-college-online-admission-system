<?php 

class UserDashboard_model extends CI_Model {
 
   function getStudentList(){
     
      $student_id = $_POST['student_id'];
      $this->db->select("*");
      $this->db->from("student_registration sr");
      $this->db->where('sr.student_id', $student_id);
      $this->db->where('sr.admission_step_one', '1');
      $this->db->where('sr.admission_step_two', '1');
      $this->db->where('sr.admission_step_three', '1');
      $this->db->where('sr.delete_bit', '0');
     // $this->db->where('parentinfo.form_fee_submitted_bit', '1');
      $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
      $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
      $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
      $studentData =  $this->db->get()->result_array();
      
      $this->db->select("*");
      $this->db->from("student_course_fees_payment_details ");
      $this->db->where('student_id', $student_id);
      $paymentData = $this->db->get()->result_array();
      if(empty($paymentData) ){
        $studentData[0]['student_payment_paid'] = 0;
      }
      else{
        $studentData[0]['student_payment_paid'] = 1;
      }
      return $studentData;
  }
  
//   function getSubjectList(){
//    $courseName = $_POST['courseName'];
//    $courseYear = $_POST['courseYear'];
//    $this->db->select("*");
//    $this->db->from("manage_subject");
//    $this->db->where('course_name', $courseName);
//    $this->db->where('course_year', $courseYear);
//    $this->db->where('delete_bit', '0');
//    return $this->db->get()->result_array();
//   }
  
   

    
}

?>