<?php 

class UserPaymentDetails_model extends CI_Model {

    function getPaymentList(){
        $StudentIdForPayment = $_POST['StudentIdForPayment'];
        $this->db->select("*");
        $this->db->from("student_course_fees_payment_details scfd");
        $this->db->where('scfd.student_id', $StudentIdForPayment);
        $this->db->where('scfd.delete_bit', '0');
        $this->db->where('parentinfo.student_admission_status', '3');
        
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = scfd.student_id");
        return $this->db->get()->result_array();
    }
    
    function getTotalFees(){
        $courseArray = [];
        $feesArray = [];
        $stud_id = $_POST['stud_id'];
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.student_id', $stud_id);
        $this->db->where('parentinfo.student_admission_status', '3');
        
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        
        $courseArray= $this->db->get()->result_array();
   
        $this->db->select("*");
        $this->db->from("manage_course_fees ");
        $this->db->where('delete_bit', '0');
        $feesArray = $this->db->get()->result_array();
        
       $name= $courseArray[0]['course_name'];
       $coursey= $courseArray[0]['course_year'];
       $acdemic = $courseArray[0]['stud_academic_year'];
     
     
       for($i = 0; $i < count($feesArray); $i++)
        {
            
           if($feesArray[$i]['course_name']== $name && $feesArray[$i]['course_year']== $coursey && $feesArray[$i]['academic_year']== $acdemic )
            { 
              $totalFees = $feesArray[$i]['total_fees'] ;
              return $totalFees;  
            }
            
       }

    }
   
    
    
    

    
}

?>