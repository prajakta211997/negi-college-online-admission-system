<?php 

class AdminForgotPassword_model extends CI_Model {

    function ForgotPasswordSendEmail(){
        $forgotEmail = $_POST['forgotEmail'];
        $query = $this->db->get_where('user_login', array('user_email' => $forgotEmail));
        if ($query->num_rows() == 1) {
            $loginArrayFromDB = $query->result_array();
            foreach($loginArrayFromDB as $loginArrayv){
                $result['name']= $loginArrayv['user_name'];
                $result['userPassword']= $this->my_simple_crypt($loginArrayv['user_password'],'d');
                $result['email']= $forgotEmail;
                $result['subject'] = 'Forgot Password Request';
                $template = 'email-template/user-forgot-password';
                $sendMail = sendUserDetailsMail($result,$template);

                $result1['name']= $loginArrayv['user_name'];
                $result1['userPassword']= $this->my_simple_crypt($loginArrayv['user_password'],'d');
                $result1['email']= admin_email;
                $result1['subject'] = 'Forgot Password Request By User';
                $template1 = 'email-template/admin-forgot-password';
                $sendMail1 = sendUserDetailsMail($result1,$template1);
                return 1;
            }
        } 
        else {
            return 2;
        }
        //return $userName;
    }

    function my_simple_crypt( $string, $action) {
        // you may change these values to your own
        $secret_key = 'inoxpa_key';
        $secret_iv = 'inoxpa_iv';
     
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
     
        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
     
        return $output;
    }
    
}

?>