<?php 

class ReceivedStudentsPaymentList_model extends CI_Model {

    function getAllCoursesName(){
        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getReligionList(){
        $this->db->select("*");
        $this->db->from("manage_religion");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }
    
    

    function getCasteList(){
        $this->db->select("*");
        $this->db->from("manage_caste mc");
        $this->db->where('mc.delete_bit', '0');
        $this->db->where('mr.delete_bit', '0');
        $this->db->join("manage_religion mr", 'mr.religion_id = mc.religion_id');
        return $this->db->get()->result_array();
    }

    function getPaymentRecivedStudentList(){
        $this->db->select("*");
        $this->db->from("student_course_fees_payment_details sc");
        $this->db->join("student_registration sr" ,"sr.student_id = sc.student_id");
        return $this->db->get()->result_array();
    }
    
    function getFilterData(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
        $leaveDateFr = $_POST['leaveDateFrom'];
        $leaveDateT = $_POST['leaveDateTo'];
        $leaveDateFrom=date("yy-m-d",strtotime($leaveDateFr)).' 00:00'; 
        $leaveDateTo=date("yy-m-d",strtotime($leaveDateT)).' 23:59';
        
        $this->db->select("*");
        $this->db->from("student_course_fees_payment_details pc");
        $this->db->join("student_registration sr" ,"sr.student_id = pc.student_id");
        $this->db->where('sr.delete_bit', '0');
       // $this->db->where('parentinfo.form_fee_submitted_bit', '1');
      
        if($courseName){
         $this->db->where('sr.course_name', $courseName);
        }
        if($courseYear){
         $this->db->where('sr.course_year', $courseYear);
        }
        if($academicYear){
         $this->db->where('sr.stud_academic_year', $academicYear);
        }
        $this->db->where('pc.submitted_by >=', $leaveDateFrom);
        $this->db->where('pc.submitted_by <=', $leaveDateTo);
        
        return $this->db->get()->result_array(); 
    }

     
    function getSingleStudentDetails(){
        $student_id = $_POST['student_id'];
        
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.student_id',$student_id);
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
        $this->db->where('parentinfo.student_admission_status', '2');
        //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        return $this->db->get()->result_array();
    }
      

    function removeFromShortlist(){
        $studentId = $_POST['studentId'];
        $data = array(
            'student_admission_status' => "1"
        );
        $this->db->where('student_id', $studentId);
        $this->db->update('student_admission_form_parent_info', $data);
       // $this->insert_user_log("Delete User");
        return true;
    }

    function addToConfirmed(){
        $studentId = $_POST['studentId'];
        $data = array(
            'student_admission_status' => "3"
        );

        $this->db->select('*');
        $this->db->from('student_registration');
        $this->db->where('student_id',$studentId);
        $this->db->where('delete_bit','0');
        $StudentArray = $this->db->get()->result_array();
        $studAcademicYear = $StudentArray[0]['stud_academic_year'];
        $studCourseName = $StudentArray[0]['course_name'];
        $studCourseYear = $StudentArray[0]['course_year'];
        $studName = $StudentArray[0]['student_name'];
        $studEmail = $StudentArray[0]['student_email'];
            
            $this->db->select("*");
            $this->db->from("manage_course ");
            $this->db->where('delete_bit', '0');
            $courseData =  $this->db->get()->result_array();
          
         for($i = 0; $i < count($courseData); $i++)
         {
            
           if($courseData[$i]['course_name']== $studCourseName && $courseData[$i]['courses_year']== $studCourseYear && $courseData[$i]['academics_year']== $studAcademicYear )
            { 
               $TotalSeat = $courseData[$i]['total_seats'] ;
               
            }
            
         }
         
          

            $this->db->select("*");
            $this->db->from("student_registration sr");
            $this->db->where('sr.course_name',$studCourseName);
            $this->db->where('sr.course_year',$studCourseYear);
            $this->db->where('sr.stud_academic_year',$studAcademicYear);
         
            $this->db->where('parentinfo.student_admission_status', '3');
            //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
            $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
            $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
            $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
            $queryOption = $this->db->get();
            $studCount = $queryOption->num_rows();
        
        if($studCount >= $TotalSeat){
            return -1;
        }
        else{
            $this->db->where('student_id', $studentId);
            $this->db->update('student_admission_form_parent_info', $data);

            
            $result['name']= $studName;
            $result['email']= $studEmail;
            $result['subject'] = 'Your admission is confirmed at M U College of Commerce';
            $template = 'email-template/online-admission-confirmation';
            $sendMail = sendMail($result,$template);
            
            return true;
        }
        
       // $this->insert_user_log("Delete User");
        
    }
    
    function addSelectedToConfirm(){
        $studentId = $_POST['studentId'];
        foreach($studentId as $studId){
            $this->db->select('*');
            $this->db->from('student_registration');
            $this->db->where('student_id',$studId);
            $this->db->where('delete_bit','0');
            $StudentArray = $this->db->get()->result_array();
            $studAcademicYear = $StudentArray[0]['stud_academic_year'];
            $studCourseName = $StudentArray[0]['course_name'];
            $studCourseYear = $StudentArray[0]['course_year'];
            $studName = $StudentArray[0]['student_name'];
            $studEmail = $StudentArray[0]['student_email'];
                
                $this->db->select("*");
                $this->db->from("manage_course ");
                $this->db->where('delete_bit', '0');
                $courseData =  $this->db->get()->result_array();
            
                for($i = 0; $i < count($courseData); $i++)
                {
                    if($courseData[$i]['course_name']== $studCourseName && $courseData[$i]['courses_year']== $studCourseYear && $courseData[$i]['academics_year']== $studAcademicYear )
                    { 
                        $TotalSeat = $courseData[$i]['total_seats'] ;
                    }
                }

                
         
          
         
                $this->db->select("*");
                $this->db->from("student_registration sr");
                $this->db->where('sr.course_name',$studCourseName);
                $this->db->where('sr.course_year',$studCourseYear);
                $this->db->where('sr.stud_academic_year',$studAcademicYear);
            
                $this->db->where('parentinfo.student_admission_status', '3');
                //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
                $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
                $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
                $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
                $queryOption = $this->db->get();
                $studCount = $queryOption->num_rows();

            
                    if($studCount >= $TotalSeat){
                        return -1;
                    }
                    else{
                        $data = array(
                            'student_admission_status' => "3"
                        );
                        $this->db->where('student_id', $studId);
                        $this->db->update('student_admission_form_parent_info', $data);


                        $result['name']= $studName;
                        $result['courseyear']= $studCourseYear;
                        $result['coursename']= $studCourseName;
                        $result['subject'] = 'Thank you for Registering at M U College of Commerce';
                        $template = 'email-template/online-admission-confirmation';
                        $sendMail = sendMail($result,$template);

                        return true;
                    }
        
        
        
        }
        
    
    }

    function getAllDataForExcel(){
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
        $this->db->where('parentinfo.student_admission_status', '2');
        //$this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        
        $StudentArrayFromDB = $this->db->get()->result_array();
        $returnArray=array();  
        $key = 1;  
        $returnArray[0] = array("Student Name","Student Mobile Number","Student Email","Student Academic Year","Student Course","Student Course Year","Gender", "Date of Birth","Mother Tongue","Address","Religion","Category","Cast","Eligibility Number","SSC Percentage","HSC Percentage","FY Percentage","SY Percentage","TY BCom Percentage");  
        foreach($StudentArrayFromDB as $StudentArray){
            $returnArray[$key] = array($StudentArray['student_name'],$StudentArray['student_mobile'],$StudentArray['student_email'],$StudentArray['stud_academic_year'],$StudentArray['course_name'],$StudentArray['course_year'], $StudentArray['gender'], $StudentArray['date_of_birth'], $StudentArray['mother_tounge'], $StudentArray['local_address'], $StudentArray['religion'], $StudentArray['category'], $StudentArray['caste'], $StudentArray['eligibility_number'],$StudentArray['ssc_percentage'],$StudentArray['hsc_percentage'],$StudentArray['fy_percentage'],$StudentArray['sy_percentage'],$StudentArray['tybcom_percentage']);  
            $key++;   
        }
        return $returnArray;
    }
    
    function getSubjectList(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $this->db->select("*");
        $this->db->from("manage_subject");
        $this->db->where('course_name', $courseName);
        $this->db->where('course_year', $courseYear);
        $this->db->where('delete_bit', '0');
        
        return $this->db->get()->result_array();
    }

    
}

?>