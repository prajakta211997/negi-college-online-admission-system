<?php 

class ManageCourse_model extends CI_Model {

    function addCourse(){
        
        $courseName = $_POST['courseName'] ;
        $courseYear = $_POST['courseYear'];
        $totalSeats = $_POST['totalSeats'];
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
        $academicYear = $_POST['academicYear'];

        
       
        $query = $this->db->get_where('manage_course', array('course_name' => $courseName,'courses_year'=>$courseYear, 'total_seats'=>$totalSeats,'academics_year'=>$academicYear, 'start_date'=>$startDate, 'end_date'=>$endDate,'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                'course_name' => $courseName,
                'courses_year'=>$courseYear,
                 'total_seats'=>$totalSeats,
                 'academics_year'=>$academicYear,
                 'start_date'=>$startDate, 
                 'end_date'=>$endDate
                    );
                
                $insert = $this->db->insert('manage_course', $data);

                 return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    }

    

    function getCoursesList(){
        $this->db->select("*");
        $this->db->from("manage_course");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function searchByCourses(){
        $coursesSearch = $_POST['coursesSearch'];
        $this->db->select("*");
        $this->db->from("manage_course");
        $this->db->like('course_name', $coursesSearch);
        $this->db->or_like('courses_year', $coursesSearch);
        $this->db->or_like('total_seats', $coursesSearch);
        
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function updateCourse(){
        $courseId = $_POST['updateCoursesId'];
        $courseName = $_POST['courseName'] ;
        $courseYear = $_POST['courseYear'];
        $totalSeats = $_POST['totalSeats'];
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
        $academicYear = $_POST['academicYear'];

        
       
        $query = $this->db->get_where('manage_course', array('course_name' => $courseName,'courses_year'=>$courseYear, 'total_seats'=>$totalSeats, 'academics_year'=>$academicYear,'start_date'=>$startDate, 'end_date'=>$endDate,'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                'course_name' => $courseName,
                'courses_year'=>$courseYear,
                 'total_seats'=>$totalSeats,
                 'academics_year'=>$academicYear,
                 'start_date'=>$startDate, 
                 'end_date'=>$endDate
                    );
                
               // $insert = $this->db->insert('manage_course', $data);
                $this->db->where('course_id', $courseId);
                $this->db->update('manage_course', $data);
                return true;
                 //return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    
    }
    function deleteCourses(){
        $updateCoursesId = $_POST['updateCoursesId'];
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('course_id', $updateCoursesId);
        $this->db->update('manage_course', $data);
       // $this->insert_user_log("Delete User");
        return true;
       
    }
    function getSingleCourseList () {
        $course_id = $_POST['course_id'];
        $this->db->select("*");
        $this->db->from("manage_course ");
       
        $this->db->where('course_id',$course_id);
        return $this->db->get()->result_array();
    }
    function getFilterData(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
        $this->db->select("*");
        $this->db->from("manage_course ");
        if($courseName){
        $this->db->where('course_name',$courseName);}
        if($courseYear){
        $this->db->where('courses_year',$courseYear);}
        if($academicYear){
        $this->db->where('academics_year',$academicYear);}
        
        return $this->db->get()->result_array();  
    }
    function getAllCoursesName(){

        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }
    
}

?>