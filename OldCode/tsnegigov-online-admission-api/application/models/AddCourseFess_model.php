<?php 

class AddCourseFess_model extends CI_Model {

    function addSingleCourseFee(){
        
        $academicYear = $_POST['academicYear'];
        $courseName  = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $courseFeesType = $_POST['courseFeesType'];
        $courseFeesSubType = $_POST['courseFeesSubType'];
        $courseDivision = $_POST['courseDivision'];
        $tutionFees   = $_POST['tutionFees'];
        $libraryFees  = $_POST['libraryFees'];
        $activitiesFees = $_POST['activitiesFees'];
        $gymkhanaFees = $_POST['gymkhanaFees'];
        $eligibilityFees = $_POST['eligibilityFees'];
        $eligibilityFormFees  = $_POST['eligibilityFormFees'];
        $admissionFees  = $_POST['admissionFees'];
        $studentWelfareFees = $_POST['studentWelfareFees'];
        $computerizationFees  = $_POST['computerizationFees'];
        $developmentFees = $_POST['developmentFees'];
        $registrationFees = $_POST['registrationFees'];
        $proRataFees = $_POST['proRataFees'];
        $disasMgmtFees = $_POST['disasMgmtFees'];
        $libraryDeposit = $_POST['libraryDeposit'];
        $cautionMoney  = $_POST['cautionMoney'];
        $medicalExam = $_POST['medicalExam'];
        $practicalFees = $_POST['practicalFees'];
        $laboratoryFees = $_POST['laboratoryFees'];
        $sissFees = $_POST['sissFees'];
        $personnelFees = $_POST['personnelFees'];
        $shortTermCourseFee = $_POST['shortTermCourseFee'];
        $securityCharges = $_POST['securityCharges'];
        $administrationCharges = $_POST['administrationCharges'];
        $seminarFees = $_POST['seminarFees'];
        $phyEdnScheme = $_POST['phyEdnScheme'];
        $alumniFees = $_POST['alumniFees'];
        $collegeDevpFees = $_POST['collegeDevpFees'];
        $termendExamFees = $_POST['termendExamFees'];
        $computerLabFees = $_POST['computerLabFees'];
        $workshopSeminarFees = $_POST['workshopSeminarFees'];
        $compInternetFees = $_POST['compInternetFees'];
        $corpusFund = $_POST['corpusFund'];
        $formFees = $_POST['formFees'];
        $nssSectionFees = $_POST['nssSectionFees'];
        $compLabMaintainanceFees = $_POST['compLabMaintainanceFees'];
        $addOnCoursesFees = $_POST['addOnCoursesFees'];
        $addExtraFees = $_POST['addExtraFees'];
        $addExtranewFees = $_POST['addExtranewFees'];
        $totalFees  = $_POST['totalFees'];
       
        $query = $this->db->get_where('manage_course_fees', array('course_name'=> $courseName, 
                                    'academic_year'=> $academicYear, 
                                    'course_year' => $courseYear, 
                                    'fees_type' => $courseFeesType,
                                    'course_division' =>$courseDivision,
                                    'fees_sub_type' => $courseFeesSubType,
                                    'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array('course_name'=> $courseName, 
                'academic_year'=> $academicYear, 
                'course_year' => $courseYear, 
                'fees_type' => $courseFeesType,
                'course_division' =>$courseDivision,
                'fees_sub_type' => $courseFeesSubType,
                'tution_fees' => $tutionFees ,
                'library_fees' => $libraryFees,
                'activity_fees' => $activitiesFees,
                
                'gymkhana_fees' => $gymkhanaFees,
                'eligibility_fees' => $eligibilityFees,
                'eligibility_form_fees' => $eligibilityFormFees,
                'admission_fees' => $admissionFees,
                'welfare_fees' => $studentWelfareFees,
                'computarization_fees' => $computerizationFees,
                'developement_fees' => $developmentFees,
                'registration_fees' => $registrationFees,
                'pro_rata_fees' => $proRataFees,
                'disas_mgmt_fees' => $disasMgmtFees,
                'library_deposite_fees' => $libraryDeposit,
                'caution_money_fees' => $cautionMoney,
                'medical_exam_fees' => $medicalExam,
                'practical_fees' => $practicalFees,
                'laboratory_fees' => $laboratoryFees,
                'siss_fees' => $sissFees,
                'personal_fees' => $personnelFees,
                'short_term_fees' => $shortTermCourseFee,
                'security_charges' => $securityCharges,
                'adminitration_charges' => $administrationCharges,
                'seminar_fees' => $seminarFees,
                'phy_edn_scheme' => $phyEdnScheme,
                'alumni_fees' => $alumniFees,
                'college_dev_fees' => $collegeDevpFees,
                'termend_exam_fees' => $termendExamFees,
                'computer_lab_fees' => $computerLabFees,
                'workshop_fees' => $workshopSeminarFees,
                'comp_internet_fees' => $compInternetFees,
                'corpus_fund' => $corpusFund,
                'form_fees' => $formFees,
                'nss_section_fees' => $nssSectionFees,
                'comp_lab_maintenace_fee' => $compLabMaintainanceFees,
                'add_on_course_fee'=>$addOnCoursesFees,
                'add_extra_fee' => $addExtraFees,
                'add_extra_new_fee' => $addExtranewFees,
                'total_fees' => $totalFees
                    );
                
                $insert = $this->db->insert('manage_course_fees', $data);

                 return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    }

    function excelCoureseFeesUpload($finalarray){
        $finalArray1 = array();
      
        foreach($finalarray as $finalarr){
            
       
        $academicYear = $finalarr['academic_year'];
        $courseName  = $finalarr['course_name'];
        $courseYear = $finalarr['course_year'];
        $courseFeesType = $finalarr['fees_type'];
        $courseDivision = $finalarr['course_division'];
        $courseFeesSubType= $finalarr['fees_sub_type'];
        $tutionFees   = $finalarr['tution_fees'];
        $libraryFees  = $finalarr['library_fees'];
        $activitiesFees = $finalarr['activity_fees'];
        $gymkhanaFees = $finalarr['gymkhana_fees'];
        $eligibilityFees = $finalarr['eligibility_fees'];
        $eligibilityFormFees  = $finalarr['eligibility_form_fees'];
        $admissionFees  = $finalarr['admission_fees'];
        $studentWelfareFees = $finalarr['welfare_fees'];
        $computerizationFees  = $finalarr['computarization_fees'];
        $developmentFees = $finalarr['developement_fees'];
        $registrationFees = $finalarr['registration_fees'];
        $proRataFees = $finalarr['pro_rata_fees'];
        $disasMgmtFees = $finalarr['disas_mgmt_fees'];
        $libraryDeposit = $finalarr['library_deposite_fees'];
        $cautionMoney  = $finalarr['caution_money_fees'];
        $medicalExam = $finalarr['medical_exam_fees'];
        $practicalFees = $finalarr['practical_fees'];
        $laboratoryFees = $finalarr['laboratory_fees'];
        $sissFees = $finalarr['siss_fees'];
        $personnelFees = $finalarr['personal_fees'];
        $shortTermCourseFee = $finalarr['short_term_fees'];
        $securityCharges = $finalarr['security_charges'];
        $administrationCharges = $finalarr['adminitration_charges'];
        $seminarFees = $finalarr['seminar_fees'];
        $phyEdnScheme = $finalarr['phy_edn_scheme'];
        $alumniFees = $finalarr['alumni_fees'];
        $collegeDevpFees = $finalarr['college_dev_fees'];
        $termendExamFees = $finalarr['termend_exam_fees'];
        $computerLabFees = $finalarr['computer_lab_fees'];
        $workshopSeminarFees = $finalarr['workshop_fees'];
        $compInternetFees = $finalarr['comp_internet_fees'];
        $corpusFund = $finalarr['corpus_fund'];
        $formFees = $finalarr['form_fees'];
        $nssSectionFees = $finalarr['nss_section_fees'];
        $compLabMaintainanceFees = $finalarr['comp_lab_maintenace_fee'];
        
        $totalFees  = $finalarr['total_fees'];
        
            $query = $this->db->get_where('manage_course_fees', array('course_name'=> $courseName, 'academic_year'=> $academicYear,  'course_year' => $courseYear, 'fees_type' => $courseFeesType,'course_division' =>$courseDivision,'fees_sub_type' => $courseFeesSubType,'delete_bit'=>'0'));
            if ($query->num_rows()) 
            {
               return -1;
            }
            else{
                $dataArray = array(
                    'course_name'=> $courseName, 
                    'academic_year'=> $academicYear, 
                    'course_year' => $courseYear, 
                    'fees_type' => $courseFeesType,
                    'course_division' =>$courseDivision,
                    'fees_sub_type' => $courseFeesSubType,
                    'tution_fees' => $tutionFees ,
                    'library_fees' => $libraryFees,
                    'activity_fees' => $activitiesFees,
                    
                    'gymkhana_fees' => $gymkhanaFees,
                    'eligibility_fees' => $eligibilityFees,
                    'eligibility_form_fees' => $eligibilityFormFees,
                    'admission_fees' => $admissionFees,
                    'welfare_fees' => $studentWelfareFees,
                    'computarization_fees' => $computerizationFees,
                    'developement_fees' => $developmentFees,
                    'registration_fees' => $registrationFees,
                    'pro_rata_fees' => $proRataFees,
                    'disas_mgmt_fees' => $disasMgmtFees,
                    'library_deposite_fees' => $libraryDeposit,
                    'caution_money_fees' => $cautionMoney,
                    'medical_exam_fees' => $medicalExam,
                    'practical_fees' => $practicalFees,
                    'laboratory_fees' => $laboratoryFees,
                    'siss_fees' => $sissFees,
                    'personal_fees' => $personnelFees,
                    'short_term_fees' => $shortTermCourseFee,
                    'security_charges' => $securityCharges,
                    'adminitration_charges' => $administrationCharges,
                    'seminar_fees' => $seminarFees,
                    'phy_edn_scheme' => $phyEdnScheme,
                    'alumni_fees' => $alumniFees,
                    'college_dev_fees' => $collegeDevpFees,
                    'termend_exam_fees' => $termendExamFees,
                    'computer_lab_fees' => $computerLabFees,
                    'workshop_fees' => $workshopSeminarFees,
                    'comp_internet_fees' => $compInternetFees,
                    'corpus_fund' => $corpusFund,
                    'form_fees' => $formFees,
                    'nss_section_fees' => $nssSectionFees,
                    'comp_lab_maintenace_fee' => $compLabMaintainanceFees,
                    'total_fees' => $totalFees
                 
            );
            }
       $finalArray1[] = $dataArray;
         
        }
       
        $this->db->insert_batch('manage_course_fees',$finalArray1);  
    
       return 1;
    }
    
    


    function getCourseFeesList(){
        $this->db->select("*");
        $this->db->from("manage_course_fees ");
       // $this->db->where('mcf.delete_bit', '0');
        $this->db->where('delete_bit', '0');
       // $this->db->join("manage_course mc " ,"mcf.course_name= mc.course_name");
        return $this->db->get()->result_array();
    }

    function searchByCourses(){
        $coursesSearch = $_POST['coursesSearch'];
        $this->db->select("*");
        $this->db->from("manage_course_fees mcf");
        $this->db->like('mc.course_name', $coursesSearch);
        $this->db->or_like('mcf.course_year', $coursesSearch);
        $this->db->or_like('mc.total_seats', $coursesSearch);
        $this->db->or_like('mcf.fees_type', $coursesSearch);
        $this->db->or_like('mcf.total_fees', $coursesSearch);
        $this->db->where('mcf.delete_bit', '0');
        $this->db->where('mc.delete_bit', '0');
        $this->db->join("manage_course mc " ,"mc.course_name= mcf.course_name");
        return $this->db->get()->result_array();
    }

    function updateCourseFee(){
        $courseFeesId = $_POST['updateCourseFeesId'];
        $academicYear = $_POST['academicYear'];
        $courseName  = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $courseFeesType = $_POST['courseFeesType'];
        $courseFeesSubType = $_POST['courseFeesSubType'];
        $courseDivision = $_POST['courseDivision'];
        $tutionFees   = $_POST['tutionFees'];
        $libraryFees  = $_POST['libraryFees'];
        $activitiesFees = $_POST['activitiesFees'];
        $gymkhanaFees = $_POST['gymkhanaFees'];
        $eligibilityFees = $_POST['eligibilityFees'];
        $eligibilityFormFees  = $_POST['eligibilityFormFees'];
        $admissionFees  = $_POST['admissionFees'];
        $studentWelfareFees = $_POST['studentWelfareFees'];
        $computerizationFees  = $_POST['computerizationFees'];
        $developmentFees = $_POST['developmentFees'];
        $registrationFees = $_POST['registrationFees'];
        $proRataFees = $_POST['proRataFees'];
        $disasMgmtFees = $_POST['disasMgmtFees'];
        $libraryDeposit = $_POST['libraryDeposit'];
        $cautionMoney  = $_POST['cautionMoney'];
        $medicalExam = $_POST['medicalExam'];
        $practicalFees = $_POST['practicalFees'];
        $laboratoryFees = $_POST['laboratoryFees'];
        $sissFees = $_POST['sissFees'];
        $personnelFees = $_POST['personnelFees'];
        $shortTermCourseFee = $_POST['shortTermCourseFee'];
        $securityCharges = $_POST['securityCharges'];
        $administrationCharges = $_POST['administrationCharges'];
        $seminarFees = $_POST['seminarFees'];
        $phyEdnScheme = $_POST['phyEdnScheme'];
        $alumniFees = $_POST['alumniFees'];
        $collegeDevpFees = $_POST['collegeDevpFees'];
        $termendExamFees = $_POST['termendExamFees'];
        $computerLabFees = $_POST['computerLabFees'];
        $workshopSeminarFees = $_POST['workshopSeminarFees'];
        $compInternetFees = $_POST['compInternetFees'];
        $corpusFund = $_POST['corpusFund'];
        $formFees = $_POST['formFees'];
        $nssSectionFees = $_POST['nssSectionFees'];
        $compLabMaintainanceFees = $_POST['compLabMaintainanceFees'];
        $addOnCoursesFees = $_POST['addOnCoursesFees'];
        $addExtraFees = $_POST['addExtraFees'];
        $addExtranewFees = $_POST['addExtranewFees'];
        $totalFees  = $_POST['totalFees'];

        
       
        $query = $this->db->get_where('manage_course_fees', array('course_name'=> $courseName, 
        'academic_year'=> $academicYear, 
        'course_year' => $courseYear, 
        'fees_type' => $courseFeesType,
        'course_division' =>$courseDivision,
        'fees_sub_type' => $courseFeesSubType,
        'tution_fees' => $tutionFees ,
        'library_fees' => $libraryFees,
        'activity_fees' => $activitiesFees,

        'gymkhana_fees' => $gymkhanaFees,
        'eligibility_fees' => $eligibilityFees,
        'eligibility_form_fees' => $eligibilityFormFees,
        'admission_fees' => $admissionFees,
        'welfare_fees' => $studentWelfareFees,
        'computarization_fees' => $computerizationFees,
        'developement_fees' => $developmentFees,
        'registration_fees' => $registrationFees,
        'pro_rata_fees' => $proRataFees,
        'disas_mgmt_fees' => $disasMgmtFees,
        'library_deposite_fees' => $libraryDeposit,
        'caution_money_fees' => $cautionMoney,
        'medical_exam_fees' => $medicalExam,
        'practical_fees' => $practicalFees,
        'laboratory_fees' => $laboratoryFees,
        'siss_fees' => $sissFees,
        'personal_fees' => $personnelFees,
        'short_term_fees' => $shortTermCourseFee,
        'security_charges' => $securityCharges,
        'adminitration_charges' => $administrationCharges,
        'seminar_fees' => $seminarFees,
        'phy_edn_scheme' => $phyEdnScheme,
        'alumni_fees' => $alumniFees,
        'college_dev_fees' => $collegeDevpFees,
        'termend_exam_fees' => $termendExamFees,
        'computer_lab_fees' => $computerLabFees,
        'workshop_fees' => $workshopSeminarFees,
        'comp_internet_fees' => $compInternetFees,
        'corpus_fund' => $corpusFund,
        'form_fees' => $formFees,
        'nss_section_fees' => $nssSectionFees,
        'comp_lab_maintenace_fee' => $compLabMaintainanceFees,
        'add_on_course_fee'=>$addOnCoursesFees,
        'add_extra_fee' => $addExtraFees,
        'add_extra_new_fee' => $addExtranewFees,
        'total_fees' => $totalFees,
        'delete_bit'=>'0'));


            if ($query->num_rows() == 1) {
            return -1;
            }else{
                $data = array('course_name'=> $courseName, 
                'academic_year'=> $academicYear, 
                'course_year' => $courseYear, 
                'fees_type' => $courseFeesType,
                'course_division' =>$courseDivision,
                'fees_sub_type' => $courseFeesSubType,
                'tution_fees' => $tutionFees ,
                'library_fees' => $libraryFees,
                'activity_fees' => $activitiesFees,

                'gymkhana_fees' => $gymkhanaFees,
                'eligibility_fees' => $eligibilityFees,
                'eligibility_form_fees' => $eligibilityFormFees,
                'admission_fees' => $admissionFees,
                'welfare_fees' => $studentWelfareFees,
                'computarization_fees' => $computerizationFees,
                'developement_fees' => $developmentFees,
                'registration_fees' => $registrationFees,
                'pro_rata_fees' => $proRataFees,
                'disas_mgmt_fees' => $disasMgmtFees,
                'library_deposite_fees' => $libraryDeposit,
                'caution_money_fees' => $cautionMoney,
                'medical_exam_fees' => $medicalExam,
                'practical_fees' => $practicalFees,
                'laboratory_fees' => $laboratoryFees,
                'siss_fees' => $sissFees,
                'personal_fees' => $personnelFees,
                'short_term_fees' => $shortTermCourseFee,
                'security_charges' => $securityCharges,
                'adminitration_charges' => $administrationCharges,
                'seminar_fees' => $seminarFees,
                'phy_edn_scheme' => $phyEdnScheme,
                'alumni_fees' => $alumniFees,
                'college_dev_fees' => $collegeDevpFees,
                'termend_exam_fees' => $termendExamFees,
                'computer_lab_fees' => $computerLabFees,
                'workshop_fees' => $workshopSeminarFees,
                'comp_internet_fees' => $compInternetFees,
                'corpus_fund' => $corpusFund,
                'form_fees' => $formFees,
                'nss_section_fees' => $nssSectionFees,
                'comp_lab_maintenace_fee' => $compLabMaintainanceFees,
                'add_on_course_fee'=>$addOnCoursesFees,
                'add_extra_fee' => $addExtraFees,
                'add_extra_new_fee' => $addExtranewFees,
                'total_fees' => $totalFees
                );
                $this->db->where('course_fee_id', $courseFeesId);
                $this->db->update('manage_course_fees', $data);
                return true;
             }
    
    }
    
    function deleteCourseFees(){
        $updateCourseFeesId = $_POST['updateCourseFeesId'];
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('course_fee_id', $updateCourseFeesId);
        $this->db->update('manage_course_fees', $data);
       // $this->insert_user_log("Delete User");
        return true;
       
    }
    function getSingleCourseFeesList () {
        $course_fee_id = $_POST['course_fee_id'];
        $this->db->select("*");
        $this->db->from("manage_course_fees");
        $this->db->where('course_fee_id',$course_fee_id);
        $this->db->where('delete_bit','0');
        return $this->db->get()->result_array();
    }
    function getFilterData(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $academicYear = $_POST['academicYear'];
        $courseFeesType = $_POST['courseFeesType'];
        $this->db->select("*");
        $this->db->from("manage_course_fees ");
       // $this->db->join("manage_course mc " ,"mc.course_id= mcf.course_id");
       // $this->db->where('mcf.delete_bit', '0');
        $this->db->where('delete_bit', '0');
        if($courseName){
        $this->db->where('course_name',$courseName);}
        if($courseYear){
        $this->db->where('course_year',$courseYear);}
        if($academicYear){
        $this->db->where('academic_year',$academicYear);}
        if($courseFeesType){
        $this->db->where('fees_type',$courseFeesType);}
        return $this->db->get()->result_array();  
    }

    function getAllCoursesName(){
        $this->db->distinct();
        $this->db->select("course_name");
        $this->db->from("manage_course");

        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function getAllDataForExcel(){
        $this->db->select("*");
        $this->db->from("manage_course_fees");
        $this->db->where('delete_bit', "0");

        $CourseArrayFromDB = $this->db->get()->result_array();
        $returnArray=array();  
        $key = 1;  
        $returnArray[0] = array("Course Name","Academic Year","Course Year","Fees Type","Tution Fees","Library Fees","Activity Fees","Gymkhana Fees","Eligibility Fees","Eligibility Form Fees","Admission Fees","Student Welfare Fees","Computerization Fees","Development Fees","Registration Fees","Pro-rata(Ashwamegh) Fees","Disas. Mgmt. Fees","Library Deposite","Medical Exam","Caution Money","Practical Fees","Laboratory Fees","S.I.S.S. Fees","Personnel Fees","Short Term Course Fee","Security Charges","Administration Charges","Seminar Fees","Phy. Edn. Scheme","Alumni Fees","College Devp. Fees","Termend Exam Fees","Computer Lab Fees","Workshop & Seminar Fees","Comp & Internet Fees","Corpus Fund","Form Fees","NSS Section Fees","Comp Lab Maintainance Fees","Total");  
        foreach($CourseArrayFromDB as $CourseArray){
            $returnArray[$key] = array($CourseArray['course_name'], $CourseArray['academic_year'] , $CourseArray['course_year'], $CourseArray['fees_type'], $CourseArray['tution_fees'], $CourseArray['library_fees'], $CourseArray['activity_fees'], $CourseArray['gymkhana_fees'], $CourseArray['eligibility_fees'], $CourseArray['eligibility_form_fees'], $CourseArray['admission_fees'], $CourseArray['welfare_fees'], $CourseArray['computarization_fees'], $CourseArray['developement_fees'], $CourseArray['registration_fees'], $CourseArray['pro_rata_fees'], $CourseArray['disas_mgmt_fees'], $CourseArray['library_deposite_fees'], $CourseArray['caution_money_fees'], $CourseArray['medical_exam_fees'], $CourseArray['practical_fees'], $CourseArray['laboratory_fees'], $CourseArray['siss_fees'], $CourseArray['personal_fees'], $CourseArray['short_term_fees'], $CourseArray['security_charges'], $CourseArray['adminitration_charges'], $CourseArray['seminar_fees'], $CourseArray['phy_edn_scheme'], $CourseArray['alumni_fees'], $CourseArray['college_dev_fees'], $CourseArray['termend_exam_fees'], $CourseArray['computer_lab_fees'], $CourseArray['workshop_fees'], $CourseArray['comp_internet_fees'], $CourseArray['corpus_fund'], $CourseArray['form_fees'], $CourseArray['nss_section_fees'], $CourseArray['comp_lab_maintenace_fee'], $CourseArray['total_fees'] );  
            $key++;   
        }
        return $returnArray;
    }
    

    
}

?>