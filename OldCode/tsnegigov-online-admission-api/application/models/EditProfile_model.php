<?php 

class EditProfile_model extends CI_Model {

    function getStudentList(){
        $finalArray =[];
        $student_id = $_POST['student_id'];
        $this->db->select("*");
        $this->db->from("student_registration sr");
        $this->db->where('sr.student_id', $student_id);
        $this->db->where('sr.admission_step_one', '1');
        $this->db->where('sr.admission_step_two', '1');
        $this->db->where('sr.admission_step_three', '1');
        $this->db->where('sr.delete_bit', '0');
       // $this->db->where('parentinfo.form_fee_submitted_bit', '1');
        $this->db->join("student_admission_form_personal_info personalinfo" ,"personalinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_academic_info academicinfo" ,"academicinfo.student_id = sr.student_id");
        $this->db->join("student_admission_form_parent_info parentinfo" ,"parentinfo.student_id = sr.student_id");
        $studentData =  $this->db->get()->result_array();
      
        $this->db->select("*");
        $this->db->from("student_course_fees_payment_details ");
        $this->db->where('student_id', $student_id);
        $paymentData = $this->db->get()->result_array();
        if(empty($paymentData) ){
            $studentData[0]['student_payment_paid'] = 0;
        }
        else{
            $studentData[0]['student_payment_paid'] = 1;
        }
        return $studentData;
    }

    

    function saveProfile(){
        $studentId = $_POST['studentId'];
        $isSindhi =  $_POST['isSindhi'];
        $fromJaihind = $_POST['fromJaihind'];
        $firstName = $_POST['firstName'];
        $middleName = $_POST['middleName'];
        $lastName = $_POST['lastName'];
        $studName = $lastName.' '.$firstName.' '.$middleName;
        $motherName = $_POST['motherName'];
        $emailId = $_POST['emailId'];
        $mobileNumber = $_POST['mobileNumber'];
        $alterMobileNumber = $_POST['alterMobileNumber'];
        $aadharNumber = $_POST['aadharNumber'];
        $voterNumber = $_POST['voterNumber'];
        $localAddress = $_POST['localAddress'];
        $nativePlace = $_POST['nativePlace'];
        $bloodGroup = $_POST['bloodGroup'];
        $gender = $_POST['gender'];
        $maritalstatus = $_POST['maritalstatus'];
        $dateOfBirth = $_POST['dateOfBirth'];
        $birthPlace = $_POST['birthPlace'];
        $district = $_POST['district'];
        $state = $_POST['state'];
        $nationality = $_POST['nationality'];
        $Passport = $_POST['Passport'];
        $religion = $_POST['religion'];
        $caste = $_POST['caste'];
        $category = $_POST['category'];
        $motherTongue = $_POST['motherTongue'];
        $isCastCertificate  = $_POST['isCastCertificate'];
        $physicallyhandi = $_POST['physicallyhandi'];
        $eligibilityNumber =$_POST['eligibilityNumber'];
        $game = $_POST['game'];
        $hobbies = $_POST['hobbies'];
        $nss = $_POST['nss'];
        $event = $_POST['event'];
        
        $optinalSubjectSelected = $_POST['optinalSubjectSelected'];
        $valueAddedSubjectSelected = $_POST['valueAddedSubjectSelected'];
        $optinalSubjectSelectedsem2 = $_POST['optinalSubjectSelectedsem2'];
        $valueAddedSubjectSelectedsem2 = $_POST['valueAddedSubjectSelectedsem2'];
        $isMaharashtra = $_POST['isMaharashtra'];
        $sscExam = $_POST['sscExam'];
        $sscBoard = $_POST['sscBoard'];
        $sscClgName = $_POST['sscClgName'];
        $sscPassingYear = $_POST['sscPassingYear'];
        $sscSeatNo = $_POST['sscSeatNo'];
        $sscMarksObtain = $_POST['sscMarksObtain'];
        $sscTotalMarks = $_POST['sscTotalMarks'];
        $sscPercentage = $_POST['sscPercentage'];
        $hscExam = $_POST['hscExam'];
        $hscBoard = $_POST['hscBoard'];
        $hscClgName = $_POST['hscClgName'];
        $hscPassingYear = $_POST['hscPassingYear'];
        $hscSeatNo = $_POST['hscSeatNo'];
        $hscMarksObtain = $_POST['hscMarksObtain'];
        $hscTotalMarks = $_POST['hscTotalMarks'];
        $hscPercentage = $_POST['hscPercentage'];
        
        $tyBComExam = $_POST['tyBComExam'];
        $tyBComBoard = $_POST['tyBComBoard'];
        $tyBComClgName = $_POST['tyBComClgName'];
        $tyBComPassingYear = $_POST['tyBComPassingYear'];
        $tyBComSeatNo = $_POST['tyBComSeatNo'];
        $tyBComMarksObtain = $_POST['tyBComMarksObtain'];
        $tyBComTotalMarks = $_POST['tyBComTotalMarks'];
        $tyBComPercentage = $_POST['tyBComPercentage'];

        $fyExam = $_POST['fyExam'];
        $fyBoard = $_POST['fyBoard'];
        $fyClgName = $_POST['fyClgName'];
        $fyPassingYear = $_POST['fyPassingYear'];
        $fySeatNo = $_POST['fySeatNo'];
        $fyMarksObtain = $_POST['fyMarksObtain'];
        $fyTotalMarks = $_POST['fyTotalMarks'];
        $fyPercentage  = $_POST['fyPercentage'];
        $syExam = $_POST['syExam'];
        $syBoard = $_POST['syBoard'];
        $syClgName = $_POST['syClgName'];
        $syPassingYear = $_POST['syPassingYear'];
        $sySeatNo = $_POST['sySeatNo'];
        $syMarksObtain = $_POST['syMarksObtain'];
        $syTotalMarks = $_POST['syTotalMarks'];
        $syPercentage = $_POST['syPercentage'];
        $isgapReason = $_POST['isgapReason'];
        $gapReason = $_POST['gapReason'];

        $fatherFirstName = $_POST['fatherFirstName'];
        $fatherMiddleName = $_POST['fatherMiddleName'];
        $fatherLastName = $_POST['fatherLastName'];
        $fatherEmail = $_POST['fatherEmail'];
        $fatherMobileNo = $_POST['fatherMobileNo'];
        $fatherOccupation = $_POST['fatherOccupation'];
        $fathercGovtEmp = $_POST['fathercGovtEmp'];
        $fatherAnnualIncome = $_POST['fatherAnnualIncome'];
        $fatherecoBack = $_POST['fatherecoBack'];

        $motherFirstName = $_POST['motherFirstName'];
        $motherMiddleName = $_POST['motherMiddleName'];
        $motherLastName = $_POST['motherLastName'];
        $motherEmail = $_POST['motherEmail'];
        $motherMobileNo = $_POST['motherMobileNo'];
        $motherOccupation = $_POST['motherOccupation'];
        $mothercGovtEmp = $_POST['mothercGovtEmp'];
        $motherAnnualIncome = $_POST['motherAnnualIncome'];
        $motherecoBack = $_POST['motherecoBack'];

        $guaradianFirstName = $_POST['guaradianFirstName'];
        $guaradianMiddleName = $_POST['guaradianMiddleName'];
        $guaradianLastName = $_POST['guaradianLastName'];
        $guaradianEmail = $_POST['guaradianEmail'];
        $guaradianMobileNo = $_POST['guaradianMobileNo'];
        $guaradianOccupation = $_POST['guaradianOccupation'];
        $guaradiancGovtEmp = $_POST['guaradiancGovtEmp'];
        $guaradianAnnualIncome = $_POST['guaradianAnnualIncome'];
        $guaradianecoBack = $_POST['guaradianecoBack'];
        
        

        $location = "uploads/".$firstName." ".$lastName;
        if(!is_dir($location)){
            mkdir($location,0755,TRUE);

        }
        if(isset($_FILES['casteCertificate'])){     
            $casteCertificate_data = $_FILES['casteCertificate']['name'];  
            $casteCertificate_path = $_FILES['casteCertificate']['tmp_name'];
            $certificatelocation = $location."/".$casteCertificate_data;
            move_uploaded_file($casteCertificate_path,$certificatelocation);
        } 
        if(isset($_FILES['physicallychallengedCertificate'])){     
            $physicallychallengedCertificate_data = $_FILES['physicallychallengedCertificate']['name'];  
            $physicallychallengedCertificate_path = $_FILES['physicallychallengedCertificate']['tmp_name'];
            $physicallychallengedCertificatelocation = $location."/".$physicallychallengedCertificate_data;
            move_uploaded_file($physicallychallengedCertificate_path,$physicallychallengedCertificatelocation);
        }
        if(isset($_FILES['sscMarksheet'])){   
            $sscMarksheet_data = $_FILES['sscMarksheet']['name']; 
            $sscMarksheet_path = $_FILES['sscMarksheet']['tmp_name'];
            $sscMarksheetlocation =$location. "/".$sscMarksheet_data;
            move_uploaded_file($sscMarksheet_path,$sscMarksheetlocation);
            
        } 
        if(isset($_FILES['hscMarksheet'])){   
            $hscMarksheet_data = $_FILES['hscMarksheet']['name']; 
            $hscMarksheet_path = $_FILES['hscMarksheet']['tmp_name'];
            $hscMarksheetlocation =$location. "/".$hscMarksheet_data;
            move_uploaded_file($hscMarksheet_path,$hscMarksheetlocation);
            
        } 

        if(isset($_FILES['tyBComMarksheet'])){   
            $tyBComMarksheet_data = $_FILES['tyBComMarksheet']['name']; 
            $tyBComMarksheet_path = $_FILES['tyBComMarksheet']['tmp_name'];
            $tyBComMarksheetlocation =$location. "/".$tyBComMarksheet_data;
            move_uploaded_file($tyBComMarksheet_path,$tyBComMarksheetlocation);
            
        } 

        if(isset($_FILES['fyMarksheet'])){   
            $fyMarksheet_data = $_FILES['fyMarksheet']['name']; 
            $fyMarksheet_path = $_FILES['fyMarksheet']['tmp_name'];
            $fyMarksheetlocation =$location. "/".$fyMarksheet_data;
            move_uploaded_file($fyMarksheet_path,$fyMarksheetlocation);
            
        } 
        if(isset($_FILES['syMarksheet'])){   
            $syMarksheet_data = $_FILES['syMarksheet']['name']; 
            $syMarksheet_path = $_FILES['syMarksheet']['tmp_name'];
            $syMarksheetlocation =$location. "/".$syMarksheet_data;
            move_uploaded_file($syMarksheet_path,$syMarksheetlocation);
            
        } 
        
        $query1 = $this->db->get_where('student_admission_form_personal_info', array(
            'is_sindhi_category'=>$isSindhi,
            'is_jaihind_college'=> $fromJaihind,
            'first_name'=> $firstName,
            'middle_name' =>$middleName,
            'last_name' => $lastName,
            'mother_name '=>$motherName,
            'email_address'=> $emailId,
            'mobile_number'=>$mobileNumber,
            'alter_mobile_number'=>$alterMobileNumber,
            'adhar_card_id' => $aadharNumber,
            'voter_id'=>$voterNumber,
            'local_address' => $localAddress,
            'native_place' => $nativePlace,
            'blood_group'=> $bloodGroup,
            'gender' => $gender,
            'marital_status'=> $maritalstatus,
            'date_of_birth'=> $dateOfBirth,
            'birth_place'=>$birthPlace,
            'district' => $district,
            'state' =>$state, 
            'nationality' => $nationality,
            'passport' => $Passport,
            'religion' => $religion,
            'caste'=> $caste,
            'category'=>$category,
            'mother_tounge' => $motherTongue,
            'is_cast_certificate'=> $isCastCertificate,
            'is_physically_handi' => $physicallyhandi,
            'eligibility_number' => $eligibilityNumber,
            'game_played' => $game,
            'hobbies' => $hobbies,
            'is_join_clg_NNS'=>$nss,
            'is_participate_in_event'=>$event,
            
        ));
        if(isset($_FILES['casteCertificate'])){
            $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_caste_certificate' =>$certificatelocation));
            }
            if(isset($_FILES['physicallychallengedCertificate'])){
                $query1 = $this->db->get_where('student_admission_form_personal_info', array('uploaded_physically_handi_certificate' =>$physicallychallengedCertificatelocation));
                }
        $query2 = $this->db->get_where('student_admission_form_academic_info', array(
            'optinal_subject_selected' => $optinalSubjectSelected,
            'value_added_subject_selected' => $valueAddedSubjectSelected,
            'optinal_subject_selected_sem2' => $optinalSubjectSelectedsem2,
            'value_added_subject_selected_sem2' => $valueAddedSubjectSelectedsem2,
            'is_maharashtra' => $isMaharashtra,
            'ssc_exam_name' => $sscExam,
            'ssc_board_university_name' => $sscBoard,
            'ssc_school_clg_name' => $sscClgName,
            'ssc_year_of_passing' => $sscPassingYear,
            'ssc_seat_number' => $sscSeatNo,
            'ssc_marks_obtained' => $sscMarksObtain,
            'ssc_total_marks' => $sscTotalMarks,
            'ssc_percentage' => $sscPercentage,
            'hsc_exam_name' => $hscExam,
            'hsc_board_university_name' => $hscBoard,
            'hsc_school_clg_name' => $hscClgName,
            'hsc_year_of_passing' => $hscPassingYear,
            'hsc_seat_number' => $hscSeatNo,
            'hsc_marks_obtained' => $hscMarksObtain,
            'hsc_total_marks' => $hscTotalMarks,
            'hsc_percentage' => $hscPercentage,
            'tybcom_exam_name' => $tyBComExam,
            'tybcom_board_university_name' => $tyBComBoard,
            'tybcom_school_clg_name' => $tyBComClgName,
            'tybcom_year_of_passing' => $tyBComPassingYear,
            'tybcom_seat_number' => $tyBComSeatNo,
            'tybcom_marks_obtained' => $tyBComMarksObtain,
            'tybcom_total_marks' => $tyBComTotalMarks,
            'tybcom_percentage' => $tyBComPercentage,
            'fy_exam_name' => $fyExam,
            'fy_board_university_name' => $fyBoard,
            'fy_school_clg_name' => $fyClgName,
            'fy_year_of_passing' => $fyPassingYear,
            'fy_seat_number' => $fySeatNo,
            'fy_marks_obtained' => $fyMarksObtain,
            'fy_total_marks' => $fyTotalMarks,
            'fy_percentage' => $fyPercentage,
            'sy_exam_name' => $syExam,
            'sy_board_university_name' => $syBoard,
            'sy_school_clg_name' => $syClgName,
            'sy_year_of_passing' => $syPassingYear,
            'sy_seat_number' => $sySeatNo,
            'sy_marks_obtained' => $syMarksObtain,
            'sy_total_marks' => $syTotalMarks,
            'sy_percentage' => $syPercentage,
            'is_gap_reason' => $isgapReason,
            'gap_reason' => $gapReason,
            
        ));
        if(isset($_FILES['sscMarksheet'])){
            $query2 = $this->db->get_where('student_admission_form_academic_info', array('ssc_uploaded_documents'=>$sscMarksheetlocation));
        }
        if(isset($_FILES['hscMarksheet'])){
            $query2 = $this->db->get_where('student_admission_form_academic_info',array('hsc_uploaded_documents'=>$hscMarksheetlocation));
            
        }
        if(isset($_FILES['tyBComMarksheet'])){
            $query1 = $this->db->get_where('student_admission_form_academic_info',array('tybcom_uploaded_documents'=>$tyBComMarksheetlocation));
            
        }
        if(isset($_FILES['fyMarksheet'])){
            $query2 = $this->db->get_where('student_admission_form_academic_info',array('fy_uploaded_documents'=>$fyMarksheetlocation));
           
        }
        if(isset($_FILES['syMarksheet'])){
            $query2 = $this->db->get_where('student_admission_form_academic_info',array('sy_uploaded_documents'=>$syMarksheetlocation));
            
        }
        
        $query3 = $this->db->get_where('student_admission_form_parent_info', array(
            'father_first_name' => $fatherFirstName,
           'father_middle_name' => $fatherMiddleName,
           'father_last_name' => $fatherLastName,
           'father_email_address' =>$fatherEmail,
           'father_mobile_number' => $fatherMobileNo,
           'father_occupation' => $fatherOccupation,
           'is_father_central_govern_employee'=>$fathercGovtEmp,
           'father_annual_income' => $fatherAnnualIncome,
           'is_father_economical_backward' => $fatherecoBack,
           'mother_first_name' => $motherFirstName,
           'mother_middle_name' => $motherMiddleName,
           'mother_last_name' => $motherLastName,
           'mother_email_address' => $motherEmail,
           'mother_mobile_number' => $motherMobileNo,
           'mother_occupation' => $motherOccupation,
           'is_mother_central_govern_employee' => $mothercGovtEmp,
           'mother_annual_income' => $motherAnnualIncome,
           'is_mother_economical_backward' => $motherecoBack,
           'guardian_first_name' => $guaradianFirstName,
           'guardian_middle_name' => $guaradianMiddleName,
           'guardian_last_name' => $guaradianLastName,
           'guardian_email_address' => $guaradianEmail,
           'guardian_mobile_number' => $guaradianMobileNo,
           'guardian_occupation' => $guaradianOccupation,
           'is_guardian_central_govern_employee' => $guaradiancGovtEmp,
           'guardian_annual_income' => $guaradianAnnualIncome,
           'is_guardian_economical_backward' => $guaradianecoBack,
           
        ));

            if ($query1->num_rows() == 1 && $query2->num_rows() == 1 && $query3->num_rows() == 1  ) {
                return -1;
            }
            else{
        
                $personaldata = array(
                    'is_sindhi_category'=>$isSindhi,
                    'is_jaihind_college'=> $fromJaihind,
                    'first_name'=> $firstName,
                    'middle_name' =>$middleName,
                    'last_name' => $lastName,
                    'mother_name '=>$motherName,
                    'email_address'=> $emailId,
                    'mobile_number'=>$mobileNumber,
                    'alter_mobile_number'=>$alterMobileNumber,
                    'adhar_card_id' => $aadharNumber,
                    'voter_id'=>$voterNumber,
                    'local_address' => $localAddress,
                    'native_place' => $nativePlace,
                    'blood_group'=> $bloodGroup,
                    'gender' => $gender,
                    'marital_status'=> $maritalstatus,
                    'date_of_birth'=> $dateOfBirth,
                    'birth_place'=>$birthPlace,
                    'district' => $district,
                    'state' =>$state,
                    'nationality' => $nationality,
                    'passport' => $Passport,
                    'religion' => $religion,
                    'caste'=> $caste,
                    'category'=>$category,
                    'mother_tounge' => $motherTongue,
                    'is_cast_certificate'=> $isCastCertificate,
                    'is_physically_handi' => $physicallyhandi,
                    'eligibility_number' => $eligibilityNumber,
                    'game_played' => $game,
                    'hobbies' => $hobbies,
                    'is_join_clg_NNS'=>$nss,
                    'is_participate_in_event'=>$event,
                    
                
                
                );
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_personal_info', $personaldata);
                if(isset($_FILES['casteCertificate'])){
                    $casteCertificate =array('uploaded_caste_certificate' =>$certificatelocation,);
                    $casteArray = array_merge($personaldata,$casteCertificate);
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_personal_info', $casteArray);
                }
                if(isset($_FILES['physicallychallengedCertificate'])){
                    $physicallychallengedCertificate =array('uploaded_physically_handi_certificate' =>$physicallychallengedCertificatelocation,);
                    $physicallyhandiArray = array_merge($personaldata,$physicallychallengedCertificate);
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_personal_info', $physicallyhandiArray);
                }

                $this->db->where('student_id',$studentId);
                $this->db->update('student_registration', array('student_name' => $studName));

                $data = array(
                    'optinal_subject_selected' => $optinalSubjectSelected,
                    'value_added_subject_selected' => $valueAddedSubjectSelected,
                    'optinal_subject_selected_sem2' => $optinalSubjectSelectedsem2,
                    'value_added_subject_selected_sem2' => $valueAddedSubjectSelectedsem2,
                    'is_maharashtra' => $isMaharashtra,
                    'ssc_exam_name' => $sscExam,
                    'ssc_board_university_name' => $sscBoard,
                    'ssc_school_clg_name' => $sscClgName,
                    'ssc_year_of_passing' => $sscPassingYear,
                    'ssc_seat_number' => $sscSeatNo,
                    'ssc_marks_obtained' => $sscMarksObtain,
                    'ssc_total_marks' => $sscTotalMarks,
                    'ssc_percentage' => $sscPercentage,
                    'hsc_exam_name' => $hscExam,
                    'hsc_board_university_name' => $hscBoard,
                    'hsc_school_clg_name' => $hscClgName,
                    'hsc_year_of_passing' => $hscPassingYear,
                    'hsc_seat_number' => $hscSeatNo,
                    'hsc_marks_obtained' => $hscMarksObtain,
                    'hsc_total_marks' => $hscTotalMarks,
                    'hsc_percentage' => $hscPercentage,
                    'tybcom_exam_name' => $tyBComExam,
                    'tybcom_board_university_name' => $tyBComBoard,
                    'tybcom_school_clg_name' => $tyBComClgName,
                    'tybcom_year_of_passing' => $tyBComPassingYear,
                    'tybcom_seat_number' => $tyBComSeatNo,
                    'tybcom_marks_obtained' => $tyBComMarksObtain,
                    'tybcom_total_marks' => $tyBComTotalMarks,
                    'tybcom_percentage' => $tyBComPercentage,
                    'fy_exam_name' => $fyExam,
                    'fy_board_university_name' => $fyBoard,
                    'fy_school_clg_name' => $fyClgName,
                    'fy_year_of_passing' => $fyPassingYear,
                    'fy_seat_number' => $fySeatNo,
                    'fy_marks_obtained' => $fyMarksObtain,
                    'fy_total_marks' => $fyTotalMarks,
                    'fy_percentage' => $fyPercentage,
                    'sy_exam_name' => $syExam,
                    'sy_board_university_name' => $syBoard,
                    'sy_school_clg_name' => $syClgName,
                    'sy_year_of_passing' => $syPassingYear,
                    'sy_seat_number' => $sySeatNo,
                    'sy_marks_obtained' => $syMarksObtain,
                    'sy_total_marks' => $syTotalMarks,
                    'sy_percentage' => $syPercentage,
                    'is_gap_reason' => $isgapReason,
                    'gap_reason' => $gapReason,
                    
                );
                
                $this->db->where('student_id',$studentId);
                $this->db->update('student_admission_form_academic_info', $data);

                if(isset($_FILES['sscMarksheet'])){
                    $sscMarksheet =array('ssc_uploaded_documents'=>$sscMarksheetlocation);
                    $sscMarksheetArray = array_merge($data,$sscMarksheet);
                    
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_academic_info', $sscMarksheetArray);
                }
                if(isset($_FILES['hscMarksheet'])){
                    $hscMarksheet =array('hsc_uploaded_documents'=>$hscMarksheetlocation);
                    $hscMarksheetArray = array_merge($data,$hscMarksheet);
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_academic_info', $hscMarksheetArray);
                }
                if(isset($_FILES['tyBComMarksheet'])){
                    $tyBComMarksheet =array('tybcom_uploaded_documents'=>$tyBComMarksheetlocation);
                    $tyBComMarksheetArray = array_merge($data,$tyBComMarksheet);
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_academic_info', $tyBComMarksheetArray);
                }
                if(isset($_FILES['fyMarksheet'])){
                    $fyMarksheet =array('fy_uploaded_documents'=>$fyMarksheetlocation);
                    $fyMarksheetArray = array_merge($data,$fyMarksheet);
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_academic_info', $fyMarksheetArray);
                }
                if(isset($_FILES['syMarksheet'])){
                    $syMarksheet =array('sy_uploaded_documents'=>$syMarksheetlocation);
                    $syMarksheetArray = array_merge($data,$syMarksheet);
                    $this->db->where('student_id',$studentId);
                    $this->db->update('student_admission_form_academic_info', $syMarksheetArray);
                }

                $parentdata = array(
                    'father_first_name' => $fatherFirstName,
                    'father_middle_name' => $fatherMiddleName,
                    'father_last_name' => $fatherLastName,
                    'father_email_address' =>$fatherEmail,
                    'father_mobile_number' => $fatherMobileNo,
                    'father_occupation' => $fatherOccupation,
                    'is_father_central_govern_employee'=>$fathercGovtEmp,
                    'father_annual_income' => $fatherAnnualIncome,
                    'is_father_economical_backward' => $fatherecoBack,
                    'mother_first_name' => $motherFirstName,
                    'mother_middle_name' => $motherMiddleName,
                    'mother_last_name' => $motherLastName,
                    'mother_email_address' => $motherEmail,
                    'mother_mobile_number' => $motherMobileNo,
                    'mother_occupation' => $motherOccupation,
                    'is_mother_central_govern_employee' => $mothercGovtEmp,
                    'mother_annual_income' => $motherAnnualIncome,
                    'is_mother_economical_backward' => $motherecoBack,
                    'guardian_first_name' => $guaradianFirstName,
                    'guardian_middle_name' => $guaradianMiddleName,
                    'guardian_last_name' => $guaradianLastName,
                    'guardian_email_address' => $guaradianEmail,
                    'guardian_mobile_number' => $guaradianMobileNo,
                    'guardian_occupation' => $guaradianOccupation,
                    'is_guardian_central_govern_employee' => $guaradiancGovtEmp,
                    'guardian_annual_income' => $guaradianAnnualIncome,
                    'is_guardian_economical_backward' => $guaradianecoBack,
                    
                );

            $this->db->where('student_id',$studentId);
            $this->db->update('student_admission_form_parent_info', $parentdata);
            return true; 
            
            }
    }
   
    function getStudentRegistrationDetails(){
        $studentId = $_POST['student_id'];
        $this->db->select("*");
        $this->db->from("student_registration");
        $this->db->where('student_id', $studentId);
        // $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }
    function getSubjectList(){
        $courseName = $_POST['courseName'];
        $courseYear = $_POST['courseYear'];
        $this->db->select("*");
        $this->db->from("manage_subject");
        $this->db->where('course_name', $courseName);
        $this->db->where('course_year', $courseYear);
        $this->db->where('delete_bit', '0');
        
        return $this->db->get()->result_array();
    }
    function getReligionList(){
        $this->db->select("*");
        $this->db->from("manage_religion");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }
    
}


?>