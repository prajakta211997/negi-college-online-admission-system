<?php 

class ManageReligion_model extends CI_Model {

    function addReligion(){
        
        $religionName = $_POST['religionName'] ;
        $query = $this->db->get_where('manage_religion', array('religion_name' => $religionName,'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                'religion_name' => $religionName,
                );
                
                $insert = $this->db->insert('manage_religion', $data);

                 return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    }

    

    function getReligionList(){
        $this->db->select("*");
        $this->db->from("manage_religion");
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function searchByReligions(){
        $religionSearch = $_POST['ReligionSearch'];
        $this->db->select("*");
        $this->db->from("manage_religion");
        $this->db->like('religion_name', $religionSearch);
        $this->db->where('delete_bit', '0');
        return $this->db->get()->result_array();
    }

    function updateReligion(){
        $religionId = $_POST['updateReligionId'];
        $religionName = $_POST['religionName'] ;
        
        $query = $this->db->get_where('manage_religion', array('religion_name' => $religionName,'delete_bit'=>'0'));
            if ($query->num_rows() == 1) {
                 return -1;
             }else{
                $data = array(
                'religion_name' => $religionName,
                
                    );
              
                $this->db->where('religion_id', $religionId);
                $this->db->update('manage_religion', $data);
                return true; //return $this->db->affected_rows() > 0 ? 1 : 0;
             }
    
    }
    function deleteReligion(){
        $updateReligionId = $_POST['updateReligionId'];
        $data = array(
            'delete_bit' => "1"
        );
        $this->db->where('religion_id', $updateReligionId);
        $this->db->update('manage_religion', $data);
        $this->db->where('religion_id', $updateReligionId);
        $this->db->update('manage_caste', $data);
       return true;
       
    }
  
    
    
    
}

?>