<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageSubject extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ManageSubject_model');
	}
    
    public function addSubject()
	{
		$response = $this->ManageSubject_model->addSubject();
		if($response != -1){
			$Castdata['status'] = array('status' => "1", "message" => "Subject added successfully.");
			$Castdata['data'] = $response;
		}else if($response == -1){
			$Castdata['status'] = array('status' => "0", "message" => "Subject Name Already Added.");
		}else{
			$Castdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Castdata));
    }
    public function getAllCoursesName()
    {
        $response = $this->ManageSubject_model->getAllCoursesName();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "Course name fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }


    public function getSubjectList()
    {
        $response = $this->ManageSubject_model->getSubjectList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
    public function searchBySubjects()
    {
        $response = $this->ManageSubject_model->searchBySubjects();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function updateSubject()
    {
        $response = $this->ManageSubject_model->updateSubject();
        if($response == 1){
                $Castsdata['status'] = array('status' => "1", "message" => "Subject Name updated successfully.");
                $Castsdata['data'] = $response;
        }else if($response == -1){
                $Castsdata['status'] = array('status' => "0", "message" => "Subject Name Alredy Exist..  Nothing to update.");
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function deleteSubject()
    {
        $response = $this->ManageSubject_model->deleteSubject();
        if($response == 1){
                $Castsdata['status'] = array('status' => "1", "message" => "Subject  deleted successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
    
    
}
