<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManagePaymentInstallment extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ManagePaymentInstallment_model');
	}
    
    public function addPaymentInstallment()
	{
		$response = $this->ManagePaymentInstallment_model->addPaymentInstallment();
		if($response != -1){
			$PaymentInstallmentdata['status'] = array('status' => "1", "message" => "Payment Installment added successfully.");
			$PaymentInstallmentdata['data'] = $response;
		}else if($response == -1){
			$PaymentInstallmentdata['status'] = array('status' => "0", "message" => "Payment Installment Already Added.");
		}else{
			$PaymentInstallmentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($PaymentInstallmentdata));
    }
    
    public function getPaymentInstallmentList()
    {
        $response = $this->ManagePaymentInstallment_model->getPaymentInstallmentList();
        if(is_array($response)){
                $PaymentInstallmentsdata['status'] = array('status' => "1", "message" => "Payment Installments fetch successfully.");
                $PaymentInstallmentsdata['data'] = $response;
        }else{
                $PaymentInstallmentsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($PaymentInstallmentsdata));
    }

    public function getCourseFees()
	{	$response = $this->ManagePaymentInstallment_model-> getCourseFees();
        if($response == NULL){
            $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong."); 
        }else{
            $Formsdata['status'] = array('status' => "1", "message" => "Fees data fetch successfully.");
            $Formsdata['data'] = $response;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
		
	}
    public function getAllCoursesName()
	{
		 $response = $this->ManagePaymentInstallment_model->getAllCoursesName();
		 if(is_array($response)){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees details fetch successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}

    public function searchByPaymentInstallments()
    {
        $response = $this->ManagePaymentInstallment_model->searchByPaymentInstallments();
        if(is_array($response)){
                $PaymentInstallmentsdata['status'] = array('status' => "1", "message" => "PaymentInstallments fetch successfully.");
                $PaymentInstallmentsdata['data'] = $response;
        }else{
                $PaymentInstallmentsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($PaymentInstallmentsdata));
    }

    
    public function updatePaymentInstallment()
    {
        $response = $this->ManagePaymentInstallment_model->updatePaymentInstallment();
        if($response == 1){
                $PaymentInstallmentsdata['status'] = array('status' => "1", "message" => "PaymentInstallment Name updated successfully.");
                $PaymentInstallmentsdata['data'] = $response;
        }else if($response == -1){
                $PaymentInstallmentsdata['status'] = array('status' => "0", "message" => "PaymentInstallment Name Alredy Exist..  Nothing to update.");
        }else{
                $PaymentInstallmentsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($PaymentInstallmentsdata));
    }

    public function deletePaymentInstallment()
    {
        $response = $this->ManagePaymentInstallment_model->deletePaymentInstallment();
        if($response == 1){
                $PaymentInstallmentsdata['status'] = array('status' => "1", "message" => "PaymentInstallment  deleted successfully.");
                $PaymentInstallmentsdata['data'] = $response;
        }else{
                $PaymentInstallmentsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($PaymentInstallmentsdata));
    }
    
    
}
