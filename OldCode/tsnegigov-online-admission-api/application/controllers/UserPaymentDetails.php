<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserPaymentDetails extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('UserPaymentDetails_model');
	}

    public function  getPaymentList()
    {
        $response = $this->UserPaymentDetails_model-> getPaymentList();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Paymentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 
    
    public function  getTotalFees()
    {
        $response = $this->UserPaymentDetails_model-> getTotalFees();
        if($response == NULL){
            $Studentdata['status'] = array('status' => "", "message" => "Opps! Something went Wrong."); 
        }else{
            $Studentdata['status'] = array('status' => "1", "message" => "Fees data fetch successfully.");
            $Studentdata['data'] = $response;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 
    

    
}
?>