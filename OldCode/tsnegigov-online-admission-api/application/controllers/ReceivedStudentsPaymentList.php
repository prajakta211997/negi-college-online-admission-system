<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReceivedStudentsPaymentList extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ReceivedStudentsPaymentList_model');
	}


    public function getReligionList()
    {
        $response = $this->ReceivedStudentsPaymentList_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }


    public function getCasteList()
    {
        $response = $this->ReceivedStudentsPaymentList_model->getCasteList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function getAllCoursesName()
    {
        $response = $this->ReceivedStudentsPaymentList_model->getAllCoursesName();
        if(is_array($response)){
                $Coursedata['status'] = array('status' => "1", "message" => "Coursedata fetch successfully.");
                $Coursedata['data'] = $response;
        }else{
                $Coursedata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Coursedata));

    }

    public function  getPaymentRecivedStudentList()
    {
        $response = $this->ReceivedStudentsPaymentList_model->getPaymentRecivedStudentList();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    }


    public function searchByDetails()
    {
        $response = $this->ReceivedStudentsPaymentList_model->searchByDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Details fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function getFilterData()
	{
		$response = $this->ReceivedStudentsPaymentList_model->getFilterData();
		if(is_array($response)){
			$Studentdata['status'] = array('status' => "1", "message" => "Fetch Course Fees list successfully.");
			$Studentdata['data'] = $response;
		}else{
			$Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
        
    public function  getSingleStudentDetails()
    {
        $response = $this->ReceivedStudentsPaymentList_model-> getSingleStudentDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 

    public function removeFromShortlist()
    {
        $response = $this->ReceivedStudentsPaymentList_model->removeFromShortlist();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student is Remove From shortlisted List.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function getAllDataForExcel()
    {
        $response = $this->ReceivedStudentsPaymentList_model->getAllDataForExcel();
        if(is_array($response)){
                $userdata['status'] = array('status' => "1", "message" => "Fetch all Student successfully.");
                $userdata['data'] = $response;
        }else{
                $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
    }

    public function addSelectedToConfirm()
    {
        $response = $this->ReceivedStudentsPaymentList_model->addSelectedToConfirm();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student Admissions are confirmed.");
            $Studentdata['data'] = $response;
        }else if($response == -1){
            $Studentdata['status'] = array('status' => "0", "message" => "Sorry ,All seats Are Full.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
    public function addToConfirmed()
    {
        $response = $this->ReceivedStudentsPaymentList_model->addToConfirmed();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student Added in  Confirmed List.");
            $Studentdata['data'] = $response;
        }else if($response == -1){
            $Studentdata['status'] = array('status' => "0", "message" => "Sorry ,All seats Are Full.");
            $Studentdata['data'] = $response;
        }
        else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function getSubjectList()
    {
        $response = $this->ReceivedStudentsPaymentList_model->getSubjectList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
        
}

    

?>