<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditProfile extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('EditProfile_model');
	}
    public function getStudentList()
        {
            $response = $this->EditProfile_model->getStudentList();
            if(is_array($response)){
                $data['status'] = array('status' => "1", "message" => "Student data  fetch successfully.");
                $data['data'] = $response;
            }else{
                $data['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
      
        public function getStudentRegistrationDetails()
    {
		$response = $this->EditProfile_model->getStudentRegistrationDetails();
		if(is_array($response)){
				$studentdata['status'] = array('status' => "1", "message" => "student data fetch successfully.");
				$studentdata['data'] = $response;
		}else{
				$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}
       

        

        public function saveProfile()
        {
                $response = $this->EditProfile_model->saveProfile();
                if($response == 1){
                        $coursesdata['status'] = array('status' => "1", "message" => "Profile updated successfully.");
                        $coursesdata['data'] = $response;
                }else if($response == -1){
                        $coursesdata['status'] = array('status' => "0", "message" => "Nothing to update.");
                }else{
                        $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                }
                $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        }
        public function getSubjectList()
    {
        $response = $this->EditProfile_model->getSubjectList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
    
    public function getReligionList()
    {
        $response = $this->EditProfile_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }

    
        
	
    
}
