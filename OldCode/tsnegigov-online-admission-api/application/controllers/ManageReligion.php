<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageReligion extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ManageReligion_model');
	}
    
    public function addReligion()
	{
		$response = $this->ManageReligion_model->addReligion();
		if($response != -1){
			$religiondata['status'] = array('status' => "1", "message" => "Religion added successfully.");
			$religiondata['data'] = $response;
		}else if($response == -1){
			$religiondata['status'] = array('status' => "0", "message" => "Religion Name Already Added.");
		}else{
			$religiondata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($religiondata));
    }
    
    public function getReligionList()
    {
        $response = $this->ManageReligion_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }

    public function searchByReligions()
    {
        $response = $this->ManageReligion_model->searchByReligions();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "Religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }

    
    public function updateReligion()
    {
        $response = $this->ManageReligion_model->updateReligion();
        if($response == 1){
                $religionsdata['status'] = array('status' => "1", "message" => "Religion Name updated successfully.");
                $religionsdata['data'] = $response;
        }else if($response == -1){
                $religionsdata['status'] = array('status' => "0", "message" => "Religion Name Alredy Exist..  Nothing to update.");
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }

    public function deleteReligion()
    {
        $response = $this->ManageReligion_model->deleteReligion();
        if($response == 1){
                $religionsdata['status'] = array('status' => "1", "message" => "Religion  deleted successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }
    
    
}
