<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserLogin extends CI_Controller {
	
	function __construct() {
		parent::__construct();
               
		$this->load->model('UserLogin_model');
	}
    
	
	
	public function checkLogin()
	{
		$response = $this->UserLogin_model->checkLogin();
		if($response == 0){
                    $userdata['status'] = array('status' => "0", "message" => "Opps! Invalid login details.");
                }else if($response == -1){
                    $userdata['status'] = array('status' => "0", "message" => "Your account is blocked by admin. Please contact to admin.");
                }else{
                    $userdata['status'] = array('status' => "1", "message" => " Logged in successfully.");
                    $userdata['data'] = $response;
                }
		$this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
	
	public function changePassword()
	{
		$response = $this->UserLogin_model->changePassword();
		if($response == 1){
			$passworddata['status'] = array('status' => "1", "message" => "Password updated successfully.");
			$passworddata['data'] = $response;
		}else{
			$passworddata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($passworddata));
        }
    
}
