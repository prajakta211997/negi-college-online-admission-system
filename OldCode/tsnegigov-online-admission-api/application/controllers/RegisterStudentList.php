<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterStudentList extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('RegisterStudentList_model');
	}


    public function getReligionList()
    {
        $response = $this->RegisterStudentList_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }


    public function getCasteList()
    {
        $response = $this->RegisterStudentList_model->getCasteList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }

    public function getAllCoursesName()
    {
        $response = $this->RegisterStudentList_model->getAllCoursesName();
        if(is_array($response)){
                $Coursedata['status'] = array('status' => "1", "message" => "Coursedata fetch successfully.");
                $Coursedata['data'] = $response;
        }else{
                $Coursedata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Coursedata));

    }

    public function  getRegisterStudentList()
    {
        $response = $this->RegisterStudentList_model-> getRegisterStudentList();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    }


    public function searchByDetails()
    {
        $response = $this->RegisterStudentList_model->searchByDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Details fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function getFilterData()
	{
		$response = $this->RegisterStudentList_model->getFilterData();
		if(is_array($response)){
			$Studentdata['status'] = array('status' => "1", "message" => "Fatch Course Fees list successfully.");
			$Studentdata['data'] = $response;
		}else{
			$Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
        
    public function  getSingleStudentDetails()
    {
        $response = $this->RegisterStudentList_model-> getSingleStudentDetails();
        if(is_array($response)){
                $Studentdata['status'] = array('status' => "1", "message" => "Studentdata fetch successfully.");
                $Studentdata['data'] = $response;
        }else{
                $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));

    } 
    public function addFeesType()
    {
        $response = $this->RegisterStudentList_model->addFeesType();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student Fees type added successfully.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }


    public function addToShortlist()
    {
        $response = $this->RegisterStudentList_model->addToShortlist();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Student is shortlisted.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }

    public function addSelectedToShortlist()
    {
        $response = $this->RegisterStudentList_model->addSelectedToShortlist();
        if($response == 1){
            $Studentdata['status'] = array('status' => "1", "message" => "Students are shortlisted.");
            $Studentdata['data'] = $response;
        }else{
            $Studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Studentdata));
    }
    
   
        

    public function getAllDataForExcel()
        {
                $response = $this->RegisterStudentList_model->getAllDataForExcel();
                if(is_array($response)){
                        $userdata['status'] = array('status' => "1", "message" => "Fetch all Student successfully.");
                        $userdata['data'] = $response;
                }else{
                        $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                }
                $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
        }
        
        public function deleteStudent()
	{
		 $response = $this->RegisterStudentList_model->deleteStudent();
		 if($response == 1){
				 $userdata['status'] = array('status' => "1", "message" => "Student account blocked successfully.");
				 $userdata['data'] = $response;
		 }else{
				 $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
        }
        
        public function getSubjectList()
    {
        $response = $this->RegisterStudentList_model->getSubjectList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
    public function saveProfile()
    {
        $response = $this->RegisterStudentList_model->saveProfile();
        if($response == 1){
                $coursesdata['status'] = array('status' => "1", "message" => "Profile updated successfully.");
                $coursesdata['data'] = $response;
        }else if($response == -1){
                $coursesdata['status'] = array('status' => "0", "message" => "Nothing to update.");
        }else{
                $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
    }

}

?>