<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class AdminDashboard extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('AdminDashboard_model');
	}

    public function getCourseWiseAnalysis()
        {
            $response = $this->AdminDashboard_model->getCourseWiseAnalysis();
            if(is_array($response)){
                $data['status'] = array('status' => "1", "message" => "Student data  fetch successfully.");
                $data['data'] = $response;
            }else{
                $data['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    
}

