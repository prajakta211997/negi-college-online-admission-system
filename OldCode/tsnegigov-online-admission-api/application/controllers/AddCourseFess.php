<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddCourseFess extends CI_Controller {
	
	function __construct() {
            parent::__construct();
            $this->load->model('AddCourseFess_model');
	}
	
	

    public function excelCoureseFeesUpload() {
        $finalarray = array();
        $retrunBit = 0;
        $returnMessage="";
        $file_data = $_FILES['CourseFeesList'];       
        $file_path = $file_data['tmp_name'];

        $this->load->library('EXcel');
        $inputFileName = $file_path;
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
		$arrayCount = count($allDataInSheet);
		

        for ($i = 2; $i <= $arrayCount; $i++) {
			
			if(empty($allDataInSheet[$i]["A"]) && empty($allDataInSheet[$i]["B"]) && empty($allDataInSheet[$i]["C"]) && empty($allDataInSheet[$i]["D"]) && empty($allDataInSheet[$i]["E"]) && empty($allDataInSheet[$i]["F"]) && empty($allDataInSheet[$i]["G"]) && empty($allDataInSheet[$i]["H"]) && empty($allDataInSheet[$i]["I"]) && empty($allDataInSheet[$i]["J"]) && empty($allDataInSheet[$i]["K"]) && empty($allDataInSheet[$i]["L"]) && empty($allDataInSheet[$i]["M"]) && empty($allDataInSheet[$i]["N"]) && empty($allDataInSheet[$i]["O"]) && empty($allDataInSheet[$i]["P"]) && empty($allDataInSheet[$i]["Q"]) && empty($allDataInSheet[$i]["R"]) && empty($allDataInSheet[$i]["S"]) && empty($allDataInSheet[$i]["T"]) && empty($allDataInSheet[$i]["U"]) && empty($allDataInSheet[$i]["V"]) && empty($allDataInSheet[$i]["W"]) && empty($allDataInSheet[$i]["X"]) && empty($allDataInSheet[$i]["Y"]) && empty($allDataInSheet[$i]["Z"]) && empty($allDataInSheet[$i]["AA"]) && empty($allDataInSheet[$i]["AB"])&& empty($allDataInSheet[$i]["AC"]) && empty($allDataInSheet[$i]["AD"])&& empty($allDataInSheet[$i]["AE"]) && empty($allDataInSheet[$i]["AF"]) && empty($allDataInSheet[$i]["AG"]) && empty($allDataInSheet[$i]["AH"]) && empty($allDataInSheet[$i]["AI"]) && empty($allDataInSheet[$i]["AJ"]) && empty($allDataInSheet[$i]["AK"]) && empty($allDataInSheet[$i]["AL"]) && empty($allDataInSheet[$i]["AM"]) && empty($allDataInSheet[$i]["AN"])&& empty($allDataInSheet[$i]["AO"])){
				$retrunBit = 1;  
			    break;
			}else{
					if (!empty($allDataInSheet[$i]["A"])) {
						if (!empty($allDataInSheet[$i]["B"])) {
							if (!empty($allDataInSheet[$i]["C"])) {
								if (!empty($allDataInSheet[$i]["D"])) {
									if (!empty($allDataInSheet[$i]["E"])) {
										if (!empty($allDataInSheet[$i]["F"])) {
											if (!empty($allDataInSheet[$i]["G"])) {
												if (!empty($allDataInSheet[$i]["H"])) {
													if (!empty($allDataInSheet[$i]["I"])) {
														if (!empty($allDataInSheet[$i]["J"])) {
															if (!empty($allDataInSheet[$i]["K"])) {
																if (!empty($allDataInSheet[$i]["L"])) {
																	if (!empty($allDataInSheet[$i]["M"])) {
																		if (!empty($allDataInSheet[$i]["N"])) {
																			if (!empty($allDataInSheet[$i]["O"])) {
																				if (!empty($allDataInSheet[$i]["P"])) {
																					if (!empty($allDataInSheet[$i]["Q"])) {
																						if (!empty($allDataInSheet[$i]["R"])) {
																							if (!empty($allDataInSheet[$i]["S"])) {
																								if (!empty($allDataInSheet[$i]["T"])) {
																									if (!empty($allDataInSheet[$i]["U"])) {
																										if (!empty($allDataInSheet[$i]["V"])) {
																											if (!empty($allDataInSheet[$i]["W"])) {
																												if (!empty($allDataInSheet[$i]["X"])) {
																													if (!empty($allDataInSheet[$i]["Y"])) {
																														if (!empty($allDataInSheet[$i]["Z"])) {
																															if (!empty($allDataInSheet[$i]["AA"])) {
																																if (!empty($allDataInSheet[$i]["AB"])) {
																																	if (!empty($allDataInSheet[$i]["AC"])) {
																																		if (!empty($allDataInSheet[$i]["AD"])) {
																																			if (!empty($allDataInSheet[$i]["AE"])) {
																																				if (!empty($allDataInSheet[$i]["AF"])) {
																																					if (!empty($allDataInSheet[$i]["AG"])) {
																																						if (!empty($allDataInSheet[$i]["AH"])) {
																																							if (!empty($allDataInSheet[$i]["AI"])) {
																																								if (!empty($allDataInSheet[$i]["AJ"])) {
																																									if (!empty($allDataInSheet[$i]["AK"])) {
																																										if (!empty($allDataInSheet[$i]["AL"])) {
																																											if (!empty($allDataInSheet[$i]["AM"])) {
																																												if (!empty($allDataInSheet[$i]["AN"])) {
																																													if (!empty($allDataInSheet[$i]["AO"])) {
																																													$individualArray = array(
																																															

																																															'course_name'=> trim($allDataInSheet[$i]["A"]), 
																																															'academic_year'=> trim($allDataInSheet[$i]["B"]), 
																																															'course_year' => trim($allDataInSheet[$i]["C"]), 
																																															'fees_type' => trim($allDataInSheet[$i]["D"]),
																																															'course_division' => trim($allDataInSheet[$i]["E"]), 
																																															'tution_fees' => trim($allDataInSheet[$i]["F"]),
																																															'library_fees' => trim($allDataInSheet[$i]["G"]),
																																															'activity_fees' => trim($allDataInSheet[$i]["H"]),
																																															'gymkhana_fees' => trim($allDataInSheet[$i]["I"]),
																																															'eligibility_fees' => trim($allDataInSheet[$i]["J"]),
																																															'eligibility_form_fees' => trim($allDataInSheet[$i]["K"]),
																																															'admission_fees' => trim($allDataInSheet[$i]["L"]),
																																															'welfare_fees' => trim($allDataInSheet[$i]["M"]),
																																															'computarization_fees' => trim($allDataInSheet[$i]["N"]),
																																															'developement_fees' => trim($allDataInSheet[$i]["O"]),
																																															'registration_fees' => trim($allDataInSheet[$i]["P"]),
																																															'pro_rata_fees' => trim($allDataInSheet[$i]["Q"]),
																																															'disas_mgmt_fees' => trim($allDataInSheet[$i]["R"]),
																																															'library_deposite_fees' => trim($allDataInSheet[$i]["S"]),
																																															'caution_money_fees' => trim($allDataInSheet[$i]["T"]),
																																															'medical_exam_fees' => trim($allDataInSheet[$i]["U"]),
																																															'practical_fees' => trim($allDataInSheet[$i]["V"]),
																																															'laboratory_fees' => trim($allDataInSheet[$i]["W"]),
																																															'siss_fees' => trim($allDataInSheet[$i]["X"]),
																																															'personal_fees' => trim($allDataInSheet[$i]["Y"]),
																																															'short_term_fees' => trim($allDataInSheet[$i]["Z"]),
																																															'security_charges' => trim($allDataInSheet[$i]["AA"]),
																																															'adminitration_charges' =>trim($allDataInSheet[$i]["AB"]),
																																															'seminar_fees' => trim($allDataInSheet[$i]["AC"]),
																																															'phy_edn_scheme' =>trim($allDataInSheet[$i]["AD"]),
																																															'alumni_fees' => trim($allDataInSheet[$i]["AE"]),
																																															'college_dev_fees' => trim($allDataInSheet[$i]["AF"]),
																																															'termend_exam_fees' => trim($allDataInSheet[$i]["AG"]),
																																															'computer_lab_fees' => trim($allDataInSheet[$i]["AH"]),
																																															'workshop_fees' => trim($allDataInSheet[$i]["AI"]),
																																															'comp_internet_fees' => trim($allDataInSheet[$i]["AJ"]),
																																															'corpus_fund' => trim($allDataInSheet[$i]["AK"]),
																																															'form_fees' => trim($allDataInSheet[$i]["AL"]),
																																															'nss_section_fees' => trim($allDataInSheet[$i]["AM"]),
																																															'comp_lab_maintenace_fee' => trim($allDataInSheet[$i]["AN"]),
																																															'total_fees' => trim($allDataInSheet[$i]["AO"])
																																														);
																																														$finalarray[] = $individualArray;
																																														$retrunBit = 1;




																																													}else{
																																														$retrunBit = 2;
																																														$returnMessage = "Your provided excel sheet contains empty value on column 'Total Fees' & row number ".$i;
																																														break;
																																														}
																																												}else{
																																													$retrunBit = 2;
																																													$returnMessage = "Your provided excel sheet contains empty value on column 'Comp Lab Maintainance Fees' & row number ".$i;
																																													break;
																																													}
																																											}else{
																																												$retrunBit = 2;
																																												$returnMessage = "Your provided excel sheet contains empty value on column 'NSS Section Fees' & row number ".$i;
																																												break;
																																												}
																																										}else{
																																											$retrunBit = 2;
																																											$returnMessage = "Your provided excel sheet contains empty value on column 'Form Fees' & row number ".$i;
																																											break;
																																											}
																																									}else{
																																										$retrunBit = 2;
																																										$returnMessage = "Your provided excel sheet contains empty value on column 'Corpus Fund' & row number ".$i;
																																										break;
																																										}
																																								}else{
																																									$retrunBit = 2;
																																									$returnMessage = "Your provided excel sheet contains empty value on column 'Comp & Internet Fees' & row number ".$i;
																																									break;
																																									}
																																							}else{
																																								$retrunBit = 2;
																																								$returnMessage = "Your provided excel sheet contains empty value on column 'Workshop & Seminar Fees' & row number ".$i;
																																								break;
																																								}
																																						}else{
																																							$retrunBit = 2;
																																							$returnMessage = "Your provided excel sheet contains empty value on column 'Computer Lab Fees' & row number ".$i;
																																							break;
																																							}
																																					}else{
																																						$retrunBit = 2;
																																						$returnMessage = "Your provided excel sheet contains empty value on column 'Termend Exam Fees' & row number ".$i;
																																						break;
																																						}
																																				}else{
																																					$retrunBit = 2;
																																					$returnMessage = "Your provided excel sheet contains empty value on column 'College Devp. Fees' & row number ".$i;
																																					break;
																																					}
																																			}else{
																																				$retrunBit = 2;
																																				$returnMessage = "Your provided excel sheet contains empty value on column 'Alumni Fees' & row number ".$i;
																																				break;
																																				}
																																		}else{
																																			$retrunBit = 2;
																																			$returnMessage = "Your provided excel sheet contains empty value on column 'Phy. Edn. Scheme' & row number ".$i;
																																			break;
																																			}
																																	}else{
																																		$retrunBit = 2;
																																		$returnMessage = "Your provided excel sheet contains empty value on column 'Seminar Fees' & row number ".$i;
																																		break;
																																		}
																																}else{
																																	$retrunBit = 2;
																																	$returnMessage = "Your provided excel sheet contains empty value on column 'Administration Charges' & row number ".$i;
																																	break;
																																	}
																															}else{
																																$retrunBit = 2;
																																$returnMessage = "Your provided excel sheet contains empty value on column 'Security Charges' & row number ".$i;
																																break;
																																}
																														}else{
																															$retrunBit = 2;
																															$returnMessage = "Your provided excel sheet contains empty value on column 'Short Term Course Fee' & row number ".$i;
																															break;
																															}
																													}else{
																														$retrunBit = 2;
																														$returnMessage = "Your provided excel sheet contains empty value on column 'Personnel Fees' & row number ".$i;
																														break;
																														}
																												}else{
																													$retrunBit = 2;
																													$returnMessage = "Your provided excel sheet contains empty value on column 'S.I.S.S. Fees' & row number ".$i;
																													break;
																													}
																											}else{
																												$retrunBit = 2;
																												$returnMessage = "Your provided excel sheet contains empty value on column 'Laboratory Fees' & row number ".$i;
																												break;
																												}
																										}else{
																											$retrunBit = 2;
																											$returnMessage = "Your provided excel sheet contains empty value on column 'Practical Fees' & row number ".$i;
																											break;
																											}
																									}else{
																										$retrunBit = 2;
																										$returnMessage = "Your provided excel sheet contains empty value on column 'Medical Exam Fees' & row number ".$i;
																										break;
																										}
																								}else{
																									$retrunBit = 2;
																									$returnMessage = "Your provided excel sheet contains empty value on column 'Caution Money' & row number ".$i;
																									break;
																									}
																							}else{
																								$retrunBit = 2;
																								$returnMessage = "Your provided excel sheet contains empty value on column 'Library Deposite' & row number ".$i;
																								break;
																								}
																						}else{
																							$retrunBit = 2;
																							$returnMessage = "Your provided excel sheet contains empty value on column 'Disas. Mgmt. Fees' & row number ".$i;
																							break;
																							}
																					}else{
																						$retrunBit = 2;
																						$returnMessage = "Your provided excel sheet contains empty value on column 'Pro-rata(Ashwamegh)' & row number ".$i;
																						break;
																						}
																				}else{
																					$retrunBit = 2;
																					$returnMessage = "Your provided excel sheet contains empty value on column 'Registration Fees' & row number ".$i;
																					break;
																					}
																			}else{
																				$retrunBit = 2;
																				$returnMessage = "Your provided excel sheet contains empty value on column 'Development Fees' & row number ".$i;
																				break;
																				}
																		}else{
																			$retrunBit = 2;
																			$returnMessage = "Your provided excel sheet contains empty value on column 'Computerization Fees' & row number ".$i;
																			break;
																			}
																	}else{
																		$retrunBit = 2;
																		$returnMessage = "Your provided excel sheet contains empty value on column 'Student Welfare Fees' & row number ".$i;
																		break;
																		}
																}else{
																	$retrunBit = 2;
																	$returnMessage = "Your provided excel sheet contains empty value on column 'Admission Fees' & row number ".$i;
																	break;
																	}
															}else{
																$retrunBit = 2;
																$returnMessage = "Your provided excel sheet contains empty value on column 'Eligibility Form Fees' & row number ".$i;
																break;
																}

														}else{
															$retrunBit = 2;
															$returnMessage = "Your provided excel sheet contains empty value on column 'Eligibility Fees' & row number ".$i;
															break;
															}
													}else{
													$retrunBit = 2;
													$returnMessage = "Your provided excel sheet contains empty value on column 'Gymkhana Fees' & row number ".$i;
													break;
													}
		
												}else{
												$retrunBit = 2;
												$returnMessage = "Your provided excel sheet contains empty value on column 'Student Activities Fees' & row number ".$i;
												break;
												}
							
											}else{
											$retrunBit = 2;
											$returnMessage = "Your provided excel sheet contains empty value on column 'Library Fees' & row number ".$i;
											break;
											}
							
										}else{
										$retrunBit = 2;
										$returnMessage = "Your provided excel sheet contains empty value on column 'Tution Fees' & row number ".$i;
										break;
										}
									}else{
										$retrunBit = 2;
										$returnMessage = "Your provided excel sheet contains empty value on column 'Course Division' & row number ".$i;
										break;
										}
						
								}else{
								$retrunBit = 2;
								$returnMessage = "Your provided excel sheet contains empty value on column 'Fees Type' & row number ".$i;
								break;
								}
							}else{
							$retrunBit = 2;
							$returnMessage = "Your provided excel sheet contains empty value on column 'Course Year' & row number ".$i;
							break;
							}
						
						}else{
						$retrunBit = 2;
						$returnMessage = "Your provided excel sheet contains empty value on column ' Academic Year ' & row number ".$i;
						break;
						}
					}else{
					$retrunBit = 2;
					$returnMessage = "Your provided excel sheet contains empty value on column 'Course Id '  & row number ".$i;
					break;
					}
				
			}
		}
		if($retrunBit == 1){
                    $response = $this->AddCourseFess_model->excelCoureseFeesUpload($finalarray);
                    if($response == 1){
                        $userdata['status'] = array('status' => "1", "message" => "Course wise Fees added successfully.");
                        $userdata['data'] = $response;
                    }else if($response == -1){
                            $userdata['status'] = array('status' => "0", "message" => "Coursewise Fees already exists.");
                    }else{
                            $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                    }
			//$userdata['status'] = array('status' => "1", "message" => "Student excel uploaded successfully.");
		}else if($retrunBit == 2){
			$userdata['status'] = array('status' => "0", "message" => $returnMessage);
		}else if($retrunBit == -1){
                    $userdata['status'] = array('status' => "0",  "message" => "Coursewise Fees already exists.");
                }else{
                    $userdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
	   
	   $this->output->set_content_type('application/json')->set_output(json_encode($userdata));
	}
	
	public function addSingleCourseFee()
	{
		$response = $this->AddCourseFess_model->addSingleCourseFee();
		if($response != -1){
			$coursedata['status'] = array('status' => "1", "message" => "Register successfully.");
			$coursedata['data'] = $response;
		}else if($response == -1){
			$coursedata['status'] = array('status' => "0", "message" => "Course Wise  Fees Details Already Added.");
		}else{
			$coursedata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursedata));
	}

	public function getFilterData()
	{
		$response = $this->AddCourseFess_model->getFilterData();
		if(is_array($response)){
			$coursesdata['status'] = array('status' => "1", "message" => "Fatch Course Fees list successfully.");
			$coursesdata['data'] = $response;
		}else{
			$coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
	} 

	public function getSingleCourseFeesList()
	{
		$response = $this->AddCourseFess_model->getSingleCourseFeesList();
		if(is_array($response)){
			$coursesdata['status'] = array('status' => "1", "message" => "Fatch course Fees list successfully.");
			$coursesdata['data'] = $response;
		}else{
			$coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
	}

	public function getCourseFeesList()
	{
		 $response = $this->AddCourseFess_model->getCourseFeesList();
		 if(is_array($response)){
				 $coursesdata['status'] = array('status' => "1", "message" => "Course Fees details fetch successfully.");
				 $coursesdata['data'] = $response;
		 }else{
				 $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
	}


	public function searchByCourses()
	{
		 $response = $this->AddCourseFess_model->searchByCourses();
		 if(is_array($response)){
				 $coursesdata['status'] = array('status' => "1", "message" => "Course Fees Details fetch successfully.");
				 $coursesdata['data'] = $response;
		 }else{
				 $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
	}
 
	public function updateCourseFee()
	{
		 $response = $this->AddCourseFess_model->updateCourseFee();
		 if($response == 1){
				 $coursesdata['status'] = array('status' => "1", "message" => "Course Fees Details updated successfully.");
				 $coursesdata['data'] = $response;
		 }else if($response == -1){
				 $coursesdata['status'] = array('status' => "0", "message" => "Nothing to update.");
		 }else{
				 $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
	}
 
	public function deleteCourseFees()
	{
		 $response = $this->AddCourseFess_model->deleteCourseFees();
		 if($response == 1){
				 $coursesdata['status'] = array('status' => "1", "message" => "Course wise Fees details deleted successfully.");
				 $coursesdata['data'] = $response;
		 }else{
				 $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
	}
	
	public function getAllDataForExcel()
    {
        $response = $this->AddCourseFess_model->getAllDataForExcel();
        if(is_array($response)){
                $coursesdata['status'] = array('status' => "1", "message" => "Fetch all course wise fee details successfully.");
                $coursesdata['data'] = $response;
        }else{
                $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
    }

	public function getAllCoursesName()
	{
		 $response = $this->AddCourseFess_model->getAllCoursesName();
		 if(is_array($response)){
				 $Formsdata['status'] = array('status' => "1", "message" => "Form Fees details fetch successfully.");
				 $Formsdata['data'] = $response;
		 }else{
				 $Formsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		 }
		 $this->output->set_content_type('application/json')->set_output(json_encode($Formsdata));
	}
	



}