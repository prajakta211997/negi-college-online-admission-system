<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManageCourse extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('ManageCourse_model');
	}
    
        public function addCourse()
	{
		$response = $this->ManageCourse_model->addCourse();
		if($response != -1){
			$coursedata['status'] = array('status' => "1", "message" => "Course added successfully.");
			$coursedata['data'] = $response;
		}else if($response == -1){
			$coursedata['status'] = array('status' => "0", "message" => "Course Already Added.");
		}else{
			$coursedata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursedata));
        }
    
        public function getCoursesList()
        {
                $response = $this->ManageCourse_model->getCoursesList();
                if(is_array($response)){
                        $coursesdata['status'] = array('status' => "1", "message" => "Courses fetch successfully.");
                        $coursesdata['data'] = $response;
                }else{
                        $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                }
                $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        }

        public function searchByCourses()
        {
                $response = $this->ManageCourse_model->searchByCourses();
                if(is_array($response)){
                        $coursesdata['status'] = array('status' => "1", "message" => "Courses fetch successfully.");
                        $coursesdata['data'] = $response;
                }else{
                        $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                }
                $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        }

        public function updateCourse()
        {
                $response = $this->ManageCourse_model->updateCourse();
                if($response == 1){
                        $coursesdata['status'] = array('status' => "1", "message" => "Course  updated successfully.");
                        $coursesdata['data'] = $response;
                }else if($response == -1){
                        $coursesdata['status'] = array('status' => "0", "message" => "Nothing to update.");
                }else{
                        $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                }
                $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        }

        public function deleteCourses()
        {
                $response = $this->ManageCourse_model->deleteCourses();
                if($response == 1){
                        $coursesdata['status'] = array('status' => "1", "message" => "Course  deleted successfully.");
                        $coursesdata['data'] = $response;
                }else{
                        $coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
                }
                $this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        }
    
        public function getSingleCourseList()
        {
		$response = $this->ManageCourse_model->getSingleCourseList();
		if(is_array($response)){
			$coursesdata['status'] = array('status' => "1", "message" => "Fatch student list successfully.");
			$coursesdata['data'] = $response;
		}else{
			$coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        }

        public function getFilterData()
	{
		$response = $this->ManageCourse_model->getFilterData();
		if(is_array($response)){
			$coursesdata['status'] = array('status' => "1", "message" => "Fatch student list successfully.");
			$coursesdata['data'] = $response;
		}else{
			$coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        } 
	
        public function getAllCoursesName()
	{
		$response = $this->ManageCourse_model->getAllCoursesName();
		if(is_array($response)){
			$coursesdata['status'] = array('status' => "1", "message" => "Fatch student list successfully.");
			$coursesdata['data'] = $response;
		}else{
			$coursesdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursesdata));
        } 
    
}
