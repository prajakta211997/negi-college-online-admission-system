<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdmissionForm extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        
		$this->load->model('AdmissionForm_model');
	}
    
    public function submitPersonalInfo()
	{
		$response = $this->AdmissionForm_model->submitPersonalInfo();
		if($response ==1){
			$studentdata['status'] = array('status' => "1", "message" => "Student personal details saved successfully.");
			$studentdata['data'] = $response;
		}else if($response == 2){
			$studentdata['status'] = array('status' => "1", "message" => "Student personal details updated Successfully.");
		}else if($response == -1){
			$studentdata['status'] = array('status' => "0", "message" => "Nothing to update.");
		}else if($response == -2){
			$studentdata['status'] = array('status' => "0", "message" => "Mandatory fields are Required.");
		}else{
			$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}

	public function submitAcademicInfo()
	{
		$response = $this->AdmissionForm_model->submitAcademicInfo();
		if($response ==1){
			$studentdata['status'] = array('status' => "1", "message" => "Student academic details saved successfully.");
			$studentdata['data'] = $response;
		}else if($response == 2){
			$studentdata['status'] = array('status' => "1", "message" => "Student academic details updated Successfully.");
		}else if($response == -1){
			$studentdata['status'] = array('status' => "0", "message" => "Nothing to update.");
		}else{
			$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}

	public function submitParentInfo()
	{
		$response = $this->AdmissionForm_model->submitParentInfo();
		if($response ==1){
			$studentdata['status'] = array('status' => "1", "message" => "Student parent details saved successfully.");
			$studentdata['data'] = $response;
		}else if($response == 2){
			$studentdata['status'] = array('status' => "1", "message" => "Student parent details updated successfully.");
		}else if($response == -1){
			$studentdata['status'] = array('status' => "1", "message" => "Student application submitted successfully.");
		}else{
			$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}
	
	public function getStudentPersonalInfo()
    {
		$response = $this->AdmissionForm_model->getStudentPersonalInfo();
		if(is_array($response)){
				$studentdata['status'] = array('status' => "1", "message" => "Student data fetch successfully.");
				$studentdata['data'] = $response;
		}else{
				$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}
	
	public function getStudentAcademicInfo()
    {
		$response = $this->AdmissionForm_model->getStudentAcademicInfo();
		if(is_array($response)){
				$studentdata['status'] = array('status' => "1", "message" => "Student data fetch successfully.");
				$studentdata['data'] = $response;
		}else{
				$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}

	public function getStudentParentInfo()
    {
		$response = $this->AdmissionForm_model->getStudentParentInfo();
		if(is_array($response)){
				$studentdata['status'] = array('status' => "1", "message" => "Student data fetch successfully.");
				$studentdata['data'] = $response;
		}else{
				$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}
	
    
    public function getStudentRegistrationDetails()
    {
		$response = $this->AdmissionForm_model->getStudentRegistrationDetails();
		if(is_array($response)){
				$studentdata['status'] = array('status' => "1", "message" => "Student data fetch successfully.");
				$studentdata['data'] = $response;
		}else{
				$studentdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($studentdata));
	}

	public function getAdmissionDate()
    {
		$response = $this->AdmissionForm_model->getAdmissionDate();
		if(is_array($response)){
				$coursedata['status'] = array('status' => "1", "message" => "Course data fetch successfully.");
				$coursedata['data'] = $response;
		}else{
				$coursedata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($coursedata));
	}

	public function getReligionList()
    {
        $response = $this->AdmissionForm_model->getReligionList();
        if(is_array($response)){
                $religionsdata['status'] = array('status' => "1", "message" => "Religions fetch successfully.");
                $religionsdata['data'] = $response;
        }else{
                $religionsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($religionsdata));
    }


	public function getCasteList()
    {
        $response = $this->AdmissionForm_model->getCasteList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
	}
	public function getSubCasteList()
    {
        $response = $this->AdmissionForm_model->getSubCasteList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Sub Castes fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
	
	public function getSubjectList()
    {
        $response = $this->AdmissionForm_model->getSubjectList();
        if(is_array($response)){
                $Castsdata['status'] = array('status' => "1", "message" => "Subjects fetch successfully.");
                $Castsdata['data'] = $response;
        }else{
                $Castsdata['status'] = array('status' => "0", "message" => "Opps! Something went Wrong.");
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($Castsdata));
    }
    
}
