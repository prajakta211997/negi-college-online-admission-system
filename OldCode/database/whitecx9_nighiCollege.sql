-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 21, 2021 at 12:29 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `whitecx9_nighiCollege`
--

-- --------------------------------------------------------

--
-- Table structure for table `manage_caste`
--

CREATE TABLE `manage_caste` (
  `caste_id` int(11) NOT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `caste_name` varchar(1000) NOT NULL,
  `sub_caste_name` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_caste`
--

INSERT INTO `manage_caste` (`caste_id`, `religion_id`, `caste_name`, `sub_caste_name`, `delete_bit`) VALUES
(1, 1, 'Agri', 'OPEN', 0),
(2, 1, 'Bhandari', 'OBC', 0),
(3, 1, 'Bhoi', 'OBC', 0),
(4, 1, 'Bhoi', 'NT-B', 0),
(5, 1, 'Dhangar', 'NT-C', 0),
(6, 1, 'Parit', 'OBC', 0),
(7, 1, 'Dhobi', 'OBC', 0),
(8, 1, 'Kasar', 'OBC', 0),
(9, 1, 'Leva Patil', 'OPEN', 0),
(10, 1, 'Lonari', 'OBC', 0),
(11, 1, 'Pachkalsi', 'OBC', 0),
(12, 1, 'Ramoshi', 'VJ', 0),
(13, 1, 'Vaishya wani', 'OBC', 0),
(14, 1, 'Vanjari', 'NT-D', 0),
(15, 5, 'Mahar', 'SC', 0),
(16, 1, 'Maratha - Kunbi', 'OBC', 0),
(17, 2, 'Bagwan', 'OBC', 0),
(18, 2, 'Bawarchi/ Bhatiyara', 'OBC', 0),
(19, 2, 'Bhandari', 'OBC', 0),
(20, 2, 'Tamboli', 'OBC', 0),
(21, 2, 'Muslim Madari', 'NT-B', 0),
(22, 2, 'Muslim Gavali', 'NT-B', 0),
(23, 2, 'Waghwale', 'NT-B', 0),
(24, 2, 'Takari', 'VJ', 0),
(25, 2, 'Chhapparband', 'VJ', 0),
(26, 2, 'Beldar', 'NT-B', 0),
(27, 2, 'Garudi', 'NT-B', 0),
(28, 2, 'Sikkalgar', 'NT-B', 0),
(29, 2, 'Madari', 'NT-B', 0),
(30, 2, 'Waghwale - Shah', 'NT-B', 0),
(31, 3, 'Christians', 'OBC', 0),
(32, 1, 'Maratha', 'OPEN', 0),
(33, 1, 'Hajam', 'SEBC', 0),
(34, 1, 'Andh', 'ST', 0),
(35, 1, 'Baiga', 'ST', 0),
(36, 1, 'Barda', 'ST', 0),
(37, 1, 'Bavacha, Bamcha', 'ST', 0),
(38, 1, 'Bhania', 'ST', 0),
(39, 6, 'Bhania', 'ST', 0),
(40, 4, 'Bharia Bhumia, Bhuinhar Bhumia, Pando', 'ST', 0),
(41, 1, 'Bhattra', 'ST', 0),
(42, 4, 'Bhattra', 'ST', 0),
(43, 1, 'Bhil, Bhil Garasia, Dholi Bhil, Dungri Bhil, Dungri Garasia, Mewasi Bhil, Rawal Bhil, Tadvi Bhil, Bhagalia, Bhilala Pawara, Vasava, Vasave', 'ST', 1),
(44, 2, 'Bhil, Bhil Garasia, Dholi Bhil, Dungri Bhil, Dungri Garasia, Mewasi Bhil, Rawal Bhil, Tadvi Bhil, Bhagalia, Bhilala Pawara, Vasava, Vasave', 'ST', 1),
(45, 3, 'Bhil, Bhil Garasia, Dholi Bhil, Dungri Bhil, Dungri Garasia, Mewasi Bhil, Rawal Bhil, Tadvi Bhil, Bhagalia, Bhilala Pawara, Vasava, Vasave', 'ST', 1),
(46, 1, 'Bhunjia', 'ST', 0),
(47, 1, 'Binjhwar', 'ST', 0),
(48, 1, 'Birhul, Birhor', 'ST', 0),
(49, 3, 'Birhul, Birhor', 'ST', 0),
(50, 4, 'Chodhara', 'ST', 0),
(51, 1, 'Dhanka, Tadvi, Tetaria , Valvi', 'ST', 0),
(52, 1, 'Dhanwar', 'ST', 0),
(53, 2, 'Dhanwar', 'ST', 0),
(54, 3, 'Dhanwar', 'ST', 0),
(55, 4, 'Dhanwar', 'ST', 0),
(56, 5, 'Dhanwar', 'ST', 0),
(57, 6, 'Dhanwar', 'ST', 0),
(58, 1, 'Dhodia', 'ST', 0),
(59, 1, 'Dubla, Talavia, Halpati', 'ST', 0),
(60, 4, 'Gamit, Gamta, Gavit, Mavchi, Padvi', 'ST', 0),
(61, 1, 'Halba, Halbi', 'ST', 0),
(62, 1, 'Kamar', 'ST', 0),
(63, 1, 'Kathodi,  Katkari, Dhor Kathodi, Dhor Kathkari Son Kathodi, Son Katkar', 'ST', 0),
(64, 3, 'Kathodi, Katkari, Dhor Kathodi, Dhor Kathkari Son Kathodi, Son Katkar', 'ST', 0),
(65, 1, 'Kawar, Kanwar, Kaur, Cherwa, Rathia, Tanwar, Chattri', 'ST', 0),
(66, 3, 'Kawar, Kanwar, Kaur, Cherwa, Rathia, Tanwar, Chattri', 'ST', 0),
(67, 1, 'Khairwar', 'ST', 0),
(68, 3, 'Khairwar', 'ST', 0),
(69, 1, 'Kharia', 'ST', 0),
(70, 1, 'Kokna, Kokni, Kukna', 'ST', 0),
(71, 1, 'Kol', 'ST', 0),
(72, 1, 'Koli Dhor, Tokre Koli, Kolcha, Kolkha', 'ST', 0),
(73, 1, 'Koli Mahadev, Dongar Koli', 'ST', 0),
(74, 1, 'Koli Malhar', 'ST', 0),
(75, 1, 'Kondh, Khond, Kandh', 'ST', 0),
(76, 3, 'Kondh, Khond, Kandh', 'ST', 0),
(77, 3, 'Koya, Bhine Koya, Rajkoya', 'ST', 0),
(78, 5, 'Nagesia, Nagasia', 'ST', 0),
(79, 1, 'Pardhan, Pathari, Saroti', 'ST', 0),
(80, 1, 'Parja', 'ST', 0),
(81, 1, 'Rathwa', 'ST', 0),
(82, 1, 'Thakur, Thakar, Ka Thakur, Ka Thakar, Ma Thakur, Ma Thakar', 'ST', 0),
(83, 1, 'Thoti', 'ST', 0),
(84, 1, 'Varli', 'ST', 0),
(85, 7, 'hydf', 'VJ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manage_course`
--

CREATE TABLE `manage_course` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `courses_year` varchar(1000) NOT NULL,
  `total_seats` int(100) NOT NULL,
  `academics_year` varchar(1000) NOT NULL,
  `start_date` varchar(1000) NOT NULL,
  `end_date` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_course`
--

INSERT INTO `manage_course` (`course_id`, `course_name`, `courses_year`, `total_seats`, `academics_year`, `start_date`, `end_date`, `delete_bit`, `submitted_by`) VALUES
(17, 'B.Com', 'First Year', 460, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:56:32'),
(18, 'B.Com', 'Second Year', 360, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:56:39'),
(19, 'B.Com', 'Third Year', 360, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:56:44'),
(20, 'BA', 'First Year', 80, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:56:50'),
(21, 'BA', 'Second Year', 80, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:56:57'),
(22, 'BA', 'Third Year', 80, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:57:03'),
(23, 'BSC', 'First Year', 80, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:57:08'),
(24, 'BSC', 'Second Year', 80, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:57:15'),
(25, 'BSC', 'Third Year', 80, '2020-2021', '2020-06-29', '2020-10-29', 0, '2020-09-10 07:57:20');

-- --------------------------------------------------------

--
-- Table structure for table `manage_course_fees`
--

CREATE TABLE `manage_course_fees` (
  `course_fee_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `academic_year` varchar(1000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `fees_type` varchar(1000) NOT NULL,
  `course_division` varchar(500) NOT NULL,
  `fees_sub_type` varchar(500) NOT NULL,
  `tution_fees` varchar(500) NOT NULL,
  `library_fees` varchar(500) NOT NULL,
  `activity_fees` varchar(500) NOT NULL,
  `gymkhana_fees` varchar(500) NOT NULL,
  `eligibility_fees` varchar(500) NOT NULL,
  `eligibility_form_fees` varchar(500) NOT NULL,
  `admission_fees` varchar(500) NOT NULL,
  `welfare_fees` varchar(500) NOT NULL,
  `computarization_fees` varchar(500) NOT NULL,
  `developement_fees` varchar(500) NOT NULL,
  `registration_fees` varchar(500) NOT NULL,
  `pro_rata_fees` varchar(500) NOT NULL,
  `disas_mgmt_fees` varchar(500) NOT NULL,
  `library_deposite_fees` varchar(500) NOT NULL,
  `caution_money_fees` varchar(500) NOT NULL,
  `medical_exam_fees` varchar(500) NOT NULL,
  `practical_fees` varchar(500) NOT NULL,
  `laboratory_fees` varchar(500) NOT NULL,
  `siss_fees` varchar(500) NOT NULL,
  `personal_fees` varchar(500) NOT NULL,
  `short_term_fees` varchar(500) NOT NULL,
  `security_charges` varchar(500) NOT NULL,
  `adminitration_charges` varchar(500) NOT NULL,
  `seminar_fees` varchar(500) NOT NULL,
  `phy_edn_scheme` varchar(500) NOT NULL,
  `alumni_fees` varchar(500) NOT NULL,
  `college_dev_fees` varchar(500) NOT NULL,
  `termend_exam_fees` varchar(500) NOT NULL,
  `computer_lab_fees` varchar(500) NOT NULL,
  `workshop_fees` varchar(500) NOT NULL,
  `comp_internet_fees` varchar(11) NOT NULL,
  `corpus_fund` varchar(500) NOT NULL,
  `form_fees` varchar(500) NOT NULL,
  `nss_section_fees` varchar(500) NOT NULL,
  `comp_lab_maintenace_fee` varchar(500) NOT NULL,
  `add_on_course_fee` varchar(500) NOT NULL,
  `add_extra_fee` varchar(500) NOT NULL,
  `add_extra_new_fee` varchar(500) NOT NULL,
  `total_fees` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manage_form_fees`
--

CREATE TABLE `manage_form_fees` (
  `form_fee_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `academic_year` varchar(500) NOT NULL,
  `form_fees` varchar(500) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_form_fees`
--

INSERT INTO `manage_form_fees` (`form_fee_id`, `course_name`, `course_year`, `academic_year`, `form_fees`, `delete_bit`, `submitted_date`) VALUES
(1, 'BA', 'First Year', '2020-2021', '105', 0, '2020-09-10 08:06:47'),
(2, 'BA', 'Second Year', '2020-2021', '105', 0, '2020-09-10 08:06:52'),
(3, 'BA', 'Third Year', '2020-2021', '105', 0, '2020-09-10 08:06:56'),
(4, 'B.Com', 'First Year', '2020-2021', '105', 0, '2020-09-10 08:06:59'),
(5, 'B.Com', 'Second Year', '2020-2021', '105', 0, '2020-09-10 08:07:03'),
(6, 'B.Com', 'Third Year', '2020-2021', '105', 0, '2020-09-10 08:07:06'),
(7, 'BSC', 'First Year', '2020-2021', '105', 0, '2020-09-10 08:07:10'),
(8, 'BSC', 'Second Year', '2020-2021', '105', 0, '2020-09-10 08:07:13'),
(9, 'BSC', 'Third Year', '2020-2021', '105', 0, '2020-09-10 08:07:18');

-- --------------------------------------------------------

--
-- Table structure for table `manage_payment_installment`
--

CREATE TABLE `manage_payment_installment` (
  `payment_install_id` int(11) NOT NULL,
  `academic_year` varchar(500) NOT NULL,
  `course_name` varchar(500) NOT NULL,
  `course_year` varchar(500) NOT NULL,
  `fees_type` varchar(500) NOT NULL,
  `total_fee` varchar(500) NOT NULL,
  `first_installment` varchar(500) NOT NULL,
  `second_installment` varchar(500) NOT NULL,
  `third_installment` varchar(500) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manage_religion`
--

CREATE TABLE `manage_religion` (
  `religion_id` int(11) NOT NULL,
  `religion_name` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_religion`
--

INSERT INTO `manage_religion` (`religion_id`, `religion_name`, `delete_bit`) VALUES
(1, 'Hinduism', 0),
(2, 'Islam', 0),
(3, 'Christianity', 0),
(4, 'Sikhism', 0),
(5, 'Buddhism', 0),
(6, 'Jainism', 0),
(7, 'Zoroastrianism', 0),
(8, 'dssdsn', 1),
(9, 'dssdns', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manage_subject`
--

CREATE TABLE `manage_subject` (
  `subject_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `course_semester` varchar(500) NOT NULL,
  `subject_type` varchar(100) NOT NULL,
  `optional_sub_type` varchar(500) NOT NULL,
  `subject_name` varchar(2000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_subject`
--

INSERT INTO `manage_subject` (`subject_id`, `course_name`, `course_year`, `course_semester`, `subject_type`, `optional_sub_type`, `subject_name`, `delete_bit`, `submitted_by`) VALUES
(1, 'B.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Compulsory English', 0, '2020-07-17 07:38:40'),
(2, 'B.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Financial Accounting', 0, '2020-07-17 07:38:46'),
(3, 'B.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Business Economics', 0, '2020-07-17 07:38:53'),
(4, 'B.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Business Mathematics and Statistics', 0, '2020-07-17 07:39:01'),
(5, 'B.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Marketing and Salesmanship', 0, '2020-07-13 14:44:29'),
(6, 'B.Com', 'First Year', 'Semester I', 'Optional', 'Optional Group A', 'Organization Skill Development', 0, '2020-07-14 11:36:25'),
(7, 'B.Com', 'First Year', 'Semester I', 'Optional', 'Optional Group A', 'Banking and finance', 0, '2020-07-14 11:36:25'),
(8, 'B.Com', 'First Year', 'Semester I', 'Optional', 'Optional Group B', 'Additional English', 0, '2020-07-14 11:38:28'),
(9, 'B.Com', 'First Year', 'Semester I', 'Optional', 'Optional Group B', 'Hindi', 0, '2020-07-17 07:39:28'),
(10, 'B.Com', 'First Year', 'Semester I', 'Optional', 'Optional Group B', 'Sindhi', 0, '2020-07-17 07:39:38'),
(11, 'B.Com', 'First Year', 'Semester I', 'Value Added', '', 'Value Education', 0, '2020-07-13 14:51:04'),
(12, 'B.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Compulsory English', 0, '2020-07-17 07:39:54'),
(13, 'B.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Financial Accounting', 0, '2020-07-17 07:40:06'),
(14, 'B.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Business Economics', 0, '2020-07-17 07:40:17'),
(15, 'B.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Business Mathematics and Statistics', 0, '2020-07-17 07:40:27'),
(16, 'B.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Marketing  and Salesmanship', 0, '2020-07-13 14:54:47'),
(17, 'B.Com', 'First Year', 'Semester II', 'Optional', 'Optional Group A', 'Organization Skill Development', 0, '2020-07-14 11:36:25'),
(18, 'B.Com', 'First Year', 'Semester II', 'Optional', 'Optional Group A', 'Banking and finance', 0, '2020-07-14 11:36:25'),
(19, 'B.Com', 'First Year', 'Semester II', 'Optional', 'Optional Group B', 'Additional English', 0, '2020-07-14 11:38:29'),
(20, 'B.Com', 'First Year', 'Semester II', 'Optional', 'Optional Group B', 'Hindi', 0, '2020-07-17 07:40:39'),
(21, 'B.Com', 'First Year', 'Semester II', 'Optional', 'Optional Group B', 'Sindhi', 0, '2020-07-17 07:40:50'),
(22, 'B.Com', 'First Year', 'Semester II', 'Value Added', '', 'Employability Skill Enhancement Programme', 0, '2020-07-13 15:00:35'),
(23, 'B.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Business Communication', 0, '2020-07-17 07:47:06'),
(24, 'B.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Corporate Accounting', 0, '2020-07-17 07:47:28'),
(25, 'B.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Business Economics(MACRO)', 0, '2020-07-14 04:14:02'),
(26, 'B.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Business Management', 0, '2020-07-17 07:47:42'),
(27, 'B.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Elements of Company Law', 0, '2020-07-14 04:14:49'),
(28, 'B.Com', 'Second Year', 'Semester III', 'Optional', '', 'Cost and Works Accounting (BASICS OF COST ACCOUNTING)', 0, '2020-07-17 07:48:31'),
(29, 'B.Com', 'Second Year', 'Semester III', 'Optional', '', 'Banking and Finance (Indian Banking System - I)', 0, '2020-07-17 07:48:44'),
(30, 'B.Com', 'Second Year', 'Semester III', 'Optional', '', 'Business Entrepreneurship', 0, '2020-07-14 04:16:23'),
(31, 'B.Com', 'Second Year', 'Semester III', 'Optional', '', 'Marketing Management', 0, '2020-07-14 04:16:36'),
(32, 'B.Com', 'Second Year', 'Semester III', 'Value Added', '', 'EVS-Environmental Awareness', 0, '2020-07-14 04:17:48'),
(33, 'B.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Business Communication', 0, '2020-07-17 07:48:56'),
(34, 'B.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Corporate Accounting', 0, '2020-07-17 07:49:10'),
(35, 'B.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Business Economics(MACRO)', 0, '2020-07-14 04:20:08'),
(36, 'B.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Business Management', 0, '2020-07-17 07:49:39'),
(37, 'B.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Elements of Company Law', 0, '2020-07-14 04:20:46'),
(38, 'B.Com', 'Second Year', 'Semester IV', 'Optional', '', 'Cost and Works Accounting (BASICS OF COST ACCOUNTING)', 0, '2020-07-17 07:49:54'),
(39, 'B.Com', 'Second Year', 'Semester IV', 'Optional', '', 'Banking and Finance (Indian Banking System - I)', 0, '2020-07-17 07:49:27'),
(40, 'B.Com', 'Second Year', 'Semester IV', 'Optional', '', 'Business Entrepreneurship', 0, '2020-07-14 04:21:38'),
(41, 'B.Com', 'Second Year', 'Semester IV', 'Optional', '', 'Marketing Management', 0, '2020-07-14 04:21:48'),
(42, 'B.Com', 'Second Year', 'Semester IV', 'Value Added', '', 'EVS-Environmental Awareness', 0, '2020-07-14 04:22:23'),
(43, 'B.Com', 'Third Year', 'Semester V', 'Compulsory', '', 'Business Regulatory Framework (Mercantile Law)', 0, '2020-07-14 04:23:02'),
(44, 'B.Com', 'Third Year', 'Semester V', 'Compulsory', '', 'Advanced Accounting.', 0, '2020-07-14 04:23:12'),
(45, 'B.Com', 'Third Year', 'Semester V', 'Compulsory', '', 'Indian and Global Economic Development', 0, '2020-07-14 04:23:31'),
(46, 'B.Com', 'Third Year', 'Semester V', 'Compulsory', '', 'Auditing and Taxation', 0, '2020-07-14 04:23:46'),
(47, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group A', 'Cost and Works Accounting', 0, '2020-07-17 07:50:09'),
(48, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group A', 'Banking and Finance', 0, '2020-07-17 07:50:28'),
(49, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group A', 'Business Entrepreneurship', 0, '2020-07-17 08:47:08'),
(50, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group A', 'Marketing Management', 0, '2020-07-17 08:47:21'),
(51, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group B', 'Cost and Works Accounting', 0, '2020-07-17 08:47:33'),
(52, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group B', 'Banking and Finance', 0, '2020-07-17 08:47:49'),
(53, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group B', 'Business Entrepreneurship', 0, '2020-07-17 08:48:04'),
(54, 'B.Com', 'Third Year', 'Semester V', 'Optional', 'Optional Group B', 'Marketing Management', 0, '2020-07-17 08:48:15'),
(55, 'BA', 'First Year', 'Semester I', 'Compulsory', '', 'Principles of Management', 0, '2020-10-14 04:59:27'),
(56, 'BA', 'First Year', 'Semester I', 'Compulsory', '', 'Business Communication Skills', 0, '2020-10-14 04:59:57'),
(57, 'BA', 'First Year', 'Semester I', 'Compulsory', '', 'Business Accounting', 0, '2020-10-14 05:00:04'),
(58, 'BA', 'First Year', 'Semester I', 'Compulsory', '', 'Business Economics (Micro)', 0, '2020-10-14 05:00:09'),
(59, 'BA', 'First Year', 'Semester I', 'Compulsory', '', 'Business Maths', 0, '2020-10-14 05:00:16'),
(60, 'BA', 'First Year', 'Semester I', 'Compulsory', '', 'Business Demography', 0, '2020-10-14 05:00:21'),
(61, 'BA', 'First Year', 'Semester I', 'Value Added', '', 'Comunication Skills for Managers', 0, '2020-10-14 05:00:27'),
(62, 'BA', 'First Year', 'Semester II', 'Compulsory', '', 'Business Organisation and Systems', 0, '2020-10-14 05:00:33'),
(63, 'BA', 'First Year', 'Semester II', 'Compulsory', '', 'Principles of Marketing', 0, '2020-10-14 05:00:40'),
(64, 'BA', 'First Year', 'Semester II', 'Compulsory', '', 'Principles of Finance', 0, '2020-10-14 05:00:46'),
(65, 'BA', 'First Year', 'Semester II', 'Compulsory', '', 'Basics of Cost Accounting', 0, '2020-10-14 05:00:52'),
(66, 'BA', 'First Year', 'Semester II', 'Compulsory', '', 'Business Statistics', 0, '2020-10-14 05:00:59'),
(67, 'BA', 'First Year', 'Semester II', 'Compulsory', '', 'Fundamentals of Computers', 0, '2020-10-14 05:01:05'),
(68, 'BA', 'First Year', 'Semester II', 'Value Added', '', 'Personality and Skills Development of Managers', 0, '2020-10-14 05:01:11'),
(69, 'BA', 'Second Year', 'Semester III', 'Compulsory', '', 'principles of human resource management', 0, '2020-10-14 05:05:14'),
(70, 'BA', 'Second Year', 'Semester III', 'Compulsory', '', 'Supply chain management', 0, '2020-10-14 05:05:14'),
(71, 'BA', 'Second Year', 'Semester III', 'Compulsory', '', 'Global competencies and personality development', 0, '2020-10-14 05:05:14'),
(72, 'BA', 'Second Year', 'Semester III', 'Compulsory', '', 'Fundamentals of rural development', 0, '2020-10-14 05:05:14'),
(73, 'BA', 'Second Year', 'Semester III', 'Optional', 'Marketing', 'Consumer Behaviour and Sales Management', 0, '2020-10-14 05:05:14'),
(74, 'BA', 'Second Year', 'Semester III', 'Optional', 'Marketing', 'Rretail Management', 0, '2020-10-14 05:05:14'),
(75, 'BA', 'Second Year', 'Semester III', 'Optional', 'Finance', 'Management Accounting', 0, '2020-10-14 05:05:14'),
(76, 'BA', 'Second Year', 'Semester III', 'Optional', 'Finance', 'Banking and Finance', 0, '2020-10-14 05:05:14'),
(77, 'BA', 'Second Year', 'Semester III', 'Value Added', '', 'Basic Course in Environmental Awareness', 0, '2020-10-14 05:05:14'),
(78, 'BA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Entrepreneurship and Small Business Management', 0, '2020-10-14 05:05:14'),
(79, 'BA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Production and Operations Management', 0, '2020-10-14 05:05:14'),
(80, 'BA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Decision Making and Risk Management', 0, '2020-10-14 05:05:14'),
(81, 'BA', 'Second Year', 'Semester IV', 'Compulsory', '', 'International Business Management', 0, '2020-10-14 05:05:14'),
(82, 'BA', 'Second Year', 'Semester IV', 'Optional', 'Marketing', 'Advertising and promotion Management', 0, '2020-10-14 05:05:14'),
(83, 'BA', 'Second Year', 'Semester IV', 'Optional', 'Marketing', 'Digital marketing', 0, '2020-10-14 05:05:14'),
(84, 'BA', 'Second Year', 'Semester IV', 'Optional', 'Finance', 'Business taxation', 0, '2020-10-14 05:05:14'),
(85, 'BA', 'Second Year', 'Semester IV', 'Optional', 'Finance', 'Financial Services', 0, '2020-10-14 05:05:14'),
(86, 'BA', 'Second Year', 'Semester IV', 'Value Added', '', 'Not Decided Yet', 1, '2020-10-14 05:05:14'),
(87, 'BA', 'Third Year', 'Semester V', 'Compulsory', '', 'Supply Chain and Logistics Management', 0, '2020-10-14 05:05:14'),
(88, 'BA', 'Third Year', 'Semester V', 'Compulsory', '', 'Entrepreneurship Development', 0, '2020-10-14 05:05:14'),
(89, 'BA', 'Third Year', 'Semester V', 'Compulsory', '', 'Business Law', 0, '2020-10-14 05:05:14'),
(90, 'BA', 'Third Year', 'Semester V', 'Compulsory', '', 'Research Methodology ( Tools and Analysis)', 0, '2020-10-14 05:05:14'),
(91, 'BA', 'Third Year', 'Semester V', 'Optional', 'Marketing', 'Sales Management', 0, '2020-10-14 05:05:14'),
(92, 'BA', 'Third Year', 'Semester V', 'Optional', 'Marketing', 'Retail Management', 0, '2020-10-14 05:05:14'),
(93, 'BA', 'Third Year', 'Semester V', 'Optional', 'Finance', 'Analysis and Interpretation of Financial Statements', 0, '2020-10-14 05:05:14'),
(94, 'BA', 'Third Year', 'Semester V', 'Optional', 'Finance', 'Long Term Finance', 0, '2020-10-14 05:05:14'),
(95, 'BA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Business Planning and Project Management', 0, '2020-10-14 05:05:14'),
(96, 'BA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Event Management', 0, '2020-10-14 05:05:14'),
(97, 'BA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Management Control System', 0, '2020-10-14 05:05:14'),
(98, 'BA', 'Third Year', 'Semester VI', 'Compulsory', '', 'E-Commerce', 0, '2020-10-14 05:05:14'),
(99, 'BA', 'Third Year', 'Semester VI', 'Optional', 'Marketing', 'Advertising and Sales Promotion', 0, '2020-10-14 05:05:14'),
(100, 'BA', 'Third Year', 'Semester VI', 'Optional', 'Marketing', 'Project/ Cases in Marketing', 0, '2020-10-14 05:05:14'),
(101, 'BA', 'Third Year', 'Semester VI', 'Optional', 'Finance', 'Financial Services', 0, '2020-10-14 05:05:14'),
(102, 'BA', 'Third Year', 'Semester VI', 'Optional', 'Finance', 'Project/ Cases in Finance', 0, '2020-10-14 05:05:14'),
(103, 'BSC', 'First Year', 'Semester I', 'Compulsory', '', 'Business Communication', 0, '2020-10-14 05:10:12'),
(104, 'BSC', 'First Year', 'Semester I', 'Compulsory', '', 'Principles of Management', 0, '2020-10-14 05:10:12'),
(105, 'BSC', 'First Year', 'Semester I', 'Compulsory', '', 'C Language', 0, '2020-10-14 05:10:12'),
(106, 'BSC', 'First Year', 'Semester I', 'Compulsory', '', 'Database Management System', 0, '2020-10-14 05:10:12'),
(107, 'BSC', 'First Year', 'Semester I', 'Compulsory', '', 'Statistics', 0, '2020-10-14 05:10:12'),
(108, 'BSC', 'First Year', 'Semester I', 'Compulsory', '', 'Computer Laboratory (Based on C and DBMS)', 0, '2020-10-14 05:10:12'),
(109, 'BSC', 'First Year', 'Semester I', 'Value Added', '', 'PPA', 0, '2020-10-14 05:10:12'),
(110, 'BSC', 'First Year', 'Semester II', 'Compulsory', '', 'Organization Behavior and Human Resource Management', 0, '2020-10-14 05:10:12'),
(111, 'BSC', 'First Year', 'Semester II', 'Compulsory', '', 'Financial Accounting', 0, '2020-10-14 05:10:12'),
(112, 'BSC', 'First Year', 'Semester II', 'Compulsory', '', 'Business Mathematics', 0, '2020-10-14 05:10:12'),
(113, 'BSC', 'First Year', 'Semester II', 'Compulsory', '', 'Relational Database', 0, '2020-10-14 05:10:12'),
(114, 'BSC', 'First Year', 'Semester II', 'Compulsory', '', 'Web Technology', 0, '2020-10-14 05:10:12'),
(115, 'BSC', 'First Year', 'Semester II', 'Compulsory', '', 'Computer Laboratory (Based on Relational Database and Web Technology)', 0, '2020-10-14 05:10:12'),
(116, 'BSC', 'First Year', 'Semester II', 'Value Added', '', 'Advance C', 0, '2020-10-14 05:10:12'),
(117, 'BSC', 'Second Year', 'Semester III', 'Compulsory', '', 'Digital Marketing', 0, '2020-10-14 05:10:12'),
(118, 'BSC', 'Second Year', 'Semester III', 'Compulsory', '', 'Data Structure', 0, '2020-10-14 05:10:12'),
(119, 'BSC', 'Second Year', 'Semester III', 'Compulsory', '', 'Software Engineering', 0, '2020-10-14 05:10:12'),
(120, 'BSC', 'Second Year', 'Semester III', 'Compulsory', '', 'Computer Laboratory (Based on DS/Angular JS/PHP/Block Chain)', 0, '2020-10-14 05:10:12'),
(121, 'BSC', 'Second Year', 'Semester III', 'Value Added', '', 'Environmental Awareness', 0, '2020-10-14 05:10:12'),
(122, 'BSC', 'Second Year', 'Semester III', 'Optional', '', 'Angular JS or PHP', 0, '2020-10-14 05:10:12'),
(123, 'BSC', 'Second Year', 'Semester III', 'Optional', '', 'Big Data or Block Chain', 0, '2020-10-14 05:10:12'),
(124, 'BSC', 'Second Year', 'Semester IV', 'Compulsory', '', 'Networking', 0, '2020-10-14 05:10:12'),
(125, 'BSC', 'Second Year', 'Semester IV', 'Compulsory', '', 'Object Oriented Concepts Through CPP', 0, '2020-10-14 05:10:12'),
(126, 'BSC', 'Second Year', 'Semester IV', 'Compulsory', '', 'Operating System', 0, '2020-10-14 05:10:12'),
(127, 'BSC', 'Second Year', 'Semester IV', 'Compulsory', '', 'Project', 0, '2020-10-14 05:10:12'),
(128, 'BSC', 'Second Year', 'Semester IV', 'Compulsory', '', 'Computer Laboratory (Based on CPP/NODE JS/Adv.PHP/Hadoop)', 0, '2020-10-14 05:10:12'),
(129, 'BSC', 'Second Year', 'Semester IV', 'Optional', '', 'NODE JS or Advance PHP or Hadoop', 0, '2020-10-14 05:10:12'),
(135, 'BSC', 'Third Year', 'Semester V', 'Value Added', '', 'IOT', 0, '2020-10-14 05:10:12'),
(142, 'BSC', 'Third Year', 'Semester VI', 'Value Added', '', 'Soft Skills Training', 0, '2020-10-14 05:10:12'),
(144, 'M.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Management Accounting', 0, '2020-07-14 04:59:27'),
(145, 'M.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Strategic Management', 0, '2020-07-14 04:59:36'),
(146, 'M.Com', 'First Year', 'Semester I', 'Optional', 'Advanced Cost Accounting and Cost system', 'Advanced Cost Accounting', 0, '2020-07-14 05:03:51'),
(147, 'M.Com', 'First Year', 'Semester I', 'Optional', 'Advanced Cost Accounting and Cost system', 'Costing Technique and Responsibility Accounting', 0, '2020-07-14 05:04:04'),
(148, 'M.Com', 'First Year', 'Semester I', 'Optional', 'Business Administration', 'Production and Operation Management', 0, '2020-07-14 05:00:28'),
(149, 'M.Com', 'First Year', 'Semester I', 'Optional', 'Business Administration', 'Financial Management', 0, '2020-07-14 05:04:49'),
(150, 'M.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Financial Analysis and Control', 0, '2020-07-14 05:01:00'),
(151, 'M.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Industrial Economics', 0, '2020-07-14 05:01:14'),
(152, 'M.Com', 'First Year', 'Semester II', 'Optional', 'Advanced Cost Accounting and Cost system', 'Application Cost Accounting', 0, '2020-07-14 05:04:12'),
(153, 'M.Com', 'First Year', 'Semester II', 'Optional', 'Advanced Cost Accounting and Cost system', 'Cost Control and Cost System', 0, '2020-07-14 05:04:22'),
(154, 'M.Com', 'First Year', 'Semester II', 'Optional', 'Business Administration', 'Business Ethics and Professional Values', 0, '2020-07-14 05:02:44'),
(155, 'M.Com', 'First Year', 'Semester II', 'Optional', 'Business Administration', 'Elements of Knowledge Management', 0, '2020-07-14 05:02:57'),
(156, 'M.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Business Finance', 0, '2020-07-14 05:06:02'),
(157, 'M.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Research Methodology for Business', 0, '2020-07-14 05:06:12'),
(158, 'M.Com', 'Second Year', 'Semester III', 'Optional', 'Advanced Cost Accounting and Cost system', 'Cost Audit', 0, '2020-07-14 05:06:29'),
(159, 'M.Com', 'Second Year', 'Semester III', 'Optional', 'Advanced Cost Accounting and Cost system', 'Management Audit', 0, '2020-07-14 05:06:41'),
(160, 'M.Com', 'Second Year', 'Semester III', 'Optional', 'Business Administration', 'Human Resource Management', 0, '2020-07-14 05:07:01'),
(161, 'M.Com', 'Second Year', 'Semester III', 'Optional', 'Business Administration', 'Organisational Behaviour', 0, '2020-07-14 05:07:12'),
(162, 'M.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Capital Market and Financial Services', 0, '2020-07-14 05:07:32'),
(163, 'M.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Industrial Economic Environment', 0, '2020-07-14 05:07:44'),
(164, 'M.Com', 'Second Year', 'Semester IV', 'Optional', 'Advanced Cost Accounting and Cost system', 'Recent Advances in Cost Auditing and Cost System', 0, '2020-07-14 05:08:02'),
(165, 'M.Com', 'Second Year', 'Semester IV', 'Optional', 'Advanced Cost Accounting and Cost system', 'Project Work/Case Studies', 0, '2020-07-14 05:08:13'),
(166, 'M.Com', 'Second Year', 'Semester IV', 'Optional', 'Business Administration', 'Recent Advances in Business Administration', 0, '2020-07-14 05:08:24'),
(167, 'M.Com', 'Second Year', 'Semester IV', 'Optional', 'Business Administration', 'Project Work/ Case Studies', 0, '2020-07-15 06:58:45'),
(168, 'BBA', 'First Year', 'Semester I', 'Compulsory', '', 'Business Organization and System', 1, '2020-07-15 09:28:53'),
(169, 'M.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Human Rights I', 0, '2020-07-18 06:40:35'),
(170, 'M.Com', 'First Year', 'Semester I', 'Compulsory', '', 'Introduction to Cyber Security I', 0, '2020-07-18 06:41:15'),
(171, 'M.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Human Rights II', 0, '2020-07-18 06:41:35'),
(172, 'M.Com', 'First Year', 'Semester II', 'Compulsory', '', 'Introduction to Cyber Security II', 0, '2020-07-18 06:42:03'),
(173, 'M.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Skill Development I - Shop Floor Officer - Retail', 0, '2020-07-18 06:45:06'),
(174, 'M.Com', 'Second Year', 'Semester III', 'Compulsory', '', 'Introduction to Cyber Security III', 0, '2020-07-18 06:45:30'),
(175, 'M.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Skill Development II - Shop Floor Officer -Retail', 0, '2020-07-18 06:46:19'),
(176, 'M.Com', 'Second Year', 'Semester IV', 'Compulsory', '', 'Introduction to Cyber Security IV', 0, '2020-07-18 06:46:40'),
(177, 'BSC', 'Third Year', 'Semester V', 'Compulsory', '', 'Core Java', 0, '2020-10-14 05:10:12'),
(178, 'BSC', 'Third Year', 'Semester V', 'Compulsory', '', 'Web Technology', 0, '2020-10-14 05:10:12'),
(179, 'BSC', 'Third Year', 'Semester V', 'Compulsory', '', 'Dotnet Programming', 0, '2020-10-14 05:10:12'),
(180, 'BSC', 'Third Year', 'Semester V', 'Compulsory', '', 'Object Oriented Software Engineering', 0, '2020-10-14 05:10:12'),
(181, 'BSC', 'Third Year', 'Semester V', 'Compulsory', '', 'Software Project', 0, '2020-10-14 05:10:12'),
(182, 'BSC', 'Third Year', 'Semester V', 'Compulsory', '', 'Lab Practical', 0, '2020-10-14 05:10:12'),
(183, 'BSC', 'Third Year', 'Semester VI', 'Compulsory', '', 'Advance Web Technology', 0, '2020-10-14 05:10:12'),
(184, 'BSC', 'Third Year', 'Semester VI', 'Compulsory', '', 'Advance Java', 0, '2020-10-14 05:10:12'),
(185, 'BSC', 'Third Year', 'Semester VI', 'Compulsory', '', 'Recent Trends in IT', 0, '2020-10-14 05:10:12'),
(186, 'BSC', 'Third Year', 'Semester VI', 'Compulsory', '', 'Software Testing', 0, '2020-10-14 05:10:12'),
(187, 'BSC', 'Third Year', 'Semester VI', 'Compulsory', '', 'Software Project', 0, '2020-10-14 05:10:12'),
(188, 'BSC', 'Third Year', 'Semester VI', 'Compulsory', '', 'Lab Practical', 0, '2020-10-14 05:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `student_admission_form_academic_info`
--

CREATE TABLE `student_admission_form_academic_info` (
  `student_academic_info_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `optinal_subject_selected` varchar(3000) NOT NULL,
  `optinal_subject_selected_sem2` varchar(2000) NOT NULL,
  `value_added_subject_selected` varchar(3000) NOT NULL,
  `value_added_subject_selected_sem2` varchar(2000) NOT NULL,
  `is_maharashtra` varchar(500) NOT NULL,
  `ssc_exam_name` varchar(1000) NOT NULL,
  `ssc_board_university_name` varchar(1000) NOT NULL,
  `ssc_school_clg_name` varchar(5000) NOT NULL,
  `ssc_year_of_passing` varchar(500) NOT NULL,
  `ssc_seat_number` varchar(1000) NOT NULL,
  `ssc_marks_obtained` varchar(500) NOT NULL,
  `ssc_total_marks` varchar(500) NOT NULL,
  `ssc_percentage` varchar(500) NOT NULL,
  `ssc_uploaded_documents` varchar(5000) NOT NULL,
  `hsc_exam_name` varchar(1000) NOT NULL,
  `hsc_board_university_name` varchar(1000) NOT NULL,
  `hsc_school_clg_name` varchar(1000) NOT NULL,
  `hsc_year_of_passing` varchar(1000) NOT NULL,
  `hsc_seat_number` varchar(1000) NOT NULL,
  `hsc_marks_obtained` varchar(500) NOT NULL,
  `hsc_total_marks` varchar(500) NOT NULL,
  `hsc_percentage` varchar(500) NOT NULL,
  `hsc_uploaded_documents` varchar(1000) NOT NULL,
  `tybcom_exam_name` varchar(500) NOT NULL,
  `tybcom_board_university_name` varchar(300) NOT NULL,
  `tybcom_school_clg_name` varchar(1000) NOT NULL,
  `tybcom_year_of_passing` varchar(300) NOT NULL,
  `tybcom_seat_number` varchar(300) NOT NULL,
  `tybcom_marks_obtained` varchar(300) NOT NULL,
  `tybcom_total_marks` varchar(300) NOT NULL,
  `tybcom_percentage` varchar(300) NOT NULL,
  `tybcom_uploaded_documents` varchar(1000) NOT NULL,
  `fy_exam_name` varchar(1000) NOT NULL,
  `fy_board_university_name` varchar(1000) NOT NULL,
  `fy_school_clg_name` varchar(5000) NOT NULL,
  `fy_year_of_passing` varchar(500) NOT NULL,
  `fy_seat_number` varchar(500) NOT NULL,
  `fy_marks_obtained` varchar(500) NOT NULL,
  `fy_total_marks` varchar(500) NOT NULL,
  `fy_percentage` varchar(500) NOT NULL,
  `fy_uploaded_documents` varchar(5000) NOT NULL,
  `sy_exam_name` varchar(1000) NOT NULL,
  `sy_board_university_name` varchar(1000) NOT NULL,
  `sy_school_clg_name` varchar(1000) NOT NULL,
  `sy_year_of_passing` varchar(500) NOT NULL,
  `sy_seat_number` varchar(500) NOT NULL,
  `sy_marks_obtained` varchar(500) NOT NULL,
  `sy_total_marks` varchar(500) NOT NULL,
  `sy_percentage` varchar(500) NOT NULL,
  `sy_uploaded_documents` varchar(1000) NOT NULL,
  `student_repeater_year` varchar(500) NOT NULL,
  `is_gap_reason` varchar(500) NOT NULL,
  `gap_reason` varchar(5000) DEFAULT NULL,
  `is_applied_other_clg` varchar(500) NOT NULL,
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_admission_form_academic_info`
--

INSERT INTO `student_admission_form_academic_info` (`student_academic_info_id`, `student_id`, `optinal_subject_selected`, `optinal_subject_selected_sem2`, `value_added_subject_selected`, `value_added_subject_selected_sem2`, `is_maharashtra`, `ssc_exam_name`, `ssc_board_university_name`, `ssc_school_clg_name`, `ssc_year_of_passing`, `ssc_seat_number`, `ssc_marks_obtained`, `ssc_total_marks`, `ssc_percentage`, `ssc_uploaded_documents`, `hsc_exam_name`, `hsc_board_university_name`, `hsc_school_clg_name`, `hsc_year_of_passing`, `hsc_seat_number`, `hsc_marks_obtained`, `hsc_total_marks`, `hsc_percentage`, `hsc_uploaded_documents`, `tybcom_exam_name`, `tybcom_board_university_name`, `tybcom_school_clg_name`, `tybcom_year_of_passing`, `tybcom_seat_number`, `tybcom_marks_obtained`, `tybcom_total_marks`, `tybcom_percentage`, `tybcom_uploaded_documents`, `fy_exam_name`, `fy_board_university_name`, `fy_school_clg_name`, `fy_year_of_passing`, `fy_seat_number`, `fy_marks_obtained`, `fy_total_marks`, `fy_percentage`, `fy_uploaded_documents`, `sy_exam_name`, `sy_board_university_name`, `sy_school_clg_name`, `sy_year_of_passing`, `sy_seat_number`, `sy_marks_obtained`, `sy_total_marks`, `sy_percentage`, `sy_uploaded_documents`, `student_repeater_year`, `is_gap_reason`, `gap_reason`, `is_applied_other_clg`, `submitted_by`) VALUES
(2, 1, 'Organization Skill Development,Additional English,Hindi,Sindhi', 'Banking and finance', '', '', 'Out of Maharashtra', 'SSC', 'Kerala', 'Shivaji HighSchool, satara', '2007', 'C085953', '400', '550', '72.73', 'uploads/Jyoti Patil/ssc.pdf', 'HSC', 'Maharashtra', 'shivaji college ,pune', '2006', 'H023234', '300', '500', '60.00', 'uploads/Jyoti Patil/HSC.pdf', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', 'gapReasonNo', '', 'otherCollegeNo', '2020-10-13 11:47:34'),
(3, 3, 'Organization Skill Development,Hindi', 'Banking and finance,Sindhi', '', '', 'Out of Maharashtra', 'SSC', 'Maharashtra', 'Shivaji HighSchool, satara', '2020', 'C085953', '495', '550', '90.00', 'uploads/Pranita Pawar/ssc.pdf', 'HSC', 'Maharashtra', 'shivaji college ,pune', '2020', 'H023234', '300', '500', '60.00', 'uploads/Pranita Pawar/HSC.pdf', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', 'gapReasonNo', '', 'otherCollegeNo', '2020-10-14 05:16:29'),
(4, 5, 'Organization Skill Development,Additional English,Hindi', 'Banking and finance', '', '', 'Out of Maharashtra', 'ssc', 'Maharashtra', 'Shivaji HighSchool, satara', '2017', 'C085958', '496', '780', '63.59', 'uploads/Komal Pawar/ssc.pdf', 'hsc', 'Maharashtra', 'shivaji college ,pune', '2019', 'H023237', '300', '500', '60.00', 'uploads/Komal Pawar/HSC.pdf', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', 'gapReasonNo', '', 'otherCollegeNo', '2020-10-15 10:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `student_admission_form_parent_info`
--

CREATE TABLE `student_admission_form_parent_info` (
  `student_parent_info_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `father_first_name` varchar(1000) NOT NULL,
  `father_middle_name` varchar(1000) NOT NULL,
  `father_last_name` varchar(1000) NOT NULL,
  `father_email_address` varchar(1000) NOT NULL,
  `father_mobile_number` varchar(1000) NOT NULL,
  `father_occupation` varchar(1000) NOT NULL,
  `is_father_central_govern_employee` varchar(500) NOT NULL,
  `father_annual_income` varchar(1000) NOT NULL,
  `is_father_economical_backward` varchar(500) NOT NULL,
  `mother_first_name` varchar(1000) NOT NULL,
  `mother_middle_name` varchar(1000) NOT NULL,
  `mother_last_name` varchar(1000) NOT NULL,
  `mother_email_address` varchar(1000) NOT NULL,
  `mother_mobile_number` varchar(1000) NOT NULL,
  `mother_occupation` varchar(1000) NOT NULL,
  `is_mother_central_govern_employee` varchar(1000) NOT NULL,
  `mother_annual_income` varchar(1000) NOT NULL,
  `is_mother_economical_backward` varchar(500) NOT NULL,
  `guardian_first_name` varchar(1000) NOT NULL,
  `guardian_middle_name` varchar(1000) NOT NULL,
  `guardian_last_name` varchar(1000) NOT NULL,
  `guardian_email_address` varchar(1000) NOT NULL,
  `guardian_mobile_number` varchar(1000) NOT NULL,
  `guardian_occupation` varchar(1000) NOT NULL,
  `is_guardian_central_govern_employee` varchar(500) NOT NULL,
  `guardian_annual_income` varchar(1000) NOT NULL,
  `is_guardian_economical_backward` varchar(1000) NOT NULL,
  `is_student_agree` varchar(500) NOT NULL,
  `student_admission_status` int(11) NOT NULL DEFAULT '1',
  `form_fee_submitted_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `course_fees_type` varchar(500) DEFAULT NULL,
  `first_installment_bit` varchar(50) NOT NULL DEFAULT '0',
  `second_installment_bit` varchar(50) NOT NULL DEFAULT '0',
  `third_installment_bit` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_admission_form_parent_info`
--

INSERT INTO `student_admission_form_parent_info` (`student_parent_info_id`, `student_id`, `father_first_name`, `father_middle_name`, `father_last_name`, `father_email_address`, `father_mobile_number`, `father_occupation`, `is_father_central_govern_employee`, `father_annual_income`, `is_father_economical_backward`, `mother_first_name`, `mother_middle_name`, `mother_last_name`, `mother_email_address`, `mother_mobile_number`, `mother_occupation`, `is_mother_central_govern_employee`, `mother_annual_income`, `is_mother_economical_backward`, `guardian_first_name`, `guardian_middle_name`, `guardian_last_name`, `guardian_email_address`, `guardian_mobile_number`, `guardian_occupation`, `is_guardian_central_govern_employee`, `guardian_annual_income`, `is_guardian_economical_backward`, `is_student_agree`, `student_admission_status`, `form_fee_submitted_bit`, `submitted_by`, `course_fees_type`, `first_installment_bit`, `second_installment_bit`, `third_installment_bit`) VALUES
(4, 1, 'Mohan', 'Santosh', 'Pawar', 'mohan@gmail.com', '7878787878', 'farmer', 'No', '0-1 Lakh', 'No', 'Kalpana', 'ghj', 'mbm', 'kalpana@gmail.com', '7898987878', 'House Wife', 'No', '0-1 Lakh', 'No', '', '', '', '', '', '', '', '', '', 'true', 3, 0, '2020-10-14 06:39:47', NULL, '0', '0', '0'),
(5, 3, 'Monah', 'santosh', 'Patil', 'pankaj@gmail.com', '7878787878', 'Farmer', 'No', '0-1 Lakh', 'No', 'Kalpana', 'Pankaj', 'Patil', 'kalpana@gmail.com', '9898767676', 'farmer', 'No', '1-2 Lakh', 'No', '', '', '', '', '', '', '', '', '', 'true', 2, 0, '2020-10-15 10:41:34', NULL, '0', '0', '0'),
(6, 5, 'Mohan', 'Santosn', 'Pawar', 'sunil@gmail.com', '7878787878', 'farmer', 'No', '0-1 Lakh', 'No', 'Kalpana', 'ghj', 'Patil', 'kalpana@gmail.com', '7898987879', 'House Wife', 'No', '0-1 Lakh', 'No', '', '', '', '', '', '', '', '', '', 'true', 1, 0, '2020-10-15 10:35:34', NULL, '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `student_admission_form_personal_info`
--

CREATE TABLE `student_admission_form_personal_info` (
  `student_personal_info_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `is_sindhi_category` varchar(500) NOT NULL,
  `is_jaihind_college` varchar(500) NOT NULL,
  `first_name` varchar(500) NOT NULL,
  `middle_name` varchar(500) NOT NULL,
  `last_name` varchar(500) NOT NULL,
  `mother_name` varchar(1000) NOT NULL,
  `email_address` varchar(1000) NOT NULL,
  `mobile_number` varchar(500) NOT NULL,
  `alter_mobile_number` varchar(500) NOT NULL,
  `adhar_card_id` varchar(1000) NOT NULL,
  `voter_id` varchar(1000) NOT NULL,
  `local_address` varchar(5000) NOT NULL,
  `correspondance_address` varchar(500) DEFAULT NULL,
  `guardian_address` varchar(500) DEFAULT NULL,
  `native_place` varchar(1000) NOT NULL,
  `blood_group` varchar(500) NOT NULL,
  `gender` varchar(500) NOT NULL,
  `marital_status` varchar(500) NOT NULL,
  `date_of_birth` varchar(500) NOT NULL,
  `dob_in_word` varchar(500) NOT NULL,
  `birth_place` varchar(500) NOT NULL,
  `district` varchar(500) NOT NULL,
  `state` varchar(500) NOT NULL,
  `nationality` varchar(500) NOT NULL,
  `passport` varchar(500) NOT NULL,
  `religion` varchar(500) NOT NULL,
  `caste` varchar(1000) NOT NULL,
  `category` varchar(1000) NOT NULL,
  `mother_tounge` varchar(500) NOT NULL,
  `is_cast_certificate` varchar(500) NOT NULL,
  `is_physically_handi` varchar(500) NOT NULL,
  `eligibility_number` varchar(500) NOT NULL,
  `roll_number` varchar(300) DEFAULT NULL,
  `game_played` varchar(2000) NOT NULL,
  `hobbies` varchar(5000) NOT NULL,
  `admission_type` varchar(500) NOT NULL,
  `regd_number` varchar(500) NOT NULL,
  `is_join_clg_NNS` varchar(500) NOT NULL,
  `is_participate_in_event` varchar(500) NOT NULL,
  `uploaded_photo` varchar(5000) NOT NULL,
  `uploaded_signature` varchar(5000) NOT NULL,
  `uploaded_caste_certificate` varchar(4000) NOT NULL,
  `uploaded_other_documents` varchar(4000) NOT NULL,
  `uploaded_physically_handi_certificate` varchar(1000) DEFAULT NULL,
  `uploaded_parent_signature` varchar(500) NOT NULL,
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_admission_form_personal_info`
--

INSERT INTO `student_admission_form_personal_info` (`student_personal_info_id`, `student_id`, `is_sindhi_category`, `is_jaihind_college`, `first_name`, `middle_name`, `last_name`, `mother_name`, `email_address`, `mobile_number`, `alter_mobile_number`, `adhar_card_id`, `voter_id`, `local_address`, `correspondance_address`, `guardian_address`, `native_place`, `blood_group`, `gender`, `marital_status`, `date_of_birth`, `dob_in_word`, `birth_place`, `district`, `state`, `nationality`, `passport`, `religion`, `caste`, `category`, `mother_tounge`, `is_cast_certificate`, `is_physically_handi`, `eligibility_number`, `roll_number`, `game_played`, `hobbies`, `admission_type`, `regd_number`, `is_join_clg_NNS`, `is_participate_in_event`, `uploaded_photo`, `uploaded_signature`, `uploaded_caste_certificate`, `uploaded_other_documents`, `uploaded_physically_handi_certificate`, `uploaded_parent_signature`, `submitted_by`) VALUES
(3, 1, '', '', 'Jyoti', 'Mohan', 'Patil', 'Kalpana', 'jyoti@gmail.com', '8989898989', '', '213232324324', '', 'Pune', 'Pune', 'Pune', 'Pune', 'AB+ve', 'Female', 'Unmarried', '07-05-2004', 'fkhsdj fjsdhf jsdhfjsd', 'fskdfh', 'Pune', 'Maharashtra', 'Indian', '', 'Hinduism', 'MARATHA', 'OPEN', 'Marathi', 'Yes', 'No', '87777777', 'b78888', '', '', 'Regular', 'cty8687', '', '', 'uploads/Jyoti Patil/StudentPhoto.jpg', 'uploads/Jyoti Patil/StudentSignature.jpg', 'uploads/Jyoti Patil/CasteCertificate.pdf', '', NULL, 'uploads/Jyoti Patil/studSignature.jpg', '2020-10-13 11:46:56'),
(4, 3, '', '', 'Pranita', 'Pradip', 'Pawar', 'Kalpana', 'sayali@gmail.com', '9867876787', '', '213232324324', '', 'Pune', 'Pune', 'Pune', 'Pune', 'A+ve', 'Female', 'Unmarried', '14-01-1994', '2nd jan nineteen ninety seven', 'Pune', 'Pune', 'Maharashtra', 'indian', '', '', '', '', 'Marathi', 'No', 'No', '', '', '', '', 'Regular', 'gdf', '', '', 'uploads/Pranita Pawar/StudentPhoto.jpg', 'uploads/Pranita Pawar/StudentSignature.jpg', '', '', NULL, 'uploads/Pranita Pawar/studSignature.jpg', '2020-10-14 04:56:07'),
(5, 5, '', '', 'Komal', 'Pradip', 'Pawar', 'Kalpana', 'kajal@gmail.com', '8976675675', '', '213232324324', '', 'Pune', 'Pune', 'Pune', 'Pune', 'A+ve', 'Female', 'Unmarried', '02-01-2000', 'second Jan two Thousand', 'Pune', 'Pune', 'Maharashtra', 'Indian', '', 'Hinduism', 'Maratha', 'OPEN', 'Marathi', 'No', 'No', '', '', '', '', 'Regular', 'D344', '', '', 'uploads/Komal Pawar/StudentPhoto.jpg', 'uploads/Komal Pawar/StudentSignature.jpg', '', '', NULL, 'uploads/Komal Pawar/studSignature.jpg', '2020-10-15 10:27:50'),
(6, 2, '', '', 'Jyoti', 'Pradip', 'Kadam', 'sdf', 'pp@gmail.com', '9856786786', '', '677777777777', '', 'dd', 'gdg', '', 'gdg', 'A+ve', 'Female', 'Unmarried', '01-02-2007', 'das', 'asd', 'dasd', 'Maharashtra', 'dsad', '', 'Hinduism', 'dsad', 'OPEN', 'da', 'No', 'No', '', '', '', '', 'Regular', '', '', '', 'uploads/Jyoti Kadam/img.png', 'uploads/Jyoti Kadam/images.png', '', '', NULL, 'uploads/Jyoti Kadam/images.png', '2021-07-20 07:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `student_course_fees_payment_details`
--

CREATE TABLE `student_course_fees_payment_details` (
  `student_payment_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `add_amount` varchar(1000) NOT NULL,
  `payment_recieved_date` varchar(500) NOT NULL,
  `comment` varchar(5000) DEFAULT NULL,
  `is_payment_recieved` int(11) NOT NULL DEFAULT '0',
  `is_payment_paid` int(11) NOT NULL DEFAULT '0',
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_registration`
--

CREATE TABLE `student_registration` (
  `student_id` int(11) NOT NULL,
  `stud_academic_year` varchar(1000) NOT NULL,
  `course_name` varchar(5000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `student_name` varchar(5000) NOT NULL,
  `student_email` varchar(5000) NOT NULL,
  `student_mobile` varchar(1000) NOT NULL,
  `student_password` varchar(5000) NOT NULL,
  `admission_step_one` varchar(10) NOT NULL DEFAULT '0',
  `admission_step_two` varchar(10) NOT NULL DEFAULT '0',
  `admission_step_three` varchar(10) NOT NULL DEFAULT '0',
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `registerd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_registration`
--

INSERT INTO `student_registration` (`student_id`, `stud_academic_year`, `course_name`, `course_year`, `student_name`, `student_email`, `student_mobile`, `student_password`, `admission_step_one`, `admission_step_two`, `admission_step_three`, `delete_bit`, `registerd_date`) VALUES
(1, '2020-2021', 'B.Com', 'First Year', 'Patil Jyoti Mohan', 'jyoti@gmail.com', '8989898989', 'qwerty', '1', '1', '1', 0, '2020-10-13 12:16:36'),
(2, '2020-2021', 'BA', 'First Year', 'Kadam Jyoti Pradip', 'pp@gmail.com', '9856786786', 'qwerty', '1', '0', '0', 0, '2021-07-20 07:36:10'),
(3, '2020-2021', 'B.Com', 'First Year', 'Pawar Pranita Pradip', 'sayali@gmail.com', '9867876787', 'qwerty', '1', '1', '1', 0, '2020-10-14 05:14:39'),
(4, '2020-2021', 'B.Com', 'First Year', 'Dange Jyoti Mohan', 'sonali@gmail.com', '9887878787', 'qwerty', '0', '0', '0', 0, '2021-07-20 07:32:30'),
(5, '2020-2021', 'B.Com', 'First Year', 'Pawar Komal Pradip', 'kajal@gmail.com', '8976675675', 'qwerty', '1', '1', '1', 0, '2020-10-15 10:35:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `log_id` int(11) NOT NULL,
  `log_user_id` varchar(500) NOT NULL,
  `user_ip_address` varchar(100) DEFAULT NULL,
  `user_devices_details` varchar(1000) DEFAULT NULL,
  `user_activity` varchar(5000) NOT NULL,
  `user_log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `user_id` int(11) NOT NULL,
  `user_name` longtext NOT NULL,
  `user_password` longtext NOT NULL,
  `user_email` varchar(500) DEFAULT NULL,
  `user_mobile` varchar(50) DEFAULT NULL,
  `user_role` varchar(1000) NOT NULL,
  `user_address` varchar(1000) DEFAULT NULL,
  `valid_till` varchar(500) DEFAULT NULL,
  `user_admin_approval_bit` tinyint(2) NOT NULL DEFAULT '0',
  `user_delete_bit` varchar(5) NOT NULL DEFAULT '0',
  `user_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_id`, `user_name`, `user_password`, `user_email`, `user_mobile`, `user_role`, `user_address`, `valid_till`, `user_admin_approval_bit`, `user_delete_bit`, `user_created_date`) VALUES
(1, 'Whitecode', 'eE4vVG5LcytYNlNsNTM0RTdsVXhzZz09', 'info@mucollege.org', '8380036301', 'admin', NULL, NULL, 1, '0', '2019-12-05 23:06:44'),
(2, 'whitecode1', 'YWtNNkliZTdBanlScnZOUUdyUGczdz09', 'info@whitecode.co.in', '8983369618', 'college', 'puneC/O Sungrace India 2nd floor, Left wing Survey #19,', '28-01-2021', 1, '0', '2020-01-26 18:41:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `manage_caste`
--
ALTER TABLE `manage_caste`
  ADD PRIMARY KEY (`caste_id`);

--
-- Indexes for table `manage_course`
--
ALTER TABLE `manage_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `manage_course_fees`
--
ALTER TABLE `manage_course_fees`
  ADD PRIMARY KEY (`course_fee_id`);

--
-- Indexes for table `manage_form_fees`
--
ALTER TABLE `manage_form_fees`
  ADD PRIMARY KEY (`form_fee_id`);

--
-- Indexes for table `manage_payment_installment`
--
ALTER TABLE `manage_payment_installment`
  ADD PRIMARY KEY (`payment_install_id`);

--
-- Indexes for table `manage_religion`
--
ALTER TABLE `manage_religion`
  ADD PRIMARY KEY (`religion_id`);

--
-- Indexes for table `manage_subject`
--
ALTER TABLE `manage_subject`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `student_admission_form_academic_info`
--
ALTER TABLE `student_admission_form_academic_info`
  ADD PRIMARY KEY (`student_academic_info_id`);

--
-- Indexes for table `student_admission_form_parent_info`
--
ALTER TABLE `student_admission_form_parent_info`
  ADD PRIMARY KEY (`student_parent_info_id`);

--
-- Indexes for table `student_admission_form_personal_info`
--
ALTER TABLE `student_admission_form_personal_info`
  ADD PRIMARY KEY (`student_personal_info_id`);

--
-- Indexes for table `student_course_fees_payment_details`
--
ALTER TABLE `student_course_fees_payment_details`
  ADD PRIMARY KEY (`student_payment_id`);

--
-- Indexes for table `student_registration`
--
ALTER TABLE `student_registration`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `manage_caste`
--
ALTER TABLE `manage_caste`
  MODIFY `caste_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `manage_course`
--
ALTER TABLE `manage_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `manage_course_fees`
--
ALTER TABLE `manage_course_fees`
  MODIFY `course_fee_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manage_form_fees`
--
ALTER TABLE `manage_form_fees`
  MODIFY `form_fee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `manage_payment_installment`
--
ALTER TABLE `manage_payment_installment`
  MODIFY `payment_install_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manage_religion`
--
ALTER TABLE `manage_religion`
  MODIFY `religion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `manage_subject`
--
ALTER TABLE `manage_subject`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT for table `student_admission_form_academic_info`
--
ALTER TABLE `student_admission_form_academic_info`
  MODIFY `student_academic_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student_admission_form_parent_info`
--
ALTER TABLE `student_admission_form_parent_info`
  MODIFY `student_parent_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_admission_form_personal_info`
--
ALTER TABLE `student_admission_form_personal_info`
  MODIFY `student_personal_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_course_fees_payment_details`
--
ALTER TABLE `student_course_fees_payment_details`
  MODIFY `student_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_registration`
--
ALTER TABLE `student_registration`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
