-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 03, 2021 at 02:22 AM
-- Server version: 5.7.23-23
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tsnegigc_ol_admission`
--

-- --------------------------------------------------------

--
-- Table structure for table `manage_caste`
--

CREATE TABLE `manage_caste` (
  `caste_id` int(11) NOT NULL,
  `religion_id` int(11) DEFAULT NULL,
  `caste_name` varchar(1000) NOT NULL,
  `sub_caste_name` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_caste`
--

INSERT INTO `manage_caste` (`caste_id`, `religion_id`, `caste_name`, `sub_caste_name`, `delete_bit`) VALUES
(1, 1, 'Agri', 'OPEN', 0),
(2, 1, 'Bhandari', 'OBC', 0),
(3, 1, 'Bhoi', 'OBC', 0),
(4, 1, 'Bhoi', 'NT-B', 0),
(5, 1, 'Dhangar', 'NT-C', 0),
(6, 1, 'Parit', 'OBC', 0),
(7, 1, 'Dhobi', 'OBC', 0),
(8, 1, 'Kasar', 'OBC', 0),
(9, 1, 'Leva Patil', 'OPEN', 0),
(10, 1, 'Lonari', 'OBC', 0),
(11, 1, 'Pachkalsi', 'OBC', 0),
(12, 1, 'Ramoshi', 'VJ', 0),
(13, 1, 'Vaishya wani', 'OBC', 0),
(14, 1, 'Vanjari', 'NT-D', 0),
(15, 5, 'Mahar', 'SC', 0),
(16, 1, 'Maratha - Kunbi', 'OBC', 0),
(17, 2, 'Bagwan', 'OBC', 0),
(18, 2, 'Bawarchi/ Bhatiyara', 'OBC', 0),
(19, 2, 'Bhandari', 'OBC', 0),
(20, 2, 'Tamboli', 'OBC', 0),
(21, 2, 'Muslim Madari', 'NT-B', 0),
(22, 2, 'Muslim Gavali', 'NT-B', 0),
(23, 2, 'Waghwale', 'NT-B', 0),
(24, 2, 'Takari', 'VJ', 0),
(25, 2, 'Chhapparband', 'VJ', 0),
(26, 2, 'Beldar', 'NT-B', 0),
(27, 2, 'Garudi', 'NT-B', 0),
(28, 2, 'Sikkalgar', 'NT-B', 0),
(29, 2, 'Madari', 'NT-B', 0),
(30, 2, 'Waghwale - Shah', 'NT-B', 0),
(31, 3, 'Christians', 'OBC', 0),
(32, 1, 'Maratha', 'OPEN', 0),
(33, 1, 'Hajam', 'SEBC', 0),
(34, 1, 'Andh', 'ST', 0),
(35, 1, 'Baiga', 'ST', 0),
(36, 1, 'Barda', 'ST', 0),
(37, 1, 'Bavacha, Bamcha', 'ST', 0),
(38, 1, 'Bhania', 'ST', 0),
(39, 6, 'Bhania', 'ST', 0),
(40, 4, 'Bharia Bhumia, Bhuinhar Bhumia, Pando', 'ST', 0),
(41, 1, 'Bhattra', 'ST', 0),
(42, 4, 'Bhattra', 'ST', 0),
(43, 1, 'Bhil, Bhil Garasia, Dholi Bhil, Dungri Bhil, Dungri Garasia, Mewasi Bhil, Rawal Bhil, Tadvi Bhil, Bhagalia, Bhilala Pawara, Vasava, Vasave', 'ST', 1),
(44, 2, 'Bhil, Bhil Garasia, Dholi Bhil, Dungri Bhil, Dungri Garasia, Mewasi Bhil, Rawal Bhil, Tadvi Bhil, Bhagalia, Bhilala Pawara, Vasava, Vasave', 'ST', 1),
(45, 3, 'Bhil, Bhil Garasia, Dholi Bhil, Dungri Bhil, Dungri Garasia, Mewasi Bhil, Rawal Bhil, Tadvi Bhil, Bhagalia, Bhilala Pawara, Vasava, Vasave', 'ST', 1),
(46, 1, 'Bhunjia', 'ST', 0),
(47, 1, 'Binjhwar', 'ST', 0),
(48, 1, 'Birhul, Birhor', 'ST', 0),
(49, 3, 'Birhul, Birhor', 'ST', 0),
(50, 4, 'Chodhara', 'ST', 0),
(51, 1, 'Dhanka, Tadvi, Tetaria , Valvi', 'ST', 0),
(52, 1, 'Dhanwar', 'ST', 0),
(53, 2, 'Dhanwar', 'ST', 0),
(54, 3, 'Dhanwar', 'ST', 0),
(55, 4, 'Dhanwar', 'ST', 0),
(56, 5, 'Dhanwar', 'ST', 0),
(57, 6, 'Dhanwar', 'ST', 0),
(58, 1, 'Dhodia', 'ST', 0),
(59, 1, 'Dubla, Talavia, Halpati', 'ST', 0),
(60, 4, 'Gamit, Gamta, Gavit, Mavchi, Padvi', 'ST', 0),
(61, 1, 'Halba, Halbi', 'ST', 0),
(62, 1, 'Kamar', 'ST', 0),
(63, 1, 'Kathodi,  Katkari, Dhor Kathodi, Dhor Kathkari Son Kathodi, Son Katkar', 'ST', 0),
(64, 3, 'Kathodi, Katkari, Dhor Kathodi, Dhor Kathkari Son Kathodi, Son Katkar', 'ST', 0),
(65, 1, 'Kawar, Kanwar, Kaur, Cherwa, Rathia, Tanwar, Chattri', 'ST', 0),
(66, 3, 'Kawar, Kanwar, Kaur, Cherwa, Rathia, Tanwar, Chattri', 'ST', 0),
(67, 1, 'Khairwar', 'ST', 0),
(68, 3, 'Khairwar', 'ST', 0),
(69, 1, 'Kharia', 'ST', 0),
(70, 1, 'Kokna, Kokni, Kukna', 'ST', 0),
(71, 1, 'Kol', 'ST', 0),
(72, 1, 'Koli Dhor, Tokre Koli, Kolcha, Kolkha', 'ST', 0),
(73, 1, 'Koli Mahadev, Dongar Koli', 'ST', 0),
(74, 1, 'Koli Malhar', 'ST', 0),
(75, 1, 'Kondh, Khond, Kandh', 'ST', 0),
(76, 3, 'Kondh, Khond, Kandh', 'ST', 0),
(77, 3, 'Koya, Bhine Koya, Rajkoya', 'ST', 0),
(78, 5, 'Nagesia, Nagasia', 'ST', 0),
(79, 1, 'Pardhan, Pathari, Saroti', 'ST', 0),
(80, 1, 'Parja', 'ST', 0),
(81, 1, 'Rathwa', 'ST', 0),
(82, 1, 'Thakur, Thakar, Ka Thakur, Ka Thakar, Ma Thakur, Ma Thakar', 'ST', 0),
(83, 1, 'Thoti', 'ST', 0),
(84, 1, 'Varli', 'ST', 0),
(85, 7, 'hydf', 'VJ', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manage_course`
--

CREATE TABLE `manage_course` (
  `course_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `courses_year` varchar(1000) NOT NULL,
  `total_seats` int(100) NOT NULL,
  `academics_year` varchar(1000) NOT NULL,
  `start_date` varchar(1000) NOT NULL,
  `end_date` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_course`
--

INSERT INTO `manage_course` (`course_id`, `course_name`, `courses_year`, `total_seats`, `academics_year`, `start_date`, `end_date`, `delete_bit`, `submitted_by`) VALUES
(1, 'B.A.', 'First Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:11:01'),
(2, 'B.A.', 'Second Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:13:04'),
(3, 'B.A.', 'Third Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:13:43'),
(4, 'B.SC', 'First Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:14:53'),
(5, 'B.SC', 'Second Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:15:34'),
(6, 'B.SC', 'Third Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:16:20'),
(7, 'B.COM', 'First Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:18:35'),
(8, 'B.COM', 'Second Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:19:03'),
(9, 'B.COM', 'Third Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:19:30'),
(10, 'BCA', 'First Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:21:47'),
(11, 'BCA', 'Second Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:22:11'),
(12, 'BCA', 'Third Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:22:38'),
(13, 'PGDCA', 'First Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:23:23'),
(14, 'PGDCA', 'Second Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:24:00'),
(15, 'PGDCA', 'Third Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:24:18'),
(16, 'M.A. In HISTORY', 'First Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:25:01'),
(17, 'M.A. In HISTORY', 'Second Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:25:22'),
(18, 'M.A. In HISTORY', 'Third Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-29 07:25:41'),
(19, 'M.A. IN POLIOTICAL SCIENCE', 'First Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-30 04:09:37'),
(20, 'M.A. IN POLIOTICAL SCIENCE', 'Second Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-30 04:10:17'),
(21, 'M.A. IN POLIOTICAL SCIENCE', 'Third Year', 500, '2021-2022', '2021-07-29', '2021-10-01', 0, '2021-07-30 04:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `manage_course_fees`
--

CREATE TABLE `manage_course_fees` (
  `course_fee_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `academic_year` varchar(1000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `fees_type` varchar(1000) NOT NULL,
  `course_division` varchar(500) NOT NULL,
  `fees_sub_type` varchar(500) NOT NULL,
  `tution_fees` varchar(500) NOT NULL,
  `library_fees` varchar(500) NOT NULL,
  `activity_fees` varchar(500) NOT NULL,
  `gymkhana_fees` varchar(500) NOT NULL,
  `eligibility_fees` varchar(500) NOT NULL,
  `eligibility_form_fees` varchar(500) NOT NULL,
  `admission_fees` varchar(500) NOT NULL,
  `welfare_fees` varchar(500) NOT NULL,
  `computarization_fees` varchar(500) NOT NULL,
  `developement_fees` varchar(500) NOT NULL,
  `registration_fees` varchar(500) NOT NULL,
  `pro_rata_fees` varchar(500) NOT NULL,
  `disas_mgmt_fees` varchar(500) NOT NULL,
  `library_deposite_fees` varchar(500) NOT NULL,
  `caution_money_fees` varchar(500) NOT NULL,
  `medical_exam_fees` varchar(500) NOT NULL,
  `practical_fees` varchar(500) NOT NULL,
  `laboratory_fees` varchar(500) NOT NULL,
  `siss_fees` varchar(500) NOT NULL,
  `personal_fees` varchar(500) NOT NULL,
  `short_term_fees` varchar(500) NOT NULL,
  `security_charges` varchar(500) NOT NULL,
  `adminitration_charges` varchar(500) NOT NULL,
  `seminar_fees` varchar(500) NOT NULL,
  `phy_edn_scheme` varchar(500) NOT NULL,
  `alumni_fees` varchar(500) NOT NULL,
  `college_dev_fees` varchar(500) NOT NULL,
  `termend_exam_fees` varchar(500) NOT NULL,
  `computer_lab_fees` varchar(500) NOT NULL,
  `workshop_fees` varchar(500) NOT NULL,
  `comp_internet_fees` varchar(11) NOT NULL,
  `corpus_fund` varchar(500) NOT NULL,
  `form_fees` varchar(500) NOT NULL,
  `nss_section_fees` varchar(500) NOT NULL,
  `comp_lab_maintenace_fee` varchar(500) NOT NULL,
  `add_on_course_fee` varchar(500) NOT NULL,
  `add_extra_fee` varchar(500) NOT NULL,
  `add_extra_new_fee` varchar(500) NOT NULL,
  `total_fees` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manage_form_fees`
--

CREATE TABLE `manage_form_fees` (
  `form_fee_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `academic_year` varchar(500) NOT NULL,
  `form_fees` varchar(500) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_form_fees`
--

INSERT INTO `manage_form_fees` (`form_fee_id`, `course_name`, `course_year`, `academic_year`, `form_fees`, `delete_bit`, `submitted_date`) VALUES
(1, 'B.A.', 'First Year', '2021-2022', '50', 0, '2021-07-29 07:28:34'),
(2, 'B.A.', 'Second Year', '2021-2022', '50', 0, '2021-07-29 07:28:53'),
(3, 'B.A.', 'Third Year', '2021-2022', '50', 0, '2021-07-29 07:29:12'),
(4, 'B.SC', 'First Year', '2021-2022', '50', 0, '2021-07-29 07:29:37'),
(5, 'B.SC', 'Second Year', '2021-2022', '50', 0, '2021-07-29 07:29:55'),
(6, 'B.SC', 'Third Year', '2021-2022', '50', 0, '2021-07-29 07:30:17'),
(7, 'B.COM', 'First Year', '2021-2022', '50', 0, '2021-07-29 07:30:52'),
(8, 'B.COM', 'Second Year', '2021-2022', '50', 0, '2021-07-29 07:31:06'),
(9, 'B.COM', 'Third Year', '2021-2022', '50', 0, '2021-07-29 07:31:24'),
(10, 'BCA', 'First Year', '2021-2022', '300', 0, '2021-07-29 07:32:04'),
(11, 'BCA', 'Second Year', '2021-2022', '300', 0, '2021-07-29 07:32:19'),
(12, 'BCA', 'Third Year', '2021-2022', '300', 0, '2021-07-29 07:32:45'),
(13, 'PGDCA', 'First Year', '2021-2022', '300', 0, '2021-07-29 07:37:00'),
(14, 'PGDCA', 'Second Year', '2021-2022', '300', 0, '2021-07-29 07:37:16'),
(15, 'PGDCA', 'Third Year', '2021-2022', '300', 1, '2021-07-30 05:12:04'),
(16, 'M.A. In HISTORY', 'First Year', '2021-2022', '100', 0, '2021-07-29 07:38:20'),
(17, 'M.A. In HISTORY', 'Second Year', '2021-2022', '100', 0, '2021-07-29 07:38:40'),
(18, 'M.A. In HISTORY', 'Third Year', '2021-2022', '100', 0, '2021-07-29 07:39:03'),
(19, 'M.A. IN POLIOTICAL SCIENCE', 'First Year', '2021-2022', '100', 0, '2021-07-30 04:11:45'),
(20, 'M.A. IN POLIOTICAL SCIENCE', 'Second Year', '2021-2022', '100', 0, '2021-07-30 04:11:35'),
(21, 'M.A. IN POLIOTICAL SCIENCE', 'Third Year', '2021-2022', '100', 0, '2021-07-30 04:11:25');

-- --------------------------------------------------------

--
-- Table structure for table `manage_payment_installment`
--

CREATE TABLE `manage_payment_installment` (
  `payment_install_id` int(11) NOT NULL,
  `academic_year` varchar(500) NOT NULL,
  `course_name` varchar(500) NOT NULL,
  `course_year` varchar(500) NOT NULL,
  `fees_type` varchar(500) NOT NULL,
  `total_fee` varchar(500) NOT NULL,
  `first_installment` varchar(500) NOT NULL,
  `second_installment` varchar(500) NOT NULL,
  `third_installment` varchar(500) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manage_religion`
--

CREATE TABLE `manage_religion` (
  `religion_id` int(11) NOT NULL,
  `religion_name` varchar(1000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_religion`
--

INSERT INTO `manage_religion` (`religion_id`, `religion_name`, `delete_bit`) VALUES
(1, 'Hinduism', 0),
(2, 'Islam', 0),
(3, 'Christianity', 0),
(4, 'Sikhism', 0),
(5, 'Buddhism', 0),
(6, 'Jainism', 0),
(7, 'Zoroastrianism', 0),
(8, 'dssdsn', 1),
(9, 'dssdns', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manage_subject`
--

CREATE TABLE `manage_subject` (
  `subject_id` int(11) NOT NULL,
  `course_name` varchar(1000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `course_semester` varchar(500) NOT NULL,
  `subject_type` varchar(100) NOT NULL,
  `optional_sub_type` varchar(500) NOT NULL,
  `subject_name` varchar(2000) NOT NULL,
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manage_subject`
--

INSERT INTO `manage_subject` (`subject_id`, `course_name`, `course_year`, `course_semester`, `subject_type`, `optional_sub_type`, `subject_name`, `delete_bit`, `submitted_by`) VALUES
(1, 'BCA', 'First Year', 'Semester I', 'Compulsory', '', 'Mathematics-I', 0, '2021-07-30 05:01:26'),
(2, 'BCA', 'First Year', 'Semester I', 'Compulsory', '', 'Applied English', 0, '2021-07-30 05:01:37'),
(3, 'BCA', 'First Year', 'Semester I', 'Compulsory', '', 'Computer Fundamentals', 0, '2021-07-30 05:01:48'),
(4, 'BCA', 'First Year', 'Semester I', 'Compulsory', '', 'C Programming', 0, '2021-07-30 05:01:55'),
(5, 'BCA', 'First Year', 'Semester I', 'Compulsory', '', 'Office Automation  Tools', 0, '2021-07-30 05:02:02'),
(6, 'BCA', 'First Year', 'Semester I', 'Compulsory', '', 'C Programming Lab-I', 0, '2021-07-30 05:02:11'),
(7, 'BCA', 'First Year', 'Semester I', 'Compulsory', '', 'Office Automation Tools  Lab-II', 0, '2021-07-30 05:02:19'),
(8, 'BCA', 'First Year', 'Semester II', 'Compulsory', '', 'Mathematics-II', 0, '2021-07-30 05:02:33'),
(9, 'BCA', 'First Year', 'Semester II', 'Compulsory', '', 'Communicative English', 0, '2021-07-30 05:02:41'),
(10, 'BCA', 'First Year', 'Semester II', 'Compulsory', '', 'Digital Electronics', 0, '2021-07-30 05:02:51'),
(11, 'BCA', 'First Year', 'Semester II', 'Compulsory', '', 'Data Structures', 0, '2021-07-30 05:02:58'),
(12, 'BCA', 'First Year', 'Semester II', 'Compulsory', '', 'Data Base Management  System', 0, '2021-07-30 05:03:07'),
(13, 'BCA', 'First Year', 'Semester II', 'Compulsory', '', 'Data Structures Lab-III', 0, '2021-07-30 05:03:14'),
(14, 'BCA', 'First Year', 'Semester II', 'Compulsory', '', 'Data Base Management System  Lab-IV', 0, '2021-07-30 05:03:23'),
(15, 'BCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Mathematics-III', 0, '2021-07-30 05:04:50'),
(16, 'BCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Business  Practices and Management', 0, '2021-07-30 05:04:36'),
(17, 'BCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Computer Organization', 0, '2021-07-30 05:04:25'),
(18, 'BCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Object Oriented Programming  with Cpp', 0, '2021-07-30 05:04:14'),
(19, 'BCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Desktop Publishing and  Designing', 0, '2021-07-30 05:05:20'),
(20, 'BCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Object Oriented Programming with Cpp  Lab-V', 0, '2021-07-30 05:05:32'),
(21, 'BCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Desktop Publishing and Designing  Lab-VI', 0, '2021-07-30 05:05:40'),
(22, 'BCA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Personnel Management', 0, '2021-07-30 05:05:57'),
(23, 'BCA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Accounting', 0, '2021-07-30 05:06:03'),
(24, 'BCA', 'Second Year', 'Semester IV', 'Compulsory', '', 'System Analysis and Design', 0, '2021-07-30 05:06:20'),
(25, 'BCA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Internet Technology and Web Page  Design', 0, '2021-07-30 05:06:44'),
(26, 'BCA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Programming in Visual Basic', 0, '2021-07-30 05:06:55'),
(27, 'BCA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Internet Technology and Web Page Design  Lab-VII', 0, '2021-07-30 05:07:10'),
(28, 'BCA', 'Second Year', 'Semester IV', 'Compulsory', '', 'Programming in Visual Basic Lab-VIII', 0, '2021-07-30 05:07:20'),
(29, 'BCA', 'Third Year', 'Semester V', 'Compulsory', '', 'Operating System', 0, '2021-07-30 05:07:46'),
(30, 'BCA', 'Third Year', 'Semester V', 'Compulsory', '', 'ECommerce', 0, '2021-07-30 05:08:02'),
(31, 'BCA', 'Third Year', 'Semester V', 'Compulsory', '', 'Management Information System', 0, '2021-07-30 05:08:14'),
(32, 'BCA', 'Third Year', 'Semester V', 'Compulsory', '', 'ASP.net Technologies', 0, '2021-07-30 05:08:22'),
(33, 'BCA', 'Third Year', 'Semester V', 'Compulsory', '', 'Computer Oriented Statistical  Methods', 0, '2021-07-30 05:08:30'),
(34, 'BCA', 'Third Year', 'Semester V', 'Compulsory', '', 'ASP.net  Technologies Lab-IX', 0, '2021-07-30 05:08:39'),
(35, 'BCA', 'Third Year', 'Semester V', 'Compulsory', '', 'Computer Oriented Statistical Methods  Lab-X', 0, '2021-07-30 05:08:49'),
(36, 'BCA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Computer Networks', 0, '2021-07-30 05:09:19'),
(37, 'BCA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Numerical Methods', 0, '2021-07-30 05:09:27'),
(38, 'BCA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Multimedia Technology', 0, '2021-07-30 05:09:36'),
(39, 'BCA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Computer Graphics', 0, '2021-07-30 05:09:44'),
(40, 'BCA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Software Engineering', 0, '2021-07-30 05:09:51'),
(41, 'BCA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Computer Graphics  Lab-XI', 0, '2021-07-30 05:09:59'),
(42, 'BCA', 'Third Year', 'Semester VI', 'Compulsory', '', 'Major Project', 0, '2021-07-30 05:10:07'),
(43, 'PGDCA', 'First Year', 'Semester I', 'Compulsory', '', 'Fundamentals of Programming  Using C', 0, '2021-07-30 05:15:41'),
(44, 'PGDCA', 'First Year', 'Semester I', 'Compulsory', '', 'PC Software', 0, '2021-07-30 05:15:47'),
(45, 'PGDCA', 'First Year', 'Semester I', 'Compulsory', '', 'Operating system', 0, '2021-07-30 05:15:54'),
(46, 'PGDCA', 'First Year', 'Semester I', 'Compulsory', '', 'Computer Organization and  Architecture', 0, '2021-07-30 05:16:01'),
(47, 'PGDCA', 'First Year', 'Semester I', 'Compulsory', '', 'Practical-I ( C Language)', 0, '2021-07-30 05:16:10'),
(48, 'PGDCA', 'First Year', 'Semester I', 'Compulsory', '', 'Practical-I I (PC Software)', 0, '2021-07-30 05:16:17'),
(49, 'PGDCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Data and File Structure', 0, '2021-07-30 05:16:34'),
(50, 'PGDCA', 'Second Year', 'Semester III', 'Compulsory', '', 'System Analysis and Design', 0, '2021-07-30 05:16:40'),
(51, 'PGDCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Object Oriented Programming and Cpp', 0, '2021-07-30 05:17:03'),
(52, 'PGDCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Data base Management system', 0, '2021-07-30 05:17:13'),
(53, 'PGDCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Practical-III (DFS Using Cpp)', 0, '2021-07-30 05:17:29'),
(54, 'PGDCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Practical-IV (Data base Management system)', 0, '2021-07-30 05:17:49'),
(55, 'PGDCA', 'Second Year', 'Semester III', 'Compulsory', '', 'Project Work', 0, '2021-07-30 05:17:55'),
(56, 'B.COM', 'First Year', 'Semester I', 'Compulsory', '', 'Financial Accounting', 0, '2021-07-30 05:19:49'),
(57, 'B.COM', 'First Year', 'Semester I', 'Compulsory', '', 'Business Organisation and  Management', 0, '2021-07-30 05:19:59'),
(58, 'B.COM', 'First Year', 'Semester I', 'Compulsory', '', 'Business Law', 0, '2021-07-30 05:20:11'),
(59, 'B.COM', 'First Year', 'Semester I', 'Compulsory', '', 'Business Statistics and Mathematics', 0, '2021-07-30 05:20:18'),
(60, 'B.COM', 'First Year', 'Semester I', 'Optional', 'Core compulsory Course', 'English', 0, '2021-07-30 05:28:26'),
(61, 'B.COM', 'First Year', 'Semester I', 'Optional', 'Core compulsory Course', 'Hindi', 0, '2021-07-30 05:28:54'),
(62, 'B.COM', 'First Year', 'Semester I', 'Optional', 'Core compulsory Course', 'Sanskrit', 0, '2021-07-30 05:29:04'),
(63, 'B.COM', 'First Year', 'Semester I', 'Optional', 'Ability-Enhancement Compulsory Course-1', 'Environmental Studies', 0, '2021-07-30 05:29:14'),
(64, 'B.COM', 'First Year', 'Semester I', 'Optional', 'Ability-Enhancement Compulsory Course-2', 'English', 0, '2021-07-30 05:29:40'),
(65, 'B.COM', 'First Year', 'Semester I', 'Optional', 'Ability-Enhancement Compulsory Course-2', 'Hindi', 0, '2021-07-30 05:30:08'),
(66, 'B.COM', 'Second Year', 'Semester III', 'Compulsory', 'Core compulsory Course', 'Company Law', 0, '2021-07-30 05:30:47'),
(67, 'B.COM', 'Second Year', 'Semester III', 'Compulsory', '', 'Income Tax Law and Practice', 0, '2021-07-30 05:30:56'),
(68, 'B.COM', 'Second Year', 'Semester III', 'Compulsory', '', 'Corporate Accounting', 0, '2021-07-30 05:31:05'),
(69, 'B.COM', 'Second Year', 'Semester III', 'Compulsory', '', 'Cost Accounting', 0, '2021-07-30 05:31:12'),
(70, 'B.COM', 'Second Year', 'Semester III', 'Optional', 'Core compulsory Course', 'English', 0, '2021-07-30 05:31:24'),
(71, 'B.COM', 'Second Year', 'Semester III', 'Optional', 'Core compulsory Course', 'Hindi', 0, '2021-07-30 05:31:34'),
(72, 'B.COM', 'Second Year', 'Semester III', 'Optional', 'Core compulsory Course', 'Sanskrit', 0, '2021-07-30 05:31:41'),
(73, 'B.COM', 'Second Year', 'Semester III', 'Optional', 'Skill-Enhancement Elective Course-1', 'Computer Applications in Business', 0, '2021-07-30 05:34:32'),
(74, 'B.COM', 'Second Year', 'Semester III', 'Optional', 'Skill-Enhancement Elective Course-2', 'E-Commerce', 0, '2021-07-30 05:34:41'),
(75, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline Specific Elective-1', 'Human Resource Management', 0, '2021-07-30 05:41:55'),
(76, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline Specific Elective-1', 'Principles of Marketing', 0, '2021-07-30 05:42:07'),
(77, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline Specific Elective-1', 'Fundamentals of Financial Management', 0, '2021-07-30 05:42:18'),
(78, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline-Specific Elective-2', 'Corporate Governance and Auditing', 0, '2021-07-30 05:43:27'),
(79, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline-Specific Elective-2', 'GST', 0, '2021-07-30 05:43:37'),
(80, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-3', 'Corporate Tax Planning', 0, '2021-07-30 05:43:50'),
(81, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-3', 'Banking and Insurance', 0, '2021-07-30 05:44:00'),
(82, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-3', 'Management Accounting', 0, '2021-07-30 05:44:09'),
(83, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-3', 'Computerised Accounting System', 0, '2021-07-30 05:44:18'),
(84, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-4', 'International Business', 0, '2021-07-30 05:44:31'),
(85, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-4', 'Office Management and Secretarial Practice', 0, '2021-07-30 05:44:44'),
(86, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-4', 'Fundamentals of Investment', 0, '2021-07-30 05:44:53'),
(87, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Discipline- Specific Elective-4', 'Consumer Protection', 0, '2021-07-30 05:45:01'),
(88, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Skill-Enhancement Elective Course-1', 'Entrepreneurship', 0, '2021-07-30 05:45:16'),
(89, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Skill-Enhancement Elective Course-2', 'Personal Selling and Salesmanship', 0, '2021-07-30 05:45:30'),
(90, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Generic Elective-1', 'Economy of Himachal Pradesh.', 0, '2021-07-30 05:45:42'),
(91, 'B.COM', 'Third Year', 'Semester V', 'Optional', 'Generic Elective-2', 'Indian Economy', 0, '2021-07-30 05:45:50');

-- --------------------------------------------------------

--
-- Table structure for table `student_admission_form_academic_info`
--

CREATE TABLE `student_admission_form_academic_info` (
  `student_academic_info_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `optinal_subject_selected` varchar(500) NOT NULL,
  `optinal_subject_selected_sem2` varchar(500) NOT NULL,
  `value_added_subject_selected` varchar(500) NOT NULL,
  `value_added_subject_selected_sem2` varchar(500) NOT NULL,
  `is_maharashtra` varchar(500) NOT NULL,
  `ssc_exam_name` varchar(500) NOT NULL,
  `ssc_board_university_name` varchar(1000) NOT NULL,
  `ssc_school_clg_name` varchar(500) NOT NULL,
  `ssc_year_of_passing` varchar(500) NOT NULL,
  `ssc_seat_number` varchar(500) NOT NULL,
  `ssc_marks_obtained` varchar(500) NOT NULL,
  `ssc_total_marks` varchar(500) NOT NULL,
  `ssc_percentage` varchar(500) NOT NULL,
  `ssc_uploaded_documents` varchar(5000) NOT NULL,
  `hsc_exam_name` varchar(500) NOT NULL,
  `hsc_board_university_name` varchar(500) NOT NULL,
  `hsc_school_clg_name` varchar(500) NOT NULL,
  `hsc_year_of_passing` varchar(500) NOT NULL,
  `hsc_seat_number` varchar(500) NOT NULL,
  `hsc_marks_obtained` varchar(500) NOT NULL,
  `hsc_total_marks` varchar(500) NOT NULL,
  `hsc_percentage` varchar(500) NOT NULL,
  `hsc_uploaded_documents` varchar(1000) NOT NULL,
  `tybcom_exam_name` varchar(500) NOT NULL,
  `tybcom_board_university_name` varchar(300) NOT NULL,
  `tybcom_school_clg_name` varchar(500) NOT NULL,
  `tybcom_year_of_passing` varchar(300) NOT NULL,
  `tybcom_seat_number` varchar(300) NOT NULL,
  `tybcom_marks_obtained` varchar(300) NOT NULL,
  `tybcom_total_marks` varchar(300) NOT NULL,
  `tybcom_percentage` varchar(300) NOT NULL,
  `tybcom_uploaded_documents` varchar(1000) NOT NULL,
  `fy_exam_name` varchar(500) NOT NULL,
  `fy_board_university_name` varchar(500) NOT NULL,
  `fy_school_clg_name` varchar(500) NOT NULL,
  `fy_year_of_passing` varchar(500) NOT NULL,
  `fy_seat_number` varchar(500) NOT NULL,
  `fy_marks_obtained` varchar(500) NOT NULL,
  `fy_total_marks` varchar(500) NOT NULL,
  `fy_percentage` varchar(500) NOT NULL,
  `fy_uploaded_documents` varchar(1000) NOT NULL,
  `fy_exam_name_sem2` varchar(500) NOT NULL,
  `fy_board_university_name_sem2` varchar(500) NOT NULL,
  `fy_school_clg_name_sem2` varchar(500) NOT NULL,
  `fy_year_of_passing_sem2` varchar(200) NOT NULL,
  `fy_seat_number_sem2` varchar(200) NOT NULL,
  `fy_marks_obtained_sem2` varchar(200) NOT NULL,
  `fy_total_marks_sem2` varchar(200) NOT NULL,
  `fy_percentage_sem2` varchar(200) NOT NULL,
  `fy_uploaded_documents_sem2` varchar(600) NOT NULL,
  `sy_exam_name` varchar(500) NOT NULL,
  `sy_board_university_name` varchar(500) NOT NULL,
  `sy_school_clg_name` varchar(500) NOT NULL,
  `sy_year_of_passing` varchar(500) NOT NULL,
  `sy_seat_number` varchar(500) NOT NULL,
  `sy_marks_obtained` varchar(500) NOT NULL,
  `sy_total_marks` varchar(500) NOT NULL,
  `sy_percentage` varchar(500) NOT NULL,
  `sy_uploaded_documents` varchar(1000) NOT NULL,
  `sy_exam_name_sem2` varchar(200) NOT NULL,
  `sy_board_university_name_sem2` varchar(500) NOT NULL,
  `sy_school_clg_name_sem2` varchar(200) NOT NULL,
  `sy_year_of_passing_sem2` varchar(200) NOT NULL,
  `sy_seat_number_sem2` varchar(200) NOT NULL,
  `sy_marks_obtained_sem2` varchar(200) NOT NULL,
  `sy_total_marks_sem2` varchar(200) NOT NULL,
  `sy_percentage_sem2` varchar(200) NOT NULL,
  `sy_uploaded_documents_sem2` varchar(1000) NOT NULL,
  `student_repeater_year` varchar(500) NOT NULL,
  `is_gap_reason` varchar(500) NOT NULL,
  `gap_reason` varchar(1000) DEFAULT NULL,
  `is_applied_other_clg` varchar(500) NOT NULL,
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_admission_form_academic_info`
--

INSERT INTO `student_admission_form_academic_info` (`student_academic_info_id`, `student_id`, `optinal_subject_selected`, `optinal_subject_selected_sem2`, `value_added_subject_selected`, `value_added_subject_selected_sem2`, `is_maharashtra`, `ssc_exam_name`, `ssc_board_university_name`, `ssc_school_clg_name`, `ssc_year_of_passing`, `ssc_seat_number`, `ssc_marks_obtained`, `ssc_total_marks`, `ssc_percentage`, `ssc_uploaded_documents`, `hsc_exam_name`, `hsc_board_university_name`, `hsc_school_clg_name`, `hsc_year_of_passing`, `hsc_seat_number`, `hsc_marks_obtained`, `hsc_total_marks`, `hsc_percentage`, `hsc_uploaded_documents`, `tybcom_exam_name`, `tybcom_board_university_name`, `tybcom_school_clg_name`, `tybcom_year_of_passing`, `tybcom_seat_number`, `tybcom_marks_obtained`, `tybcom_total_marks`, `tybcom_percentage`, `tybcom_uploaded_documents`, `fy_exam_name`, `fy_board_university_name`, `fy_school_clg_name`, `fy_year_of_passing`, `fy_seat_number`, `fy_marks_obtained`, `fy_total_marks`, `fy_percentage`, `fy_uploaded_documents`, `fy_exam_name_sem2`, `fy_board_university_name_sem2`, `fy_school_clg_name_sem2`, `fy_year_of_passing_sem2`, `fy_seat_number_sem2`, `fy_marks_obtained_sem2`, `fy_total_marks_sem2`, `fy_percentage_sem2`, `fy_uploaded_documents_sem2`, `sy_exam_name`, `sy_board_university_name`, `sy_school_clg_name`, `sy_year_of_passing`, `sy_seat_number`, `sy_marks_obtained`, `sy_total_marks`, `sy_percentage`, `sy_uploaded_documents`, `sy_exam_name_sem2`, `sy_board_university_name_sem2`, `sy_school_clg_name_sem2`, `sy_year_of_passing_sem2`, `sy_seat_number_sem2`, `sy_marks_obtained_sem2`, `sy_total_marks_sem2`, `sy_percentage_sem2`, `sy_uploaded_documents_sem2`, `student_repeater_year`, `is_gap_reason`, `gap_reason`, `is_applied_other_clg`, `submitted_by`) VALUES
(2, 1, 'Organization Skill Development,Additional English,Hindi,Sindhi', 'Banking and finance', '', '', 'Out of Maharashtra', 'SSC', 'Kerala', 'Shivaji HighSchool, satara', '2007', 'C085953', '400', '550', '72.73', 'uploads/Jyoti Patil/ssc.pdf', 'HSC', 'Maharashtra', 'shivaji college ,pune', '2006', 'H023234', '300', '500', '60.00', 'uploads/Jyoti Patil/HSC.pdf', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', 'gapReasonNo', '', 'otherCollegeNo', '2020-10-13 11:47:34'),
(3, 3, 'Organization Skill Development,Hindi', 'Banking and finance,Sindhi', '', '', 'Out of Maharashtra', 'SSC', 'Maharashtra', 'Shivaji HighSchool, satara', '2020', 'C085953', '495', '550', '90.00', 'uploads/Pranita Pawar/ssc.pdf', 'HSC', 'Maharashtra', 'shivaji college ,pune', '2020', 'H023234', '300', '500', '60.00', 'uploads/Pranita Pawar/HSC.pdf', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', 'gapReasonNo', '', 'otherCollegeNo', '2020-10-14 05:16:29'),
(4, 5, 'Organization Skill Development,Additional English,Hindi', 'Banking and finance', '', '', 'Out of Maharashtra', 'ssc', 'Maharashtra', 'Shivaji HighSchool, satara', '2017', 'C085958', '496', '780', '63.59', 'uploads/Komal Pawar/ssc.pdf', 'hsc', 'Maharashtra', 'shivaji college ,pune', '2019', 'H023237', '300', '500', '60.00', 'uploads/Komal Pawar/HSC.pdf', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', 'gapReasonNo', '', 'otherCollegeNo', '2020-10-15 10:32:37');

-- --------------------------------------------------------

--
-- Table structure for table `student_admission_form_parent_info`
--

CREATE TABLE `student_admission_form_parent_info` (
  `student_parent_info_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `father_first_name` varchar(1000) NOT NULL,
  `father_middle_name` varchar(1000) NOT NULL,
  `father_last_name` varchar(1000) NOT NULL,
  `father_email_address` varchar(1000) NOT NULL,
  `father_mobile_number` varchar(1000) NOT NULL,
  `father_occupation` varchar(1000) NOT NULL,
  `is_father_central_govern_employee` varchar(500) NOT NULL,
  `father_annual_income` varchar(1000) NOT NULL,
  `is_father_economical_backward` varchar(500) NOT NULL,
  `mother_first_name` varchar(1000) NOT NULL,
  `mother_middle_name` varchar(1000) NOT NULL,
  `mother_last_name` varchar(1000) NOT NULL,
  `mother_email_address` varchar(1000) NOT NULL,
  `mother_mobile_number` varchar(1000) NOT NULL,
  `mother_occupation` varchar(1000) NOT NULL,
  `is_mother_central_govern_employee` varchar(1000) NOT NULL,
  `mother_annual_income` varchar(1000) NOT NULL,
  `is_mother_economical_backward` varchar(500) NOT NULL,
  `guardian_first_name` varchar(1000) NOT NULL,
  `guardian_middle_name` varchar(1000) NOT NULL,
  `guardian_last_name` varchar(1000) NOT NULL,
  `guardian_email_address` varchar(1000) NOT NULL,
  `guardian_mobile_number` varchar(1000) NOT NULL,
  `guardian_occupation` varchar(1000) NOT NULL,
  `is_guardian_central_govern_employee` varchar(500) NOT NULL,
  `guardian_annual_income` varchar(1000) NOT NULL,
  `is_guardian_economical_backward` varchar(1000) NOT NULL,
  `is_student_agree` varchar(500) NOT NULL,
  `student_admission_status` int(11) NOT NULL DEFAULT '1',
  `form_fee_submitted_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `course_fees_type` varchar(500) DEFAULT NULL,
  `first_installment_bit` varchar(50) NOT NULL DEFAULT '0',
  `second_installment_bit` varchar(50) NOT NULL DEFAULT '0',
  `third_installment_bit` varchar(50) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_admission_form_parent_info`
--

INSERT INTO `student_admission_form_parent_info` (`student_parent_info_id`, `student_id`, `father_first_name`, `father_middle_name`, `father_last_name`, `father_email_address`, `father_mobile_number`, `father_occupation`, `is_father_central_govern_employee`, `father_annual_income`, `is_father_economical_backward`, `mother_first_name`, `mother_middle_name`, `mother_last_name`, `mother_email_address`, `mother_mobile_number`, `mother_occupation`, `is_mother_central_govern_employee`, `mother_annual_income`, `is_mother_economical_backward`, `guardian_first_name`, `guardian_middle_name`, `guardian_last_name`, `guardian_email_address`, `guardian_mobile_number`, `guardian_occupation`, `is_guardian_central_govern_employee`, `guardian_annual_income`, `is_guardian_economical_backward`, `is_student_agree`, `student_admission_status`, `form_fee_submitted_bit`, `submitted_by`, `course_fees_type`, `first_installment_bit`, `second_installment_bit`, `third_installment_bit`) VALUES
(4, 1, 'Mohan', 'Santosh', 'Pawar', 'mohan@gmail.com', '7878787878', 'farmer', 'No', '0-1 Lakh', 'No', 'Kalpana', 'ghj', 'mbm', 'kalpana@gmail.com', '7898987878', 'House Wife', 'No', '0-1 Lakh', 'No', '', '', '', '', '', '', '', '', '', 'true', 3, 0, '2020-10-14 06:39:47', NULL, '0', '0', '0'),
(5, 3, 'Monah', 'santosh', 'Patil', 'pankaj@gmail.com', '7878787878', 'Farmer', 'No', '0-1 Lakh', 'No', 'Kalpana', 'Pankaj', 'Patil', 'kalpana@gmail.com', '9898767676', 'farmer', 'No', '1-2 Lakh', 'No', '', '', '', '', '', '', '', '', '', 'true', 2, 0, '2020-10-15 10:41:34', NULL, '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `student_admission_form_personal_info`
--

CREATE TABLE `student_admission_form_personal_info` (
  `student_personal_info_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `is_sindhi_category` varchar(500) NOT NULL,
  `is_jaihind_college` varchar(500) NOT NULL,
  `first_name` varchar(500) NOT NULL,
  `middle_name` varchar(500) NOT NULL,
  `last_name` varchar(500) NOT NULL,
  `mother_name` varchar(1000) NOT NULL,
  `email_address` varchar(1000) NOT NULL,
  `mobile_number` varchar(500) NOT NULL,
  `alter_mobile_number` varchar(500) NOT NULL,
  `adhar_card_id` varchar(1000) NOT NULL,
  `voter_id` varchar(1000) NOT NULL,
  `local_address` varchar(5000) NOT NULL,
  `correspondance_address` varchar(500) DEFAULT NULL,
  `guardian_address` varchar(500) DEFAULT NULL,
  `native_place` varchar(1000) NOT NULL,
  `blood_group` varchar(500) NOT NULL,
  `gender` varchar(500) NOT NULL,
  `marital_status` varchar(500) NOT NULL,
  `date_of_birth` varchar(500) NOT NULL,
  `dob_in_word` varchar(500) NOT NULL,
  `birth_place` varchar(500) NOT NULL,
  `district` varchar(500) NOT NULL,
  `state` varchar(500) NOT NULL,
  `nationality` varchar(500) NOT NULL,
  `passport` varchar(500) NOT NULL,
  `religion` varchar(500) NOT NULL,
  `caste` varchar(1000) NOT NULL,
  `category` varchar(1000) NOT NULL,
  `mother_tounge` varchar(500) NOT NULL,
  `is_cast_certificate` varchar(500) NOT NULL,
  `is_physically_handi` varchar(500) NOT NULL,
  `eligibility_number` varchar(500) NOT NULL,
  `roll_number` varchar(300) DEFAULT NULL,
  `game_played` varchar(2000) NOT NULL,
  `hobbies` varchar(5000) NOT NULL,
  `admission_type` varchar(500) NOT NULL,
  `regd_number` varchar(500) NOT NULL,
  `is_join_clg_NNS` varchar(500) NOT NULL,
  `is_participate_in_event` varchar(500) NOT NULL,
  `uploaded_photo` varchar(5000) NOT NULL,
  `uploaded_signature` varchar(5000) NOT NULL,
  `uploaded_caste_certificate` varchar(4000) NOT NULL,
  `uploaded_other_documents` varchar(4000) NOT NULL,
  `uploaded_physically_handi_certificate` varchar(1000) DEFAULT NULL,
  `uploaded_parent_signature` varchar(500) NOT NULL,
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_admission_form_personal_info`
--

INSERT INTO `student_admission_form_personal_info` (`student_personal_info_id`, `student_id`, `is_sindhi_category`, `is_jaihind_college`, `first_name`, `middle_name`, `last_name`, `mother_name`, `email_address`, `mobile_number`, `alter_mobile_number`, `adhar_card_id`, `voter_id`, `local_address`, `correspondance_address`, `guardian_address`, `native_place`, `blood_group`, `gender`, `marital_status`, `date_of_birth`, `dob_in_word`, `birth_place`, `district`, `state`, `nationality`, `passport`, `religion`, `caste`, `category`, `mother_tounge`, `is_cast_certificate`, `is_physically_handi`, `eligibility_number`, `roll_number`, `game_played`, `hobbies`, `admission_type`, `regd_number`, `is_join_clg_NNS`, `is_participate_in_event`, `uploaded_photo`, `uploaded_signature`, `uploaded_caste_certificate`, `uploaded_other_documents`, `uploaded_physically_handi_certificate`, `uploaded_parent_signature`, `submitted_by`) VALUES
(3, 1, '', '', 'Jyoti', 'Mohan', 'Patil', 'Kalpana', 'jyoti@gmail.com', '8989898989', '', '213232324324', '', 'Pune', 'Pune', 'Pune', 'Pune', 'AB+ve', 'Female', 'Unmarried', '07-05-2004', 'fkhsdj fjsdhf jsdhfjsd', 'fskdfh', 'Pune', 'Maharashtra', 'Indian', '', 'Hinduism', 'MARATHA', 'OPEN', 'Marathi', 'Yes', 'No', '87777777', 'b78888', '', '', 'Regular', 'cty8687', '', '', 'uploads/Jyoti Patil/StudentPhoto.jpg', 'uploads/Jyoti Patil/StudentSignature.jpg', 'uploads/Jyoti Patil/CasteCertificate.pdf', '', NULL, 'uploads/Jyoti Patil/studSignature.jpg', '2020-10-13 11:46:56'),
(4, 3, '', '', 'Pranita', 'Pradip', 'Pawar', 'Kalpana', 'sayali@gmail.com', '9867876787', '', '213232324324', '', 'Pune', 'Pune', 'Pune', 'Pune', 'A+ve', 'Female', 'Unmarried', '14-01-1994', '2nd jan nineteen ninety seven', 'Pune', 'Pune', 'Maharashtra', 'indian', '', 'Hinduism', 'maratha', 'SEBC', 'Marathi', 'No', 'No', '', '', '', '', 'Regular', 'gdf', '', '', 'uploads/Pranita Pawar/StudentPhoto.jpg', 'uploads/Pranita Pawar/StudentSignature.jpg', '', '', NULL, 'uploads/Pranita Pawar/studSignature.jpg', '2021-07-26 05:11:27'),
(5, 5, '', '', 'Komal', 'Pradip', 'Pawar', 'Kalpana', 'kajal@gmail.com', '8976675675', '', '213232324324', '', 'Pune', 'Pune', 'Pune', 'Pune', 'A+ve', 'Female', 'Unmarried', '02-01-2000', 'second Jan two Thousand', 'Pune', 'Pune', 'Maharashtra', 'Indian', '', 'Hinduism', 'Maratha', 'OPEN', 'Marathi', 'No', 'No', '', '', '', '', 'Regular', 'D344', '', '', 'uploads/Komal Pawar/StudentPhoto.jpg', 'uploads/Komal Pawar/StudentSignature.jpg', '', '', NULL, 'uploads/Komal Pawar/studSignature.jpg', '2020-10-15 10:27:50'),
(6, 2, '', '', 'Jyoti', 'Pradip', 'Kadam', 'sdf', 'pp@gmail.com', '9856786786', '', '677777777777', '', 'dd', 'gdg', '', 'gdg', 'A+ve', 'Female', 'Unmarried', '01-02-2007', 'das', 'asd', 'dasd', 'Maharashtra', 'dsad', '', 'Hinduism', 'dsad', 'OPEN', 'da', 'No', 'No', '', '', '', '', 'Regular', '', '', '', 'uploads/Jyoti Kadam/img.png', 'uploads/Jyoti Kadam/images.png', '', '', NULL, 'uploads/Jyoti Kadam/images.png', '2021-07-20 07:36:10'),
(7, 6, '', '', 'prajakta', 'Pandurang', 'Dudhane', 'gdfg', 'prajakta.dudhane@whitecode.co.in', '7058752536', '', '988888888888', '', 'Pune', 'Pune', 'Pune', 'Pune', 'B+ve', 'Female', 'Unmarried', '03-04-2004', 'dvd gdf gdgd', 'Pune', 'Pune', 'Maharashtra', 'Indian', '', 'Hinduism', 'maratha', 'SEBC', 'marathi', 'No', 'No', '', '', '', '', 'Regular', '', '', '', 'uploads/prajakta Dudhane/img.png', 'uploads/prajakta Dudhane/sign.png', '', '', NULL, 'uploads/prajakta Dudhane/sign.png', '2021-07-26 05:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `student_course_fees_payment_details`
--

CREATE TABLE `student_course_fees_payment_details` (
  `student_payment_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `add_amount` varchar(1000) NOT NULL,
  `payment_recieved_date` varchar(500) NOT NULL,
  `comment` varchar(5000) DEFAULT NULL,
  `is_payment_recieved` int(11) NOT NULL DEFAULT '0',
  `is_payment_paid` int(11) NOT NULL DEFAULT '0',
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `submitted_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_registration`
--

CREATE TABLE `student_registration` (
  `student_id` int(11) NOT NULL,
  `stud_academic_year` varchar(1000) NOT NULL,
  `course_name` varchar(5000) NOT NULL,
  `course_year` varchar(1000) NOT NULL,
  `student_name` varchar(5000) NOT NULL,
  `student_email` varchar(5000) NOT NULL,
  `student_mobile` varchar(1000) NOT NULL,
  `student_password` varchar(5000) NOT NULL,
  `admission_step_one` varchar(10) NOT NULL DEFAULT '0',
  `admission_step_two` varchar(10) NOT NULL DEFAULT '0',
  `admission_step_three` varchar(10) NOT NULL DEFAULT '0',
  `delete_bit` int(11) NOT NULL DEFAULT '0',
  `registerd_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_registration`
--

INSERT INTO `student_registration` (`student_id`, `stud_academic_year`, `course_name`, `course_year`, `student_name`, `student_email`, `student_mobile`, `student_password`, `admission_step_one`, `admission_step_two`, `admission_step_three`, `delete_bit`, `registerd_date`) VALUES
(1, '2020-2021', 'B.Com', 'First Year', 'Patil Jyoti Mohan', 'jyoti@gmail.com', '8989898989', 'qwerty', '1', '1', '1', 0, '2020-10-13 12:16:36'),
(2, '2020-2021', 'BA', 'second Year', 'Kadam Jyoti Pradip', 'pp@gmail.com', '9856786786', 'qwerty', '1', '0', '0', 0, '2021-07-22 04:29:12'),
(3, '2020-2021', 'B.Com', 'First Year', 'Pawar Pranita Pradip', 'sayali@gmail.com', '9867876787', 'qwerty', '1', '1', '1', 0, '2020-10-14 05:14:39'),
(4, '2020-2021', 'B.Com', 'Second Year', 'Dange Jyoti Mohan', 'sonali@gmail.com', '9887878787', 'qwerty', '0', '0', '0', 0, '2021-07-26 04:58:01'),
(5, '2020-2021', 'B.Com', 'First Year', 'Pawar Komal Pradip', 'kajal@gmail.com', '8976675675', 'qwerty', '1', '1', '1', 0, '2020-10-15 10:35:34'),
(7, '2020-2021', 'B.Com', 'First Year', 'fsf sfsd sdf', 'pra@gmail.com', '9888888888', 'qwerty', '0', '0', '0', 0, '2021-07-27 07:06:03'),
(8, '2020-2021', 'B.Com', 'First Year', 'gdfg gdf gfd', 'dd@gmail.com', '9888888888', 'qwerty', '0', '0', '0', 0, '2021-07-27 07:10:33'),
(9, '2020-2021', 'B.Com', 'First Year', 'dsa dad das', 'aa@gmail.com', '9888888888', 'qwerty', '0', '0', '0', 0, '2021-07-27 07:13:11'),
(10, '2020-2021', 'B.Com', 'First Year', 'Dudhane prajakta Pandurang', 'prajakta.dudhane@whitecode.co.in', '7058752536', 'qwerty', '0', '0', '0', 0, '2021-07-27 08:06:57'),
(11, '2021-2022', 'B.Com', 'First Year', 'ljk prajjj kjh', 'prajakta1997dudhane@gmail.com', '7058752536', 'qwerty', '0', '0', '0', 0, '2021-07-27 08:14:43'),
(12, '2021-2022', 'B.COM', 'First Year', 'Shinde Amol Kailas', 'amol.shinde@whitecode.co.in', '8983369618', 'amol@2021', '0', '0', '0', 0, '2021-07-30 10:04:34');

-- --------------------------------------------------------

--
-- Table structure for table `user_log`
--

CREATE TABLE `user_log` (
  `log_id` int(11) NOT NULL,
  `log_user_id` varchar(500) NOT NULL,
  `user_ip_address` varchar(100) DEFAULT NULL,
  `user_devices_details` varchar(1000) DEFAULT NULL,
  `user_activity` varchar(5000) NOT NULL,
  `user_log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `user_id` int(11) NOT NULL,
  `user_name` longtext NOT NULL,
  `user_password` longtext NOT NULL,
  `user_email` varchar(500) DEFAULT NULL,
  `user_mobile` varchar(50) DEFAULT NULL,
  `user_role` varchar(1000) NOT NULL,
  `user_address` varchar(1000) DEFAULT NULL,
  `valid_till` varchar(500) DEFAULT NULL,
  `user_admin_approval_bit` tinyint(2) NOT NULL DEFAULT '0',
  `user_delete_bit` varchar(5) NOT NULL DEFAULT '0',
  `user_created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`user_id`, `user_name`, `user_password`, `user_email`, `user_mobile`, `user_role`, `user_address`, `valid_till`, `user_admin_approval_bit`, `user_delete_bit`, `user_created_date`) VALUES
(1, 'Whitecode', 'eE4vVG5LcytYNlNsNTM0RTdsVXhzZz09', 'info@mucollege.org', '8380036301', 'admin', NULL, NULL, 1, '0', '2019-12-05 23:06:44'),
(2, 'whitecode1', 'YWtNNkliZTdBanlScnZOUUdyUGczdz09', 'info@whitecode.co.in', '8983369618', 'college', 'puneC/O Sungrace India 2nd floor, Left wing Survey #19,', '28-01-2021', 1, '0', '2020-01-26 18:41:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `manage_caste`
--
ALTER TABLE `manage_caste`
  ADD PRIMARY KEY (`caste_id`);

--
-- Indexes for table `manage_course`
--
ALTER TABLE `manage_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `manage_course_fees`
--
ALTER TABLE `manage_course_fees`
  ADD PRIMARY KEY (`course_fee_id`);

--
-- Indexes for table `manage_form_fees`
--
ALTER TABLE `manage_form_fees`
  ADD PRIMARY KEY (`form_fee_id`);

--
-- Indexes for table `manage_payment_installment`
--
ALTER TABLE `manage_payment_installment`
  ADD PRIMARY KEY (`payment_install_id`);

--
-- Indexes for table `manage_religion`
--
ALTER TABLE `manage_religion`
  ADD PRIMARY KEY (`religion_id`);

--
-- Indexes for table `manage_subject`
--
ALTER TABLE `manage_subject`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `student_admission_form_academic_info`
--
ALTER TABLE `student_admission_form_academic_info`
  ADD PRIMARY KEY (`student_academic_info_id`);

--
-- Indexes for table `student_admission_form_parent_info`
--
ALTER TABLE `student_admission_form_parent_info`
  ADD PRIMARY KEY (`student_parent_info_id`);

--
-- Indexes for table `student_admission_form_personal_info`
--
ALTER TABLE `student_admission_form_personal_info`
  ADD PRIMARY KEY (`student_personal_info_id`);

--
-- Indexes for table `student_course_fees_payment_details`
--
ALTER TABLE `student_course_fees_payment_details`
  ADD PRIMARY KEY (`student_payment_id`);

--
-- Indexes for table `student_registration`
--
ALTER TABLE `student_registration`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `manage_caste`
--
ALTER TABLE `manage_caste`
  MODIFY `caste_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `manage_course`
--
ALTER TABLE `manage_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `manage_course_fees`
--
ALTER TABLE `manage_course_fees`
  MODIFY `course_fee_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manage_form_fees`
--
ALTER TABLE `manage_form_fees`
  MODIFY `form_fee_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `manage_payment_installment`
--
ALTER TABLE `manage_payment_installment`
  MODIFY `payment_install_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manage_religion`
--
ALTER TABLE `manage_religion`
  MODIFY `religion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `manage_subject`
--
ALTER TABLE `manage_subject`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `student_admission_form_academic_info`
--
ALTER TABLE `student_admission_form_academic_info`
  MODIFY `student_academic_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_admission_form_parent_info`
--
ALTER TABLE `student_admission_form_parent_info`
  MODIFY `student_parent_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `student_admission_form_personal_info`
--
ALTER TABLE `student_admission_form_personal_info`
  MODIFY `student_personal_info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `student_course_fees_payment_details`
--
ALTER TABLE `student_course_fees_payment_details`
  MODIFY `student_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_registration`
--
ALTER TABLE `student_registration`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
